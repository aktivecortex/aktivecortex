Aktive Cortex
=======

**Aktive Cortex** is a framework designed to deliver **scalable, high-performance Enterprise solutions**. 

**Aktive Cortex** extends [**Axon Framework**](http://www.axonframework.org/) to provide a **JMS based asynchronous and distributed CQRS** implementation ready to boost your next application.

For more information visit the [wiki](/aktivecortex/aktivecortex/wiki)

