/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.web.presentation.support;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import javax.annotation.PostConstruct;

public class CustomJacksonObjectMapper extends ObjectMapper {
    
    @PostConstruct
    public void afterPropertiesSet() throws Exception {
        getSerializationConfig().setSerializationInclusion(Inclusion.NON_NULL); // ignore null attributes
        getSerializationConfig().disable(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS); // serialize date as text
        getSerializationConfig().enable(SerializationConfig.Feature.INDENT_OUTPUT); // pretty print json
    }
}
