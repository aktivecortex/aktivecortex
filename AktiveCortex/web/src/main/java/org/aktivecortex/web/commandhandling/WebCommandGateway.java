/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.web.commandhandling;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.commandhandling.CommandGateway;
import org.aktivecortex.api.notification.ProgressEvaluator;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Map;

/**
 * REST interface for command sending over the {@link org.aktivecortex.api.commandhandling.AsyncCommandBus}.
 * <p/>
 * <p>Implementations will take advantage of the {@link HttpServletRequest} argument to return a
 * {@link ResponseEntity} containing the identifier of the {@link org.aktivecortex.api.notification.Process} descriptor
 * that tracks the command execution</p>
 * <p/>
 * <p>The overloaded method that accept a {@link ProgressEvaluator} will enable
 * the {@link org.aktivecortex.api.notification.Progress} tracking of the process</p>
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
public interface WebCommandGateway extends CommandGateway {

    String HEADER_PROC_LOCATION = "X-Cortex-Location";
    String HEADER_PROC_ID = "X-Cortex-PID";

    /**
     * Send a batch of command over the command bus and return a {@link ResponseEntity} that contains the
     * {@link org.aktivecortex.api.notification.Process} identifier.
     *
     * <p>The {@link HttpServletRequest} argument is needed to build the return argument</p>
     *
     * @param request  The {@link HttpServletRequest} argument needed to build the return argument
     * @param commands The array of {@link Command} to be sent.
     * @return {@link ResponseEntity} that contains the {@link org.aktivecortex.api.notification.Process} identifier.
     */
    ResponseEntity<String> send(HttpServletRequest request, Command... commands);
/**
     * Send a batch of command over the command bus and return a {@link ResponseEntity} that contains the
     * {@link org.aktivecortex.api.notification.Process} identifier.
     *
     * <p>The {@link HttpServletRequest} argument is needed to build the return argument</p>
     *
     * @param request  The {@link HttpServletRequest} argument needed to build the return argument
     * @param httpHeaders The {@link org.springframework.util.MultiValueMap} with additional
 *                        http headers to be placed in the response
 * @param commands The array of {@link Command} to be sent.
     * @return {@link ResponseEntity} that contains the {@link org.aktivecortex.api.notification.Process} identifier.
     */
    ResponseEntity<String> send(HttpServletRequest request, Map<String,Collection<String>> httpHeaders, Command... commands);

    /**
     * Send a batch of command over the command bus and return a {@link ResponseEntity} that contains the
     * {@link org.aktivecortex.api.notification.Process} identifier.
     *
     * <p>The {@link HttpServletRequest} needed to build the return argument</p>
     * <p>The {@link ProgressEvaluator} permits {@link org.aktivecortex.api.notification.Progress} tracking
     * of the {@link org.aktivecortex.api.notification.Process}</p>
     *
     * @param request           The {@link HttpServletRequest} needed to build the return argument
     * @param progressEvaluator <p>The {@link ProgressEvaluator} permits {@link org.aktivecortex.api.notification.Progress} tracking
     *                          of the {@link org.aktivecortex.api.notification.Process}</p>
     * @param commands          The array of {@link Command} to be sent.
     * @return {@link ResponseEntity} that contains the {@link org.aktivecortex.api.notification.Process} identifier.
     */
    ResponseEntity<String> send(HttpServletRequest request, ProgressEvaluator progressEvaluator, Command... commands);

    /**
     * Send a batch of command over the command bus and return a {@link ResponseEntity} that contains the
     * {@link org.aktivecortex.api.notification.Process} identifier.
     *
     * <p>The {@link HttpServletRequest} needed to build the return argument</p>
     * <p>The {@link ProgressEvaluator} permits {@link org.aktivecortex.api.notification.Progress} tracking
     * of the {@link org.aktivecortex.api.notification.Process}</p>
     *
     * @param request           The {@link HttpServletRequest} needed to build the return argument
     * @param httpHeaders       The {@link org.springframework.util.MultiValueMap} with additional
     *                          http headers to be placed in the response
     * @param progressEvaluator <p>The {@link ProgressEvaluator} permits {@link org.aktivecortex.api.notification.Progress} tracking
     *                          of the {@link org.aktivecortex.api.notification.Process}</p>
     * @param commands          The array of {@link Command} to be sent.
     * @return {@link ResponseEntity} that contains the {@link org.aktivecortex.api.notification.Process} identifier.
     */
    ResponseEntity<String> send(HttpServletRequest request, Map<String,Collection<String>> httpHeaders, ProgressEvaluator progressEvaluator, Command... commands);
}
