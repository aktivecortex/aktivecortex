/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.web.tracing.support;


import com.github.kristofa.brave.EndPointSubmitter;
import org.axonframework.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestTraceInterceptor extends HandlerInterceptorAdapter {

    private final static Logger LOGGER = LoggerFactory.getLogger(RequestTraceInterceptor.class);

    private EndPointSubmitter endPointSubmitter;

    private volatile boolean initialized;

    @Autowired
    public void setEndPointSubmitter(EndPointSubmitter endPointSubmitter) {
        Assert.notNull(endPointSubmitter, "EndPointSubmitter can't be null!");
        this.endPointSubmitter = endPointSubmitter;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!initialized) {
            initialized = true;
            final String localAddr = request.getLocalAddr();
            final int localPort = request.getLocalPort();
            final String contextPath = request.getContextPath();
            final String serviceName = StringUtils.hasText(contextPath) ? contextPath : localAddr;
            LOGGER.info("Setting tracing endpoint actual values: {addr: [{}] - port: [{}] - contextpath: [{}]}",
                    localAddr, localPort, serviceName);
            endPointSubmitter.submit(localAddr, localPort, serviceName);
        }
        return true;
    }
}
