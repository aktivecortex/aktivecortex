/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.web.notification;

import org.aktivecortex.api.notification.Process;
import org.aktivecortex.api.notification.ProcessQueryService;
import org.aktivecortex.core.notification.ProcessImpl;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *  REST Controller that exposes query operations for Process resources.
 *  <p></p>
 *
 *  @since 1.0
 *  @author Domenico Maria Giffone
 */
@RequestMapping(value = "/async/processes")
public class PullNotificationService {
	
	private ProcessQueryService query;

    public void setNotificationManager(ProcessQueryService query) {
        this.query = query;
    }

    @RequestMapping(method = RequestMethod.GET)
	public @ResponseBody Collection<Process> getMyProcesses() {
		return asRead(query.getMyProcesses());
	}
	
	@RequestMapping(method = RequestMethod.GET, params = { "all=true" })
	public @ResponseBody Collection<? extends Process> getAllProcesses() {
		return query.getAllProcesses();
	}
	
	/**
	 * 
	 */
	private Map<String, Object> getEvent(Process Process) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("type", "event");
		result.put("name", "message");
		result.put("data", Process);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, params = { "pending=true" })
	public @ResponseBody Collection<Process> getMyPendingProcesses() {
		return asRead(query.getPendingProcesses());
	}
	
	@RequestMapping(value="{processId}", method = RequestMethod.GET)
	public @ResponseBody Process findProcess(@PathVariable("processId") String processId) {
		return read(query.findProcess(processId));
	}
	
	private Collection<Process> asRead(Collection<? extends Process> Processes) {
		Collection<Process> result = new ArrayList<Process>(Processes.size());
		for (Process Process : Processes) {
			result.add(read(Process));
		}
		return result;
	}

	private Process read(Process Process) {
		if (null != Process && !Process.isRead()) {
			synchronized (Process) {
                ((ProcessImpl)Process).setRead(true);
			}
		}
        return Process;
	}

}
