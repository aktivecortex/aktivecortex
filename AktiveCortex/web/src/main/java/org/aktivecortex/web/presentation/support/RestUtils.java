/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.web.presentation.support;

import org.aktivecortex.api.notification.Process;
import org.aktivecortex.core.notification.LinkImpl;
import org.aktivecortex.core.notification.ProcessImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

public class RestUtils {

	public static final String ASYNC_HANDLED_REASON = "in progress";
	public static final String SYNTAX_ERROR_REASON = "malformed syntax";
	public static final String ALREADY_SUBMITTED_REASON = "request already submitted";
	public static final String URL_REDIRECT_REASON = "URL has been temporarily redirected to another URL";


	public enum HttpResponseMessage {

		ASYNC_HANDLED_MULTIPART_POST(ASYNC_HANDLED_REASON, HttpStatus.CREATED),
		ASYNC_HANDLED_POST(ASYNC_HANDLED_REASON, HttpStatus.ACCEPTED), // async handled requests should return 202
		ASYNC_HANDLED_PUT(ASYNC_HANDLED_REASON, HttpStatus.ACCEPTED), // async handled requests should return 202
		ASYNC_HANDLED_DELETE(ASYNC_HANDLED_REASON, HttpStatus.ACCEPTED), // async handled requests should return 202
		INPUT_VALIDATION_FAILED(SYNTAX_ERROR_REASON, HttpStatus.BAD_REQUEST),
		DOUBLE_SUBMIT(ALREADY_SUBMITTED_REASON, HttpStatus.CONFLICT),
		INTEGRITY_VIOLATION_FAILURE(SYNTAX_ERROR_REASON, HttpStatus.PRECONDITION_FAILED),
		URL_REDIRECT(URL_REDIRECT_REASON, HttpStatus.MOVED_TEMPORARILY);

		private String reason;
		private HttpStatus statusCode;

		private HttpResponseMessage(String text, HttpStatus statusCode) {
			this.reason = text;
			this.statusCode = statusCode;
		}
		public String getReason() {
			return reason;
		}
		public HttpStatus getStatusCode() {
			return statusCode;
		}

	}

    public static ResponseEntity<String> INPUT_VALIDATION_FAILURE = buildResponseEntity(HttpResponseMessage.INPUT_VALIDATION_FAILED);

	public static ResponseEntity<String> buildResponseEntity(Process p, HttpHeaders headers) {
		return new ResponseEntity<String>(p.getId(), headers, HttpStatus.ACCEPTED);
	}
	
	public static ResponseEntity<String> buildResponseEntity(Process p, HttpStatus status, HttpHeaders headers) {
		return new ResponseEntity<String>(p.getId(), headers, status);
	}
	
	public static ResponseEntity<String> buildResponseEntity(HttpResponseMessage message, HttpHeaders headers) {
		return new ResponseEntity<String>(message.getReason(), headers, message.getStatusCode());
	}

	public static ResponseEntity<String> buildResponseEntity(HttpResponseMessage message) {
		return new ResponseEntity<String>(message.getReason(), message.getStatusCode());
	}

	public static ResponseEntity<String> buildResponseEntity(String message, HttpStatus status) {
		return new ResponseEntity<String>(message, status);
	}
	
	public static ResponseEntity<String> buildResponseEntity(String message, HttpHeaders headers, HttpStatus status) {
		return new ResponseEntity<String>(message, headers, status);
	}

	public static String buildLocationURI(HttpServletRequest req, Process process) {
		String context = req.getContextPath();
		String requestUrl = req.getRequestURL().toString();
		String revProxy = (String) req.getSession().getServletContext().getAttribute("org.aktivecortex.tools.internalurlprefix");
		String serverName = requestUrl.substring(0, requestUrl.indexOf(context));
		URI uri = new UriTemplate("{serverName}{revProxy}{context}/async/processes/{processId}").expand(serverName, revProxy, context, process.getId());
        ProcessImpl implementation = (ProcessImpl) process;
        synchronized (implementation) {
			implementation.withLink(new LinkImpl().withRel("self").withHref(uri.toASCIIString()));
		}
		return uri.toASCIIString();
	}

	public static String buildLocationURI(HttpServletRequest req, String resourceRelativeURI) {
		String requestUrl = req.getRequestURL().toString();
		URI uri = new UriTemplate("{requestUrl}/{resourceRelativeURI}").expand(requestUrl, resourceRelativeURI);
		return uri.toASCIIString();
	}

}
