/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.web.notification;

import org.aktivecortex.api.audit.SecurityContext;
import org.aktivecortex.api.notification.support.Observable;
import org.atmosphere.cpr.*;
import org.atmosphere.websocket.WebSocketEventListenerAdapter;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.aktivecortex.api.notification.support.Observable.PROCESS_PROPERTY_NAME;

/**
 * Atmosphere PubSub Controller that push {@link org.aktivecortex.api.notification.Process}
 * updates to web subscribers.
 * <p>This class acts as a remoting service with format adapting duties, that bridges clients interested to notifications
 * with the object that collects and manages the tracking of processes.</p>
 *
 * @author Domenico Maria Giffone
 * @since 1.0
 */
@RequestMapping(value = "/notification/processes")
public class PushNotificationService implements PropertyChangeListener {

	private static final Logger logger = LoggerFactory.getLogger(PushNotificationService.class);

	private SecurityContext context;

    /**
     * Security Context setter.
     * <p>
     * Security Context is needed to ensure that each subscriber
     * sees only updates of the processes he initiated.
     * </p>
     * @param securityContext
     */
    public void setSecurityContext(SecurityContext securityContext) {
		this.context = securityContext;
	}

    /**
     * Observable setter.
     * <p>Here we set the object that we want to observe-</p>
     * @param observable
     */
	public void setObservable(Observable observable) {
		observable.addPropertyChangeListener(this);
	}

	/**
	 * This method permits client subscription to process updates.
     * <p>
     * The socket associated with the request will remain open.
     * <p>
     * When an update event is received through the {@link #propertyChange} method
     * it's written as a JSON string on the subscriber socket.
     * </p>
     * </p>
     * <p>This call is non-blocking as long as the Servlet Container is using NIO to serve http requests.</p>
     * <p>For example Apache Tomcat has an appropriate NIO Connector for the http protocol. <pre>{@code
     *
     *  <Connector protocol="org.apache.coyote.http11.Http11NioProtocol"  port="8080" redirectPort="8443"/>
     * }</pre></p>
     *
	 * 
	 * @param request
	 * @return void
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Void> subscribe(HttpServletRequest request) throws Exception {
		AtmosphereResource resource = (AtmosphereResource) request.getAttribute(FrameworkConfig.ATMOSPHERE_RESOURCE);
		this.doGet(resource, request, resource.getResponse());
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	private void doGet(AtmosphereResource r, HttpServletRequest req, HttpServletResponse res) {
		r.addEventListener(new WebSocketEventListenerAdapter());
		res.setContentType("text/html;charset=utf-8");
		Broadcaster b = lookupBroadcaster(context.getSessionId());
		r.setBroadcaster(b);
		String header = req.getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);
		if (HeaderConfig.LONG_POLLING_TRANSPORT.equalsIgnoreCase(header)) {
			req.setAttribute(ApplicationConfig.RESUME_ON_BROADCAST, Boolean.TRUE);
			r.suspend(-1, false);
		} else {
			r.suspend(-1);
		}
	}

	/**
	 * Retrieve the {@link Broadcaster} based on the session id.
	 * 
	 * @return the {@link Broadcaster} based on the session id.
	 */
	Broadcaster lookupBroadcaster(Object id) {
		return BroadcasterFactory.getDefault().lookup(id, true);
	}

    /**
     * Observer implementing method that receive update events.
     * @param evt
     */
	@Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (PROCESS_PROPERTY_NAME.equals(evt.getPropertyName())) {
            try {
                String msg = new ObjectMapper().writeValueAsString(evt.getNewValue());
                Broadcaster b = lookupBroadcaster(evt.getPropagationId());
                b.broadcast(msg);
            } catch (Exception e) {
                logger.error("unable to publish message", e);
            }
        }
    }

}
