/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.web.presentation.support;


import org.aktivecortex.api.message.MessageHeadersConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.common.base.Strings.isNullOrEmpty;
import static org.aktivecortex.web.commandhandling.WebCommandGateway.HEADER_PROC_ID;


/**
 * HandlerInterceptor that capture Request Header Cortex Process ID {@link org.aktivecortex.web.commandhandling.WebCommandGateway#HEADER_PROC_ID} if present
 * and inject it in {@link org.slf4j.MDC} context.
 * <p>Useful when a long running process is made of one or more external step.</p>
 */
public class ProcessContinuationInterceptor extends HandlerInterceptorAdapter {

    private final static Logger LOGGER = LoggerFactory.getLogger(ProcessContinuationInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String processId = request.getHeader(HEADER_PROC_ID);
        if (! isNullOrEmpty(processId)) {
            MDC.put(MessageHeadersConstants.NOTIFICATION_PROCESS_ID, processId);
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        MDC.clear();
    }
}
