/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.web.commandhandling;

import com.google.common.collect.ImmutableListMultimap;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.commandhandling.AsyncCommandBus;
import org.aktivecortex.api.notification.ConversationManager;
import org.aktivecortex.api.notification.Process;
import org.aktivecortex.api.notification.ProcessQueryService;
import org.aktivecortex.api.notification.ProgressEvaluator;
import org.aktivecortex.core.commandhandling.DefaultCommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Multimaps.asMap;
import static org.aktivecortex.web.presentation.support.RestUtils.buildLocationURI;
import static org.aktivecortex.web.presentation.support.RestUtils.buildResponseEntity;

/**
 * Web oriented extension of the {@link DefaultCommandGateway} class
 * to support the development of REST services that send commands on the {@link AsyncCommandBus}
 * <p>By implementing the {@link WebCommandGateway}</p> interface it will build a {@link ResponseEntity}
 * containing the Process Id that describes the command sent.
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
@Component
public class DefaultWebCommandGateway extends DefaultCommandGateway implements WebCommandGateway {

    /**
     * Spring Autowired constructor
     *
     * @param commandBus
     * @param conversationManager
     * @param processQueryService
     */
    @Autowired
    public DefaultWebCommandGateway(AsyncCommandBus commandBus,
                                    ConversationManager conversationManager,
                                    ProcessQueryService processQueryService) {
        super(commandBus, conversationManager, processQueryService);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<String> send(HttpServletRequest request, Command... commands) {
        Process p = send(commands);
        return responseEntity(request, p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<String> send(HttpServletRequest request, Map<String,Collection<String>> httpHeaders, Command... commands) {
        Process p = send(commands);
        return responseEntity(request,httpHeaders, p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<String> send(HttpServletRequest request, ProgressEvaluator progressEvaluator, Command... commands) {
        Process p = send(progressEvaluator, commands);
        return responseEntity(request, p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<String> send(HttpServletRequest request, Map<String,Collection<String>> httpHeaders, ProgressEvaluator progressEvaluator, Command... commands) {
        Process p = send(progressEvaluator, commands);
        return responseEntity(request,httpHeaders, p);
    }

    private ResponseEntity<String> responseEntity(HttpServletRequest request, Map<String,Collection<String>> headers, Process p) {
        checkNotNull(headers, "Headers can't be null");
        final ImmutableListMultimap.Builder<String, String> builder = new ImmutableListMultimap.Builder<String, String>()
                .put(HEADER_PROC_LOCATION, buildLocationURI(request, p))
                .put(HEADER_PROC_ID, p.getId());
        for (Map.Entry<String, Collection<String>> entry : headers.entrySet()) {
            builder.putAll(entry.getKey(), entry.getValue());
        }
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.putAll(asMap(builder.build()));
        return buildResponseEntity(p, httpHeaders);
    }

    private ResponseEntity<String> responseEntity(HttpServletRequest request, Process p) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set(HEADER_PROC_LOCATION, buildLocationURI(request, p));
        responseHeaders.set(HEADER_PROC_ID, p.getId());
        return buildResponseEntity(p,responseHeaders);
    }


}
