/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail;


import org.aktivecortex.core.mail.Attachment.MimeType;
import org.aktivecortex.core.serializer.ProtostuffSerializer;
import org.aktivecortex.dbc.exception.PreConditionException;
import org.apache.commons.lang.ArrayUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.*;

public class MessageBuilderTest {
	
	private static final String SUBJECT = "Test e-mail";
	
	private static final String BODY = "This is a test";
	
	private static final String FROM_NAME = "Domenico Giffone";
	
	private static final String FROM_ADRS = "d.giffone@reply.it";
	
	private static final String[] TO_ADRS = new String[] {"coli0-support@reply.it", "r.vanzo@reply.it"};
	
	private static final String[] CC_ADRS = new String[] {"bar@mail.com", "foo@mail.com"};
	
	private static final String[] BCC_ADRS = new String[] {"bar@hidden.com", "foo@hidden.com"};
	
	private static final Attachment<? extends Serializable> FILE = new FileAttachment("build.xml", "build.xml", MimeType.XML);

	private static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
	
	private EmailMessage email;
	
	private DeliveryOptions options;

	private ProtostuffSerializer<EmailMessage> serializer;
	
	// TODO registrare la IdStrategyFactory nello ProtostuffRuntimeSchemaAdapter
//	static {
//			System.setProperty("protostuff.runtime.id_strategy_factory", "org.aktivecortex.core.mail.MessageBuilderTest$IdStrategyFactory");
//		}
//
//		public static class IdStrategyFactory implements IdStrategy.Factory {
//
//			DefaultIdStrategy strategy = new DefaultIdStrategy();
//
//			public IdStrategyFactory() {
//			}
//
//			public IdStrategy create() {
//				return strategy;
//			}
//
//			public void postCreate() {
//				strategy.registerCollection(new CollectionSchema.MessageFactory() {
//
//					@Override
//					public Collection<AssociationValue> newMessage() {
//						return new AssociationValuesImpl();
//					}
//
//					@Override
//					public Class<AssociationValues> typeClass() {
//						return AssociationValues.class;
//					}
//
//				});
//				strategy.registerMap(new MapSchema.MessageFactory() {
//
//					@Override
//					public Map<String, Object> newMessage() {
//						return new MessageHeaders(null, null);
//					}
//
//					@Override
//					public Class<MessageHeaders> typeClass() {
//						return MessageHeaders.class;
//					}
//				});
//			}
//
//		}


	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		options = new DeliveryOptions();
		options.setSenderName(FROM_NAME);
		options.setSenderAddress(FROM_ADRS);
		options.setToAddresses(TO_ADRS);
		options.setCCAddresses(CC_ADRS);
		options.setBCCAddresses(BCC_ADRS);
		Set<Attachment<?>> attachments = new HashSet<Attachment<?>>();
		attachments.add(FILE);
		email = new EmailMessage(options, SUBJECT, BODY, null, null, attachments);
		this.serializer = new ProtostuffSerializer<EmailMessage>();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testNewBuilder() {
		EmailMessage msg = new MessageBuilder()
		.withSubject(SUBJECT)
		.withTextBody(BODY)
		.withSenderName(FROM_NAME)
		.withSenderAddress(FROM_ADRS)
		.withToAddresses(TO_ADRS)
		.withCCAddresses(CC_ADRS)
		.withBCCAddresses(BCC_ADRS)
		.build();
		assertTrue(msg.equalsIgnoreOptions(email));
	}
	
	@Test
	public void testWithAttachments() {
		EmailMessage msg = new MessageBuilder()
		.withSubject(SUBJECT)
		.withTextBody(BODY)
		.withSenderName(FROM_NAME)
		.withSenderAddress(FROM_ADRS)
		.withToAddresses(TO_ADRS)
		.withCCAddresses(CC_ADRS)
		.withBCCAddresses(BCC_ADRS)
		.withAttachment(FILE)
		.build();
		assertTrue(msg.hasAttachments());
		assertEquals(msg.getAttachements(), newHashSet(FILE));
		assertFalse(msg.getAttachements().equals(newHashSet(new FileAttachment(".project", ".project", MimeType.XML))));
	}
	
	@Test
	public void testWithByteAttachments() {
		ByteArrayAttachment attachment = new ByteArrayAttachment(new StringBuilder().append(LOREM_IPSUM).toString().getBytes(), MimeType.PLAIN_TEXT, "Lorem Ipsum");
		EmailMessage msg = new MessageBuilder()
		.withSubject(SUBJECT)
		.withTextBody(BODY)
		.withSenderName(FROM_NAME)
		.withSenderAddress(FROM_ADRS)
		.withToAddresses(TO_ADRS)
		.withCCAddresses(CC_ADRS)
		.withBCCAddresses(BCC_ADRS)
		.withAttachment(attachment)
		.build();
		assertTrue(msg.hasAttachments());
		assertEquals(msg.getAttachements(), newHashSet(attachment));
		Byte[] byteContent = (Byte[]) msg.getAttachements().iterator().next().getContent();
		String content = new String(ArrayUtils.toPrimitive(byteContent));
		assertEquals(LOREM_IPSUM, content);
	}
	
	
	
	@Test
	public void testSerialization() throws IOException, ClassNotFoundException {
		ByteArrayAttachment attachment = new ByteArrayAttachment(new StringBuilder().append(LOREM_IPSUM).toString().getBytes(), MimeType.PLAIN_TEXT, "Lorem Ipsum");
		EmailMessage msg = new MessageBuilder()
		.withSubject(SUBJECT)
		.withTextBody(BODY)
		.withSenderName(FROM_NAME)
		.withSenderAddress(FROM_ADRS)
		.withToAddresses(TO_ADRS)
		.withCCAddresses(CC_ADRS)
		.withBCCAddresses(BCC_ADRS)
		.withAttachment(attachment)
		.build();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(msg);
		oos.close();
		
		byte[] buff = baos.toByteArray();
		InputStream is = new ByteArrayInputStream(buff);
		ObjectInputStream ois = new ObjectInputStream(is);
		Object read = ois.readObject();
		EmailMessage clone = (EmailMessage) read;
		Byte[] byteContent = (Byte[]) clone.getAttachements().iterator().next().getContent();
		String content = new String(ArrayUtils.toPrimitive(byteContent));
		System.out.println("testSerialization");
		System.out.println("original text: " + LOREM_IPSUM);
		System.out.println("clone text:    " + content);
		System.out.println("original: " + msg.toString());
		System.out.println("clone:    " + clone.toString());
		assertEquals(LOREM_IPSUM, content);
		assertEquals(msg, clone);
	}
	
	@Test
	public void testProtostuffSerialization()  {
		ByteArrayAttachment attachment = new ByteArrayAttachment(new StringBuilder().append(LOREM_IPSUM).toString().getBytes(), MimeType.PLAIN_TEXT, "Lorem Ipsum");
		EmailMessage msg = new MessageBuilder()
		.withSubject(SUBJECT)
		.withTextBody(BODY)
		.withSenderName(FROM_NAME)
		.withSenderAddress(FROM_ADRS)
		.withToAddresses(TO_ADRS)
		.withCCAddresses(CC_ADRS)
		.withBCCAddresses(BCC_ADRS)
		.withAttachment(attachment)
		.build();
		
		byte[] buff = serializer.serialize(msg);
		EmailMessage clone = serializer.deserialize(buff);
		Byte[] byteContent = (Byte[]) clone.getAttachements().iterator().next().getContent();
		String content = new String(ArrayUtils.toPrimitive(byteContent));
		System.out.println("testSerialization");
		System.out.println("original text: " + LOREM_IPSUM);
		System.out.println("clone text:    " + content);
		System.out.println("original: " + msg.toString());
		System.out.println("clone:    " + clone.toString());
		assertEquals(LOREM_IPSUM, content);
		assertEquals(msg, clone);
	}
	
	@Test(expected= PreConditionException.class)
	public void testNullValues() throws IOException, ClassNotFoundException {
		Map<String, Object> model = newHashMap();
		model.put("mailText", null);
		EmailMessage msg = new MessageBuilder()
		.withSubject(null)
		.withTextBody("ciao")
		.withModelAndTemplate(model, null)
		.withSenderName(null)
		.withSenderAddress(FROM_ADRS)
		.withCCAddresses(new String[] {null})
		.withToAddresses(TO_ADRS)
		.build();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(msg);
		oos.close();
		
		byte[] buff = baos.toByteArray();
		InputStream is = new ByteArrayInputStream(buff);
		ObjectInputStream ois = new ObjectInputStream(is);
		Object read = ois.readObject();
		EmailMessage clone = (EmailMessage) read;
		System.out.println("testNullValues");
		System.out.println("original: " + msg.toString());
		System.out.println("clone: " + clone.toString());
		assertEquals(msg, clone);
	}
	
	@Test
	public void testCloneMail() {
		EmailMessage msg = MessageBuilder.fromMessage(email).build();
//		assertEquals(msg, email);
		assertTrue(msg.equalsIgnoreOptions(email));
	}
	
	@Test
	public void testCloneAndChangeMail() {
		EmailMessage msg = MessageBuilder.fromMessage(email)
		.withSubject("new subject")
		.build();
		assertTrue(msg.equalsIgnoreOptions(new EmailMessage(options, "new subject", BODY, null, null, null)));
	}
	
	@Test
	public void testToString() {
		System.out.println("testToString");
		System.out.println(email);
	}

}

