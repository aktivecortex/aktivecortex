/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.aktivecortex.dbc.assertion.Contract;
import org.apache.commons.lang.StringUtils;
import org.axonframework.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MessageBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageBuilder.class);
    public static final String TIMESTAMP = "timestamp";
    private final Map<String, Object> headers = new HashMap<String, Object>();
	private final Set<Attachment<?>> attachments = new HashSet<Attachment<?>>();
	private final DeliveryOptions deliveryOptions;
	private final EmailMessage originalMessage;
	private volatile boolean modified;
	private String subject;
	private String bodyAsText;
	private String bodyAsHTML;

	/**
	 *
	 */
	public MessageBuilder() {
		this.deliveryOptions = new DeliveryOptions();
		this.originalMessage = null;
		modified = true;
	}

	/**
	 * @param options
	 * @param originalMessage
	 */
	private MessageBuilder(DeliveryOptions options, EmailMessage originalMessage) {
		Assert.notNull(originalMessage, "originalMessage must not be null");
		Assert.notNull(options, "deliveryOptions must not be null");
		this.originalMessage = originalMessage;
		this.subject = originalMessage.getSubject();
		this.bodyAsText = originalMessage.getBodyAsText();
		this.bodyAsHTML = originalMessage.getBodyAsHtml();
		this.deliveryOptions = new DeliveryOptions(options);
		copyHeaders(originalMessage.getHeaders());
	}

	public static MessageBuilder fromMessage(EmailMessage originalMessage) {
		return new MessageBuilder(originalMessage.getDeliveryOptions(), originalMessage);
	}

	public MessageBuilder withSubject(String emalSubject) {
		this.subject = emalSubject;
		return this;
	}

	public MessageBuilder withTextBody(String body) {
		this.bodyAsText = body;
		return this;
	}

	public MessageBuilder withHTMLBody(String body) {
		this.bodyAsHTML = body;
		return this;
	}

	public MessageBuilder withBCCAddresses(String... bccAddresses) {
		deliveryOptions.setBCCAddresses(bccAddresses);
		return this;
	}

	public MessageBuilder withCCAddresses(String... ccAddresses) {
		deliveryOptions.setCCAddresses(ccAddresses);
		return this;
	}

	public MessageBuilder withSenderAddress(String senderAddress) {
		Contract.REQUIRE.isTrue(StringUtils.isNotBlank(senderAddress),
				"L'indirizzo del mittente non può essere nullo o vuoto",
				"L'indirizzo del mittente fornito non è un valore valido: [{}]", senderAddress);
		deliveryOptions.setSenderAddress(senderAddress);
		return this;
	}



	public MessageBuilder withSenderName(String senderName) {
		deliveryOptions.setSenderName(senderName);
		return this;
	}

	public MessageBuilder withToAddresses(String... toAddresses) {
		Contract.REQUIRE.isTrue(null!= toAddresses && toAddresses.length > 0,
				"Deve esistere almeno un destinatario",
				"Array degli indirizzi dei destinatari nullo o vuoto.");
		for (String address : toAddresses) {
			Contract.REQUIRE.isTrue(StringUtils.isNotBlank(address),
					"L'indirizzo del destinatario non può essere nullo o vuoto",
					"L'indirizzo del destinatario fornito non è un valore valido: [{}]", address);
		}
		deliveryOptions.setToAddresses(toAddresses);
		return this;
	}

	public  MessageBuilder withAttachment(Attachment<?>...  args) {
		for (Attachment<?> fileAttachment : args) {
			this.attachments.add(fileAttachment);
		}
		return this;
	}

	public MessageBuilder setHeaderIfAbsent(String headerName, Object headerValue) {
		if (headers.get(headerName) == null) {
            setHeader(headerName, headerValue);
        }
		return this;
	}

	public MessageBuilder withModelAndTemplate(Map<String, Object> mapModel, String templateName) {
		setHeader(MessageHeaders.TEMPLATE, templateName);
		setHeader(MessageHeaders.MODEL, mapModel);
		return this;
	}

	public MessageBuilder removeHeader(String headerName) {
		if (StringUtils.isNotBlank(headerName) && !headerName.equals("id") && !headerName.equals(TIMESTAMP)) {
			Object removedValue = headers.remove(headerName);
			if (removedValue != null) {
                modified = true;
            }
		}
		return this;
	}

	public MessageBuilder copyHeaders(Map<String, Object> headersToCopy) {
		Set<Entry<String, Object>> entries = headersToCopy.entrySet();
		for (Entry<String, Object> entry : entries) {
			if (!isReadOnly(entry.getKey())) {
                setHeader(entry.getKey(), entry.getValue());
            }
		}
		return this;
	}

	private boolean isReadOnly(String headerName) {
		return "id".equals(headerName) || TIMESTAMP.equals(headerName);
	}

	public MessageBuilder setHeader(String headerName, Object headerValue) {
		Assert.isTrue(!isReadOnly(headerName), (new StringBuilder()).append("The '").append(headerName).append("' header is read-only.").toString());
		if (StringUtils.isNotBlank(headerName) && !headerName.equals("id") && !headerName.equals(TIMESTAMP)) {
			verifyType(headerName, headerValue);
			if (headerValue == null) {
				Object removedValue = headers.remove(headerName);
				if (removedValue != null) {
                    modified = true;
                }
			} else {
				Object replacedValue = headers.put(headerName, headerValue);
				if (!headerValue.equals(replacedValue)) {
                    modified = true;
                }
			}
		}
		return this;
	}

	private void verifyType(String headerName, Object headerValue) {
		if (headerName != null && headerValue != null) {
            if ("id".equals(headerName)) {
                Assert.isTrue(headerValue instanceof String, (new StringBuilder()).append("The '").append(headerName)
                        .append("' header value must be a String.").toString());
            }
            else if (TIMESTAMP.equals(headerName)) {
                Assert.isTrue(headerValue instanceof Long, (new StringBuilder()).append("The '").append(headerName).append("' header value must be a Long.")
                        .toString());
            }
        }
	}

	public EmailMessage build() {
		if (!modified && originalMessage != null) {
            return originalMessage;
        }
		verifyProperties();
		return new EmailMessage(deliveryOptions, subject, bodyAsText, bodyAsHTML, headers, attachments);
	}

	private void verifyProperties() {
		verifySender();
		verifyRecipients();
		verifyContent();
	}

	@SuppressWarnings("unchecked")
	private void verifyContent() {
		boolean emptySubject=false, emptyContent=false;
		if (StringUtils.isBlank(subject)) {
			LOGGER.warn("Subject empty or null!");
			emptySubject = true;
		}
		Object result = headers.get(MessageHeaders.TEMPLATE);
		String template = null;
		if (result instanceof String) {
			 template= (String) result;
		}
		result = headers.get(MessageHeaders.MODEL);
		Map<String, Object> model = null;
		if (result instanceof Map) {
			model = (Map<String, Object>) result;
		}
		if (StringUtils.isBlank(bodyAsText) && StringUtils.isBlank(bodyAsHTML) && (StringUtils.isBlank(template) || (null == model || model.isEmpty()))) {
			LOGGER.warn("Body empty or null");
			emptyContent = true;
		}
		Contract.REQUIRE.isTrue(!(emptyContent && emptySubject),
					"Email message must contains either a not empty subject or body",
					"Subject and message body are both null or empty!.");
	}

	@SuppressWarnings("unchecked")
	private void verifyRecipients() {
		Collection<String> recipients = new ArrayList<String>();
		Collection<String> toAddresses = (Collection<String>) deliveryOptions.get(DeliveryOptions.TO);
		if (null == toAddresses || toAddresses.isEmpty()) {
			Contract.REQUIRE.isTrue(null!= toAddresses && toAddresses.size() > 0,
					"A direct recipient must exists [TO]",
					"Recipient address list [TO] empty or null.");
		}
		recipients.addAll(toAddresses);
		Collection<String> ccAddresses = (Collection<String>) deliveryOptions.get(DeliveryOptions.CC);
		if (!(null == ccAddresses || ccAddresses.isEmpty())) {
			recipients.addAll(ccAddresses);
		}
		Collection<String> bccAddresses = (Collection<String>) deliveryOptions.get(DeliveryOptions.BCC);
		if (!(null == bccAddresses || bccAddresses.isEmpty())) {
			recipients.addAll(bccAddresses);
		}
		for (String address : recipients) {
			Contract.REQUIRE.isTrue(StringUtils.isNotBlank(address),
					"Recipient address must be not null or empty",
					"Invalid recipient address provided: [{}]", address);
		}
	}

	/**
	 *
	 */
	private void verifySender() {
		Object result = deliveryOptions.get(DeliveryOptions.SENDER_ADRS);
		String senderAddress = null;
		if (result instanceof String) {
			senderAddress = (String) result;
		}
		Contract.REQUIRE.isTrue(StringUtils.isNotBlank(senderAddress),
				"Sender address must be not null or empty",
				"Invalid sender address provided: [{}]", senderAddress);
	}
}
