/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail;

import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.axonframework.util.Assert;

public class DeliveryOptions implements Map<String, Object>, Serializable {

	private static final long serialVersionUID = 1L;

	static final String TO = "TO";
	static final String CC = "CC";
	static final String BCC = "BCC";

	static final String SENDER_ADRS = "SENDER_ADRS";
	static final String SENDER_NAME = "SENDER_NAME";

	private Map<String, Object> headers;

	public DeliveryOptions() {
		headers = new HashMap<String, Object>();
	}

	DeliveryOptions(Map<String, Object> headers) {
		Assert.notNull(headers, "headers must not be null");
		this.headers = new HashMap<String, Object>(headers);
	}

	public void setToAddresses(String... toAddresses) {
		setRecipients(DeliveryOptions.TO, toAddresses);
	}

	public void setCCAddresses(String... ccAddresses) {
		setRecipients(DeliveryOptions.CC, ccAddresses);
	}

	public void setBCCAddresses(String... bccAddresses) {
		setRecipients(DeliveryOptions.BCC, bccAddresses);
	}

	public void setSenderName(String senderName) {
		headers.put(DeliveryOptions.SENDER_NAME, senderName);
	}

	public void setSenderAddress(String senderAddress) {
		headers.put(DeliveryOptions.SENDER_ADRS, senderAddress);
	}

	private void setRecipients(String key, String[] addresses) {
		headers.put(key, newHashSet(addresses));
	}

	public void clear() {
		headers.clear();
	}

	public boolean containsKey(Object key) {
		return headers.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return headers.containsValue(value);
	}

	public Set<Entry<String, Object>> entrySet() {
		return headers.entrySet();
	}

	@Override
	public boolean equals(Object o) {
		return headers.equals(o);
	}

	public Object get(Object key) {
		return headers.get(key);
	}

	@Override
	public int hashCode() {
		return headers.hashCode();
	}

	public boolean isEmpty() {
		return headers.isEmpty();
	}

	public Set<String> keySet() {
		return headers.keySet();
	}

	public Object put(String key, Object value) {
		return headers.put(key, value);
	}

	public void putAll(Map<? extends String, ? extends Object> m) {
		headers.putAll(m);
	}

	public Object remove(Object key) {
		return headers.remove(key);
	}

	public int size() {
		return headers.size();
	}

	public Collection<Object> values() {
		return headers.values();
	}

}
