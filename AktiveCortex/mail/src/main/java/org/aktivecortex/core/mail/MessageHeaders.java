/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail;

import com.dyuproject.protostuff.MapSchema;
import com.dyuproject.protostuff.runtime.DefaultIdStrategy;
import com.dyuproject.protostuff.runtime.RuntimeEnv;
import com.eaio.uuid.UUID;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.SetMultimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

public class MessageHeaders implements Map<String, Object>, Serializable {

    static {
        DefaultIdStrategy strategy = (DefaultIdStrategy) RuntimeEnv.ID_STRATEGY;
        strategy.registerMap(new MapSchema.MessageFactory() {

            @Override
            public Map<String, Object> newMessage() {
                return new MessageHeaders(null, null);
            }

            @Override
            public Class<MessageHeaders> typeClass() {
                return MessageHeaders.class;
            }
        });
    }

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageHeaders.class);
	public static final String ID = "id";
	public static final String TIMESTAMP = "timestamp";
	public static final String ATTACHMENTS = "ATTACHMENTS";
	public static final String MODEL = "MODEL";
	public static final String TEMPLATE = "TEMPLATE";
	private final SetMultimap<String, Object> headers;

	public MessageHeaders(Map<String, Object> args, Iterable<Attachment<?>> attachments) {
		this.headers = HashMultimap.create();
		if (args != null) {
			for (Entry<String, Object> entry : args.entrySet()) {
				this.headers.put(entry.getKey(), entry.getValue());
			}
		}
		if (null != args) {
		this.headers.put(ID, new UUID().toString());  //NOSONAR
		this.headers.put(TIMESTAMP, System.currentTimeMillis());
		}
		if (attachments != null) {
			this.headers.putAll(ATTACHMENTS, attachments);
		}
	}

	public String getId() {
		return (String) get(ID, String.class);
	}

	public Long getTimestamp() {
		return (Long) get(TIMESTAMP, Long.class);
	}

	@SuppressWarnings("unchecked")
	public Object get(String key, Class type) {
		Object value = get(key);
		if (value == null) {
            return null;
        }
		if (!type.isAssignableFrom(value.getClass())) {
            throw new IllegalArgumentException((new StringBuilder()).append("Incorrect type specified for header '").append(key).append("'. Expected [")
                    .append(type).append("] but actual type is [").append(value.getClass()).append("]").toString());
        }
		return value;
	}

	@Override
	public int hashCode() {
		return headers.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
            return true;
        }
		if (obj != null && (obj instanceof MessageHeaders)) {
			MessageHeaders other = (MessageHeaders) obj;
			return headers.equals(other.headers);
		}
		return false;
	}

	@Override
	public String toString() {
		return headers.toString();
	}

	@Override
	public boolean containsKey(Object key) {
		return headers.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return headers.containsValue(value);
	}

    @Override
	public Set<Map.Entry<String, Object>> entrySet() {
		return Collections.unmodifiableSet(headers.entries());
	}

	@Override
	public Object get(Object key) {
		Set<Object> result = headers.get((String) key);
		return (result!= null && result.size() == 1) ? Iterables.getOnlyElement(result) : result;
	}

	@Override
	public boolean isEmpty() {
		return headers.isEmpty();
	}

	@Override
	public Set<String> keySet() {
		return Collections.unmodifiableSet(headers.keySet());
	}

	@Override
	public int size() {
		return headers.size();
	}

	@Override
	public Collection<Object> values() {
		return Collections.unmodifiableCollection(headers.values());
	}

	@Override
	public Object put(String key, Object value) {
		Object pv = headers.get(key);
		headers.put(key, value);
		return pv;
	}

	@Override
	public void putAll(@SuppressWarnings("rawtypes") Map t) {
		throw new UnsupportedOperationException("MessageHeaders is immutable.");
	}

	@Override
	public Object remove(Object key) {
		throw new UnsupportedOperationException("MessageHeaders is immutable.");
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("MessageHeaders is immutable.");
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		Collection<String> keysToRemove = new ArrayList<String>();
		for (Map.Entry<String, Object> entry : headers.entries()) {
			if (!(entry.getValue() instanceof Serializable)) {
                keysToRemove.add(entry.getKey());
            }
		}
		for (String key : keysToRemove) {
			LOGGER.info((new StringBuilder()).append("removing non-serializable header: ").append(key).toString());
			headers.removeAll(key);
		}
		out.defaultWriteObject();
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
	}

}
