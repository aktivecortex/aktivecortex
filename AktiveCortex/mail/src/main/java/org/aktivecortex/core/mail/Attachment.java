/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail;

import java.io.Serializable;

import com.google.common.base.Objects;


public class Attachment<T> implements Serializable {

	private static final long serialVersionUID = 1L;

    private T content;
    private MimeType mimeType;
    private String title;

	public T getContent() {
		return content;
	}

	public MimeType getMimeType() {
		return mimeType;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + contentHashCode();
		result = prime * result + ((mimeType == null) ? 0 : mimeType.hashCode());
		return result;
	}

    protected int contentHashCode() {
        return ((content == null) ? 0 : content.hashCode());
    }

    @Override
	public boolean equals(Object obj) {
		if (this == obj) {
            return true;
        }
		if (obj == null) {
            return false;
        }
		if (getClass() != obj.getClass()) {
            return false;
        }
		final Attachment other = (Attachment) obj;
		if (title == null) {
			if (other.title != null) {
                return false;
            }
		} else if (!title.equals(other.title)) {
            return false;
        }
        if (contentDiffers(other)) {
            return false;
        }
        if (mimeType == null) {
			if (other.mimeType != null) {
                return false;
            }
		} else if (!mimeType.equals(other.mimeType)) {
            return false;
        }
		return true;
	}

    protected boolean contentDiffers(Object obj) {
        final Attachment other = (Attachment) obj;
        if (content == null) {
            if (other.content != null) {
                return true;
            }
        } else if (!content.equals(other.content)) {
            return true;
        }
        return false;
    }

    /**
	 * @param content
	 * @param mimeType
	 * @param title
	 */
	protected Attachment(T content, MimeType mimeType, String title) {
		this.content = content;
		this.mimeType = mimeType;
		this.title = title;
	}

	public enum MimeType
	{
	    ZIP ("application/zip", "zip"),
	    XLS ("application/vnd.ms-excel", "xls"),
	    PDF ("application/pdf", "pdf"),
	    HTML ("text/html", "html"),
	    XML ("application/xml", "xml"),
	    PLAIN_TEXT ("text/plain", "txt");



	    private String contentType;
	    private String fileExtension;

	    private MimeType (String contentType, String fileExtension)
	    {
	        this.contentType = contentType;
	        this.fileExtension = fileExtension;
	    }

	    public String getContentType()
	    {
	        return contentType;
	    }

	    public String getFileExtension()
	    {
	        return fileExtension;
	    }
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("name", getTitle()).add("mime-type",
				mimeType.getContentType()).add("content", getContent()).toString();
	}

}
