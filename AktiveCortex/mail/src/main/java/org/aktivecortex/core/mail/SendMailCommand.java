/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail;

import java.util.Map;

import org.aktivecortex.api.command.AbstractCommand;

import com.google.common.collect.ImmutableMap;

public class SendMailCommand extends AbstractCommand {

	private static final long serialVersionUID = 1L;
	
	private final EmailMessage message;
	

	public EmailMessage getMessage() {
		return message;
	}
	
	/**
	 * @param message
	 */
	public SendMailCommand(EmailMessage message) {
		this.message = message;
	}


	@Override
	public Map<String, Object> getSignificantFields() {
		return ImmutableMap.of("message", (Object) message);
	}
	
	@Override
	public String getRoutingKey() {
		return null;
	}


	@Override
	public String getAggregateIdentifier() {
		return null;
	}

	@Override
	public String getAggregateType() {
		return null;
	}

}
