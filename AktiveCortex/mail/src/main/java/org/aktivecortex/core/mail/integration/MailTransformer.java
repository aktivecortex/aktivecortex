/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail.integration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.aktivecortex.core.mail.Attachment;
import org.aktivecortex.core.mail.EmailMessage;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Transformer;
import org.springframework.mail.MailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.ui.velocity.VelocityEngineUtils;


public class MailTransformer {

	private static final Logger logger = LoggerFactory.getLogger(MailTransformer.class);

	private JavaMailSender mailSender;

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	private VelocityEngine velocityEngine;

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	@SuppressWarnings("cast")
	@Transformer
	public MailMessage transform(EmailMessage source) {
		logger.debug("Transforming e-mail message [{}]", source);
		Multipart multipart = new MimeMultipart();
		MimeBodyPart messagePart = new MimeBodyPart();
		MimeMessage message = mailSender.createMimeMessage();
		try {
			// validate delivery options
			if (StringUtils.isBlank(source.getSender()) || source.getToAddresses().isEmpty() || StringUtils.isBlank(source.getSubject())) {
				String errorMessage = String.format("Trasformation failed. Illegal arguments: mailFrom:[%s], mailTo: [%s], subject: [%s]", source.getSender(), source.getToAddresses(), source.getSubject());
				logger.warn(errorMessage);
				throw new IllegalArgumentException(errorMessage);
			}

			// set delivery options
			message.setFrom(new InternetAddress(source.getSender()));

			for (String to : extractAddresses(source.getToAddresses())) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
				logger.debug("recipient TO: " + to);
			}

			for (String cc : extractAddresses(source.getCCAddresses())) {
				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
				logger.debug("recipient CC: " + cc);
			}

			for (String bcc : extractAddresses(source.getBCCAddresses())) {
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
				logger.debug("recipient BCC: " + bcc);
			}

			// Set subject
			message.setSubject(source.getSubject());

			// Set body
			// Prefer template if exists
			if (source.hasModel() && source.hasTemplate()) {
				logger.debug("Setting html body by merging template [{}] with model [{}]", source.getTemplateName(), source.getModel());
				String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, source.getTemplateName(), source.getModel());
				messagePart.setText(body, null, "html");
			} else {
				if (source.hasHtmlBody()) {
					logger.debug("Setting static html body");
					messagePart.setText(source.getBodyAsHtml(), null, "html");
				}
			}
			if (source.hasTextBody()) {
				logger.debug("Setting static text body");
				messagePart.setText(source.getBodyAsText());
			}
			multipart.addBodyPart(messagePart);

			// Add attachments
			if (source.hasAttachments()) {
				for (Attachment<?> attachment : source.getAttachements()) {
					logger.debug("Attaching content [{}]", attachment.getTitle());
					MimeBodyPart attachmentPart = new MimeBodyPart();
					Object payload = attachment.getContent();
					DataHandler handler = null;
					if (payload instanceof File) {
						handler = new DataHandler(new FileDataSource((File)payload));
					} else if (payload instanceof Byte[]) {
						byte[] a = ArrayUtils.toPrimitive((Byte[])payload);
						 
						handler = new DataHandler(new ByteArrayDataSource(
								a, 
								attachment.getMimeType().getContentType())); 
					} else {
						throw new IllegalArgumentException("Attachment type not handled: " + payload.getClass().getName());
					}
					attachmentPart.setDataHandler(handler);
					attachmentPart.setFileName(attachment.getTitle());
					multipart.addBodyPart(attachmentPart);
				}
			}
			logger.info("e-mail created: [{}]", source);
			message.setContent(multipart);
			return new MimeMailMessage(message);
		} catch (MessagingException e) {
			logger.error("error: ", e);
			throw new RuntimeException(e);
		}
	}

	private Collection<String> extractAddresses(Collection<String> addresses) {
		List<String> result = new ArrayList<String>();
		for (String address : addresses) {
			String[] innerAddresses = address.split(";");
			for (String string : innerAddresses) {
				result.add(string);
			}
		}
		return result;

	}

}
