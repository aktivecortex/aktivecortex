/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail;

import java.io.Serializable;

import org.apache.commons.lang.ArrayUtils;

public class ByteArrayAttachment  extends Attachment<Byte[]> implements Serializable {

	private static final long serialVersionUID = 1L;

	public ByteArrayAttachment(byte[] content, MimeType mimeType, String title) {
		super(ArrayUtils.toObject(content), mimeType, title);
	}

    @Override
    protected boolean contentDiffers(Object obj) {
        final ByteArrayAttachment other = (ByteArrayAttachment) obj;
        if (getContent() == null) {
            if (other.getContent() != null) {
                return true;
            }
        } else if (!ArrayUtils.isEquals(getContent(), other.getContent())) {
            return true;
        }
        return false;
    }

   @Override
    protected int contentHashCode() {
        return ((getContent() == null) ? 0 : ArrayUtils.hashCode(getContent()));
    }


}
