/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.mail;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.axonframework.util.Assert;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import com.google.common.base.Objects;


public class EmailMessage implements Serializable {
	private static final long serialVersionUID = -7445879931383374314L;

	/**
	 * (sender, to, cc, etc...)
	 */
	private final MessageHeaders headers;

	private String subject;

	private String bodyAsText;

	private String bodyAsHtml;


	public EmailMessage(DeliveryOptions deliveryOptions, String subject, String bodyAsText, String bodyAsHtml, Map<String, Object> headers, Set<Attachment<?>> attachments) {
		this.subject = subject;
		this.bodyAsText = bodyAsText;
		this.bodyAsHtml = bodyAsHtml;
		Assert.notNull(deliveryOptions, "deliveryOptions must not be null");
        Map<String, Object> tmpHeaders = new HashMap<String, Object>();
		if (null != headers) {
            tmpHeaders.putAll(headers);
        }
        tmpHeaders. putAll(deliveryOptions);
        this.headers = new MessageHeaders(tmpHeaders, attachments);
	}

	public MessageHeaders getHeaders() {
		return headers;
	}

	public String getSubject() {
		return subject;
	}

	public String getBodyAsText() {
		return bodyAsText;
	}

	public String getBodyAsHtml() {
		return bodyAsHtml;
	}

	public Collection<String> getToAddresses() {
		Collection<String> addresses = (Collection<String>) headers.get(DeliveryOptions.TO);
		return (null == addresses || addresses.isEmpty()) ? new ArrayList<String>() : copyOf(addresses);
	}
	public Collection<String> getCCAddresses() {
		Collection<String> addresses = (Collection<String>) headers.get(DeliveryOptions.CC);
		return (null == addresses || addresses.isEmpty()) ? new ArrayList<String>() : copyOf(addresses);
	}

	public Collection<String> getBCCAddresses() {
		Collection<String> addresses = (Collection<String>) headers.get(DeliveryOptions.BCC);
		return (null == addresses || addresses.isEmpty()) ? new ArrayList<String>() : copyOf(addresses);
	}

	public String getSenderName() {
		Object result = headers.get(DeliveryOptions.SENDER_NAME);
		if (result instanceof String) {
			return (String) result;
		}
		return null;
	}

	public String getSenderAdddress() {
		Object result = headers.get(DeliveryOptions.SENDER_ADRS);
		if (result instanceof String) {
			return (String) result;
		}
		return null;
	}

	DeliveryOptions getDeliveryOptions() {
		DeliveryOptions options = new DeliveryOptions();
		options.setSenderName(getSenderName());
		options.setSenderAddress(getSenderAdddress());
		options.setToAddresses(getToAddresses().toArray(new String[getToAddresses().size()]));
		options.setCCAddresses(getCCAddresses().toArray(new String[getCCAddresses().size()]));
		options.setCCAddresses(getBCCAddresses().toArray(new String[getBCCAddresses().size()]));
		return options ;
	}

	public String getSender() {
		if (StringUtils.isBlank(getSenderAdddress())) {
			return null;
		}
		if (StringUtils.isBlank(getSenderName())) {
			return getSenderAdddress();
		}
		StringBuilder sb = new StringBuilder();
		sb.append(getSenderName());
		sb.append(" <");
		sb.append(getSenderAdddress());
		sb.append(">");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bodyAsHtml == null) ? 0 : bodyAsHtml.hashCode());
		result = prime * result + ((bodyAsText == null) ? 0 : bodyAsText.hashCode());
		result = prime * result + ((headers == null) ? 0 : headers.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final EmailMessage other = (EmailMessage) obj;
		if (bodyAsHtml == null) {
			if (other.bodyAsHtml != null) {
                return false;
            }
		} else if (!bodyAsHtml.equals(other.bodyAsHtml)) {
            return false;
        }
		if (bodyAsText == null) {
			if (other.bodyAsText != null) {
                return false;
            }
		} else if (!bodyAsText.equals(other.bodyAsText)) {
            return false;
        }
		if (headers == null) {
			if (other.headers != null) {
                return false;
            }
		} else if (!headers.equals(other.headers)) {
            return false;
        }
		if (subject == null) {
			if (other.subject != null) {
                return false;
            }
		} else if (!subject.equals(other.subject)) {
            return false;
        }
		return true;
	}

	public Set<Attachment> getAttachements() {
		Object result =  headers.get(MessageHeaders.ATTACHMENTS);
		if (result instanceof Attachment) {
			Attachment file = (Attachment) result;
			return newHashSet(file);
		}
		Set<Attachment> attachments = (Set<Attachment>) result;
		return (attachments== null || attachments.size() == 0) ? new HashSet<Attachment>() : attachments;
	}

	public boolean hasAttachments() {
		return getAttachements().size() > 0;
	}

	public Map<String, Object>  getModel() {
		Map<String, Object> model = (Map<String, Object>) headers.get(MessageHeaders.MODEL);
		return (model == null) ? new HashMap<String, Object>() : model;
	}

	public boolean hasModel() {
		return getModel().size() > 0;
	}

	public String getTemplateName()  {
		return (String) headers.get(MessageHeaders.TEMPLATE);
	}

	public boolean hasTemplate()  {
		return (StringUtils.isNotEmpty(getTemplateName()) && StringUtils.isNotBlank(getTemplateName()));
	}

	public boolean hasHtmlBody() {
		return (StringUtils.isNotEmpty(bodyAsHtml) && StringUtils.isNotBlank(bodyAsHtml));
	}

	public boolean hasTextBody() {
		return (StringUtils.isNotEmpty(bodyAsText) && StringUtils.isNotBlank(bodyAsText));
	}

	public boolean equalsIgnoreOptions(EmailMessage other) {
		return EqualsBuilder.reflectionEquals(this, other, new String[]{"headers"});
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
		.add("From", getSender())
		.add("Send Date", new DateTime(headers.getTimestamp()).toString(ISODateTimeFormat.basicDateTimeNoMillis()))
		.add("To", getToAddresses())
		.add("CC", getCCAddresses())
		.add("BCC", getBCCAddresses())
		.add("Subject", getSubject())
		.add("Body", getBodyAsText())
		.add("Headers", headers)
		.toString();
	}



}

