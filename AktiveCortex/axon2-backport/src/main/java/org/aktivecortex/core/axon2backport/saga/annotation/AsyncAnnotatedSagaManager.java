/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.annotation;

import com.github.kristofa.brave.ServerSpanThreadBinder;
import com.github.kristofa.brave.Span;
import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.Disruptor;
import org.aktivecortex.core.axon2backport.saga.GenericSagaFactory;
import org.aktivecortex.core.axon2backport.saga.SagaFactory;
import org.aktivecortex.core.axon2backport.saga.SagaRepository;
import org.aktivecortex.core.axon2backport.saga.SagaRepositoryFactory;
import org.aktivecortex.core.axon2backport.unitofwork.NoTransactionManager;
import org.aktivecortex.core.axon2backport.unitofwork.TransactionManager;
import org.axonframework.domain.Event;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.saga.SagaManager;
import org.axonframework.util.Assert;
import org.axonframework.util.AxonConfigurationException;
import org.axonframework.util.Subscribable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.concurrent.*;

/**
 * A SagaManager implementation that processes Sagas asynchronously. Incoming events are placed on a queue and
 * processed by a given number of processors. Modified saga state is persisted in batches to the repository, to
 * minimize communication overhead with backends.
 * <p/>
 * This SagaManager implementation guarantees a "happens before" type processing for each Saga. That means that the
 * behavior of asynchronously processed events is exactly identical as the behavior if the events were processed
 * completely sequentially.
 *
 * @author Allard Buijze
 * @author Domenico Maria Giffone
 * @since 2.0
 */
public class AsyncAnnotatedSagaManager implements SagaManager, Subscribable {
    
    private static final int SHUTDOWN_WAIT_IN_SECONDS = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncAnnotatedSagaManager.class);

    private static final WaitStrategy DEFAULT_WAIT_STRATEGY = new BlockingWaitStrategy();
    private static final int DEFAULT_BUFFER_SIZE = 512;
    private static final int DEFAULT_PROCESSOR_COUNT = 1;

    private final SagaAnnotationInspector[] sagaAnnotationInspectors;
    private final EventBus eventBus;
    private volatile Disruptor<AsyncSagaProcessingEvent> disruptor;

    private boolean shutdownExecutorOnStop = true;
    private Executor executor = Executors.newCachedThreadPool();

    private SagaRepositoryFactory sagaRepositoryFactory;
    private volatile SagaFactory sagaFactory = new GenericSagaFactory();
    private TransactionManager transactionManager = new NoTransactionManager();
    private int processorCount = DEFAULT_PROCESSOR_COUNT;
    private int bufferSize = DEFAULT_BUFFER_SIZE;
    private WaitStrategy waitStrategy = DEFAULT_WAIT_STRATEGY;
    private SagaManagerStatus sagaManagerStatus = new SagaManagerStatus();
    private long startTimeout = 5000;
    private ServerSpanThreadBinder tracingThreadBinder = new NoOpServerSpanThreadBinder();
    private UnhandledAsyncSagaEventListener unhandledAsyncSagaEventListener = new NoOpAsyncSagaEventListener();
    /**
     * Initializes an Asynchronous Saga Manager using default values for the given <code>sagaTypes</code> to listen to
     * events on the given <code>eventBus</code>.
     * <p/>
     * After initialization, the SagaManager must be explicitly started using the {@link #start()} method.
     *
     * @param eventBus  The Event Bus from which the Saga Manager will process events
     * @param sagaTypes The types of Saga this saga manager will process incoming events for
     */
    @SuppressWarnings({"unchecked"})
    public AsyncAnnotatedSagaManager(EventBus eventBus, Class<? extends AbstractAnnotatedSaga>... sagaTypes) {
        this.eventBus = eventBus;
        sagaAnnotationInspectors = new SagaAnnotationInspector[sagaTypes.length];
        for (int i = 0; i < sagaTypes.length; i++) {
            sagaAnnotationInspectors[i] = new SagaAnnotationInspector(sagaTypes[i]);
        }
        LOGGER.info("Saga Manager created");
    }

    /**
     * Starts the Saga Manager by starting the processor threads and subscribes it with the <code>eventBus</code>. If
     * the saga manager is already started, it is only re-subscribed to the event bus.
     */
    public synchronized void start() {
        Assert.state(sagaRepositoryFactory != null, "Cannot start without a sagaRepositoryFactory");
        final SagaRepository sagaRepository = sagaRepositoryFactory.createSagaRepository();
        if (executor == null) {
            executor = Executors.newCachedThreadPool();
            shutdownExecutorOnStop = true;
        }
    	
        if (disruptor == null) {
            sagaManagerStatus.setStatus(true);
            disruptor = new Disruptor<AsyncSagaProcessingEvent>(new AsyncSagaProcessingEvent.Factory(),
                                                                new ValidatingExecutor(executor, startTimeout),
                                                                new MultiThreadedClaimStrategy(bufferSize),
                                                                waitStrategy);
            disruptor.handleExceptionsWith(new LoggingExceptionHandler());
            disruptor.handleEventsWith(AsyncSagaEventProcessor.createInstances(sagaRepository,
                                                                               transactionManager, processorCount,
                                                                               tracingThreadBinder, unhandledAsyncSagaEventListener));
            disruptor.start();
        }
        subscribe();
        LOGGER.info("Saga Manager started");
    }

    /**
     * Unsubscribes this Saga Manager from the event bus and stops accepting new events. The method is blocked until
     * all scheduled events have been processed. Note that any manually provided Executors using ({@link
     * #setExecutor(java.util.concurrent.Executor)} are not shut down.
     * <p/>
     * If the Saga Manager was already stopped, nothing happens.
     */
    public synchronized void stop() {
        sagaManagerStatus.setStatus(false);
        unsubscribe();
        if (disruptor != null) {
            disruptor.shutdown();
            if (shutdownExecutorOnStop && executor instanceof ExecutorService) {
                ((ExecutorService) executor).shutdown();
            }
        }
        if (executor != null && shutdownExecutorOnStop && executor instanceof ExecutorService){
        		shutdownAndAwaitTermination((ExecutorService) executor);
            	executor = null;
        }
        disruptor = null;
        LOGGER.info("Saga Manager stopped");
    }

    @Override
    public void unsubscribe() {
        eventBus.unsubscribe(this);
    }

    @Override
    public void subscribe() {
        eventBus.subscribe(this);
    }

    @Override
    public void handle(final Event event) {
        for (final SagaAnnotationInspector annotationInspector : sagaAnnotationInspectors) {
            final HandlerConfiguration handler = annotationInspector.findHandlerConfiguration(event);
            if (handler.isHandlerAvailable()) {
                final AbstractAnnotatedSaga newSagaInstance;
                switch (handler.getCreationPolicy()) {
                    case ALWAYS:
                    case IF_NONE_FOUND:
                        newSagaInstance = (AbstractAnnotatedSaga) sagaFactory.createSaga(annotationInspector.getSagaType());
                        break;
                    default:
                        newSagaInstance = null;
                        break;
                }
                disruptor.publishEvent(new SagaProcessingEventTranslator(event, annotationInspector, handler, 
                                                                         newSagaInstance, tracingThreadBinder));
            }
        }
    }


    public Class<?> getTargetType() {
        return sagaAnnotationInspectors[0].getSagaType();
    }

    private static final class SagaProcessingEventTranslator implements EventTranslator<AsyncSagaProcessingEvent> {
        private final Event event;
        private final SagaAnnotationInspector annotationInspector;
        private final HandlerConfiguration handler;
        private final AbstractAnnotatedSaga newSagaInstance;
        private final ServerSpanThreadBinder tracingThreadBinder;

        private SagaProcessingEventTranslator(Event event, SagaAnnotationInspector annotationInspector,
                                              HandlerConfiguration handler, AbstractAnnotatedSaga newSagaInstance,
                                              ServerSpanThreadBinder serverSpanThreadBinder) {
            this.event = event;
            this.annotationInspector = annotationInspector;
            this.handler = handler;
            this.newSagaInstance = newSagaInstance;
            this.tracingThreadBinder = serverSpanThreadBinder;
        }

        @SuppressWarnings({"unchecked"})
        @Override
        public void translateTo(AsyncSagaProcessingEvent entry, long sequence) {
        	entry.reset(event, annotationInspector.getSagaType(), handler, newSagaInstance,
                    MDC.getCopyOfContextMap(), tracingThreadBinder.getCurrentServerSpan());
            if (LOGGER.isDebugEnabled()){
                LOGGER.debug("#{} event published to ring buffer [{}]", sequence, event);
            }
        }
    }

    /**
     * Sets the executor that provides the threads for the processors. Note that you must ensure that this executor
     * is capable of delivering <em>all</em> of the required threads at once. If that is not the case, the Saga Manager
     * might hang while waiting for the executor to provide them. Must be set <em>before</em> the SagaManager is
     * started.
     * <p/>
     * By default, a thread is created for each processor.
     *
     * @param executor the executor that provides the threads for the processors
     * @see #setProcessorCount(int)
     */
    public synchronized void setExecutor(Executor executor) {
        Assert.state(disruptor == null, "Cannot set executor after SagaManager has started");
        this.shutdownExecutorOnStop = false;
        this.executor = executor;
    }

    /**
     * Sets the saga repository factory to store and load Sagas from. Must be set <em>before</em> the SagaManager is
     * started.
     * <p/>
     *
     * @param sagaRepositoryFactory the saga repository factory
     */
    public synchronized void setSagaRepositoryFactory(SagaRepositoryFactory sagaRepositoryFactory) {
        Assert.state(disruptor == null, "Cannot set sagaRepositoryFactory when SagaManager has started");
        this.sagaRepositoryFactory = sagaRepositoryFactory;
    }

    /**
     * Sets the SagaFactory responsible for creating new Saga instances when required. Must be set <em>before</em> the
     * SagaManager is started.
     * <p/>
     * Defaults to a {@link GenericSagaFactory} instance.
     *
     * @param sagaFactory the SagaFactory responsible for creating new Saga instances
     */
    public synchronized void setSagaFactory(SagaFactory sagaFactory) {
        Assert.state(disruptor == null, "Cannot set sagaFactory when SagaManager has started");
        this.sagaFactory = sagaFactory;
    }

    /**
     * Sets the TransactionManager used to manage any transactions required by the underlying storage mechanism. Note
     * that batch sizes set by this transaction manager are ignored. Must be set <em>before</em> the
     * SagaManager is started.
     * <p/>
     * By default, no transactions are managed.
     *
     * @param transactionManager the TransactionManager used to manage any transactions required by the underlying
     *                           storage mechanism.
     */
    public synchronized void setTransactionManager(TransactionManager transactionManager) {
        Assert.state(disruptor == null, "Cannot set transactionManager when SagaManager has started");
        this.transactionManager = transactionManager;
    }

    /**
     * Sets the number of processors (threads) to process events with. Ensure that the given {@link
     * #setExecutor(java.util.concurrent.Executor) executor} is capable of processing this amount of concurrent tasks.
     * Must be set <em>before</em> the SagaManager is started.
     * <p/>
     * Defaults to 1.
     *
     * @param processorCount the number of processors (threads) to process events with
     */
    public synchronized void setProcessorCount(int processorCount) {
        Assert.state(disruptor == null, "Cannot set processorCount when SagaManager has started");
        this.processorCount = processorCount;
    }

    /**
     * Sets the amount of time (in milliseconds) the AsyncSagaManager will wait for the async processors to be assigned
     * a thread from the executor. This is used to ensure that the executor provides a thread for the processors,
     * instead of queueing them. This typically occurs when using a thread pool with a core pool size smaller than the
     * processorCount.
     * <p/>
     * Must be set before calling {@link #start()}. Defaults to 5000 (5 seconds).
     *
     * @param startTimeout the number of millis to wait for the processor to have been assigned a thread. Defaults to
     *                     5000 (5 seconds).
     */
    public synchronized void setStartTimeout(long startTimeout) {
        this.startTimeout = startTimeout;
    }


    /**
     * Sets the size of the processing buffer. This is equal to the amount of events that may awaiting for processing
     * before the input is blocked. Must be set <em>before</em> the SagaManager is started.
     * <p/>
     * Note that this value <em>must</em> be a power of 2.
     * <p/>
     * Defaults to 512.
     *
     * @param bufferSize The size of the processing buffer. Must be a power of 2.
     */
    public synchronized void setBufferSize(int bufferSize) {
        Assert.isTrue(Integer.bitCount(bufferSize) == 1, "The buffer size must be a power of 2");
        Assert.state(disruptor == null, "Cannot set bufferSize when SagaManager has started");
        this.bufferSize = bufferSize;
    }

    /**
     * Sets the WaitStrategy to use when event processors need to wait for incoming events.
     * <p/>
     * Defaults to a BlockingWaitStrategy.
     *
     * @param waitStrategy the WaitStrategy to use when event processors need to wait for incoming events
     */
    public synchronized void setWaitStrategy(WaitStrategy waitStrategy) {
        Assert.state(disruptor == null, "Cannot set waitStrategy when SagaManager has started");
        this.waitStrategy = waitStrategy;
    }
    
    public synchronized void setTracingThreadBinder(ServerSpanThreadBinder tracingThreadBinder) {
        this.tracingThreadBinder = tracingThreadBinder;
    }

    public synchronized void setUnhandledAsyncSagaEventListener(UnhandledAsyncSagaEventListener unhandledAsyncSagaEventListener) {
        this.unhandledAsyncSagaEventListener = unhandledAsyncSagaEventListener;
    }

    /**
     * Shutdown the thread executor, waiting for a clean executor termination.
     * <p/>
     * If the executor does not terminate within the expected interval then the shutdown is forced.
     *
     * @param pool the Executor to stop
     */
    private void shutdownAndAwaitTermination(ExecutorService pool) {
        // Disable new tasks from being submitted
        pool.shutdown();
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(SHUTDOWN_WAIT_IN_SECONDS, TimeUnit.SECONDS)) {
                LOGGER.warn("Pool did not terminate within {} seconds, forcing shutdown...", SHUTDOWN_WAIT_IN_SECONDS);
                // Cancel currently executing tasks
                pool.shutdownNow();
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(SHUTDOWN_WAIT_IN_SECONDS, TimeUnit.SECONDS)) {
                    LOGGER.warn("Pool did not terminate within expected interval");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Exposes the running state of the SagaManager.
     */
    static class SagaManagerStatus {

        private volatile boolean isRunning;

        private void setStatus(boolean running) {
            isRunning = running;
        }

        /**
         * Indicates whether the SagaManager that provided this instance is (still) running.
         *
         * @return <code>true</code> if the SagaManager is running, otherwise <code>false</code>
         */
        public boolean isRunning() {
            return isRunning;
        }
    }

    /**
     * Executor wrapper that checks whether a given task is executed immediately.
     */
    private static class ValidatingExecutor implements Executor {

        private final Executor delegate;
        private final long timeoutMillis;

        /**
         * Initialize the Executor that delegates to the given <code>executor</code> and wait for at most
         * <code>timeoutMillis</code> for tasks to actually start.
         *
         * @param executor
         * @param timeoutMillis
         */
        public ValidatingExecutor(Executor executor, long timeoutMillis) {
            this.delegate = executor;
            this.timeoutMillis = timeoutMillis;
        }

        @Override
        public void execute(Runnable command) {
            final StartDetectingRunnable task = new StartDetectingRunnable(command);
            delegate.execute(task);
            try {
                if (!task.awaitStarted(timeoutMillis, TimeUnit.MILLISECONDS)) {
                    throw new AxonConfigurationException("It seems that the given Executor is not providing a thread "
                            + "for the AsyncSagaManager. Ensure that the "
                            + "corePoolSize is larger than the processor count.");
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Runnable implementation that exposes the fact it has started executing.
     */
    private static class StartDetectingRunnable implements Runnable {

        private final Runnable delegate;
        private final CountDownLatch cdl = new CountDownLatch(1);

        /**
         * @param command The actual runnable to execute
         */
        public StartDetectingRunnable(Runnable command) {
            this.delegate = command;
        }

        @Override
        public void run() {
            cdl.countDown();
            delegate.run();
        }

        /**
         * Indicates whether the task has started, potentially waiting for the given <code>timeout</code>.
         *
         * @param timeout The amount of time to wait for the process to start
         * @param unit    The unit of time
         * @return whether the task has started or not
         *
         * @throws InterruptedException when the thread receives an interrupt while waiting for the start notification
         */
        public boolean awaitStarted(long timeout, TimeUnit unit) throws InterruptedException {
            return cdl.await(timeout, unit);
        }
    }

    private class NoOpServerSpanThreadBinder implements ServerSpanThreadBinder {
        @Override
        public Span getCurrentServerSpan() {
            return null;
        }

        @Override
        public void setCurrentSpan(Span span) {
        }
    }

    private class NoOpAsyncSagaEventListener implements UnhandledAsyncSagaEventListener {

        @Override
        public void onEvent(Event event) {
        }

    }

    private static final class LoggingExceptionHandler implements ExceptionHandler {

        private static final  Logger LOGGER = LoggerFactory.getLogger(LoggingExceptionHandler.class);

        @Override
        public void handleEventException(Throwable ex, long sequence, Object event) {
            LOGGER.warn("A fatal exception occurred while processing an Event for a Saga. "
                    + "Processing will continue with the next Event", ex);
        }

        @Override
        public void handleOnStartException(Throwable ex) {
            LOGGER.warn("An exception occurred while starting the AsyncAnnotatedSagaManager.", ex);
        }

        @Override
        public void handleOnShutdownException(Throwable ex) {
            LOGGER.warn("An exception occurred while shutting down the AsyncAnnotatedSagaManager.", ex);
        }
    }
}
