/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.repository.jpa;

import java.util.Arrays;

import org.aktivecortex.core.axon2backport.saga.Saga;
import org.aktivecortex.core.axon2backport.saga.repository.SagaSerializer;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

/**
 * Java Persistence Entity allowing sagas to be stored in a relational database.
 *
 * @author Allard Buijze
 * @since 0.7
 */
@Entity
public class SagaEntry {

    @SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
    @Id
    private String sagaId; 

    @Basic
    private String sagaType;
    
    @Lob
    private byte[] serializedSaga;

    @Transient
    private transient Saga saga;

    /**
     * Constructs a new SagaEntry for the given <code>saga</code>. The given saga must be serializable. The provided
     * saga is not modified by this operation.
     *
     * @param saga       The saga to store
     * @param serializer The serialization mechanism to convert the Saga to a byte stream
     */
    public SagaEntry(Saga saga, SagaSerializer serializer) {
        this.sagaId = saga.getSagaIdentifier();
        this.serializedSaga = serializer.serialize(saga);
        this.sagaType = saga.getClass().getSimpleName();
        this.saga = saga;
    }

    /**
     * Returns the Saga instance stored in this entry.
     *
     * @param serializer The serializer to decode the Saga
     * @return the Saga instance stored in this entry
     */
    public Saga getSaga(SagaSerializer serializer) {
        if (saga != null) {
            return saga;
        }
        return serializer.deserialize(serializedSaga);
    }

    /**
     * Constructor required by JPA. Do not use.
     *
     */
    protected SagaEntry() {
        // required by JPA
    }
    

    public String getSagaId() {
		return sagaId;
	}

	public String getSagaType() {
		return sagaType;
	}

	/**
     * Returns the serialized form of the Saga.
     *
     * @return the serialized form of the Saga
     */
    public byte[] getSerializedSaga() {
        return serializedSaga; //NOSONAR
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sagaId == null) ? 0 : sagaId.hashCode());
		result = prime * result
				+ ((sagaType == null) ? 0 : sagaType.hashCode());
		result = prime * result + Arrays.hashCode(serializedSaga);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
            return true;
        }
		if (obj == null) {
            return false;
        }
		if (getClass() != obj.getClass()) {
            return false;
        }
		SagaEntry other = (SagaEntry) obj;
		if (sagaId == null) {
			if (other.sagaId != null) {
                return false;
            }
		} else if (!sagaId.equals(other.sagaId)) {
            return false;
        }
		if (sagaType == null) {
			if (other.sagaType != null) {
                return false;
            }
		} else if (!sagaType.equals(other.sagaType)) {
            return false;
        }
		if (!Arrays.equals(serializedSaga, other.serializedSaga)) {
            return false;
        }
		return true;
	}
    
    
}
