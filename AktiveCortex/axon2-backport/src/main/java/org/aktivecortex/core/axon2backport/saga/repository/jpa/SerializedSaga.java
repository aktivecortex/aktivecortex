/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.repository.jpa;

import static java.lang.String.format;

import java.util.Arrays;

import org.axonframework.util.Assert;

public class SerializedSaga {

    private final byte[] data;
    private final String type;

    /**
     * Initializes a SimpleSerializedObject using given <code>data</code> and <code>serializedType</code>.
     *
     * @param data           The data of the serialized object
     * @param Type       The type of data
     */
    @SuppressWarnings("all")
    public SerializedSaga(byte[] data, String type) {
        Assert.notNull(data, "Data for a serialized object cannot be null");
        Assert.notNull(type, "The type identifier of the serialized object");
        this.data = data;
        this.type = type;
    }

   
    public byte[] getData() {
        return data;
    }

    public Class<byte[]> getContentType() {
        return byte[].class;
    }

    public String getType() {
        return type;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
            return true;
        }
		if (obj == null) {
            return false;
        }
		if (getClass() != obj.getClass()) {
            return false;
        }
		SerializedSaga other = (SerializedSaga) obj;
		if (!Arrays.equals(data, other.data)) {
            return false;
        }
		if (type == null) {
			if (other.type != null) {
                return false;
            }
		} else if (!type.equals(other.type)) {
            return false;
        }
		return true;
	}


	@Override
    public String toString() {
        return format("SerializedSaga [%s]", type);
    }
}
