/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.spring;

import org.aktivecortex.core.axon2backport.saga.SagaRepository;
import org.aktivecortex.core.axon2backport.saga.SagaRepositoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.Assert;

/**
 * Returns a SagaRepository bean from the Spring ApplicationContext.
 * Useful if you want to return a prototype scoped bean for the SagaRepository.
 * This can be needed if you want to perform an hot restart of the SagaManager 
 *  
 * @author Domenico Maria Giffone
 *
 */
public class SpringSagaRepositoryFactory implements SagaRepositoryFactory, ApplicationContextAware {
	
	private static final String UNKNOWN_BEAN_SCOPE = "UNKNOWN";
	private static final String SINGLETON_BEAN_SCOPE = "Singleton";
	private static final String PROTOTYPE_BEAN_SCOPE = "Prototype";

	private static final Logger logger = LoggerFactory.getLogger(SpringSagaRepositoryFactory.class);

	private ApplicationContext applicationContext;
	
	private String beanName;

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	@Override
	public SagaRepository createSagaRepository() {
		Assert.notNull(beanName);
		String beanScope = UNKNOWN_BEAN_SCOPE;
		boolean isSingleton = applicationContext.isSingleton(beanName);
		boolean isPrototype = applicationContext.isPrototype(beanName);
		if (isSingleton) {
			beanScope = SINGLETON_BEAN_SCOPE;
		}
		if (isPrototype) {
			beanScope = PROTOTYPE_BEAN_SCOPE;
		}
		SagaRepository repo = applicationContext.getBean(beanName, SagaRepository.class);
		logger.info("returning {} scoped SagaRepository instance from Spring ApplicationContext [{}]", beanScope, repo);
		return repo;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

}
