/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.aktivecortex.core.axon2backport.unitofwork;


/**
 *
 * @author d.giffone
 */
public final class TransactionHelper {

    private TransactionHelper() {
    }

    private static final ThreadLocal<Object> THREAD_LOCAL_TRANSACTION = new ThreadLocal<Object>();
    
    public static void start(TransactionManager transactionManager) {
        Object backingTransaction = THREAD_LOCAL_TRANSACTION.get();
        if (backingTransaction == null) {
            backingTransaction = transactionManager.startTransaction();
            THREAD_LOCAL_TRANSACTION.set(backingTransaction);
        }
    }

    public static void commit(TransactionManager transactionManager) {
        Object backingTransaction = THREAD_LOCAL_TRANSACTION.get();
        if (backingTransaction != null) {
            transactionManager.commitTransaction(backingTransaction);
            THREAD_LOCAL_TRANSACTION.remove();
        }
    }

    public static void rollback(TransactionManager transactionManager) {
        Object backingTransaction = THREAD_LOCAL_TRANSACTION.get();
        if (null != backingTransaction) {
            transactionManager.rollbackTransaction(backingTransaction);
            THREAD_LOCAL_TRANSACTION.remove();
        }
    }
}
