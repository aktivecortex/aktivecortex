/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.annotation;

import org.axonframework.domain.Event;

/**
 * An Event Listener for unhandled saga events.
 * <p>It's primary duty is to notify completion of processes started by commands
 * that raise events but those events don't lead to any saga activity.
 * <p>in this case it is possible to consider the process as finished as it didn't result in new activities.
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
public interface UnhandledAsyncSagaEventListener {

    void onEvent(Event event);

}
