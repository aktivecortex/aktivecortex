/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga;


/**
 * SagaRepositoryFactory interface is used by the application to obtain an application SagaRepository. .
 * Depending on the application environment, this may be a prototype scoped bean or a singleton scope bean.
 * <p/>
 *
 * @author Domenico Maria Giffone
 */
public interface SagaRepositoryFactory {

    /**
     * Returns an instance (new or previously created) of a SagaRepository.
     *
     * @return an instance (new or previously created) of a SagaRepository.
     */
    SagaRepository createSagaRepository();
}
