/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.annotation;

import com.github.kristofa.brave.ServerSpanThreadBinder;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.LifecycleAware;
import org.aktivecortex.core.axon2backport.saga.AssociationValue;
import org.aktivecortex.core.axon2backport.saga.Saga;
import org.aktivecortex.core.axon2backport.saga.SagaRepository;
import org.aktivecortex.core.axon2backport.unitofwork.TransactionHelper;
import org.aktivecortex.core.axon2backport.unitofwork.TransactionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Processes events by forwarding it to Saga instances "owned" by each processor. This processor uses a consistent
 * hashing algorithm to assign the owner of each Saga.
 *
 * @author Allard Buijze
 * @author Domenico Maria Giffone
 * @since 2.0
 */
final class AsyncSagaEventProcessor implements EventHandler<AsyncSagaProcessingEvent>, LifecycleAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncSagaEventProcessor.class);

    private final TransactionManager transactionManager;
    private final SagaRepository sagaRepository;
    private final Map<String, Saga> processedSagas = new TreeMap<String, Saga>();
    private final Map<String, Saga> newlyCreatedSagas = new TreeMap<String, Saga>();
    private final Map<String, Map> contextMaps = new TreeMap<String, Map>();
    private final int processorCount;
    private final int processorId;
    private final String processorName;
    private ServerSpanThreadBinder tracingThreadBinder;
    private UnhandledAsyncSagaEventListener unhandledAsyncSagaEventListener;

    /**
     * Creates the Disruptor Event Handlers for invoking Sagas. The size of the array returned is equal to the given
     * <code>processorCount</code>.
     *
     * @param sagaRepository     The repository which provides access to the Sagas
     * @param transactionManager The TransactionManager that creates and commits transactions
     * @param processorCount     The number of processors to create
     * @return an array containing the Disruptor Event Handlers to invoke Sagas.
     */
    static EventHandler<AsyncSagaProcessingEvent>[] createInstances(SagaRepository sagaRepository,
                                                                    TransactionManager transactionManager, int processorCount,
                                                                    ServerSpanThreadBinder serverSpanThreadBinder, UnhandledAsyncSagaEventListener unhandledAsyncSagaEventListener) {
        AsyncSagaEventProcessor[] processors = new AsyncSagaEventProcessor[processorCount];
        for (int processorId = 0; processorId < processorCount; processorId++) {
            processors[processorId] = new AsyncSagaEventProcessor(sagaRepository, processorCount, processorId,
                    transactionManager, serverSpanThreadBinder, unhandledAsyncSagaEventListener);
        }
        return processors;
    }

    private AsyncSagaEventProcessor( SagaRepository sagaRepository,
                                    int processorCount, int processorId,
                                    TransactionManager transactionManager,
                                    ServerSpanThreadBinder serverSpanThreadBinder,
                                    UnhandledAsyncSagaEventListener unhandledAsyncSagaEventListener) {
        this.sagaRepository = sagaRepository;
        this.processorCount = processorCount;
        this.processorId = processorId;
        this.transactionManager = transactionManager;
        this.tracingThreadBinder = serverSpanThreadBinder;
        this.unhandledAsyncSagaEventListener = unhandledAsyncSagaEventListener;
        this.processorName = String.format("Event Processor %s/%s", processorId + 1, processorCount);
        LOGGER.info("{} started", processorName);
    }

    @Override
    public void onEvent(AsyncSagaProcessingEvent entry, long sequence, boolean endOfBatch) throws Exception {
        boolean sagaInvoked = invokeExistingSagas(entry, sequence);
        AssociationValue associationValue;
        switch (entry.getHandler().getCreationPolicy()) {
            case ALWAYS:
                associationValue = entry.getAssociationValue();
                if (associationValue != null && ownedByCurrentProcessor(entry.getNewSaga().getSagaIdentifier())) {
                    processNewSagaInstance(entry, associationValue, sequence);
                }
                break;
            case IF_NONE_FOUND:
                associationValue = entry.getAssociationValue();
                boolean shouldCreate = associationValue != null && entry.waitForSagaCreationVote(
                        sagaInvoked, processorCount, ownedByCurrentProcessor(entry.getNewSaga().getSagaIdentifier()));
                if (shouldCreate) {
                    if (null != entry.getContextMap() && !entry.getContextMap().isEmpty()) {
                        MDC.setContextMap(entry.getContextMap());
                        contextMaps.put(entry.getNewSaga().getSagaIdentifier(), entry.getContextMap());
                    }
                    if (null != entry.getTracingSpan()) {
                        tracingThreadBinder.setCurrentSpan(entry.getTracingSpan());
                    }
                    processNewSagaInstance(entry, associationValue, sequence);
                }
            case NONE: {
                final boolean ownNotification = (null == entry.getNewSaga()) && ownedByCurrentProcessor(sequence);
                boolean shouldNotify = entry.waitForUnhandledEventVote(sagaInvoked, processorCount, ownNotification);
                if (shouldNotify) {
                    if (null != entry.getContextMap() && !entry.getContextMap().isEmpty()) {
                        MDC.setContextMap(entry.getContextMap());
                    }
                    if (null != entry.getTracingSpan()) {
                        tracingThreadBinder.setCurrentSpan(entry.getTracingSpan());
                    }
                    notifyUnhandledEvent(entry);
                }

            }
        }
        if (endOfBatch) {
            persistProcessedSagas();
        }
        MDC.clear();
    }

    private void notifyUnhandledEvent(AsyncSagaProcessingEvent entry) {
        unhandledAsyncSagaEventListener.onEvent(entry.getPublishedEvent());
    }

    private void processNewSagaInstance(AsyncSagaProcessingEvent entry, AssociationValue associationValue, long sequence) {
        TransactionHelper.start(transactionManager);
        final AbstractAnnotatedSaga newSaga = entry.getNewSaga();
        newSaga.handle(entry.getPublishedEvent());
        newSaga.associateWith(associationValue);
        processedSagas.put(newSaga.getSagaIdentifier(), newSaga);
        newlyCreatedSagas.put(newSaga.getSagaIdentifier(), newSaga);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("{} - processed event #{} of new saga instance", processorName, sequence);
        }
    }

    @SuppressWarnings("unchecked")
    private void persistProcessedSagas() {
        Set<String> committedSagas = new HashSet<String>();
        if (!processedSagas.isEmpty()) {
            TransactionHelper.start(transactionManager);
            for (Saga saga : processedSagas.values()) {
                final Map contextMap = contextMaps.get(saga.getSagaIdentifier());
                if (null != contextMap && !contextMap.isEmpty()) {
                    MDC.setContextMap(contextMap);
                }
                if (newlyCreatedSagas.containsKey(saga.getSagaIdentifier())) {
                    sagaRepository.add(saga);
                } else {
                    sagaRepository.commit(saga);
                }
                committedSagas.add(saga.getSagaIdentifier());
            }
        }
        TransactionHelper.commit(transactionManager);
        processedSagas.keySet().removeAll(committedSagas);
        newlyCreatedSagas.keySet().removeAll(committedSagas);
        contextMaps.keySet().removeAll(committedSagas);
    }

    private boolean invokeExistingSagas(AsyncSagaProcessingEvent entry, long sequence) {
        boolean sagaInvoked = false;
        final Class<? extends Saga> sagaType = entry.getSagaType();
        Set<String> sagaIds = sagaRepository.find(sagaType, entry.getAssociationValue());
        for (String sagaId : sagaIds) {
            if (ownedByCurrentProcessor(sagaId) && !processedSagas.containsKey(sagaId)) {
                TransactionHelper.start(transactionManager);
                processedSagas.put(sagaId, sagaRepository.load(sagaId));
            }
        }
        for (Saga saga : processedSagas.values()) {
            if (sagaType.isInstance(saga) && saga.isActive()
                    && saga.getAssociationValues().contains(entry.getAssociationValue())) {
                if (null != entry.getContextMap() && !entry.getContextMap().isEmpty()) {
                    MDC.setContextMap(entry.getContextMap());
                    contextMaps.put(saga.getSagaIdentifier(), entry.getContextMap());
                }
                if (null != entry.getTracingSpan()) {
                    tracingThreadBinder.setCurrentSpan(entry.getTracingSpan());
                }
                try {
                    TransactionHelper.start(transactionManager);
                    saga.handle(entry.getPublishedEvent());
                } catch (Exception e) {
                    LOGGER.error("Saga threw an exception while handling an Event. Ignoring and moving on...", e);
                }
                sagaInvoked = true;
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("{} - processed event #{} of existing saga instance", processorName, sequence);
                }
            }
        }
        return sagaInvoked;
    }

    private boolean ownedByCurrentProcessor(String sagaIdentifier) {
        return processedSagas.containsKey(sagaIdentifier)
                || Math.abs(sagaIdentifier.hashCode() & Integer.MAX_VALUE) % processorCount == processorId;
    }

    private boolean ownedByCurrentProcessor(long sequence) {
        return Math.abs(sequence & Integer.MAX_VALUE) % processorCount == processorId;
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onShutdown() {
        try {
            persistProcessedSagas();
        } catch (Exception e) {
            LOGGER.error("An exception occurred while attempting to persist Saga state", e);
        }
    }
}
