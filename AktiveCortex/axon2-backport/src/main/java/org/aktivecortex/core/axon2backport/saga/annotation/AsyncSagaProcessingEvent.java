/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.annotation;

import com.github.kristofa.brave.Span;
import com.lmax.disruptor.EventFactory;
import org.aktivecortex.core.axon2backport.saga.AssociationValue;
import org.aktivecortex.core.axon2backport.saga.Saga;
import org.aktivecortex.core.axon2backport.saga.repository.SagaSerializer;
import org.axonframework.domain.Event;

import java.util.Map;

/**
 * Placeholder for information required by the AsyncSagaEventProcessor for processing Events.
 *
 * @author Allard Buijze
 * @author Domenico Maria Giffone
 * @since 2.0
 */
class AsyncSagaProcessingEvent {

    private Event publishedEvent;
    private int retryCount = 0;
    private Map contextMap;
    private Span tracingSpan;
    private HandlerConfiguration handler;
    private Class<? extends AbstractAnnotatedSaga> sagaType;
    private SagaSerializer serializer;
    private volatile AbstractAnnotatedSaga newSaga;
    private AssociationValue associationValue;
    private final AsyncSagaCreationElector elector = new AsyncSagaCreationElector();
    private final AsyncSagaCreationElector unhandledEventNotifierElector = new AsyncSagaCreationElector();

   public Span getTracingSpan() {
        return tracingSpan;
    }

    public Map getContextMap() {
		return contextMap;
	}

	/**
     * Returns the event that has been published on the EventBus. This is the event that will trigger Sagas.
     *
     * @return the event that has been published on the EventBus
     */
    public Event getPublishedEvent() {
        return publishedEvent;
    }

    /**
     * Returns the handler that can process the published Event.
     *
     * @return the handler that can process the published Event
     */
    public HandlerConfiguration getHandler() {
        return handler;
    }

    /**
     * Returns the association value based on the handler.
     *
     * @return the association value based on the handler
     */
    public AssociationValue getAssociationValue() {
        if (handler == null) {
            return null;
        }
        return associationValue;
    }

    /**
     * Returns the type of Saga being processed.
     *
     * @return the type of Saga being processed
     */
    public Class<? extends Saga> getSagaType() {
        return sagaType;
    }

    /**
     * Forces the current thread to wait for the voting to complete if it is responsible for creating the Saga. As soon
     * as an invocation has been recorded, the waiting thread is released.
     *
     * @param didEventInvocation  indicates whether the current processor found a Saga to process
     * @param processorCount      The total number of processors expected to cast a vote
     * @param ownsNewSagaInstance Indicates whether the current processor "owns" the to-be-created saga instance.
     * @return <code>true</code> if the current processor should create the new instance, <code>false</code> otherwise.
     */
    public boolean waitForSagaCreationVote(boolean didEventInvocation, int processorCount,
                                           boolean ownsNewSagaInstance) {
        return elector.waitForSagaCreationVote(didEventInvocation, processorCount, ownsNewSagaInstance);
    }
    /**
     * Forces the current thread to wait for the voting to complete if it is responsible for notifying unhandled events.
     * As soon as an invocation has been recorded, the waiting thread is released.
     *
     * @param didEventInvocation  indicates whether the current processor found a Saga to process
     * @param processorCount      The total number of processors expected to cast a vote
     * @param ownsNotification Indicates whether the current processor "owns" the to-be-created saga instance.
     * @return <code>true</code> if the current processor should create the new instance, <code>false</code> otherwise.
     */
    public boolean waitForUnhandledEventVote(boolean didEventInvocation, int processorCount,
                                           boolean ownsNotification) {
        return unhandledEventNotifierElector.waitForSagaCreationVote(didEventInvocation, processorCount, ownsNotification);
    }

    /**
     * Returns the new Saga instance that should be used when processing an Event that creates a new Saga instance
     *
     * @return the new Saga instance
     */
    public AbstractAnnotatedSaga getNewSaga() {
       return newSaga;
    }

    /**
     * Reset this entry for processing a new EventMessage
     *
     * @param event           The EventMessage to process
     * @param sagaType        The type of Saga to process this EventMessage
     * @param handler         The handler handling this message
     * @param contextMap      The Map that store context values that must be propagated between threads
     */
    public void reset(Event event, Class<? extends AbstractAnnotatedSaga> sagaType,
    		HandlerConfiguration handler,  AbstractAnnotatedSaga newSaga,
            Map contextMap, Span tracingSpan) { //NOSONAR
        this.elector.clear();
        this.publishedEvent = event;
        this.sagaType = sagaType;
        this.handler = handler;
        this.newSaga = newSaga;
        this.associationValue = handler.getAssociationValue();
        this.contextMap = contextMap;
        this.tracingSpan = tracingSpan;
        this.retryCount = 0;
    }

    /**
     * The Factory class for AsyncSagaProcessingEvent instances.
     */
    static class Factory implements EventFactory<AsyncSagaProcessingEvent> {
        @Override
        public AsyncSagaProcessingEvent newInstance() {
            return new AsyncSagaProcessingEvent();
        }
    }

    @Override
    public String toString() {
        return String.format("publishedEvent:[%s] - targetSaga:[%s]", this.publishedEvent, this.sagaType);
    }

}
