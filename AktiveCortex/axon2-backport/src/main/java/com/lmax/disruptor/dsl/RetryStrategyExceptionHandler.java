/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lmax.disruptor.dsl;

import com.lmax.disruptor.BatchEventProcessor;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.LifecycleAware;

public interface RetryStrategyExceptionHandler {
	
	/**
     * Strategy for handling uncaught exceptions when processing an event.
     *
     * If the strategy wishes to suspend further processing by the {@link BatchEventProcessor}
     * then is should throw a {@link RuntimeException}.
     * If the strategy wishes to ignore the failed event and to proceed to the next 
     * then it should return true.
     * If the strategy intends to retry the failed event then it should return false.
     *
     * @param ex the exception that propagated from the {@link EventHandler}.
     * @param sequence of the event which cause the exception.
     * @param event being processed when the exception occurred.
     *
     * @return true to false, depending on whether you want to proceed to the next event 
     * or to process again the current event.
     *
     */
    boolean handleAndMoveForward(Throwable ex, long sequence, Object event);

    /**
     * Callback to notify of an exception during {@link LifecycleAware#onStart()}
     *
     * @param ex throw during the starting process.
     */
    void handleOnStartException(Throwable ex);

    /**
     * Callback to notify of an exception during {@link LifecycleAware#onShutdown()}
     *
     * @param ex throw during the shutdown process.
     */
    void handleOnShutdownException(Throwable ex);

}
