/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.annotation;

import org.aktivecortex.core.axon2backport.saga.Saga;
import org.aktivecortex.core.axon2backport.saga.SagaRepository;
import org.aktivecortex.core.axon2backport.saga.SagaRepositoryFactory;
import org.aktivecortex.core.axon2backport.saga.repository.inmemory.InMemorySagaRepository;
import org.axonframework.domain.DomainEvent;
import org.axonframework.eventhandling.EventBus;
import org.junit.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author Allard Buijze
 */
public class AsyncAnnotatedSagaManagerRunner {

    private static final int LIFECYCLE_COUNT = 100000;
    private AsyncAnnotatedSagaManager sagaManager;
    private AsyncAnnotatedSagaManagerRunner.StubInMemorySagaRepository sagaRepository;
    private ExecutorService executorService;

    public static void main(String[] args) throws InterruptedException {
        int lifecycleCount = LIFECYCLE_COUNT;
        if (args.length > 0) {
            lifecycleCount = Integer.parseInt(args[0]);
        }
        AsyncAnnotatedSagaManagerRunner runner = new AsyncAnnotatedSagaManagerRunner();
        runner.setUp();
        try {
            runner.testMultipleDisconnectedSagaLifeCycle(lifecycleCount);
        } finally {
            runner.tearDown();
        }
    }

    public void setUp() {
        sagaManager = new AsyncAnnotatedSagaManager(mock(EventBus.class), StubAsyncSaga.class);
        sagaRepository = new StubInMemorySagaRepository();
        sagaManager.setSagaRepositoryFactory(new SagaRepositoryFactory()  {
			
			@Override
			public SagaRepository createSagaRepository() {
				return sagaRepository;
			}
		});
        executorService = Executors.newCachedThreadPool();
        sagaManager.setExecutor(executorService);
        sagaManager.setProcessorCount(3);
        sagaManager.setBufferSize(1024);
    }

    public void testMultipleDisconnectedSagaLifeCycle(int lifecycleCount) throws InterruptedException {
        sagaManager.start();
        assertEquals(0, sagaRepository.getKnownSagas());
        long t0 = System.currentTimeMillis();
        for (int t = 0; t < lifecycleCount; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t)) {
                sagaManager.handle(message);
            }
        }
        sagaManager.stop();
        long t1 = System.currentTimeMillis();
        System.out.println("It took " + (t1 - t0) + " ms to process " + lifecycleCount + " Saga life cycles.");
        System.out.println("That is " + (lifecycleCount * 6 * 1000) / (t1 - t0) + " events per second.");
        executorService.shutdown();
        assertTrue("Service refused to stop in 1 second", executorService.awaitTermination(1, TimeUnit.SECONDS));
        assertEquals("Incorrect known saga count", lifecycleCount, sagaRepository.getKnownSagas());
        assertEquals("Incorrect live saga count", 0, sagaRepository.getLiveSagas());
    }

    private DomainEvent[] createSimpleLifeCycle(String firstAssociation, String newAssociation) {
    	DomainEvent[] messages = new DomainEvent[6];
        messages[0] = new ForceCreateNewEvent(firstAssociation);
        messages[1] = new OptionallyCreateNewEvent(firstAssociation);
        messages[2] = new UpdateEvent(firstAssociation);
        messages[3] = new AddAssociationEvent(firstAssociation, newAssociation);
        messages[4] = new OptionallyCreateNewEvent(newAssociation);
        messages[5] = new DeleteEvent(firstAssociation);
        return messages;
    }

    @After
    public void tearDown() {
        sagaManager.stop();
    }

    public static class StubAsyncSaga extends AbstractAnnotatedSaga {

        @StartSaga(forceNew = false)
        @SagaEventHandler(associationProperty = "association")
        public void handleOptionallyCreateNew(OptionallyCreateNewEvent event) {
        }

        @StartSaga(forceNew = true)
        @SagaEventHandler(associationProperty = "association")
        public void handleOptionallyCreateNew(ForceCreateNewEvent event) {
        }

        @SagaEventHandler(associationProperty = "association")
        public void handleAddAssociation(AddAssociationEvent event) {
            associateWith("association", event.getNewAssociation());
        }

        @SagaEventHandler(associationProperty = "association")
        public void handleUpdate(UpdateEvent event) {
        }

        @EndSaga
        @SagaEventHandler(associationProperty = "association")
        public void handleDelete(DeleteEvent event) {
        }
    }

    private static class OptionallyCreateNewEvent extends AbstractSagaTestEvent {

        private OptionallyCreateNewEvent(String association) {
            super(association);
        }
    }

    private static class ForceCreateNewEvent extends AbstractSagaTestEvent {

        private ForceCreateNewEvent(String association) {
            super(association);
        }
    }

    private static class UpdateEvent extends AbstractSagaTestEvent {

        private UpdateEvent(String association) {
            super(association);
        }
    }

    private static class DeleteEvent extends AbstractSagaTestEvent {

        private DeleteEvent(String association) {
            super(association);
        }
    }

    private static class AddAssociationEvent extends AbstractSagaTestEvent {

        private final String newAssociation;

        private AddAssociationEvent(String association, String newAssociation) {
            super(association);
            this.newAssociation = newAssociation;
        }

        public String getNewAssociation() {
            return newAssociation;
        }
    }

    private static class AbstractSagaTestEvent extends DomainEvent {

        private final String association;

        private AbstractSagaTestEvent(String association) {
            this.association = association;
        }

        public String getAssociation() {
            return association;
        }
    }

    private class StubInMemorySagaRepository extends InMemorySagaRepository {

        private AtomicInteger knownSagas = new AtomicInteger();
        private AtomicInteger liveSagas = new AtomicInteger();

        @Override
        public void commit(Saga saga) {
            if (!saga.isActive()) {
                liveSagas.decrementAndGet();
            }
            super.commit(saga);
        }

        @Override
        public void add(Saga saga) {
            knownSagas.incrementAndGet();
            liveSagas.incrementAndGet();
            super.add(saga);
        }

        public int getKnownSagas() {
            return knownSagas.get();
        }

        public int getLiveSagas() {
            return liveSagas.get();
        }
    }
}
