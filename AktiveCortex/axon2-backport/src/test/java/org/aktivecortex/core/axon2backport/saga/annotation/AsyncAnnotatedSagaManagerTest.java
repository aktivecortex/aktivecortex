/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.annotation;

import org.aktivecortex.core.axon2backport.saga.Saga;
import org.aktivecortex.core.axon2backport.saga.SagaRepository;
import org.aktivecortex.core.axon2backport.saga.SagaRepositoryFactory;
import org.aktivecortex.core.axon2backport.saga.repository.inmemory.InMemorySagaRepository;
import org.axonframework.domain.DomainEvent;
import org.axonframework.eventhandling.EventBus;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.HeuristicMixedException;
import javax.transaction.RollbackException;
import javax.transaction.TransactionRolledbackException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @author Allard Buijze
 * @author Domenico Maria Giffone
 */
public class AsyncAnnotatedSagaManagerTest {

	private static final Logger logger = LoggerFactory.getLogger(AsyncAnnotatedSagaManagerTest.class);

    private AsyncAnnotatedSagaManager testSubject;

    private EventBus eventBus;

   // private ExecutorService executorService;

    private FixedSagaRepositoryProvider sagaRepositoryProvider;

    Set<Class<? extends Throwable>> transientExceptions = new HashSet<Class<? extends Throwable>>();

    @BeforeClass
    public static void startProfiler() throws InterruptedException {
//    	logger.info("waiting 30 seconds to let you start a profiler");
//    	Thread.sleep(30 * 1000);
    }

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() {
    	eventBus = mock(EventBus.class);
    	//executorService = Executors.newCachedThreadPool();
    	initTransientExceptions();
    	initFixedStubRepositoryProvider();
    }


	private void initFixedStubRepositoryProvider() {
		sagaRepositoryProvider = new FixedSagaRepositoryProvider();
	}

	private void initTestSubject(Class<? extends AbstractAnnotatedSaga>...sagas) {
		testSubject = new AsyncAnnotatedSagaManager(eventBus, sagas);
		testSubject.setSagaRepositoryFactory(sagaRepositoryProvider);
        testSubject.setProcessorCount(3);
        testSubject.setBufferSize(64);
	}


	private void initTransientExceptions() {
		transientExceptions.clear();
    	transientExceptions.add(HeuristicMixedException.class);
    	transientExceptions.add(RollbackException.class);
    	transientExceptions.add(TransactionRolledbackException.class);
	}

    @After
    public void tearDown() {
//        testSubject.stop();
    }

    @Test
    public void testSingleSagaLifeCycle() throws InterruptedException {
    	initTestSubject(StubAsyncSaga.class);
        testSubject.start();
        assertEquals(0, sagaRepositoryProvider.getCurrentStubRepository().getKnownSagas());
        for (DomainEvent message : createSimpleLifeCycle("one", "two", true)) {
            testSubject.handle(message);
        }
        testSubject.stop();
       // executorService.shutdown();
        //assertTrue("Service refused to stop in 1 second", executorService.awaitTermination(1, TimeUnit.SECONDS));
        assertEquals("Incorrect known saga count", 1, sagaRepositoryProvider.getCurrentStubRepository().getKnownSagas());
        assertEquals("Incorrect live saga count", 0, sagaRepositoryProvider.getCurrentStubRepository().getLiveSagas());
    }

    @Test
    public void testMultipleDisconnectedSagaLifeCycle() throws InterruptedException {
    	initTestSubject(StubAsyncSaga.class);
        testSubject.start();
		assertEquals(0, sagaRepositoryProvider.getCurrentStubRepository().getKnownSagas());
        for (int t = 0; t < 1000; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t, (t & 1) == 0)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
//        executorService.shutdown();
//        assertTrue("Service refused to stop in 1 second", executorService.awaitTermination(1, TimeUnit.SECONDS));
        int knownSagas = sagaRepositoryProvider.getCurrentStubRepository().getKnownSagas();
        int liveSagas = sagaRepositoryProvider.getCurrentStubRepository().getLiveSagas();
        logger.info("SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect known saga count", 1000, knownSagas);
		assertEquals("Incorrect live saga count", 0, liveSagas);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testNullAssociationIgnoresEvent() {
        initTestSubject(StubAsyncSaga.class, AnotherStubAsyncSaga.class, ThirdStubAsyncSaga.class);
        testSubject.start();

        testSubject.handle(new ForceCreateNewEvent(null));
        testSubject.handle(new OptionallyCreateNewEvent(null));

        testSubject.stop();
//        executorService.shutdown();
//        assertTrue("Service refused to stop in 1 second", executorService.awaitTermination(1, TimeUnit.SECONDS));
        assertEquals("Incorrect known saga count", 0, sagaRepositoryProvider.getCurrentStubRepository().getKnownSagas());
        assertEquals("Incorrect live saga count", 0, sagaRepositoryProvider.getCurrentStubRepository().getLiveSagas());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testExceptionsFromHandlerAreIgnored() throws InterruptedException {
    	initTestSubject(StubAsyncSaga.class);

        testSubject.start();

        testSubject.handle(new ForceCreateNewEvent("test"));
        testSubject.handle(new GenerateErrorOnHandlingEvent("test"));

        testSubject.stop();
//        executorService.shutdown();
//        assertTrue("Service refused to stop in 1 second", executorService.awaitTermination(1, TimeUnit.SECONDS));
        assertEquals("Incorrect known saga count", 1, sagaRepositoryProvider.getCurrentStubRepository().getKnownSagas());
        assertEquals("Incorrect live saga count", 1, sagaRepositoryProvider.getCurrentStubRepository().getLiveSagas());
    }

    @SuppressWarnings("unchecked")
	@Test
    public void testMultipleDisconnectedSagaLifeCycle_WithOptionalStart() throws InterruptedException {
    	initTestSubject(StubAsyncSaga.class, AnotherStubAsyncSaga.class, ThirdStubAsyncSaga.class);

        testSubject.start();
        assertEquals(0, sagaRepositoryProvider.getCurrentStubRepository().getKnownSagas());
        for (int t = 0; t < 500; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t, false)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
//        executorService.shutdown();
//        assertTrue("Service refused to stop in 1 second", executorService.awaitTermination(1, TimeUnit.SECONDS));
        int knownSagas = sagaRepositoryProvider.getCurrentStubRepository().getKnownSagas();
        int liveSagas = sagaRepositoryProvider.getCurrentStubRepository().getLiveSagas();
        logger.info("SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect live saga count", 0, liveSagas);
        assertEquals("Incorrect known saga count", 1500, knownSagas);
    }

    private List<DomainEvent> createSimpleLifeCycle(String firstAssociation, String newAssociation,
                                                     boolean includeForceStart) {
        List<DomainEvent> publicationList = new ArrayList<DomainEvent>();
        if (includeForceStart) {
            publicationList.add(new ForceCreateNewEvent(firstAssociation));
        }
        publicationList.add(new OptionallyCreateNewEvent(firstAssociation));
        publicationList.add(new UpdateEvent(firstAssociation));
        publicationList.add(new AddAssociationEvent(firstAssociation, newAssociation));
        publicationList.add(new OptionallyCreateNewEvent(newAssociation));
        publicationList.add(new DeleteEvent(newAssociation));
        // this exception should never be thrown, as the previous event ends the saga
        return publicationList;
    }

    public static class ThirdStubAsyncSaga extends AnotherStubAsyncSaga {

		/**
		 *
		 */
		private static final long serialVersionUID = 1L;

    }

    public static class AnotherStubAsyncSaga extends StubAsyncSaga {

		/**
		 *
		 */
		private static final long serialVersionUID = 1L;

    }

    public static class StubAsyncSaga extends AbstractAnnotatedSaga {

    	private static final Logger logger = LoggerFactory.getLogger("TestSaga");

        /**
		 *
		 */
		private static final long serialVersionUID = 1L;

		@StartSaga(forceNew = false)
        @SagaEventHandler(associationProperty = "association")
        public void handleOptionallyCreateNew(OptionallyCreateNewEvent event) {
        }

        @StartSaga(forceNew = true)
        @SagaEventHandler(associationProperty = "association")
        public void handleForceCreateNew(ForceCreateNewEvent event) {
        }

        @SagaEventHandler(associationProperty = "association")
        public void handleAddAssociation(AddAssociationEvent event) {
            associateWith("association", event.getNewAssociation());
        }

        @SagaEventHandler(associationProperty = "association")
        public void handleUpdate(UpdateEvent event) {
        }

        @SagaEventHandler(associationProperty = "association")
        public void createError(GenerateErrorOnHandlingEvent event) {
            throw new RuntimeException("Stub");
        }

        @EndSaga
        @SagaEventHandler(associationProperty = "association")
        public void handleDelete(DeleteEvent event) {
        }
    }

    private static class OptionallyCreateNewEvent extends AbstractSagaTestEvent {

        /**
		 *
		 */
		private static final long serialVersionUID = 1L;

		private OptionallyCreateNewEvent(String association) {
            super(association);
        }
    }

    private static class ForceCreateNewEvent extends AbstractSagaTestEvent {

        /**
		 *
		 */
		private static final long serialVersionUID = 1L;

		private ForceCreateNewEvent(String association) {
            super(association);
        }
    }

    private static class UpdateEvent extends AbstractSagaTestEvent {

        /**
		 *
		 */
		private static final long serialVersionUID = 1L;

		private UpdateEvent(String association) {
            super(association);
        }
    }

    private static class DeleteEvent extends AbstractSagaTestEvent {

        /**
		 *
		 */
		private static final long serialVersionUID = 1L;

		private DeleteEvent(String association) {
            super(association);
        }
    }

    private static class GenerateErrorOnHandlingEvent extends AbstractSagaTestEvent {

        /**
		 *
		 */
		private static final long serialVersionUID = 1L;

		private GenerateErrorOnHandlingEvent(String association) {
            super(association);
        }
    }

    private static class AddAssociationEvent extends AbstractSagaTestEvent {

        /**
		 *
		 */
		private static final long serialVersionUID = 1L;
		private final String newAssociation;

        private AddAssociationEvent(String association, String newAssociation) {
            super(association);
            this.newAssociation = newAssociation;
        }

        public String getNewAssociation() {
            return newAssociation;
        }
    }

    private static class AbstractSagaTestEvent extends DomainEvent {

        /**
		 *
		 */
		private static final long serialVersionUID = 1L;
		private final String association;

        private AbstractSagaTestEvent(String association) {
            this.association = association;
        }

        public String getAssociation() {
            return association;
        }
    }

    private class FixedSagaRepositoryProvider implements SagaRepositoryFactory {

    	private StubInMemorySagaRepository repository;

		public FixedSagaRepositoryProvider() {
			this.repository = new StubInMemorySagaRepository();
		}

		@Override
		public SagaRepository createSagaRepository() {
			return repository;
		}

		public StubInMemorySagaRepository getCurrentStubRepository() {
			return repository;
		}
    }

    public static class StubInMemorySagaRepository extends InMemorySagaRepository {

        private Set<String> knownSagas = new ConcurrentSkipListSet<String>();
        private Set<String> liveSagas = new ConcurrentSkipListSet<String>();

        @Override
        public void commit(Saga saga) {
            assertTrue(knownSagas.contains(saga.getSagaIdentifier()));
            if (!saga.isActive()) {
                liveSagas.remove(saga.getSagaIdentifier());
            }
            super.commit(saga);
        }

        @Override
        public void add(Saga saga) {
            knownSagas.add(saga.getSagaIdentifier());
            liveSagas.add(saga.getSagaIdentifier());
            super.add(saga);
        }

        public int getKnownSagas() {
            return knownSagas.size();
        }

        public int getLiveSagas() {
            return liveSagas.size();
        }
    }

}
