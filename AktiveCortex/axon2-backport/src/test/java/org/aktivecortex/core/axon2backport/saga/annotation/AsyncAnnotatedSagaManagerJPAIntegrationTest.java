/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.axon2backport.saga.annotation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.RollbackException;
import javax.transaction.TransactionRolledbackException;

import org.aktivecortex.core.axon2backport.saga.*;
import org.aktivecortex.core.axon2backport.saga.repository.SagaSerializer;
import org.aktivecortex.core.axon2backport.saga.spring.SpringResourceInjector;
import org.aktivecortex.core.axon2backport.unitofwork.SpringTransactionManager;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.domain.DomainEvent;
import org.axonframework.eventhandling.EventBus;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * @author Allard Buijze
 * @author Domenico Maria Giffone
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/META-INF/spring/saga-repository-test.xml")
public class AsyncAnnotatedSagaManagerJPAIntegrationTest {

    private static final Logger logger = LoggerFactory.getLogger("TEST");
    
    private AsyncAnnotatedSagaManager testSubject;
    
    private EventBus eventBus;
    
    private StubSagaRepositoryFactory sagaRepositoryFactory;
    
    private static final LoadingCache<String, AtomicInteger> failedAttempts = CacheBuilder.newBuilder()
            .build(
            new CacheLoader<String, AtomicInteger>() {
                @Override
                public AtomicInteger load(String arg0) throws Exception {
                    return new AtomicInteger();
                }
            });
    
    private Set<Class<? extends Throwable>> transientExceptions = new HashSet<Class<? extends Throwable>>();
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    private PlatformTransactionManager transactionManager;
    
    @Autowired
    private ResourceInjector resourceInjector;
    
    private SpringTransactionManager springTransactionManager;
    
    @Autowired
    private ApplicationContext applicationContext;

    @BeforeClass
    public static void startProfiler() throws InterruptedException {
//    	logger.info("wating 30 seconds to let you start a profiler");
//    	Thread.sleep(30 * 1000);
    }

    @Before
    public void setUp() {
        eventBus = mock(EventBus.class);
        initTransientExceptions();
        initTxManager();
        initStubRepositoryProvider();
    }

    private void initStubRepositoryProvider() {
        sagaRepositoryFactory = new StubSagaRepositoryFactory(applicationContext, "sagaRepository");
    }

    private void initTestSubject(Class<? extends AbstractAnnotatedSaga>... sagas) {
        testSubject = new AsyncAnnotatedSagaManager(eventBus, sagas);
        final GenericSagaFactory sagaFactory = new GenericSagaFactory();
        sagaFactory.setResourceInjector(resourceInjector);
        testSubject.setSagaFactory(sagaFactory);
        testSubject.setSagaRepositoryFactory(sagaRepositoryFactory);
        testSubject.setProcessorCount(3);
        testSubject.setBufferSize(64);
        testSubject.setTransactionManager(springTransactionManager);
    }

    @Transactional
    public void clearSagaStore() {
        new TransactionTemplate(transactionManager).execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                entityManager.createQuery("DELETE FROM SagaEntry").executeUpdate();
                entityManager.createQuery("DELETE FROM AssociationValueEntry").executeUpdate();
            }
        });
    }

    private void initTxManager() {
        springTransactionManager = new SpringTransactionManager();
        springTransactionManager.setTransactionManager(transactionManager);
    }

    private void initTransientExceptions() {
        transientExceptions.clear();
        transientExceptions.add(HeuristicMixedException.class);
        transientExceptions.add(RollbackException.class);
        transientExceptions.add(TransactionRolledbackException.class);
    }

    @After
    public void tearDown() {
        clearSagaStore();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSingleSagaLifeCycle() throws InterruptedException {
        initTestSubject(StubAsyncSaga.class);
        testSubject.start();
        assertEquals(0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        for (DomainEvent message : createSimpleLifeCycle("one", "two", true)) {
            testSubject.handle(message);
        }
        testSubject.stop();
        assertEquals("Incorrect known saga count", 1, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        assertEquals("Incorrect live saga count", 0, sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMultipleDisconnectedSagaLifeCycle() throws InterruptedException {
        initTestSubject(StubAsyncSaga.class);
        testSubject.start();
        assertEquals(0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        for (int t = 0; t < 100; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t, (t & 1) == 0)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
        int knownSagas = sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas();
        int liveSagas = sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas();
        logger.info("SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect known saga count", 100, knownSagas);
        assertEquals("Incorrect live saga count", 0, liveSagas);
    }

    @SuppressWarnings("unchecked")
    //@Test
    public void testNullAssociationIgnoresEvent() throws InterruptedException {
        initTestSubject(StubAsyncSaga.class, AnotherStubAsyncSaga.class, ThirdStubAsyncSaga.class);
        testSubject.start();

        testSubject.handle(new ForceCreateNewEvent(null));
        testSubject.handle(new OptionallyCreateNewEvent(null));

        testSubject.stop();
        assertEquals("Incorrect known saga count", 0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        assertEquals("Incorrect live saga count", 0, sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testExceptionsFromHandlerAreIgnored() throws InterruptedException {
        initTestSubject(StubAsyncSaga.class);

        testSubject.start();

        testSubject.handle(new ForceCreateNewEvent("test"));
        testSubject.handle(new GenerateErrorOnHandlingEvent("test"));

        testSubject.stop();
        assertEquals("Incorrect known saga count", 1, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        assertEquals("Incorrect live saga count", 1, sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMultipleDisconnectedSagaLifeCycle_WithOptionalStart() throws InterruptedException {
        initTestSubject(StubAsyncSaga.class, AnotherStubAsyncSaga.class, ThirdStubAsyncSaga.class);

        testSubject.start();
        assertEquals(0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        for (int t = 0; t < 100; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t, false)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
        int knownSagas = sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas();
        int liveSagas = sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas();
        logger.info("SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect live saga count", 0, liveSagas);
        assertEquals("Incorrect known saga count", 300, knownSagas);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testHotRestart() {
        initTestSubject(StubAsyncSaga.class, AnotherStubAsyncSaga.class, ThirdStubAsyncSaga.class);

        testSubject.start();
        assertEquals(0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        for (int t = 0; t < 100; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t, false)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
        int knownSagas = sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas();
        int liveSagas = sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas();
        logger.info("SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect live saga count", 0, liveSagas);
        assertEquals("Incorrect known saga count", 300, knownSagas);
        testSubject.start();
        assertEquals(0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        for (int t = 0; t < 100; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t, false)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
        knownSagas = sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas();
        liveSagas = sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas();
        logger.info("SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect live saga count", 0, liveSagas);
        assertEquals("Incorrect known saga count", 300, knownSagas);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testHotRestartWithPendingSagas() {
        initTestSubject(StubAsyncSaga.class, AnotherStubAsyncSaga.class, ThirdStubAsyncSaga.class);

        testSubject.start();
        assertEquals(0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        for (int t = 0; t < 10; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t, false, true)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
        int knownSagas = sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas();
        int liveSagas = sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas();
        logger.info("(1) - SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect live saga count", 30, liveSagas);
        assertEquals("Incorrect known saga count", 30, knownSagas);

        assertEquals("Incorrect persistent saga count", 30, entityManager.createQuery("select sae FROM SagaEntry sae").getResultList().size());
        clearSagaStore();
        assertEquals("Incorrect persistent saga count", 0, entityManager.createQuery("select sae FROM SagaEntry sae").getResultList().size());
        testSubject.start();
        assertEquals(0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        for (int t = 0; t < 100; t++) {
            for (DomainEvent message : createSimpleLifeCycle("association-" + t, "newAssociation-" + t, false)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
        knownSagas = sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas();
        liveSagas = sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas();
        logger.info("(2) - SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect live saga count", 0, liveSagas);
        assertEquals("Incorrect known saga count", 300, knownSagas);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDepInjectionOnSerializedSaga() throws InterruptedException {
        initTestSubject(InjectableAsyncSaga.class);

        testSubject.start();
        assertEquals(0, sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas());
        int totalSagaEvents = 100;
        int expectedSagas = 10;
        for (int t = 0; t < expectedSagas; t++) {
            for (DomainEvent message : createSimpleLifeCycle("saga-" + t, totalSagaEvents)) {
                testSubject.handle(message);
            }
        }
        testSubject.stop();
        int knownSagas = sagaRepositoryFactory.getCurrentStubRepository().getKnownSagas();
        int liveSagas = sagaRepositoryFactory.getCurrentStubRepository().getLiveSagas();
        logger.info("SAGA COUNT: {} - SAGA ALIVE: {}", knownSagas, liveSagas);
        assertEquals("Incorrect live saga count", 0, liveSagas);
        assertEquals("Incorrect known saga count", expectedSagas, knownSagas);
    }

    private List<DomainEvent> createSimpleLifeCycle(String associationKey, int totalSagaEvents) {
        List<DomainEvent> publicationList = new ArrayList<DomainEvent>();
        publicationList.add(new CountingCreateEvent(associationKey, totalSagaEvents));
        for (int i = 2; i < totalSagaEvents; i++) {
            publicationList.add(new CountingUpdateEvent(associationKey));
        }
        publicationList.add(new CountingConditionalEndEvent(associationKey));
        return publicationList;
    }

    private List<DomainEvent> createSimpleLifeCycle(String firstAssociation, String newAssociation, boolean includeForceStart) {
        return createSimpleLifeCycle(firstAssociation, newAssociation, includeForceStart, false);
    }

    private List<DomainEvent> createSimpleLifeCycle(String firstAssociation, String newAssociation,
            boolean includeForceStart, boolean leavePendingSaga) {
        List<DomainEvent> publicationList = new ArrayList<DomainEvent>();
        if (includeForceStart) {
            publicationList.add(new ForceCreateNewEvent(firstAssociation));
        }
        publicationList.add(new OptionallyCreateNewEvent(firstAssociation));
        publicationList.add(new UpdateEvent(firstAssociation));
        publicationList.add(new AddAssociationEvent(firstAssociation, newAssociation));
        publicationList.add(new OptionallyCreateNewEvent(newAssociation));
        if (!leavePendingSaga) {
            publicationList.add(new DeleteEvent(newAssociation));
        }
        // this exception should never be thrown, as the previous event ends the saga
        return publicationList;
    }

    public static class ThirdStubAsyncSaga extends AnotherStubAsyncSaga {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
    }

    public static class AnotherStubAsyncSaga extends StubAsyncSaga {

        /**
         * New
         *
         */
        private static final long serialVersionUID = 1L;
    }

    public static class StubAsyncSaga extends AbstractAnnotatedSaga {

        private static final long serialVersionUID = 1L;

        @StartSaga(forceNew = false)
        @SagaEventHandler(associationProperty = "association")
        public void handleOptionallyCreateNew(OptionallyCreateNewEvent event) {
        }

        @StartSaga(forceNew = true)
        @SagaEventHandler(associationProperty = "association")
        public void handleForceCreateNew(ForceCreateNewEvent event) {
        }

        @SagaEventHandler(associationProperty = "association")
        public void handleAddAssociation(AddAssociationEvent event) {
            associateWith("association", event.getNewAssociation());
        }

        @SagaEventHandler(associationProperty = "association")
        public void handleUpdate(UpdateEvent event) {
        }

        @SagaEventHandler(associationProperty = "association")
        public void createError(GenerateErrorOnHandlingEvent event) {
            throw new RuntimeException("Stub");
        }

        @EndSaga
        @SagaEventHandler(associationProperty = "association")
        public void handleDelete(DeleteEvent event) {
        }
    }

    private static class OptionallyCreateNewEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;

        private OptionallyCreateNewEvent(String association) {
            super(association);
        }
    }

    private static class ForceCreateNewEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;

        private ForceCreateNewEvent(String association) {
            super(association);
        }
    }

    private static class UpdateEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;

        private UpdateEvent(String association) {
            super(association);
        }
    }

    private static class DeleteEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;

        private DeleteEvent(String association) {
            super(association);
        }
    }

    private static class GenerateErrorOnHandlingEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;

        private GenerateErrorOnHandlingEvent(String association) {
            super(association);
        }
    }

    private static class AddAssociationEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;
        private final String newAssociation;

        private AddAssociationEvent(String association, String newAssociation) {
            super(association);
            this.newAssociation = newAssociation;
        }

        public String getNewAssociation() {
            return newAssociation;
        }
    }

    private static class AbstractSagaTestEvent extends DomainEvent {

        private static final long serialVersionUID = 1L;
        private final String association;

        private AbstractSagaTestEvent(String association) {
            this.association = association;
        }

        public String getAssociation() {
            return association;
        }
    }

    private static class CountingCreateEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;
        private final int expectedEvents;

        private CountingCreateEvent(String association, int expectedEvents) {
            super(association);
            this.expectedEvents = expectedEvents;
        }

        public int getExpectedEvents() {
            return expectedEvents;
        }
    }

    private static class CountingUpdateEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;

        private CountingUpdateEvent(String association) {
            super(association);
        }
    }

    private static class CountingConditionalEndEvent extends AbstractSagaTestEvent {

        private static final long serialVersionUID = 1L;

        private CountingConditionalEndEvent(String association) {
            super(association);
        }

    }

    public static class InjectableAsyncSaga extends CountingAsyncSaga {

        private static final long serialVersionUID = 1L;
        @Qualifier("testBus")
        @Autowired
        private transient CommandBus bus;

        @Override
        @SagaEventHandler(associationProperty = "association")
        public void handleUpdate(CountingUpdateEvent event) {
            Assert.notNull(bus, "Autowiring didn't work!");
            super.handleUpdate(event);
        }
    }

    public static class CountingAsyncSaga extends AbstractAnnotatedSaga {

        private static final long serialVersionUID = 1L;
        private int eventsReceived = 0;
        private int eventsExpected = 0;
        @Autowired
        private transient PlatformTransactionManager transactionManager;

        @StartSaga()
        @SagaEventHandler(associationProperty = "association")
        public void handleCreate(CountingCreateEvent event) {
            associateWith("association", event.getAssociation());
            this.eventsExpected = event.getExpectedEvents();
            eventsReceived++;
        }

        @SagaEventHandler(associationProperty = "association")
        public void handleUpdate(CountingUpdateEvent event) {
            eventsReceived++;
        }

        @SagaEventHandler(associationProperty = "association")
        public void handleConditionalEnd(CountingConditionalEndEvent event) {
            eventsReceived++;
            if (eventsExpected == eventsReceived) {
                end();
                logger.info(String.format("%s - Saga correctly ended. ExpectedEventCount: %s - ReceivedEventCount: %s", Thread.currentThread().getName(), eventsExpected, eventsReceived));
            }  else {
                logger.info(String.format("%s - Something went wrong. ExpectedEventCount: %s - ReceivedEventCount: %s", Thread.currentThread().getName(), eventsExpected, eventsReceived));
            }
        }

    }

    private class StubSagaRepositoryFactory implements SagaRepositoryFactory {

        private static final String UNKNOWN_BEAN_SCOPE = "UNKNOWN";
        private static final String SINGLETON_BEAN_SCOPE = "Singleton";
        private static final String PROTOTYPE_BEAN_SCOPE = "Prototype";
        private ApplicationContext applicationContext;
        private StubJpaSagaRepository currentStubRepository;
        private String beanName;

        public StubSagaRepositoryFactory(ApplicationContext applicationContext, String beanName) {
            this.beanName = beanName;
            this.applicationContext = applicationContext;
        }

        @Override
        public SagaRepository createSagaRepository() {
            Assert.notNull(beanName);
            String beanScope = UNKNOWN_BEAN_SCOPE;
            boolean isSingleton = applicationContext.isSingleton(beanName);
            boolean isPrototype = applicationContext.isPrototype(beanName);
            if (isSingleton) {
                beanScope = SINGLETON_BEAN_SCOPE;
            }
            if (isPrototype) {
                beanScope = PROTOTYPE_BEAN_SCOPE;
            }
            SagaRepository repo = applicationContext.getBean(beanName, SagaRepository.class);
            logger.info("returning {} scoped SagaRepository instance from Spring ApplicationContext [{}]", beanScope, repo);
            currentStubRepository = new StubJpaSagaRepository(repo);
            return currentStubRepository;
        }

        public StubJpaSagaRepository getCurrentStubRepository() {
            return currentStubRepository;
        }
    }

    private class StubJpaSagaRepository implements SagaRepository {

        private SagaRepository delegate;

        private Set<String> knownSagas = new ConcurrentSkipListSet<String>();
        private Set<String> liveSagas = new ConcurrentSkipListSet<String>();


        public StubJpaSagaRepository(SagaRepository delegate) {
            this.delegate = delegate;
        }

        @Override
        public void commit(Saga saga) {
            assertTrue(knownSagas.contains(saga.getSagaIdentifier()));
            if (!saga.isActive()) {
                liveSagas.remove(saga.getSagaIdentifier());
            }
            delegate.commit(saga);
        }

        @Override
        public void add(Saga saga) {
            knownSagas.add(saga.getSagaIdentifier());
            liveSagas.add(saga.getSagaIdentifier());
            delegate.add(saga);
            if (!saga.isActive()) {
                liveSagas.remove(saga.getSagaIdentifier());
            }
        }

        public int getKnownSagas() {
            return knownSagas.size();
        }

        public int getLiveSagas() {
            return liveSagas.size();
        }

        @Override
        public Set<String> find(Class<? extends Saga> type,
                                AssociationValue associationValue) {
            return delegate.find(type, associationValue);
        }

        @Override
        public Saga load(String sagaIdentifier) {
            return delegate.load(sagaIdentifier);
        }
    }
}
