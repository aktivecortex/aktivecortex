/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.commandhandling.interceptors.afterreceive;

import org.aktivecortex.api.audit.AuditLogger;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageHeaders;
import org.aktivecortex.api.notification.ProgressNotifier;
import org.aktivecortex.core.commandhandling.interceptors.AfterReceiveInterceptorChain;
import org.aktivecortex.core.commandhandling.interceptors.AfterReceiveMessageHandlerInterceptor;
import org.aktivecortex.core.eventbus.DistributedCluster;
import org.aktivecortex.core.notification.support.UnitOfWorkProgressListener;
import org.aktivecortex.core.utils.io.Utils;
import org.axonframework.domain.Event;
import org.axonframework.unitofwork.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.List;

import static org.aktivecortex.api.message.MessageHeadersConstants.*;


public class TaskProgressInterceptor implements AfterReceiveMessageHandlerInterceptor<Command> {

    private ProgressNotifier progressNotifier;

    private DistributedCluster distributedCluster;

    public void setDistributedCluster(DistributedCluster distributedCluster) {
        this.distributedCluster = distributedCluster;
    }

    public void setProgressNotifier(ProgressNotifier progressNotifier) {
        this.progressNotifier = progressNotifier;
    }

    @Override
    public Object handle(Message<Command> message, UnitOfWork unitOfWork, AfterReceiveInterceptorChain chain) {
        UnitOfWorkProgressListener auditListener = new UnitOfWorkProgressListener(message, new AuditLogger() {

            @Override
            public void logFailed(Message<Command> message, Throwable failureCause, List<Event> events) {
                progressNotifier.taskFailed(message, failureCause);
            }

            @Override
            public void logSuccessful(Message<Command> message, List<Event> events) {
                if (!hasSideEffects(events)) {
                    progressNotifier.processCompleted();
                } else {
                    progressNotifier.taskCompleted(message);
                }
            }

            /**
             * Use getHandledEvents() to let competing listeners being able to notify task completion.
             * This is the most appropriate behavior for projection listeners.
             * <p>Use getSagaEvents() if you want to notify task completion here
             * even for events targeted to competing listeners.
             * In this case process lifetime will not include the listener span.
             * @param events
             * @return
             */
            private boolean hasSideEffects(List<Event> events) {
                for (Event e : events) {
                    if (distributedCluster.getHandledEvents().contains(e.getClass())) {
                        return true;
                    }
                }
                return false;
            }
        });
        unitOfWork.registerListener(auditListener);
        setDelivered(message);
        return chain.proceed();
    }

	private void setDelivered(Message<Command> message) {
			MessageHeaders metadata = message.getMessageHeaders();
			DateTime arrivalTime = new DateTime();
			metadata.put(SHIPPING_DELIVERED, Boolean.TRUE);
			metadata.put(SHIPPING_RECEIVED_ON, arrivalTime);
			metadata.put(SHIPPING_RECEIVER, Utils.getHostname());
			metadata.put(SHIPPING_DURATION, new Duration(metadata.getTimestamp(), arrivalTime));
	}

}
