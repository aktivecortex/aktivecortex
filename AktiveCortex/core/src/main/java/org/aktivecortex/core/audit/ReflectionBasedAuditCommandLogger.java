/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.audit;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.axonframework.auditing.AuditLogger;
import org.axonframework.domain.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.apache.commons.lang.builder.ToStringStyle.SHORT_PREFIX_STYLE;


public class ReflectionBasedAuditCommandLogger implements AuditLogger {

	private static final Logger logger = LoggerFactory.getLogger(ReflectionBasedAuditCommandLogger.class);

	@Override
	public void logFailed(Object message, Throwable failureCause, List<Event> events) {
		Message<Command> commandMessage = (Message<Command>) message;
		Command command = commandMessage.getPayload();
		StringBuilder sb = new StringBuilder();
		sb.append("\nFailure occurred during execution of command named: ").append(command.getClass().getName()).append("\n\t Exception name: ").append(
				failureCause.getClass().getName()).append("\n\t Exception message: ").append(failureCause.getMessage()).append("\n\t Command: ").append(
				command.toString()).append(" - ").append(ReflectionToStringBuilder.toString(command, SHORT_PREFIX_STYLE))
				.append("\n\t MessageHeaders: ").append(ReflectionToStringBuilder.toString(commandMessage.getMessageHeaders(), SHORT_PREFIX_STYLE));
		if (null != events && events.size() > 0) {
			sb.append("\n\t List of events fired during execution:");
			for (Event event : events) {
				sb.append("\n\t\t").append(toStringEvent(event));
			}
		}
		sb.append("\nPlease see log for more details.");
		logger.error(sb.toString());
	}

	@Override
	public void logSuccessful(Object message, Object returnValue, List<Event> events) {
		Message<Command> commandMessage = (Message<Command>) message;
		Command command = commandMessage.getPayload();
		StringBuilder sb = new StringBuilder();
		sb.append("\nSuccessful execution of command named: ").append(command.getClass().getName()).append("\n\t Command: ")
		.append(command.toString()).append(" - ").append(ReflectionToStringBuilder.toString(command, SHORT_PREFIX_STYLE))
		.append("\n\t MessageHeaders: ").append(ReflectionToStringBuilder.toString(commandMessage.getMessageHeaders(), SHORT_PREFIX_STYLE));
		if (null != returnValue) {
			sb.append("\n\t Return value: ").append(returnValue.toString());
		}
		if (null != events && events.size() > 0) {
			sb.append("\n\t List of events fired during execution:");
			for (Event event : events) {
				sb.append("\n\t\t").append(toStringEvent(event));
			}
		}
		logger.info(sb.toString());
	}

	private String toStringEvent(Event event) {
		return ReflectionToStringBuilder.toString(event, SHORT_PREFIX_STYLE);
	}

}
