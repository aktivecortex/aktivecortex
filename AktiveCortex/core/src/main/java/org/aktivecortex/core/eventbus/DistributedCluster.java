/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.eventbus;

import com.google.common.collect.Sets;
import org.axonframework.domain.Event;
import org.axonframework.domain.MutableEventMetaData;
import org.axonframework.eventhandling.SimpleCluster;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import static org.aktivecortex.api.message.MessageHeadersConstants.ROUTING_KEY;


public class DistributedCluster extends SimpleCluster implements InitializingBean  {
	
	public static final String CLUSTER_EVENTS = "DIST_CLUSTER_EVENTS";

	public static final String CLUSTER_SAGA_EVENTS = "DIST_CLUSTER_SAGA_EVENTS";

	public static final String CLUSTER_PARTITIONS = "DIST_CLUSTER_PARTITIONS";

	public static final String COMPETING_EVENT_LISTENERS = "DIST_COMPETING_EVENT_LISTENERS";
	
	public static final String COMPETING_SAGAS = "DIST_SAGAS";
	
	public static final String DESTINATION_NAME = "DIST_DESTINATION_NAME";
	
	public static final String DESTINATION_TYPE = "DIST_DESTINATION_TYPE";
	
	public static final String KEY_MAP = "DIST_KEY_MAP";

	protected static final boolean DEFAULT_ORDERED_DELIVERY = false;
	
	protected static final boolean ORDERED_DELIVERY_ENSURED = true;
	
	public enum DestinationType{QUEUE, TOPIC};
	
	private boolean orderedDelivery; 
	
	/**
	 * 
	 */
	protected DistributedCluster() {
		this(DEFAULT_ORDERED_DELIVERY);
	}

	/**
	 * 
	 */
	protected DistributedCluster(boolean orderedDelivery) {
		super();
		this.orderedDelivery = orderedDelivery;
	}

	public void setDestinationName(String destinationName) {
		this.getMetaData().setProperty(DESTINATION_NAME, destinationName);
	}

	public void setDestinationType(DestinationType destinationType) {
		this.getMetaData().setProperty(DESTINATION_TYPE, destinationType);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {  //NOSONAR
		Assert.isTrue(this.getMetaData().isPropertySet(DESTINATION_NAME), "property destinationName not set");
		Assert.isTrue(this.getMetaData().isPropertySet(DESTINATION_TYPE), "property destinationType not set");
	}
	
	@SuppressWarnings("unchecked")
	public Set<Class<?>> getHandledEvents() {
		Set<Class<?>> events = (Set<Class<?>>) getMetaData().getProperty(CLUSTER_EVENTS);
		if (null == events || events.isEmpty()) {
			throw new IllegalStateException("Event set not initialized");
		}
		return events;
	}

    @SuppressWarnings("unchecked")
	public Set<Class<?>> getSagaEvents() {
		Set<Class<?>> events = (Set<Class<?>>) getMetaData().getProperty(CLUSTER_SAGA_EVENTS);
		if (null == events || events.isEmpty()) {
			return Sets.newHashSet();
		}
		return events;
	}
	
	public boolean isEventHandled(Event ev) {
		if (getHandledEvents().contains(ev.getClass())) {
			if (isOrderedDeliveryEnsured()) {
				addRoutingInfos(ev);
			}
			return true;
		}
		return false;
	}

	public String getDestinationName() {
		return (String) this.getMetaData().getProperty(DESTINATION_NAME);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getMetaData() == null) ? 0 : getMetaData().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
            return true;
        }
		if (obj == null) {
            return false;
        }
		if (getClass() != obj.getClass()) {
            return false;
        }
		final DistributedCluster other = (DistributedCluster) obj;
		if (getMetaData() == null) {
			if (other.getMetaData() != null) {
                return false;
            }
		} else if (!getMetaData().equals(other.getMetaData())) {
            return false;
        }
		return true;
	}

	protected void addRoutingInfos(Event ev) {
		Serializable key = getRoutingKey(ev.getClass());
		if (null!=key) {
			MutableEventMetaData md = (MutableEventMetaData) ev.getMetaData();
			md.put(ROUTING_KEY, key);
		}
	}

	@SuppressWarnings("unchecked")
	private Serializable getRoutingKey(Class<? extends Event> className) {
		Map<String, String> keys = (Map<String, String>) getMetaData().getProperty(DistributedCluster.KEY_MAP);
		return keys.get(className.getName());
	}
	
	public boolean isOrderedDeliveryEnsured() {
		return orderedDelivery;
	}
	
	public void setOrderedDeliveryEnsured(boolean orderedDelivery) {
		this.orderedDelivery = orderedDelivery;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb
		.append("\tName: ")
		.append(this.getClass().getSimpleName())
		.append("\n\tDestination: ")
		.append(getMetaData().getProperty(DESTINATION_NAME))
		.append("\n\tDestination-Type: ")
		.append(getMetaData().getProperty(DESTINATION_TYPE))
		.append("\n\tOrdered-Delivery: ")
		.append(orderedDelivery)
		.append("\n\tHandled-Events: ")
		.append(getMetaData().getProperty(CLUSTER_EVENTS))
		.append("\n\tCompeting-Event-Listeners: ")
		.append(getMetaData().getProperty(COMPETING_EVENT_LISTENERS))
		.append("\n\tSagas: ")
		.append(getMetaData().getProperty(COMPETING_SAGAS))
		.append("\n\tKey/Events-Partitions: ")
		.append(getMetaData().getProperty(CLUSTER_PARTITIONS))
		.append("\n\tEvent/Key-Associations: ")
		.append(getMetaData().getProperty(KEY_MAP));
		return sb.toString();
	}
	
}
