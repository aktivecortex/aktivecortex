/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.eventhandling.scheduling;

import org.aktivecortex.core.notification.NotificationUtils;
import org.axonframework.domain.ApplicationEvent;
import org.axonframework.domain.EventMetaData;
import org.axonframework.eventhandling.scheduling.quartz.Quartz2FireEventJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.MDC;

import java.util.Map;

import static org.aktivecortex.api.message.MessageHeadersConstants.NOTIFICATION_PROCESS_ID;
import static org.aktivecortex.api.message.MessageHeadersConstants.SHIPPING_PROCESS_DISPATCHER;
import static org.aktivecortex.core.notification.NotificationUtils.restoreContext;

/**
 * Quartz Job that propagate notification properties stored in event metadata to the thread local MDC context .
 *
 * @author Domenico Maria Giffone.
 * @since 1.3
 */
public class Quartz2FireEventJobWithMDC extends Quartz2FireEventJob {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        final ApplicationEvent event = (ApplicationEvent) context.getJobDetail().getJobDataMap().get(EVENT_KEY);
        final Map contextMap = MDC.getCopyOfContextMap();
        final EventMetaData metaData = event.getMetaData();
        if (null != metaData.get(NOTIFICATION_PROCESS_ID)) {
            MDC.put(NOTIFICATION_PROCESS_ID, (String) metaData.get(NOTIFICATION_PROCESS_ID));
        }
        if (null != metaData.get(SHIPPING_PROCESS_DISPATCHER)) {
            MDC.put(SHIPPING_PROCESS_DISPATCHER, (String) metaData.get(SHIPPING_PROCESS_DISPATCHER));
        }
        try {
            super.execute(context);
        } finally {
            restoreContext(contextMap);
        }
    }
}