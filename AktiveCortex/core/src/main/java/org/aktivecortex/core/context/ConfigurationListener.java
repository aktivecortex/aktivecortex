/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.context;

import org.axonframework.eventhandling.Cluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class ConfigurationListener implements ApplicationListener<ContextRefreshedEvent> {
	
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationListener.class);
	
	//private @Value("#{'${APPLICATION.BUILDTIME}'}") String buildTime;
	//private @Value("#{'${APPLICATION.VERSION}'}") String version;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent ev) {
		Cluster nonCompetingCluster = ev.getApplicationContext().getBean("nonCompetingCluster", Cluster.class);
		Cluster competingCluster = ev.getApplicationContext().getBean("competingCluster", Cluster.class);
		logger.info("\n------------ ------------ Non Competing Cluster  ------------ ------------\n{}\n------------ ------------ ----------  ---------- ------------ ------------", nonCompetingCluster);
		logger.info("\n------------ ------------   Competing Cluster    ------------ ------------\n{}\n------------ ------------ ----------  ---------- ------------ ------------", competingCluster);
//		if (null != buildTime) {
//			logger.info("\n\n *********** version: {} ************\n\n", version);
//			logger.info("\n\n *********** builded on: {} ************\n\n", buildTime);
//			
//		}
	}

}
