/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message;

import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageHeaders;
import org.aktivecortex.api.message.Routable;
import org.axonframework.domain.Event;

import static org.aktivecortex.api.message.MessageHeadersConstants.ROUTING_KEY;


public class EventMessage implements Message<Event> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9142428812442932646L;
	
	private final Event payload;


	private final MessageHeaders headers;

	public EventMessage(Event event) {
		this.payload = event;
		this.headers = new DefaultMessageHeaders();
	}

	@Override
	public String getRoutingKey() {
        if (payload instanceof Routable) {
            return  ((Routable) payload).getRoutingKey();
        }
		return (String) payload.getMetaDataValue(ROUTING_KEY);
	}

	@Override
	public MessageHeaders getMessageHeaders() {
		return headers;
	}

	@Override
	public Event getPayload() {
		return payload;
	}

}
