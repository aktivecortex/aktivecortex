/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.saga;

import com.google.common.base.Strings;
import org.aktivecortex.core.axon2backport.saga.annotation.AbstractAnnotatedSaga;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.*;
import org.slf4j.Logger;

public abstract class AbstractSaga extends AbstractAnnotatedSaga {

    private static final String EMPTY_STRING = "";

	private static final String SEPARATOR = "------------------------------------------";

	private final StringBuilder logger = new StringBuilder("Saga Summary:\n\n\t");

    protected abstract Logger getLogger();

    private DateTime startTime;

    private DateTime stepTime;

    public DateTime getStartTime() {
        return startTime;
    }

    protected void appendLog(final String message,boolean info, boolean banner) {
		
		if(Strings.isNullOrEmpty(message)) {
            return;
        }

        String format = null;

        if (banner) {
            format = message;
        } else {
            format = String.format("[%s] - %s", this.getSagaIdentifier(), message);
        }

        if(info) {
            getLogger().info(format);
        }
		else {
            getLogger().debug(format);
        }
		
		if (banner) {
            logger.append(message).append("\n\t");
        } else {
            logger.append(String.format("[%s] - ", printTime(DateTime.now()))).append(message).append("\n\t");
        }
	}
	
	protected void appendLog(String message, boolean banner){
		appendLog(message,false, banner);
	}
	
	protected void appendSeparator(){
		appendLog(SEPARATOR, true);
	}
	
	public void appendElapsedTime(String label){
		final DateTime now = DateTime.now();
		final Duration elapsed=new Duration(stepTime,now);
		final Duration duration=new Duration(startTime,now);
		
		appendLog("{} - [elapsed: {} - since start: {}]",
				new Object[]{label,printTime(elapsed),printTime(duration)});
		stepTime=new DateTime();
	}
	
	public void appendElapsedTime(String label,int rows){
		final DateTime now =DateTime.now();
		final Duration elapsed=new Duration(stepTime,now);
		final Duration duration=new Duration(startTime,now);
		
		final long media=duration.getMillis()/rows;
		appendLog("{} - [elapsed: {} - average time: {} - since start: {}]",
				new Object[]{label,printTime(elapsed),media,printTime(duration)});
		
		stepTime=new DateTime();
	}
	
	protected void appendStartLog(String message){
		startTime=new DateTime();
		stepTime=new DateTime();

		appendLog(EMPTY_STRING, true);
		appendLog(SEPARATOR,true, true);
		appendLog(String.format("START SAGA \"%s\" - [%s]", this.getClass().getSimpleName(), this.getSagaIdentifier()),true, true);
		if(message!=null) {
            appendLog(message, true, true);
        }
		appendLog("at: ".concat(printDate(new DateTime())),true, true);
		appendLog(SEPARATOR,true, true);
	}
	
	protected void appendEndLog(String message){
		DateTime endTime=new DateTime();
		Duration elapsed=new Duration(startTime,endTime);
		
		appendLog(EMPTY_STRING, true);
		appendLog(SEPARATOR, true, true);
		appendLog(String.format("END SAGA \"%s\" - [%s]", this.getClass().getSimpleName(), this.getSagaIdentifier()),true, true);
		if(message!=null) {
            appendLog(message, true, true);
        }
		appendLog("at: ".concat(printDate(new DateTime())),true, true);
		appendLog("elapsed time: ".concat(printTime(elapsed)),true, true);
		appendLog(SEPARATOR,true, true);
		getLogger().info(logger.append("\n\n").toString());
	}
	
	
	
	protected void appendLog(String message,Object arg){
		appendLog(org.slf4j.helpers.MessageFormatter.format(message,arg).getMessage(), false);
	}
	
	protected void appendLog(String message,Object arg,Object arg2){
		appendLog(org.slf4j.helpers.MessageFormatter.format(message,arg,arg2).getMessage(), false);
	}
	
	protected void appendLog(String message, Object[] objs){		
		appendLog(org.slf4j.helpers.MessageFormatter.arrayFormat(message,objs).getMessage(), false);
	}
	
	public static String printTime(Duration elapsed){		
		PeriodFormatter timeFmt = PeriodFormat.getDefault();		
		return timeFmt.print(elapsed.toPeriod());
	}
	
	public static String printDate(DateTime t){
		DateTimeFormatter timeFmt = ISODateTimeFormat.dateTime();
		return timeFmt.print(t);
	}
	
	public static String printTime(DateTime time){		
		DateTimeFormatter timeFmt = DateTimeFormat.forPattern("HH:mm:ss");		
		return timeFmt.print(time);
	}
	
}
