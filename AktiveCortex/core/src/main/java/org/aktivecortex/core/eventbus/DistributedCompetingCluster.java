/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.eventbus;

import org.springframework.util.Assert;

import java.util.Collections;
import java.util.Set;


public class DistributedCompetingCluster extends DistributedCluster {
	

	public void setCompetingListeners(Set<String> competingListeners) {
		this.getMetaData().setProperty(COMPETING_EVENT_LISTENERS, Collections.unmodifiableSet(competingListeners));
	}
	
	public void setSagas(Set<String> sagas) {
		this.getMetaData().setProperty(COMPETING_SAGAS, Collections.unmodifiableSet(sagas));
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();
		Assert.isTrue(this.getMetaData().isPropertySet(COMPETING_EVENT_LISTENERS), "property competingListeners not set");
	}

	/**
	 * 
	 */
	protected DistributedCompetingCluster() {
		this(ORDERED_DELIVERY_ENSURED);
	}

	/**
	 * @param orderedDelivery
	 */
	protected DistributedCompetingCluster(boolean orderedDelivery) {
		super(orderedDelivery);
	}
	
	

	
}
