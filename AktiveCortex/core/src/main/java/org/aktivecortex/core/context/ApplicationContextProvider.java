/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationContextProvider implements ApplicationContextAware {

	private static ApplicationContext ctx = null;

	public static ApplicationContext getApplicationContext() {
		return ctx;
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		ctx = context;
	}

	public static <T> T getBean(Class<T> arg0) {
		return ctx.getBean(arg0);
	}

	public static <T> T getBean(String arg0, Class<T> arg1) {
		return ctx.getBean(arg0, arg1);
	}

	public static Object getBean(String arg0, Object... arg1) {
		return ctx.getBean(arg0, arg1);
	}

	public static Object getBean(String arg0) {
		return ctx.getBean(arg0);
	}

}
