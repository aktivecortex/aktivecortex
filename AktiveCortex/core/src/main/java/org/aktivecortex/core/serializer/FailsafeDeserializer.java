/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.serializer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import org.axonframework.serializer.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FailsafeDeserializer<T> implements Serializer<T> {
	private Logger logger = LoggerFactory.getLogger(FailsafeDeserializer.class);
	private final List<Serializer<T>> deserializers = new LinkedList<Serializer<T>>();
	private final Serializer<T> serializer;

	public FailsafeDeserializer(Serializer<T> selectedSerializer,
			Serializer<T>[] selectedDeserializers) {
		serializer = selectedSerializer;
		logger.info("Serializer [{}] registered", serializer.getClass()
				.getName());
		for (Serializer<T> deserializer : selectedDeserializers) {
			deserializers.add(deserializer);
			logger.info("Deserializer [{}] registered ", deserializer
					.getClass().getName());
		}
	}

	@Override
	public T deserialize(InputStream arg0) throws IOException {
		for (Serializer<T> deserializer : deserializers) {
			try {
				return deserializer.deserialize(arg0);
			} catch (Exception e) {
				logger.warn(
						"Serializer [{}] failed with exception: [{}] - reason: [{}]",
						new Object[] { deserializer.getClass().getName(),
								e.getCause().getClass().getName(),
								e.getCause().getMessage() });
			}
		}
		throw new RuntimeException(
				"None of the following serializers was able to perform the operation: "
						+ deserializers.toString());
	}

	@Override
	public T deserialize(byte[] arg0) {
		for (Serializer<T> deserializer : deserializers) {
			try {
				return deserializer.deserialize(arg0);
			} catch (Exception e) {
				logger.warn(
						"Serializer [{}] failed for reason: [{}] with exception: [{}]",
						new Object[] { deserializer.getClass().getName(),
								e.getMessage(), e });
			}
		}
		throw new RuntimeException(
				"None of the following serializers was able to perform the operation: "
						+ deserializers.toString());
	}

	@Override
	public byte[] serialize(T arg0) {
		return serializer.serialize(arg0);
	}

	@Override
	public void serialize(T arg0, OutputStream arg1) throws IOException {
		serializer.serialize(arg0, arg1);
	}
}
