/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification.support;

import org.aktivecortex.api.notification.ProgressNotifier;
import org.aktivecortex.core.axon2backport.saga.annotation.UnhandledAsyncSagaEventListener;
import org.axonframework.domain.Event;
import org.springframework.util.Assert;

/**
 * Notifies the end of the process for all events not managed by the sagas.
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
public class NotificationUnhandledSagaEventListener implements UnhandledAsyncSagaEventListener {

    private ProgressNotifier progressNotifier;

    public NotificationUnhandledSagaEventListener(ProgressNotifier progressNotifier) {
        Assert.notNull(progressNotifier, "ProgressNotifier can't be null!");
        this.progressNotifier = progressNotifier;
    }

    @Override
    public void onEvent(Event event) {
        progressNotifier.processCompleted();
    }
}
