/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.notification.ConversationManager;
import org.aktivecortex.api.notification.ProgressNotifier;
import org.slf4j.MDC;

import static org.aktivecortex.api.message.MessageHeadersConstants.NOTIFICATION_PROCESS_ID;
import static org.aktivecortex.core.notification.ResultImpl.FAILURE;
import static org.aktivecortex.core.notification.ResultImpl.SUCCESS;

public class ProgressLocalNotifierImpl implements ProgressNotifier {
	
	private ConversationManager conversationManager;

	public void setConversationManager(ConversationManager conversationManager) {
		this.conversationManager = conversationManager;
	}

	@Override
	public void taskFailed(Message<Command> message, Throwable t) {
		conversationManager.completeTask(MDC.get(NOTIFICATION_PROCESS_ID), message.getPayload().getClass().getCanonicalName(), FAILURE, t.getLocalizedMessage());
	}

	@Override
	public void taskCompleted(Message<Command> message) {
		conversationManager.completeTask(MDC.get(NOTIFICATION_PROCESS_ID), message.getPayload().getClass().getCanonicalName(), SUCCESS);
	}
    @Override
	public void processCompleted() {
		conversationManager.completeProcess(MDC.get(NOTIFICATION_PROCESS_ID), SUCCESS);
	}

}
