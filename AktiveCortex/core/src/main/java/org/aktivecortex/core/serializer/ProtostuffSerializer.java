/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.serializer;

import static com.dyuproject.protostuff.LinkedBuffer.allocate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import org.axonframework.serializer.Serializer;
import org.axonframework.util.SerializationException;

import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.ProtostuffIOUtil;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.runtime.ProtostuffRuntimeSchemaAdapter;
import com.dyuproject.protostuff.runtime.RuntimeSchemaAdapter;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.Sun14ReflectionProvider;
import java.io.Closeable;

public class ProtostuffSerializer<T> implements Serializer<T>, Closeable {

	protected static final int BUFFER_SIZE = 1 * 512;

    public static final int CACHE_CONCURRENCY_LEVEL = 32;

    public static final int CACHE_SIZE = 64;

    private static final LoadingCache<String, LinkedBuffer> BUFFERS = CacheBuilder
                .newBuilder()
                .concurrencyLevel(CACHE_CONCURRENCY_LEVEL)
                .maximumSize(CACHE_SIZE)
                .build(new CacheLoader<String, LinkedBuffer>() {
            @Override
            public LinkedBuffer load(String k) throws Exception { //NOSONAR
                return allocate(BUFFER_SIZE);
            }
        });

	private static final ReflectionProvider SUN_14_REFLECTION_PROVIDER = new Sun14ReflectionProvider();

	private final RuntimeSchemaAdapter adapter;

	/**
	 * @param adapter
	 */
	public ProtostuffSerializer(RuntimeSchemaAdapter adapter) {
		this.adapter = adapter;
	}

	/**
	 *
	 */
	public ProtostuffSerializer() {
		this.adapter = new ProtostuffRuntimeSchemaAdapter();
	}



	@Override
	public T deserialize(InputStream inputStream) throws IOException {
		final ObjectInputStream ois = new ObjectInputStream(inputStream);
        try {
            int size = ois.readInt();
    		if (0 == size) {
                return null;
            }
    		byte[] array = new byte[size];
    		ois.readFully(array, 0, size);
    		final String className = new String(array);
    		size = ois.readInt();
    		array = new byte[size];
    		ois.readFully(array, 0, size);
    		try {
    			@SuppressWarnings("unchecked")
    			final Class<T> clazz = (Class<T>) getClass().getClassLoader().loadClass(className);
    			@SuppressWarnings("unchecked")
    			final T proto = (T) SUN_14_REFLECTION_PROVIDER.newInstance(clazz);
    			final Schema<T> schema = adapter.getSchema( clazz );
    			ProtostuffIOUtil.mergeFrom(array, proto, schema);
    			return proto;
    		} catch (Exception e) {
    			throw new SerializationException("could not load class " + className, e);
    		}
        } catch (IOException e) {
            throw new SerializationException("An exception occurred while trying to de-serialize a stored Deliverable", e);
        } finally {
        	ois.close();
        }
	}

	@Override
	public T deserialize(byte[] bytes) {
		final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		try {
			return deserialize(bais);
        } catch (IOException e) {
            throw new SerializationException(
                    "The impossible has happened: a ByteArrayInputStream throwing an IO Exception", e);
        } finally {
        	try {
				bais.close();
			} catch (IOException e) {
				throw new SerializationException(
	                    "The impossible has happened: a ByteArrayInputStream throwing an IO Exception", e);
			}
        }
	}

	@Override
	public byte[] serialize(T instance) {
		 final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		 byte[] array;
	        try {
	            serialize(instance, baos);
	            array = baos.toByteArray();
	        } catch (IOException e) {
	            throw new SerializationException(
	                    "An exception occurred while trying to serialize a Deliverable for storage",
	                    e);
	        } finally {
	        	try {
					baos.flush();
					baos.close();
				} catch (IOException e) {
					throw new SerializationException(
		                    "An exception occurred while trying to serialize a Deliverable for storage",
		                    e);
				}
	        }
	        return array;
	}

	@Override
	public void serialize(T instance, OutputStream outputStream) throws IOException {
		final String className = instance.getClass().getName();
		final ObjectOutputStream oos = new ObjectOutputStream(outputStream);
		byte[] array;
		if (null == instance) {
			oos.writeInt(0);
			return;
		}
		try {
			oos.writeInt(className.getBytes().length);
			oos.write(className.getBytes());
			@SuppressWarnings("unchecked")
			final Class<T> clazz = (Class<T>) instance.getClass();
			final Schema<T> schema = adapter.getSchema(clazz);
			final LinkedBuffer buffer = BUFFERS.getUnchecked(Thread.currentThread().getName());
			try {
				array = ProtostuffIOUtil.toByteArray(instance, schema, buffer);
			} finally {
				buffer.clear();
			}
			oos.writeInt(array.length);
			oos.write(array);
		} finally {
			oos.flush();
			oos.close();
		}
	}

    @Override
    public void close() throws IOException {
        BUFFERS.cleanUp();
    }

}
