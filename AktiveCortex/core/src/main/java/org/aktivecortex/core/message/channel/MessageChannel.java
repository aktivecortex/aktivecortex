/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.channel;

import org.aktivecortex.api.message.Message;

/**
 * A basic interface defining common behavior for sending messages
 * <p>"Heavily inspired" by Spring Integration interface with the same name
 * 
 * @author Domenico Maria Giffone <domenico.giffone at gmail,com>
 * @param <T> The channel specialized type.
 */
public interface MessageChannel<T> {

    public boolean send(Message<T> message);

    public boolean subscribe(MessageHandler<T> messagehandler);

    public boolean unsubscribe(MessageHandler<T> messagehandler);
}
