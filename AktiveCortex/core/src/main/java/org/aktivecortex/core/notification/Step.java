/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.util.Entry;
import org.aktivecortex.dbc.assertion.Contract;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

/**
 * Helper class that describe a logical step of a {@link org.aktivecortex.api.notification.Process}
 * <p>It supports {@link ProgressEvaluatorBuilder} to build the {@link org.aktivecortex.api.notification.ProgressEvaluator}
 * for a process at command send time.</p>
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
class Step {

    private String name;

    private String description;

    private double stepWeight;

    private int commandAmount;

    private List<Entry<String, Integer>>  expectedCommands = new ArrayList<Entry<String, Integer>>();

    Step(String name, String description, double stepWeight) {
        this.name = name;
        this.description = description;
        this.stepWeight = stepWeight;
    }

    public double getStepWeight() {
        return stepWeight;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public <T extends Command> boolean contains(Class<T> command) {
        return getCommands().contains(command.getCanonicalName());
    }

    public Set<String> getCommands() {
        Set<String> result = newHashSet();
        for (Entry<String, Integer> command: expectedCommands) {
            result.add(command.getKey()) ;
        }
        return  result;
    }

    public int getExpectedAmount(String commandFQCN) {
        for (Entry<String, Integer> command: expectedCommands) {
            if (command.getKey().equals(commandFQCN)) {
                return command.getValue();
            }
        }
        return 0;
    }

    public <T extends Command> void addCommand(Class<T> expectedType, int expectedAmount) {
        Contract.REQUIRE.isFalse(contains(expectedType),"Command Uniqueness",
                "Command: [{}] already added to this phase: [{}]", expectedType.getCanonicalName(), name);
        expectedCommands.add(new Entry<String, Integer>(expectedType.getCanonicalName(), expectedAmount));
        commandAmount += expectedAmount;
    }

    public double getSingleCommandWeight() {
        return stepWeight / commandAmount;
    }
}
