/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.utils.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class Utils {

	private static final Logger logger = LoggerFactory.getLogger(Utils.class);

	private static final  String hostname;
	private static final  String ipaddress;

	static {
		String host = "UNKOWN_HOST";
		String ip = "127.0.0.1";
		try {
			final InetAddress localHost = InetAddress.getLocalHost();
			host = localHost.getHostName();
			ip = localHost.getHostAddress();
			logger.info("Hostname: [{}] - Ip address: [{}]", host, ip);
		} catch (java.net.UnknownHostException uhe) {
		} finally {
			hostname = host;
			ipaddress = ip;
		}
	}

	public static String getIpAddress() {
		return ipaddress;
	}

	public static String getHostname() {
		return hostname;
	}

	public static List<String> getPath(Context ctx, String path, String leafName) {
        NamingEnumeration<NameClassPair> list;
        List<String> result = new ArrayList<String>();
        try {
            list = ctx.list(path);
            while (list.hasMore()) {
                NameClassPair ncPair = list.next();
                final String currentPath = (path.length() == 0) ? ncPair.getName()
                        : new StringBuilder().append(path).append("/").append(ncPair.getName()).toString();
                if (ncPair.getName().equals(leafName)) {
                    result.add(currentPath);
                } else {
                    result.addAll(getPath(ctx, currentPath, leafName));
                }
            }
        } catch (NamingException ex) {
            //ignore leaf node exception
        }
        return result;
    }


}
