/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.spi.impl;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

import org.aktivecortex.core.message.spi.IMessagingSystemFactory;
import org.aktivecortex.core.message.spi.SendAdapter;
import org.aktivecortex.core.message.spi.IMessagingSystemFactory.ConfigurationKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Inspired by slf4j-api
 * https://github.com/qos-ch/slf4j/blob/master/slf4j-api/src/main/java/org/slf4j/LoggerFactory.java
 * @author d.giffone
 *
 */
public final class MessagingSystemFactory {

	private static final String UNSUCCESSFUL_INIT_MSG = "org.aktivecortex.core.message.spi.impl.MessagingSystemFactory could not be successfully initialized.";
	private static final String STATIC_MESSAGING_SYSTEM_PATH = "org/aktivecortex/core/message/spi/impl/StaticMessagingSystemBinder.class";
	private static final Logger LOGGER = LoggerFactory.getLogger(MessagingSystemFactory.class);
	
    private static final String THREAD_UNKNOWN = "Thread Name Unknown";
	private static final int STATE_UNINITIALIZED = 0;
	private static final int STATE_ONGOING_INITIALIZATION = 1;
	private static final int STATE_FAILED_INITIALIZATION = 2;
	private static final int STATE_SUCCESSFUL_INITIALIZATION = 3;
	private static final int STATE_NOP_FALLBACK_INITIALIZATION = 4;
	private static final IMessagingSystemFactory NOP_FALLBACK_FACTORY = new NOPMessagingSystemFactory();

	private static int currentState = STATE_UNINITIALIZED;
	private static String initializingThreadName = THREAD_UNKNOWN;

    private MessagingSystemFactory(){
        //  Hide Utility Class Constructor
    }

	public static SendAdapter getSendAdapter() {
		return getConcreteFactory().getSendAdapter();
	}
	
	public static Object getProperty(ConfigurationKey key) {
		return getConcreteFactory().getProperty(key);
	}

	public static boolean isPropertyDefined(ConfigurationKey key) {
		return getConcreteFactory().isPropertyDefined(key);
	}


	private static IMessagingSystemFactory getConcreteFactory() {
		if (currentState == STATE_UNINITIALIZED) {
			currentState = STATE_ONGOING_INITIALIZATION;
			initializingThreadName = Thread.currentThread().getName();
			init();
		}
		switch (currentState) {
		case STATE_SUCCESSFUL_INITIALIZATION:
			return StaticMessagingSystemBinder.getInstance().getFactory();
		case STATE_NOP_FALLBACK_INITIALIZATION:
			return NOP_FALLBACK_FACTORY;
		case STATE_FAILED_INITIALIZATION:
				throw new IllegalStateException(UNSUCCESSFUL_INIT_MSG);
		case STATE_ONGOING_INITIALIZATION:
			LOGGER.error("SendAdapterFactory initialization already in progress.");
			LOGGER.error("Initialization started by thread with name: [{}]", initializingThreadName);
			LOGGER.error("Current thread name: [{}]", Thread.currentThread().getName());
			LOGGER.error("Active Cortex initialization must be performed by a single thread.");
			throw new IllegalStateException(UNSUCCESSFUL_INIT_MSG);
		}
		throw new IllegalStateException("Unreachable code");
	}
	
	private static void init() {
		bind();
		
	}

	private static void bind() {
		try {
			Set<URL> staticSendAdapterBinderPathSet = findAvailableBinderPathSet();
			reportBindingAmbiguity(staticSendAdapterBinderPathSet);
			// next line does the actual binding
			StaticMessagingSystemBinder.getInstance();
			currentState = STATE_SUCCESSFUL_INITIALIZATION;
			reportActualBinding(staticSendAdapterBinderPathSet);
		} catch (NoClassDefFoundError ncde) {
			String msg = ncde.getMessage();
			if (messageContainsStaticSendAdapterBinding(msg)) {
				currentState = STATE_NOP_FALLBACK_INITIALIZATION;
				LOGGER.error("Failed to load class \"org.aktivecortex.core.message.spi.impl.StaticMessagingSystemBinder\".");
				LOGGER.error("Defaulting to no-operation (NOP) SendAdapter implementation.");
			} else {
				failedBinding(ncde);
				throw ncde;
			}
		} catch (Exception e) {
			failedBinding(e);
			throw new IllegalStateException("Unexpected initialization failure", e);
		}
	}

	private static void failedBinding(Throwable t) {
		currentState = STATE_FAILED_INITIALIZATION;
		LOGGER.error("Failed to instantiate Aktive Cortex SendAdapterFactory", t);
	}

	private static boolean messageContainsStaticSendAdapterBinding(String msg) {
		if (null == msg) {
            return false;
        }
		if (msg.indexOf("org/aktivecortex/core/message/spi/impl/StaticMessagingSystemBinder") != -1) {
            return true;
        }
		if (msg.indexOf("org.aktivecortex.core.message.spi.impl.StaticMessagingSystemBinder") != -1) {
            return true;
        }
		return false;
	}

	private static void reportActualBinding(Set<URL> staticSendAdapterBinderPathSet) {
		if (staticSendAdapterBinderPathSet.size() > 1) {
			LOGGER.warn("Actual binding is of type [{}]", StaticMessagingSystemBinder.getInstance().getBindingClassAsStr());
		}
	}

	private static void reportBindingAmbiguity(Set<URL> staticSendAdapterBinderPathSet) {
		if (staticSendAdapterBinderPathSet.size() > 1) {
			LOGGER.warn("Class path contains multiple Aktive Cortex MessagingSystem bindings.");
			for (URL path : staticSendAdapterBinderPathSet) {
				LOGGER.warn("Found binding in [{}]", path);
			}
			LOGGER.warn("You must provide a single binding implementation.");
		}
		
	}

	@SuppressWarnings("static-access")
	private static Set<URL> findAvailableBinderPathSet() {
		Set<URL> result = new LinkedHashSet<URL>();
		try {
			ClassLoader sendAdapterFactoryClassLoader = MessagingSystemFactory.class.getClassLoader();
			Enumeration<URL> paths;
			if (sendAdapterFactoryClassLoader == null) {
				paths = ClassLoader.getSystemResources(STATIC_MESSAGING_SYSTEM_PATH);
			} else {
				paths = sendAdapterFactoryClassLoader.getSystemResources(STATIC_MESSAGING_SYSTEM_PATH);
			}
			while (paths.hasMoreElements()) {
				URL path = paths.nextElement();
				result.add(path);
			}
		} catch (IOException e) {
			LOGGER.error("Error getting resource from path", e);
		}
		return result;
	}

}
