/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.store.command;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.aktivecortex.api.command.Command;
import org.joda.time.DateTime;


@Entity
class CommandEntry {

	@SuppressWarnings("unused")
	@Id
	private String id;

	@SuppressWarnings("unused")
	@Basic
	private String type;

	@SuppressWarnings("unused")
	@Basic
	private long timeStamp;

	@SuppressWarnings("unused")
	@Basic
	private String aggregateIdentifier;
	
	@SuppressWarnings("unused")
	@Basic
	private String aggregateType;

	@SuppressWarnings("unused")
	@Basic
	@Lob
	private byte[] serializedCommand;

	protected CommandEntry() {
	}

	/**
	 * @param command
	 * @param serializedCommand
	 */
    @SuppressWarnings("all")
	protected CommandEntry(Command command, byte[] serializedCommand) {
		this.id = command.getCommandId();
		this.type = command.getClass().getSimpleName();
		this.timeStamp = new DateTime().getMillis();
		this.aggregateType = command.getAggregateType();
		this.aggregateIdentifier = command.getAggregateIdentifier();
		this.serializedCommand = serializedCommand;
	}






}
