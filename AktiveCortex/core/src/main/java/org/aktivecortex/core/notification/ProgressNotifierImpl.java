/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.notification.ProgressNotifier;
import org.aktivecortex.core.utils.io.Utils;
import org.slf4j.MDC;

import static org.aktivecortex.api.message.MessageHeadersConstants.SHIPPING_DISPATCHER;
import static org.aktivecortex.api.message.MessageHeadersConstants.SHIPPING_PROCESS_DISPATCHER;


public class ProgressNotifierImpl implements ProgressNotifier {

    private ProgressNotifier localNotifier;
    private ProgressNotifier remoteNotifier;

    public void setLocalNotifier(ProgressNotifier localNotifier) {
        this.localNotifier = localNotifier;
    }

    public void setRemoteNotifier(ProgressNotifier remoteNotifier) {
        this.remoteNotifier = remoteNotifier;
    }

    @Override
    public void taskCompleted(Message<Command> message) {
        getProgressNotifier(SHIPPING_DISPATCHER).taskCompleted(message);
        if (!MDC.get(SHIPPING_DISPATCHER).equals(MDC.get(SHIPPING_PROCESS_DISPATCHER))) {
             getProgressNotifier(SHIPPING_PROCESS_DISPATCHER).taskCompleted(message);
        }
    }

    @Override
    public void processCompleted() {
        getProgressNotifier(SHIPPING_PROCESS_DISPATCHER).processCompleted();
    }

    @Override
    public void taskFailed(Message<Command> message, Throwable t) {
        getProgressNotifier(SHIPPING_DISPATCHER).taskFailed(message, t);
    }

    private ProgressNotifier getProgressNotifier(String destinationKey) {
        return (Utils.getHostname().equals(MDC.get(destinationKey))) ? localNotifier : remoteNotifier;
    }


}
