/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.utils.logging;

import java.io.Serializable;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class LogHelper implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String SEPARATOR = "------------------------------------------";
	private static final String NEW_LINE ="\n";
	private StringBuilder logger;
	private StringBuilder freeze;

	private DateTime initTime;
	private DateTime startTime;
	private DateTime stepTime;

	private boolean verbose;

	public LogHelper(String title) {
		this(title,true);
	}

	public LogHelper(String title,boolean verbose) {
		initTime=new DateTime();
		startTime=new DateTime();
		stepTime=new DateTime();

		logger=new StringBuilder();
		freeze=new StringBuilder();

		this.verbose=verbose;

		if(verbose){
			appendNewLine();
			appendSeparator();
			appendLog(title);
			appendSeparator();
		}else{
			appendLog(title);
		}
	}

	public boolean isVerbose() {
		return verbose;
	}
	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}


	public void appendLog(String message){
		if(null==message || "".equals(message)) {
            return;
        }
		logger.append(message).append("\n");
	}

	public void appendSeparator(){
		appendLog(SEPARATOR);
	}

	public void appendNewLine(){
		appendLog(NEW_LINE);
	}

	public long appendElapsedTime(String message){
		DateTime now=new DateTime();
		Duration elapsed=new Duration(stepTime,now);
		Duration duration=new Duration(startTime,now);

		appendLog("{}: {} [since start: {}] - [{}]", new Object[]{message,printTime(elapsed),printTime(duration),printTime(now)});
		stepTime=new DateTime();
		return duration.getMillis();
	}

	public void reset(String msg){
		DateTime now=new DateTime();
		Duration elapsed=new Duration(startTime,now);
		Duration duration=new Duration(initTime,now);
		String tf=org.slf4j.helpers.MessageFormatter.arrayFormat(
				"{}: {} [since start: {}] - [{}]",
				new Object[]{msg,printTime(elapsed),printTime(duration),printTime(now)}).getMessage();
		freeze.append(tf).append("\n");
		startTime=new DateTime();
		stepTime=new DateTime();
	}

	public void appendLog(String message,Object arg){
		appendLog(org.slf4j.helpers.MessageFormatter.format(message,arg).getMessage());
	}

	public void appendLog(String message,Object arg,Object arg2){
		appendLog(org.slf4j.helpers.MessageFormatter.format(message,arg,arg2).getMessage());
	}

	public void appendLog(String message, Object[] objs){
		appendLog(org.slf4j.helpers.MessageFormatter.arrayFormat(message,objs).getMessage());
	}

	public static String printTime(Duration elapsed){
		PeriodFormatter timeFmt = PeriodFormat.getDefault();
		return timeFmt.print(elapsed.toPeriod());
	}

	public static String printTime(DateTime time){
		DateTimeFormatter timeFmt = DateTimeFormat.forPattern("HH:mm:ss");
		return timeFmt.print(time);
	}

	public String getString() {
		return getString(0);
	}

	public String getString(long limit) {
		DateTime now=new DateTime();
		Duration elapsed=new Duration(initTime,now);
		if (verbose){
			appendSeparator();
		}
		if(limit > 0 && verbose){
			appendLog("[{} millis limit exceeded]",limit);
			appendSeparator();
		}
		if(freeze.length()>0){
			logger.append(freeze.toString());
			appendSeparator();
		}

		if (verbose){
			appendLog("'End' - elapsed time: {} since start", printTime(elapsed));
		}

		return (elapsed.getMillis() >= limit) ? logger.toString() : null;
	}

}
