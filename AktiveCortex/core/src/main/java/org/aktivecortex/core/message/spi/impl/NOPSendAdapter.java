/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.spi.impl;

import org.aktivecortex.core.message.spi.SendAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class NOPSendAdapter implements SendAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(NOPSendAdapter.class);
	
	public static final SendAdapter NOP_ADAPTER = new NOPSendAdapter();
	
	protected NOPSendAdapter(){}

	@Override
	public void afterPropertiesSet() throws Exception {
		logger.warn("a No Operation SendAdapter will be used to send messages. No Messages will ever been exchanged in this way");
		
	}

	@Override
	public boolean send(byte[] message, String messageId, String routingKey) {
		logger.warn("fake send of message - bytes: [{}], messageId:[{}], routingKey: [{}]", 
				new Object[]{message.length, messageId, routingKey});
		return false;
	}

}
