/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message;

import org.aktivecortex.api.command.AbstractCommand;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.dbc.assertion.Contract;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Maps.newHashMap;

public final class DefaultCommandBatch extends AbstractCommand implements CommandBatch {

	private static final long serialVersionUID = 7038249698190175608L;

	private List<Command> commands;

	private String routingKey;

	private String aggregateId;

	private String aggregateType;

   	/**
	 * @param args
	 */
	public DefaultCommandBatch(Command... args) {
		commands = newLinkedList();
		for (Command command : args) {
			initSequence(command);
			commands.add(command);
		}
	}

	private void initSequence(Command command) {
		String commandKey = command.getRoutingKey();
		if (null == routingKey) {
			routingKey = commandKey;
			aggregateId = command.getAggregateIdentifier();
			aggregateType = command.getAggregateType();
		} else {
			Contract.REQUIRE.isTrue(routingKey.equals(commandKey), "All commands in the sequence must share the same aggregate target", 
					"The command [{}] is targeted to a different aggregate [{}] than the aggregate [{}] of the first command in the sequence", 
					new Object[]{command, commandKey, routingKey});
		}
	}

	@Override
    public List<Command> getCommands() {
		return commands;
	}

	
	@Override
	public String getRoutingKey() {
		return routingKey;
	}

	@Override
	public String getCommandId() {
		return commands.toString();
	}


	public String getAggregateIdentifier() {
		return aggregateId;
	}

	public String getAggregateType() {
		return aggregateType;
	}

	@Override
	public Map<String, Object> getSignificantFields() {
		Map<String, Object> result = newHashMap();
		for (Command command : commands) {
			result.putAll(command.getSignificantFields());
		}
		return result;
	}

}
