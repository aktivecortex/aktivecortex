/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.utils.datetime;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.ReadableInstant;
import org.joda.time.chrono.GregorianChronology;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class JodaTimeConverter {

	// JodaTime Formatter are thread safeO
	private static final DateTimeFormatter dateFmt = ISODateTimeFormat.date(); // expected pattern yyyy-MM-dd
	private static final DateTimeFormatter dateTimeFmt = ISODateTimeFormat.dateTimeParser(); // expected yyyy-MM-dd'T'HH:mm:ss.SSSZZ
	private static final DateTimeFormatter timeFmt = ISODateTimeFormat.timeParser(); // expected HH:mm:ss.SSSZZ

	private static final DateTimeFormatter datePrt = ISODateTimeFormat.date(); // expected pattern yyyy-MM-dd
	private static final DateTimeFormatter dateTimePrt = ISODateTimeFormat.dateTime(); // expected yyyy-MM-dd'T'HH:mm:ss.SSSZZ
	private static final DateTimeFormatter timePrt = ISODateTimeFormat.time(); // expected HH:mm:ss.SSSZZ

	  public static DateTime parseDate(String s) {
	       return dateFmt.parseDateTime(s);
	   }

	   public static DateTime parseTime(String s) {
	       return timeFmt.parseDateTime(s);
	   }

	   public static DateTime parseDateTime(String s) {
	       return dateTimeFmt.parseDateTime(s);
	   }

	   public static String printDate(ReadableInstant dt) {
	       return datePrt.print(dt);
	   }

	   public static String printTime(ReadableInstant dt) {
	       return timePrt.print(dt);
	   }

	   public static String printDateTime(ReadableInstant dt) {
	      return dateTimePrt.print(dt);
	   }

	   public static Interval getNewInterval(Object dtStart,Object dtEnd){
		   DateTime start=new DateTime(dtStart).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).withChronology(GregorianChronology.getInstance());
		   DateTime end=new DateTime(dtEnd).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(0).withChronology(GregorianChronology.getInstance());
		   return new Interval(start,end);
	   }

	   public static Interval getNewInterval(long dtStart,long dtEnd){
		   return getNewInterval(new DateTime(dtStart),new DateTime(dtEnd));
	   }

	   public static boolean contains(Interval esterno,Interval interno){

		   Interval es=getNewInterval(esterno.getStart(), esterno.getEnd());
		   Interval in=getNewInterval(interno.getStart(), interno.getEnd());

//		   return es.equals(in) || es.contains(in);
		   return es.equals(in) || es.overlaps(in);
	   }

}
