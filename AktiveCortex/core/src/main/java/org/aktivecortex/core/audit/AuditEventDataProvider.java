/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.audit;

import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageHeaders;
import org.axonframework.auditing.AuditDataProvider;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static org.aktivecortex.api.message.MessageHeadersConstants.SECURITY_CONSTANTS;


/**
 * Inject security metadata to the published event by taking them from the Command being executed.
 */
public class AuditEventDataProvider implements AuditDataProvider {

	@Override
	public Map<String, Serializable> provideAuditDataFor(Object obj) {
		Map<String, Serializable> metadata = new HashMap<String, Serializable>();
		@SuppressWarnings("unchecked")
		MessageHeaders headers = ((Message) obj).getMessageHeaders();
        for (String key : SECURITY_CONSTANTS) {
			metadata.put(key, headers.get(key));

        }
		return metadata;
	}
}
