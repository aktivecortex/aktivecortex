/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.eventbus;

import com.google.common.collect.ImmutableMap;
import org.aktivecortex.core.message.EventMessage;
import org.aktivecortex.core.message.channel.MessageChannel;
import org.aktivecortex.core.message.channel.MessageHandler;
import org.axonframework.domain.Event;
import org.axonframework.eventhandling.Cluster;
import org.axonframework.eventhandling.EventBusTerminal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.google.common.collect.Lists.newArrayList;

public class DistributedEventBusTerminal implements EventBusTerminal, InitializingBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(DistributedEventBusTerminal.class);

	private Map<String, MessageChannel<Event>> channels;

	private List<DistributedCluster> clusters = new CopyOnWriteArrayList<DistributedCluster>();

	private final ConcurrentMap<DistributedCluster, MessageHandler<Event>> handlers = new ConcurrentHashMap<DistributedCluster, MessageHandler<Event>>();

	@Override
	public void onClusterCreated(final Cluster cluster) {
		if (!(cluster instanceof DistributedCluster)) {
			throw new IllegalArgumentException(String.format("Wrong channel type %s. Expected type is JmsCluster", cluster.getClass().getSimpleName()));
		}
		DistributedCluster jmsCluster = (DistributedCluster) cluster;
		MessageHandler<Event> clusterAdapter = new ClusterMessageHandlerAdapter(cluster);
		MessageHandler<Event> oldHandler = handlers.putIfAbsent(jmsCluster, clusterAdapter);
		if (oldHandler == null) {
			clusters.add(jmsCluster);
			LOGGER.info("Registering cluster [{}]", cluster.getClass().getSimpleName());
			MessageChannel<Event> channel = selectChannel(jmsCluster);
			channel.subscribe(clusterAdapter);
		}
	}

	private MessageChannel<Event> selectChannel(DistributedCluster cluster) {
		String destinationName = cluster.getDestinationName();
		MessageChannel<Event> channel = channels.get(destinationName);
		LOGGER.info("Channel found for destination [{}]. Binding cluster [{}] to this channel", destinationName, cluster.getClass().getSimpleName());
		if (null == channel) {
			throw new IllegalArgumentException(String.format("No suitable channel for the destination provided with the cluster: %s", destinationName));
		}
		return channel;
	}

	@Override
	public void publish(Event event) {
		List<String> destinations = selectDestinations(event);
		for (String destinationName : destinations) {
			MessageChannel<Event> channel = channels.get(destinationName);
			if (null == channel) {
				throw new IllegalArgumentException(String.format("No suitable channel for this destination: %s. Please verify this configuration", destinationName));
			}
			channel.send(new EventMessage(event));
		}
	}

	private List<String> selectDestinations(Event event) {
		List<String> result = newArrayList();
		for (DistributedCluster cluster : clusters) {
			if (cluster.isEventHandled(event)) {
				result.add(cluster.getDestinationName());
			}
		}
		if (result.isEmpty()) {
			final String message = String.format("Publishing skipped. No suitable cluster found for this kind of event: %s.", event.getClass());
			LOGGER.debug(message);
		} 
		return result;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(channels, "property channels not set");
		Assert.notEmpty(channels, "channels can't be empty");
	}

	public void setChannels(Map<String, MessageChannel<Event>> channels) {
		this.channels = ImmutableMap.copyOf(channels);
	}

}
