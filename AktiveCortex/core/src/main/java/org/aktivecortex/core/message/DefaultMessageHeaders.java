/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message;

import org.aktivecortex.api.message.MessageHeaders;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static org.aktivecortex.api.message.MessageHeadersConstants.DELIVERY_COUNT;

public class DefaultMessageHeaders implements MessageHeaders {

	private static final long serialVersionUID = 6364748679487434079L;
    private static final String IDENTIFIER_KEY = "_identifier";
    private static final String TIMESTAMP_KEY = "_timestamp";

    private final Map<String, Serializable> values = new HashMap<String, Serializable>();

    /**
     * Create a meta-data instance with the given <code>timestamp</code> and <code>eventIdentifier</code> as initial
     * values.
     *
     */
    DefaultMessageHeaders() {
        values.put(IDENTIFIER_KEY, UUID.fromString(new com.eaio.uuid.UUID().toString()));
        values.put(TIMESTAMP_KEY, new DateTime());
    }
    
    DefaultMessageHeaders(MessageHeaders headers) {
		for (String key : headers.keySet()) {
			values.put(key, headers.get(key));
		}
	}

	@Override
    public DateTime getTimestamp() {
    	return (DateTime) values.get(TIMESTAMP_KEY);
    }
    
    @Override
    public String getMessageIdentifier() {
    	return (String) values.get(IDENTIFIER_KEY);
    }
    
    @Override
	public int getDeliveryCount() {
		Integer count =  (Integer) values.get(DELIVERY_COUNT);
		if (null == count){
			count = Integer.valueOf(0);
			values.put(DELIVERY_COUNT, count);
		}
		return count;
	}

	@Override
	public boolean isDelivered() {
		return getDeliveryCount() > 0;
	}

	@Override
	public void setDelivered() {
		values.put(DELIVERY_COUNT, getDeliveryCount() + 1);
	}

    /**
     * Put a key-value pair in the meta data.
     *
     * @param key   The key for which to insert a value
     * @param value The value to insert
     */
    public void put(String key, Serializable value) {
        values.put(key, value);
    }

    @Override
    public Serializable get(String key) {
        return values.get(key);
    }

    @Override
    public boolean containsKey(String key) {
        return values.containsKey(key);
    }

    @Override
    public Set<String> keySet() {
        return values.keySet();
    }
    
    
    @Override
	public void putAll(Map<? extends String, ? extends Serializable> arg0) {
		values.putAll(arg0);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Set<String> keys = keySet();
		boolean previousLine = false;
		sb.append("[");
		for (String key : keys) {
			if (previousLine) {
				sb.append(", ");
			}
			sb.append("'")
			.append(key)
			.append("':'")
			.append(get(key))
			.append("'");
			previousLine=true;
		}
		return sb.append("]").toString();
	}

}
