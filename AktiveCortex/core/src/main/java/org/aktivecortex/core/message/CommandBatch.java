/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message;

import org.aktivecortex.api.command.Command;

import java.util.List;

/**
 * interface that represents batches of commands sent to the same aggregate.
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
public interface CommandBatch extends Command {

    /**
     * Returns the list of commands included in this batch
     * @return the list of commands
     */
    List<Command> getCommands();
}
