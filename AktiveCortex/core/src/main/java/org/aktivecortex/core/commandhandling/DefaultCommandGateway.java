/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.commandhandling;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.commandhandling.AsyncCommandBus;
import org.aktivecortex.api.commandhandling.CommandGateway;
import org.aktivecortex.api.notification.ProgressEvaluator;
import org.aktivecortex.api.notification.ConversationManager;
import org.aktivecortex.api.notification.Process;
import org.aktivecortex.api.notification.ProcessQueryService;
import org.aktivecortex.api.notification.Task;
import org.aktivecortex.core.notification.ProcessImpl;
import org.aktivecortex.core.utils.io.Utils;
import org.slf4j.MDC;

import java.util.Arrays;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static org.aktivecortex.api.message.MessageHeadersConstants.*;

/**
 * Convenient class to support command dispatching on the {@link AsyncCommandBus} for clients.
 * <p>Class duties is to dispatch commands and associate a {@link Process} description
 * to return to the client.</p>
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
public class DefaultCommandGateway implements CommandGateway {

    private AsyncCommandBus commandBus;
    private ConversationManager conversationManager;
    private ProcessQueryService processQueryService;

    public DefaultCommandGateway(AsyncCommandBus commandBus,
                                 ConversationManager conversationManager,
                                 ProcessQueryService queryService) {
        this.commandBus = commandBus;
        this.conversationManager = conversationManager;
        this.processQueryService = queryService;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Process send(ProgressEvaluator progressEvaluator, Command... commands) {
        Task t = conversationManager.createTask(getPayloadId(commands), progressEvaluator);
        commandBus.dispatchAsync(getNotificationContextMap(t), commands);
        ProcessImpl p = (ProcessImpl) processQueryService.findProcess(t.getProcessId());
        return p;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Process send(Command... commands) {
        return send(null, commands);
    }


    private String getPayloadId(Command[] commands) {
        return commands.length == 1 ? commands[0].toString() : Arrays.toString(commands);
    }

    private Map<String, String> getNotificationContextMap(Task t) {
        Map<String, String> contextMap = newHashMap();
        if (null == MDC.get(SHIPPING_PROCESS_DISPATCHER)) {
            contextMap.put(SHIPPING_PROCESS_DISPATCHER, Utils.getHostname());
        }
        contextMap.put(NOTIFICATION_TASK_ID, t.getId());
        contextMap.put(NOTIFICATION_PROCESS_ID, t.getProcessId());
        return contextMap;
    }

}
