/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.serializer.saga;

import org.aktivecortex.core.axon2backport.saga.Saga;
import org.aktivecortex.core.axon2backport.saga.repository.SagaSerializer;
import org.aktivecortex.core.serializer.ProtostuffSerializer;

import java.io.Closeable;
import java.io.IOException;

public class ProtostuffSagaSerializer implements SagaSerializer, Closeable {

	@SuppressWarnings("rawtypes")
	private final ProtostuffSerializer delegate = new ProtostuffSerializer();

	@Override
	public Saga deserialize(byte[] serializedSaga) {
		return (Saga) delegate.deserialize(serializedSaga);
	}

	@Override
	public Object deserializeAssociationValue(byte[] serializedAssociationValue) {
		return delegate.deserialize(serializedAssociationValue);
	}

	@SuppressWarnings("unchecked")
	@Override
	public byte[] serialize(Saga saga) {
		return delegate.serialize(saga);
	}

	@SuppressWarnings("unchecked")
	@Override
	public byte[] serializeAssociationValue(Object associationValue) {
		return delegate.serialize(associationValue);
	}

    @Override
    public void close() throws IOException {
        delegate.close();
    }



}
