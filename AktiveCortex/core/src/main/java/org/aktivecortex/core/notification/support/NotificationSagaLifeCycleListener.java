/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification.support;

import org.aktivecortex.api.message.MessageHeadersConstants;
import org.aktivecortex.api.notification.ProgressNotifier;
import org.aktivecortex.core.axon2backport.saga.Saga;
import org.aktivecortex.core.axon2backport.saga.SagaLifeCycleListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.util.Assert;

/**
 * This life cycle listener is meant to notify process completion at saga deletion moment.
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */

public class NotificationSagaLifeCycleListener implements SagaLifeCycleListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationSagaLifeCycleListener.class);

    private ProgressNotifier progressNotifier;

    public NotificationSagaLifeCycleListener(ProgressNotifier progressNotifier) {
        Assert.notNull(progressNotifier, "ProgressNotifier can't be null!");
        this.progressNotifier = progressNotifier;
    }

    @Override
    public void onCreate(Saga saga) {
    }

    @Override
    public void onUpdate(Saga saga) {
    }

    @Override
    public void onDelete(Saga saga) {
        progressNotifier.processCompleted();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Notified process {} completion!", MDC.get(MessageHeadersConstants.NOTIFICATION_PROCESS_ID));
        }
    }
}
