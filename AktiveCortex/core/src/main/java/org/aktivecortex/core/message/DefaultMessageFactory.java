/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageFactory;
import org.aktivecortex.api.notification.ConversationManager;

public class DefaultMessageFactory implements MessageFactory<Command> {
	
	private final ConversationManager conversationManager;

	public DefaultMessageFactory(ConversationManager manager) {
		this.conversationManager = manager;
	}

	@Override
	public Message<Command> createMessage(Command payload) {
        return new CommandMessage(payload);
	}

    @Override
	public Message<Command> createBatch(Command... payloads) {
        DefaultCommandBatch payload = new DefaultCommandBatch(payloads);
        DefaultCommandBatchMessage message = new DefaultCommandBatchMessage(payload);
        return message;
	}

}
