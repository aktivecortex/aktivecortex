/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification.support;

import org.aktivecortex.api.audit.AuditLogger;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.axonframework.domain.AggregateRoot;
import org.axonframework.domain.Event;
import org.axonframework.unitofwork.UnitOfWorkListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class UnitOfWorkProgressListener implements UnitOfWorkListener {

    private final AuditLogger auditLogger;
    private final Message<Command> command;
    private final List<Event> recordedEvents = new ArrayList<Event>();

    /**
     * @param auditLogger
     * @param command
     */
    public UnitOfWorkProgressListener(Message<Command> command, AuditLogger auditLogger) {
        this.auditLogger = auditLogger;
        this.command = command;
    }

    @Override
    public void afterCommit() {
        auditLogger.logSuccessful(command, recordedEvents);
    }

    @Override
    public void onCleanup() {
        // Nothing to do right now
    }

    @Override
    public void onPrepareCommit(Set<AggregateRoot> aggregateRoots, List<Event> events) {
        recordedEvents.addAll(events);
    }

    @Override
    public void onRollback(Throwable failureCause) {
        auditLogger.logFailed(command, failureCause, recordedEvents);
    }
}
