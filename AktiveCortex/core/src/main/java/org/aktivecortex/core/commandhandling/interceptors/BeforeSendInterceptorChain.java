/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.commandhandling.interceptors;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;

/**
 * An chain of responsibility  of {@link AfterReceiveMessageHandlerInterceptor} specialized
 * on the before send phase command post processing.
 * 
 * @author Domenico Maria Giffone <domenico.giffone at gmail,com>
 */
public interface BeforeSendInterceptorChain {
	
    Object proceed();

    Object proceed(Message<Command> message);

}
