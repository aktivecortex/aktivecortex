/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.spi.impl;

import org.aktivecortex.core.message.spi.IMessagingSystemFactory;


/**
 * Dummy implementation
 * 
 * @author d.giffone
 *
 */
public final class StaticMessagingSystemBinder {
	
	private static final StaticMessagingSystemBinder instance = new StaticMessagingSystemBinder();

	public static final StaticMessagingSystemBinder getInstance() {
		return instance;
	}

	private StaticMessagingSystemBinder() {
		throw new UnsupportedOperationException("This code should have never been executed");
	}

	public IMessagingSystemFactory getFactory() {
		throw new UnsupportedOperationException("This code should have never been executed");
	}

	public String getBindingClassAsStr() {
		throw new UnsupportedOperationException("This code should have never been executed");
	}

}
