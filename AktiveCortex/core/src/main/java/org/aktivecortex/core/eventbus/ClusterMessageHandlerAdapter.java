/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.eventbus;

import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessagingException;
import org.aktivecortex.core.message.channel.MessageHandler;
import org.axonframework.domain.Event;
import org.axonframework.eventhandling.Cluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ClusterMessageHandlerAdapter implements MessageHandler<Event> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClusterMessageHandlerAdapter.class);

    private final Cluster cluster;

    public ClusterMessageHandlerAdapter(Cluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public void handleMessage(Message<Event> message) throws MessagingException {
        Event event = message.getPayload();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("[{}] event type received on cluster [{}]", event.getClass().getName(), cluster.getClass().getSimpleName());
        }
        cluster.publish(event);
    }
}
