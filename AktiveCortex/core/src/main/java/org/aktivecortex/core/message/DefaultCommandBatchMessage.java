/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Message envelope for a batch of commands
 */

public class DefaultCommandBatchMessage extends CommandMessage implements CommandBatchMessage {

    public DefaultCommandBatchMessage(DefaultCommandBatch payload) {
        super(payload);
    }

    @Override
    public  List<Message<Command>> getNestedMessages() {
        @SuppressWarnings("unchecked")
        List<Command> commands = ((CommandBatch) getPayload()).getCommands();
        List<Message<Command>> result = newArrayList();
        for (Command command : commands) {
            result.add(new CommandMessage(command, getMessageHeaders()));
        }
        return result;
    }

}
