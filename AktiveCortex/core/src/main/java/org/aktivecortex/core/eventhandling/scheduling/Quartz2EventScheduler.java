/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.eventhandling.scheduling;

import org.axonframework.domain.ApplicationEvent;
import org.axonframework.eventhandling.scheduling.quartz.Quartz2FireEventJob;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;

/**
 * EventScheduler implementation that delegates scheduling and triggering to a Quartz Scheduler
 * with support for MDC context propagation.
 *
 *
 * @author Domenico Maria Giffone.
 * @since 1.3
 */
public class Quartz2EventScheduler extends org.axonframework.eventhandling.scheduling.quartz.Quartz2EventScheduler {
    @Override
    protected JobDetail buildJobDetail(ApplicationEvent event, Object source, JobKey jobKey) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(Quartz2FireEventJob.EVENT_KEY, event);
        return JobBuilder.newJob(Quartz2FireEventJobWithMDC.class)
                .withDescription(String.format("%s, scheduled by %s.",
                        event.getClass().getName(),
                        source.toString()))
                .withIdentity(jobKey)
                .usingJobData(jobDataMap)
                .build();
    }
}
