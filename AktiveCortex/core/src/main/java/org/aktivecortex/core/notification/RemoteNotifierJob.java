/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class RemoteNotifierJob extends QuartzJobBean {
	
	private ProgressRemoteNotifierImpl actualWorker;
	
	public void setActualWorker(ProgressRemoteNotifierImpl actualWorker) {
		this.actualWorker = actualWorker;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		actualWorker.publish();
	}

}
