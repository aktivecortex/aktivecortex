/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.commandhandling.interceptors.afterreceive;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.core.audit.AuditingUnitOfWorkListener;
import org.aktivecortex.core.commandhandling.interceptors.AfterReceiveInterceptorChain;
import org.aktivecortex.core.commandhandling.interceptors.AfterReceiveMessageHandlerInterceptor;
import org.axonframework.auditing.AuditDataProvider;
import org.axonframework.auditing.AuditLogger;
import org.axonframework.auditing.EmptyDataProvider;
import org.axonframework.auditing.NullAuditLogger;
import org.axonframework.unitofwork.UnitOfWork;

public class AuditingInterceptor implements AfterReceiveMessageHandlerInterceptor<Command> {

	private AuditDataProvider auditDataProvider = EmptyDataProvider.INSTANCE;
	private AuditLogger auditLogger = NullAuditLogger.INSTANCE;

	@Override
	public Object handle(Message<Command> message, UnitOfWork unitOfWork, AfterReceiveInterceptorChain chain) {
		AuditingUnitOfWorkListener auditListener = new AuditingUnitOfWorkListener(message, auditDataProvider,
				auditLogger);
		unitOfWork.registerListener(auditListener);
		Object returnValue = chain.proceed();
		auditListener.setReturnValue(returnValue);
		return returnValue;
	}
	
	/**
     * Sets the AuditingDataProvider for this interceptor. Defaults to the {@link EmptyDataProvider}, which provides no
     * correlation information.
     *
     * @param auditDataProvider the instance providing the auditing information to attach to the events.
     */
    public void setAuditDataProvider(AuditDataProvider auditDataProvider) {
        this.auditDataProvider = auditDataProvider;
    }

    /**
     * Sets the logger that writes events to the audit logs. Defaults to the {@link
     * org.axonframework.auditing.NullAuditLogger}, which does nothing.
     *
     * @param auditLogger the logger that writes events to the audit logs
     */
    public void setAuditLogger(AuditLogger auditLogger) {
        this.auditLogger = auditLogger;
    }

}
