/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.commandhandling.interceptors.beforesend;

import org.aktivecortex.api.audit.SecurityContext;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.core.commandhandling.interceptors.BeforeSendInterceptorChain;
import org.aktivecortex.core.commandhandling.interceptors.BeforeSendMessageHandlerInterceptor;

public class AuditCommandDataProvider implements BeforeSendMessageHandlerInterceptor<Command> {
	
	private SecurityContext securityContext;

	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}

	public AuditCommandDataProvider() {
		super();
	}

	public AuditCommandDataProvider(SecurityContext securityContext) {
		super();
		this.securityContext = securityContext;
	}

	@Override
	public Object handle(Message<Command> message, BeforeSendInterceptorChain chain) {
		if (!(null == securityContext || null == securityContext.getContextAttributes())) {
			message.getMessageHeaders().putAll(securityContext.getContextAttributes());
		}
		return chain.proceed();
	}


}
