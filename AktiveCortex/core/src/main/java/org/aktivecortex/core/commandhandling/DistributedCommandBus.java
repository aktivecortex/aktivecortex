/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.commandhandling;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.commandhandling.AsyncCommandBus;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageFactory;
import org.aktivecortex.core.commandhandling.interceptors.*;
import org.aktivecortex.core.commandhandling.interceptors.afterreceive.TaskProgressInterceptor;
import org.aktivecortex.core.message.CommandBatch;
import org.aktivecortex.core.message.CommandBatchMessage;
import org.aktivecortex.core.message.DefaultCommandBatch;
import org.aktivecortex.core.message.channel.MessageChannel;
import org.aktivecortex.core.message.channel.MessageHandler;
import org.axonframework.commandhandling.*;
import org.axonframework.unitofwork.DefaultUnitOfWorkFactory;
import org.axonframework.unitofwork.UnitOfWork;
import org.axonframework.unitofwork.UnitOfWorkFactory;
import org.axonframework.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.lang.String.format;

/**
 *  An {@link AsyncCommandBus} implementation that delegates asynchronous handling 
 *  to an underlying JMS backed {@link MessageChannel} implementation.
 *  
 *  @see MessageChannel
 *  @see CommandBus
 *  @see MessageHandler
 * 
 * @author Domenico Maria Giffone <domenico.giffone at gmail,com>
 */
public class DistributedCommandBus implements AsyncCommandBus, CommandBus, MessageHandler<Command>, InitializingBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(DistributedCommandBus.class);

	private volatile Iterable<? extends AfterReceiveMessageHandlerInterceptor<Command>> afterReceiveInterceptors = Collections.emptyList();
	private volatile Iterable<? extends AfterReceiveMessageHandlerInterceptor<Command>> strippedAfterReceiveInterceptors = Collections.emptyList();

	private volatile Iterable<? extends BeforeSendMessageHandlerInterceptor<Command>> beforeSendInterceptors = Collections.emptyList();

	private MessageChannel<Command> channel;

	public void setChannel(MessageChannel<Command> channel) {
		this.channel = channel;
	}
	
	private MessageFactory<Command> messageFactory;
	
	public void setMessageFactory(MessageFactory<Command> messageFactory) {
		this.messageFactory = messageFactory;
	}


	/**
	 * Registers the given list of interceptors to the command bus. All incoming
	 * commands will pass through the interceptors at the given order before the
	 * command is passed to the handler for processing. After handling, the
	 * <code>afterCommandHandling</code> methods are invoked on the interceptors
	 * in the reverse order.
	 * 
	 * @param interceptors
	 *            The interceptors to invoke when commands are dispatched
	 */
	public void setAfterReceiveInterceptors(List<? extends AfterReceiveMessageHandlerInterceptor<Command>> interceptors) {
		this.afterReceiveInterceptors = interceptors;
        Predicate<AfterReceiveMessageHandlerInterceptor<Command>> notPrgInt = Predicates.not(new Predicate<AfterReceiveMessageHandlerInterceptor<Command>>() {
            @Override
            public boolean apply(AfterReceiveMessageHandlerInterceptor<Command> input) {
                return input.getClass().isAssignableFrom(TaskProgressInterceptor.class);
            }
        });
        this.strippedAfterReceiveInterceptors = Iterables.filter(interceptors, notPrgInt);
    }
	
	public void setBeforeSendInterceptors(Iterable<? extends BeforeSendMessageHandlerInterceptor<Command>> beforeSendInterceptors) {
		this.beforeSendInterceptors = beforeSendInterceptors;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(channel, "channel property not set");
		Assert.notNull(messageFactory, "messageFactory property not set");
		channel.subscribe(this);
	}

	// AXON Command Bus override

	// ----

	private final ConcurrentMap<Class<?>, CommandHandler<?>> subscriptions = new ConcurrentHashMap<Class<?>, CommandHandler<?>>();
	private UnitOfWorkFactory unitOfWorkFactory = new DefaultUnitOfWorkFactory();
	private RollbackConfiguration rollbackConfiguration = new RollbackOnAllExceptionsConfiguration();
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void dispatch(Object command) {
		Message<Command> message = (Message<Command>) command;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("processing command {}", message.getPayload());
        }
		inProcDispatch(message, afterReceiveInterceptors);
        if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("processed");
        }
	}

	@SuppressWarnings("unchecked")
	@Override
	public <R> void dispatch(Object command, CommandCallback<R> callback) {
		inProcDispatch((Message<Command>) command, callback, afterReceiveInterceptors);
	}


	protected void inProcDispatch(Message<Command> command, Iterable<? extends AfterReceiveMessageHandlerInterceptor<Command>> interceptors) {
		CommandHandler commandHandler = findCommandHandlerFor(command.getPayload());
		try {
			doInProcDispatch(command, commandHandler, interceptors);
		}  catch (Exception ex) {
			LOGGER.error(format("Processing of a [%s] resulted in an exception: ", command.getPayload().getClass().getSimpleName()),
                    ex);
			throw new RuntimeException(ex);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected <R> void inProcDispatch(Message<Command> command, final CommandCallback<R> callback,
                                      Iterable<? extends AfterReceiveMessageHandlerInterceptor<Command>> interceptors) {
		CommandHandler handler = findCommandHandlerFor(command.getPayload());
		try {
			Object result = doInProcDispatch(command, handler, interceptors);
			callback.onSuccess((R) result);
		} catch (Exception ex) {
			callback.onFailure(ex);
			throw new RuntimeException(ex);
		}
	}

	@SuppressWarnings("rawtypes")
	private CommandHandler findCommandHandlerFor(Command command) {
		final CommandHandler handler = subscriptions.get(command.getClass());
		if (handler == null) {
			throw new NoHandlerForCommandException(format("No handler was subscribed to commands of type [%s]", command
					.getClass().getSimpleName()));
		}
		return handler;
	}

	private Object doInProcDispatch(Message<Command> command, @SuppressWarnings("rawtypes") CommandHandler commandHandler,
                                    Iterable<? extends AfterReceiveMessageHandlerInterceptor<Command>> interceptors) throws Exception {
		UnitOfWork unitOfWork = unitOfWorkFactory.createUnitOfWork();
		AfterReceiveInterceptorChain chain = new DefaultAfterReceiveInterceptorChain(command, unitOfWork, commandHandler, interceptors);

		Object returnValue;
		try {
			returnValue = chain.proceed();
		} catch (Exception ex) {
			if (rollbackConfiguration.rollBackOn(ex)) {
				unitOfWork.rollback(ex);
			} else {
				unitOfWork.commit();
			}
			throw ex;
		}
		unitOfWork.commit();
		return returnValue;
	}

	/**
	 * Subscribe the given <code>handler</code> to commands of type
	 * <code>commandType</code>. If a subscription already exists for the given
	 * type, then the new handler takes over the subscription.
	 * 
	 * @param commandType
	 *            The type of command to subscribe the handler to
	 * @param handler
	 *            The handler instance that handles the given type of command
	 * @param <T>
	 *            The Type of command
	 */
	@Override
	public <T> void subscribe(Class<T> commandType, CommandHandler<? super T> handler) {
		subscriptions.put(commandType, handler);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> void unsubscribe(Class<T> commandType, CommandHandler<? super T> handler) {
		subscriptions.remove(commandType, handler);
	}

	/**
	 * Convenience method that allows you to register command handlers using a
	 * Dependency Injection framework. The parameter of this method is a
	 * <code>Map&lt;Class&lt;T&gt;, CommandHandler&lt;? super T&gt;&gt;</code>.
	 * The key represents the type of command to register the handler for, the
	 * value is the actual handler.
	 * 
	 * @param handlers
	 *            The handlers to subscribe in the form of a Map of Class -
	 *            CommandHandler entries.
	 */
	@SuppressWarnings({ "unchecked" })
	public void setSubscriptions(Map<?, ?> handlers) {
		for (Map.Entry<?, ?> entry : handlers.entrySet()) {
			subscribe((Class<?>) entry.getKey(), (CommandHandler) entry.getValue());
		}
	}

	/**
	 * Sets the UnitOfWorkFactory that provides the UnitOfWork instances for
	 * handling incoming commands. Defaults to a
	 * {@link DefaultUnitOfWorkFactory}.
	 * 
	 * @param unitOfWorkFactory
	 *            The UnitOfWorkFactory providing UoW instances for this Command
	 *            Bus.
	 */
	public void setUnitOfWorkFactory(UnitOfWorkFactory unitOfWorkFactory) {
		this.unitOfWorkFactory = unitOfWorkFactory;
	}

	/**
	 * Sets the RollbackConfiguration that allows you to change when the
	 * UnitOfWork is committed. If not set the
	 * RollbackOnAllExceptionsConfiguration will be used, which triggers a
	 * rollback on all exceptions.
	 * 
	 * @param rollbackConfiguration
	 *            The RollbackConfiguration.
	 */
	public void setRollbackConfiguration(RollbackConfiguration rollbackConfiguration) {
		this.rollbackConfiguration = rollbackConfiguration;
	}

	// ----

	// AXON Command Bus override
	
	@Override
	@Transactional
	public void dispatchAsync(Command... commands) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("sending command(s): {}", Arrays.asList(commands));
        }
		BeforeSendInterceptorChain chain = new DefaultBeforeSendInterceptorChain(beforeSendInterceptors, this.channel, messageFactory, null, commands);
        chain.proceed();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("command(s) sent");
        }
	}

    @Override
    @Transactional
	public void dispatchAsync(Map<String, String> additionalHeaders, Command... commands) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("sending command(s): {}", Arrays.asList(commands));
        }
		BeforeSendInterceptorChain chain = new DefaultBeforeSendInterceptorChain(beforeSendInterceptors, this.channel, messageFactory, additionalHeaders, commands);
        chain.proceed();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("command(s) sent");
        }
	}

    @Override
    public void handleMessage(Message<Command> message) {
        if (message.getPayload() != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("received command: {}", message.getPayload());
            }
            routePayload(message);
        } else {
            LOGGER.warn("Wrong type received: " + message.getClass().getName());
            throw new IllegalArgumentException(String.format(
                    "The payload of incoming messages must be of type %s or type %s", Command.class.getName(),
                    DefaultCommandBatch.class.getName()));
        }
    }

    protected void routePayload(Message<Command> message) {
        if (message.getPayload() instanceof CommandBatch) {
            // wrapping the batch with an external Unit of Work
            UnitOfWork wrappingUoW = unitOfWorkFactory.createUnitOfWork();
            try {
                final List<Message<Command>> nestedMessages = ((CommandBatchMessage) message).getNestedMessages();
                final int size = nestedMessages.size();
                final int i1 = size - 1;
                for (int i = 0; i < size; i++) {
                    Message<Command> nestedMessage = nestedMessages.get(i);
                    if (i == i1){
                     inProcDispatch(nestedMessage, afterReceiveInterceptors);
                    } else {
                     inProcDispatch(nestedMessage, strippedAfterReceiveInterceptors);
                    }

                }
            } catch (Exception ex) {
                if (rollbackConfiguration.rollBackOn(ex)) {
                    wrappingUoW.rollback(ex);
                } else {
                    wrappingUoW.commit();
                }
                throw new RuntimeException(ex);
            }
            wrappingUoW.commit();

        } else {
            dispatch(message);
        }
    }

}
