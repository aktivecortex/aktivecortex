/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.audit;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.core.store.command.CommandStore;
import org.axonframework.commandhandling.CommandHandlerInterceptor;
import org.axonframework.commandhandling.InterceptorChain;
import org.axonframework.unitofwork.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

@Deprecated
public class CommandStoreInterceptor implements CommandHandlerInterceptor {

	private final Logger logger = LoggerFactory.getLogger(CommandStoreInterceptor.class);

	private CommandStore commandStore;

	public void setCommandStore(CommandStore commandStore) {
		this.commandStore = commandStore;
	}

	@Override
	public Object handle(Object command, UnitOfWork unitOfWork, InterceptorChain chain) throws Throwable {             //NOSONAR
		logger.debug("Incoming command: [{}]", command);
        try {
        	commandStore.addCommand((Command) command);
        	return chain.proceed();
        } catch (Exception e) {
            logger.warn(format(" execution of command [%s] failed due to exception [%s]", command, e));
            throw e;
        }
	}

}
