/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.spi;



public interface IMessagingSystemFactory {
	
	
	enum ConfigurationKey {
		/**
		 * the vendor specific property of the messaging subsystem 
		 * that defines ordered message delivery.
		 * 
		 * i.e 	Weblogic Unit-of-Order (http://docs.oracle.com/cd/E12840_01/wls/docs103/jms/uoo.html)
		 * 		ActiveMQ Message Groups (http://activemq.apache.org/message-groups.html) 
		 * 		
		 */
		ORDERED_DELIVERY_PROPERTY  
	}

	SendAdapter getSendAdapter();
	
	Object getProperty(ConfigurationKey key);
	
	boolean isPropertyDefined(ConfigurationKey key);

}
