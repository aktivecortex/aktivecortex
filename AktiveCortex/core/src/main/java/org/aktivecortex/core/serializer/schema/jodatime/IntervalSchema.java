/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.serializer.schema.jodatime;

import java.io.IOException;
import java.lang.reflect.Field;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.base.BaseInterval;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.runtime.RuntimeSchema;

public class IntervalSchema implements Schema<Interval> {

	private static final String IEND_MILLIS = "iEndMillis";
	private static final String ISTART_MILLIS = "iStartMillis";
	public static final IntervalSchema INTERVAL_SCHEMA = new IntervalSchema();

	static {
		RuntimeSchema.register(Interval.class, INTERVAL_SCHEMA);
	}

	@Override
	public String getFieldName(int number) {
		switch (number) {
		case 1:
			return IntervalSchema.ISTART_MILLIS;
		case 2:
			return IntervalSchema.IEND_MILLIS;
		default:
			return null;
		}
	}

	@Override
	public int getFieldNumber(String name) {
		if (IntervalSchema.ISTART_MILLIS.equals(name)) {
            return 1;
        }
		if (IntervalSchema.IEND_MILLIS.equals(name)) {
            return 2;
        }
		return 0;
	}

	@Override
	public boolean isInitialized(Interval interval) {
		return null != interval && interval.getEndMillis() >= interval.getStartMillis();
	}

	@Override
	public String messageFullName() {
		return Interval.class.getName();
	}

	@Override
	public String messageName() {
		return Interval.class.getSimpleName();
	}

	@Override
	public Interval newMessage() {
		final DateTime dateTime = new DateTime();
		return new Interval(dateTime, dateTime);
	}

	@Override
	public Class<? super Interval> typeClass() {
		return Interval.class;
	}

	@Override
	public void mergeFrom(Input input, Interval interval) throws IOException {
		try {
			int number = input.readFieldNumber(this);
			String iStartValue = "";
			String iEndValue = "";
			while (number > 0) {
				if (number == 1) {
					iStartValue = input.readString();
				} else if (number == 2) {
					iEndValue = input.readString();
				}
				number = input.readFieldNumber(this);
			}
			Field fieldS = BaseInterval.class.getDeclaredField(ISTART_MILLIS);
			Field fieldE = BaseInterval.class.getDeclaredField(IEND_MILLIS);
			fieldS.setAccessible(true);
			fieldE.setAccessible(true);
			fieldS.set(interval, new DateTime(iStartValue).getMillis());
			fieldE.set(interval, new DateTime(iEndValue).getMillis());
		} catch (Exception ex) {
			throw new IOException(ex);
		}

	}

	@Override
	public void writeTo(Output output, Interval interval) throws IOException {
		output.writeString(1, interval.getStart().toString(), false);
		output.writeString(2, interval.getEnd().toString(), false);
	}

}
