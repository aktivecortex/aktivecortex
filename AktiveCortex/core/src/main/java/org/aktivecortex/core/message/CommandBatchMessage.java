/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;

import java.util.List;

/**
 * The message-oriented, companion interface, of the {@link CommandBatch} interface.
 * <p>Represents a message with a payload consisting of multiple commands,
 * all targeted to the same aggregate.</p>
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
public interface CommandBatchMessage extends Message<Command> {

    /**
     * Helper method that unrolls the batch of commands contained in the message payload
     * as many new messages each containing a single command.
     *
     * @return the list of messages each containing a command of the batch.
     */
    List<Message<Command>> getNestedMessages();
}
