/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.dao.support;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.aktivecortex.api.dao.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.StringUtils;

public class PagingHelper<T> {

	private static final String QUERY_WITH_PRE = "with Q1 as (";
    private static final String QUERY_WITH_POST = "), Q2 as (select count(1) TOT_ROWS from Q1) select q2.TOT_ROWS, q1.* from q1, q2";
    private static final String NEWLINE = "\n";

    private static final Logger log = LoggerFactory.getLogger(PagingHelper.class);

	public Page<T> fetchPage(final SimpleJdbcCall jc,
			final SqlParameterSource paramSource,
			final String outParamName,
			final int startIndex,
			final int pageSize) {

		// execute the call
		final Map<String, Object> m = jc.execute(paramSource);

		// retrieve results
		@SuppressWarnings("unchecked")
		final List<T> results = (List<T>) m.get(outParamName);

		return fetchPage(results, startIndex, pageSize);
	}

	public Page<T> fetchPage(final List<T> totalItems, final int startIndex, final int pageSize) {
		// determine how many rows are available
		final int rowCount = totalItems.size();

		// create the page object
		final Page<T> page = new Page<T>(rowCount);

		final List<T> pageItems = page.getPageItems();

		int currentRow = 0;
		Iterator<T> it = totalItems.iterator();

		// fetch a single page of results
		while (it.hasNext() && currentRow < startIndex + pageSize) {
			final T item = it.next();
			if (currentRow >= startIndex) {
				pageItems.add(item);
			}
			currentRow++;
		}
		return page;
	}
	public Page<T> fetchPage1(final NamedParameterJdbcTemplate jt, final String sqlFetchRows, String fieldToOrder, String direction, final SqlParameterSource paramSource, final int startIndex, final int pageSize, final RowMapper<T> rowMapper){
		return fetchPage1(jt, insertOrderClause(sqlFetchRows, fieldToOrder, direction), paramSource, startIndex, pageSize, rowMapper);
	}
	
	private String insertOrderClause(String stmnt, String fieldToOrder, String direction) {
		StringBuilder sb = new StringBuilder(stmnt);
		if (StringUtils.hasText(fieldToOrder)) {
			sb.append(" ORDER BY ");
			sb.append(fieldToOrder);
			sb.append(("ASC".equalsIgnoreCase(direction.trim()))? " ASC": " DESC");
		}
		return sb.toString();
	}

	public Page<T> fetchPage1(final NamedParameterJdbcTemplate jt, final String sqlFetchRows, final SqlParameterSource paramSource, final int startIndex, final int pageSize, final RowMapper<T> rowMapper) {
		StringBuilder sql_to_execute = new StringBuilder();
		sql_to_execute.append(QUERY_WITH_PRE);
		sql_to_execute.append(NEWLINE + sqlFetchRows);
		sql_to_execute.append(NEWLINE + QUERY_WITH_POST);
		
		log.debug("sql_to_execute = {}", sql_to_execute);
//		log.debug("startIndex = {}", startIndex);
//		log.debug("pageSize = {}", pageSize);

		return jt.query(sql_to_execute.toString(), paramSource, new ResultSetExtractor<Page<T>>() {
			public Page<T> extractData(final ResultSet rs) throws SQLException, DataAccessException {
				final List<T> pageItems = new ArrayList<T>();
				int currentRow = 0;
				int rowCount = 0;
				while (rs.next() && currentRow < startIndex + pageSize) {
//					log.debug("PagingHelper.fetchPage1 : currentRow = {}", currentRow);
					if (currentRow >= startIndex) {
						pageItems.add(rowMapper.mapRow(rs, currentRow));
					}
					if (rowCount == 0) {
						rowCount = rs.getInt("TOT_ROWS");
//						log.debug("[fetchPage1] TOT_ROWS: {}", rs.getInt("TOT_ROWS"));
					}
					currentRow++;
				}
				return new Page<T>(rowCount, pageItems);
			}
		});
	}
}
