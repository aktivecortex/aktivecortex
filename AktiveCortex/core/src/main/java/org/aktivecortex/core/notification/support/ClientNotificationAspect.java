/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification.support;

import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageHeaders;
import org.aktivecortex.core.utils.io.Utils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import static org.aktivecortex.api.message.MessageHeadersConstants.*;

/**
 *
 * @author Domenico Maria Giffone.
 * @since 1.x
 */
@Component
@Aspect
public class ClientNotificationAspect {

    @Pointcut(value = "execution(* org.aktivecortex.core.message.channel.MessageChannel+.send(..))")
    public void sendPointcut() {}

    @Around("sendPointcut()")
    public Object injectNotificationContext(ProceedingJoinPoint joinPoint) throws Throwable {
        final Message message = (Message) joinPoint.getArgs()[0];
        final MessageHeaders messageHeaders = message.getMessageHeaders();

        if (null != MDC.get(NOTIFICATION_PROCESS_ID)) {
            messageHeaders.put(NOTIFICATION_PROCESS_ID, MDC.get(NOTIFICATION_PROCESS_ID));
        }
        if (null != MDC.get(SHIPPING_PROCESS_DISPATCHER)) {
            messageHeaders.put(SHIPPING_PROCESS_DISPATCHER, MDC.get(SHIPPING_PROCESS_DISPATCHER));
        }
        messageHeaders.put(SHIPPING_DISPATCHER, Utils.getHostname());

        // send message
        return joinPoint.proceed();

    }


}
