/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification;

import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.MapMaker;
import org.aktivecortex.api.audit.SecurityContext;
import org.aktivecortex.api.message.MessageHeadersConstants;
import org.aktivecortex.api.notification.*;
import org.aktivecortex.api.notification.Process;
import org.aktivecortex.api.notification.ProgressEvaluator;
import org.aktivecortex.api.notification.support.Observable;
import org.aktivecortex.core.utils.io.Utils;
import org.axonframework.util.Assert;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.google.common.collect.Collections2.filter;
import static org.aktivecortex.api.message.MessageHeadersConstants.NOTIFICATION_TASK_ID;
import static org.aktivecortex.core.notification.ResultImpl.FAILURE;
import static org.aktivecortex.core.notification.ResultImpl.SUCCESS;
import static org.aktivecortex.core.notification.StateImpl.*;

public class ConversationManagerImpl implements ProcessQueryService, ConversationManager, Observable {

    private static final int MAX_PROCESSES = 100 * 100;

    private static final int PROCESS_EXPIRATION_IN_MM = 30;   // 1/2 Hour write expiration

    private static final int DEF_MILLIS_BETWEEN_NOTIFICATIONS = 1000; // 1 secs

    private static final Duration DEF_DURATION_BETWEEN_NOTIFICATIONS = new Duration(DEF_MILLIS_BETWEEN_NOTIFICATIONS);

    private static final Logger logger = LoggerFactory.getLogger(ConversationManager.class);

    private static final ProgressEvaluator NOP_EVALUATOR = new ProgressEvaluator() {

        @Override
        public Progress evaluate(Progress currentProgress, String payloadFQCN) {
            return currentProgress;
        }
    };
    private static final double DEF_PROGRESS_DELTA = 0.10;

    private final AtomicLong startedProcesses = new AtomicLong(0);

    private Set<PropertyChangeListener> listeners = new CopyOnWriteArraySet<PropertyChangeListener>();

    private final Map<String, ProgressEvaluator> evaluatorMap = new MapMaker().makeMap();

    private final Map<String, DateTime> updateTimes = new MapMaker().makeMap();

    private Duration durationBetweenNotification = DEF_DURATION_BETWEEN_NOTIFICATIONS;

    public void setMillisBetweenNotification(int millisBetweenNotification) {
        this.durationBetweenNotification = new Duration(millisBetweenNotification);
    }

    private SecurityContext ctxProvider;

    public void setSecurityContextProvider(SecurityContext provider) {
        this.ctxProvider = provider;
    }

    /**
     * Key is processId
     */
    private final LoadingCache<String, ProcessImpl> processes = CacheBuilder.newBuilder()
            .maximumSize(MAX_PROCESSES)
            .expireAfterWrite(PROCESS_EXPIRATION_IN_MM, TimeUnit.MINUTES)
            .build(new CacheLoader<String, ProcessImpl>() {
                @Override
                public ProcessImpl load(String processId) throws Exception {
                    final Date startTime = new Date();
                    return new ProcessImpl()
                            .withId(processId)
                            .withStartTime(startTime)
                            .withState(SUBMITTED)
                            .withProgress(new ProgressImpl()
                                    .withCompletion(0D)
                                    .withTimestamp(startTime))
                            .withUsername(ctxProvider.getUsername())
                            .withSessionId(ctxProvider.getSessionId());
                }
            });


    @Override
    public Task createTask(String payloadId) {
       return createTask(payloadId, null);
    }

    @Override
    public Task createTask(String payloadId, ProgressEvaluator evaluator) {
        return new TaskImpl()
                .withId(payloadId)
                .withProcessId(getCurrentProcessId(payloadId, evaluator));
    }

    private String getCurrentProcessId(String payloadId, ProgressEvaluator evaluator) {
        String processId = MDC.get(MessageHeadersConstants.NOTIFICATION_PROCESS_ID);
        Assert.isFalse(null != processId && null != evaluator,
                "Can't associate a ProcessEvaluator to an existing running Process");
        if (null == processId) {
            processId = createProcess(payloadId);
            if (null!= evaluator) {
                evaluatorMap.put(processId, evaluator);
            }
        }
        return processId;
    }

    private String createProcess(String payloadId) {
        String processId = getProcessId();
        processes.getUnchecked(processId)
                .withId(processId)
                .withType(getPayloadType(payloadId))
                .withStartTaskId(payloadId);
        updateTimes.put(processId, new DateTime());
        return processId;
    }

    private String getPayloadType(String payloadId) {
        int start = 0;
        if (payloadId.startsWith("[")) {
           start = 1;
        }
        return payloadId.substring(start, payloadId.indexOf("-"));
    }

    private String getProcessId() {
        return new StringBuilder()
                .append(Utils.getHostname())
                .append("-")
                .append(startedProcesses.getAndIncrement()).toString();
    }

    @Override
    public void completeTask(String processId, String payloadFQCN, Result result) {
        completeTask(processId, payloadFQCN, result, null);
    }

    @Override
    public void completeTask(String processId, String payloadFQCN, Result result, String cause) {
       completeTask(processId, payloadFQCN, result, cause, new DateTime());
    }

    @Override
    public void completeTask(String processId, String payloadFQCN, Result result, String cause, DateTime at) {
       updateProcessStatus(processId, payloadFQCN, result, cause, at);
    }

    private void updateProcessStatus(String processId, String payloadFQCN, Result result, String cause, DateTime at) {
        if (FAILURE.equals(result) && isFirstTaskExecution(processId)) {
            completeProcess(processId, result, cause, at);
        } else {
            updateProcessStatus(processId, payloadFQCN, at);
        }
    }

    private boolean isFirstTaskExecution(String processId) {
        ProcessImpl process = processes.asMap().get(processId);
        if (null == process) {
            return false;
        }
        return process.getStartTaskId().equals(MDC.get(NOTIFICATION_TASK_ID));
    }

    private void updateProcessStatus(String processId, String payloadFQCN, DateTime at) {
        ProcessImpl process = processes.asMap().get(processId);
        if (null != process) {
            double prevCompletion = process.getProgress().getCompletion();
            ProgressImpl progress = (ProgressImpl) getEvaluator(processId)
                    .evaluate(process.getProgress(), payloadFQCN);
            double delta = progress.getCompletion() - prevCompletion;
            if (SUBMITTED.equals(process.getState())) {
                process = updateCached(process, STARTED, progress);
                notifyListeners(process);
            } else {
                process = updateCached(process, (StateImpl) process.getState(), progress);
                if(shouldNotify(processId, delta)) {
                    notifyListeners(process);
                }
            }
        }
    }

    private ProcessImpl updateCached(ProcessImpl process, StateImpl state, ProgressImpl progress) {
        ProcessImpl clone = process.clone().withState(state).withProgress(progress);
        processes.put(clone.getId(), clone);
        return clone;
    }

    private ProgressEvaluator getEvaluator(String processId) {
        ProgressEvaluator evaluator = evaluatorMap.get(processId);
        if (null == evaluator) {
            evaluator = NOP_EVALUATOR;
        }
        return evaluator;
    }

    private boolean shouldNotify(String processId, double delta) {
        return new Duration(updateTimes.get(processId), new DateTime()).isLongerThan(durationBetweenNotification) || delta > DEF_PROGRESS_DELTA;
    }

    @Override
    public void completeProcess(String processId, Result result) {
        completeProcess(processId, result, null, new DateTime());
    }

    @Override
    public void completeProcess(String processId, Result result, String cause, DateTime at) {
        ProcessImpl process = processes.asMap().get(processId);
        if (null != process) {
            processes.invalidate(processId);
            evaluatorMap.remove(processId);
            updateTimes.remove(processId);
            process.withState(COMPLETED)
                    .withProgress(new ProgressImpl()
                    .withCompletion(1D)
                    .withTimestamp(at.toDate()))
                    .withResult(ResultImpl.fromValue(result.value()))
                    .withDuration(new Duration(process.getStartTime().getTime(), at.getMillis()).toString());
            notifyListeners(process);
        }
    }

    private void notifyListeners(Process process) {
        PropertyChangeEvent evt = new PropertyChangeEvent(PROPERTY_CHANGE_SOURCE, PROCESS_PROPERTY_NAME, null, process);
        evt.setPropagationId(process.getSessionId());
        for (PropertyChangeListener listener : listeners) {
            listener.propertyChange(evt);
        }
        updateTimes.put(process.getId(), new DateTime());
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners.add(listener)) {
            logger.info("added new listener with name [{}]", listener.getClass().getName());
        }
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (listeners.remove(listener)) {
            logger.info("removed new listener with name [{}]", listener.getClass().getName());
            return true;
        }
        return false;
    }

    @Override
    public Process findProcess(String processId) {
        return processes.asMap().get(processId);
    }

    @Override
    public Collection<Process> getMyProcesses() {
        final String sessionId = ctxProvider.getSessionId();
        final Iterable<? extends Process> filtered = Iterables.filter(
                processes.asMap().values(),
                new Predicate<Process>() {
                    @Override
                    public boolean apply(Process process) {
                        return process.getSessionId().equals(sessionId);
                    }
                });
        return ImmutableList.copyOf(filtered);
    }

    @Override
    public Collection<Process> getProcesses(final State progress) {
        final String sessionId = ctxProvider.getSessionId();
        final Iterable<? extends Process> filtered = Iterables.filter(
                processes.asMap().values(),
                new Predicate<Process>() {
                    @Override
                    public boolean apply(Process process) {
                        return process.getSessionId().equals(sessionId)
                                && process.getState().equals(progress);
                    }
                });
        return ImmutableList.copyOf(filtered);
    }

    public Collection<Process> getCompletedProcesses() {
        return getProcesses(COMPLETED);
    }

    public Collection<Process> getSuccessProcesses() {
        return filter(getCompletedProcesses(), new Predicate<Process>() {
            @Override
            public boolean apply(Process arg0) {
                return arg0.getResult().equals(SUCCESS);
            }
        });
    }

    public Collection<Process> getFailedProcesses() {
        return filter(getCompletedProcesses(), new Predicate<Process>() {
            @Override
            public boolean apply(Process arg0) {
                return arg0.getResult().equals(ResultImpl.FAILURE);
            }
        });
    }

    @Override
    public Collection<Process> getAllProcesses() {
        final Collection<? extends Process> values = processes.asMap().values();
        return ImmutableList.copyOf(values);
    }

    @Override
    public Collection<Process> getPendingProcesses() {
        final String sessionId = ctxProvider.getSessionId();
        final Iterable<? extends Process> filtered = Iterables.filter(
                processes.asMap().values(),
                new Predicate<Process>() {
                    @Override
                    public boolean apply(Process process) {
                        return process.getSessionId().equals(sessionId)
                                && !process.getState().equals(COMPLETED);
                    }
                });
        return ImmutableList.copyOf(filtered);
    }
}
