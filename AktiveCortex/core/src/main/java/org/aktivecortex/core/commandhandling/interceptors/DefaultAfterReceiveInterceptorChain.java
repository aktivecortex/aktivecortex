/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.commandhandling.interceptors;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.unitofwork.UnitOfWork;

import java.util.Iterator;

public class DefaultAfterReceiveInterceptorChain implements AfterReceiveInterceptorChain {

    private final Message<Command> command;
    private final CommandHandler handler;
    private Iterator<? extends AfterReceiveMessageHandlerInterceptor<Command>> chain;
    private UnitOfWork unitOfWork;

    /**
     * Initialize the default interceptor chain to dispatch the given <code>command</code>, through the
     * <code>chain</code>, to the <code>handler</code>.
     *
     * @param command    The command to dispatch through the interceptor chain
     * @param unitOfWork The UnitOfWork the command is executed in
     * @param handler    The handler for the command
     * @param chain      The interceptor composing the chain
     */
    public DefaultAfterReceiveInterceptorChain(Message<Command> command, UnitOfWork unitOfWork, CommandHandler<?> handler, Iterable<? extends AfterReceiveMessageHandlerInterceptor<Command>> chain) {
        this.command = command;
        this.handler = handler;
        this.chain = chain.iterator();
        this.unitOfWork = unitOfWork;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings({"all"})
    public Object proceed(Message<Command> commandProceedWith)  {
        if (chain.hasNext()) {
            return chain.next().handle(commandProceedWith, unitOfWork, this);
        } else {
            try {
                return handler.handle(commandProceedWith.getPayload(), unitOfWork);
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        }
    }

    @Override
    public Object proceed() {
        return proceed(command);
    }
}
