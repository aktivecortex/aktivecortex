/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.channel.adapter;

import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessagingException;
import org.aktivecortex.core.message.channel.MessageChannel;
import org.aktivecortex.core.message.channel.MessageDispatcher;
import org.aktivecortex.core.message.channel.MessageHandler;
import org.aktivecortex.core.message.spi.SendAdapter;
import org.axonframework.serializer.Serializer;
import org.axonframework.util.Assert;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.ImmutableList.copyOf;


@Transactional(propagation=Propagation.SUPPORTS)
public class ChannelAdapter<T> implements MessageChannel<T>, MessageDispatcher<T>, InitializingBean {

	private final Collection<MessageHandler<T>> handlers = new OrderedAwareLinkedHashSet<MessageHandler<T>>();

	private String adapterName;
	
	public void setAdapterName(String adapterName) {
		Assert.notNull(adapterName, "Adapter Name can't be null");
        this.adapterName = adapterName;
	}

	private SendAdapter sendAdapter;

    public void setSendAdapter(SendAdapter sendAdapter) {
        Assert.notNull(sendAdapter, "Send Adapter can't be null");
        this.sendAdapter = sendAdapter;
    }
	
	private Serializer<Message<T>> serializer;
	
	public void setSerializer(Serializer<Message<T>> serializer) {
        Assert.notNull(serializer, "Serializer can't be null");
		this.serializer = serializer;
	}

    @Override
    public void afterPropertiesSet() throws Exception {      //NOSONAR
        Assert.notNull(sendAdapter, "send adapter property required");
        Assert.notNull(serializer, "serializer property required");
        this.sendAdapter.afterPropertiesSet();
    }

	/**
	 * Returns a copied, unmodifiable List of this dispatcher's handlers. This
	 * is provided for access by subclasses.
	 */
	protected List<MessageHandler<T>> getHandlers() {
		return copyOf(this.handlers);
	}

	/**
	 * Add the handler to the internal Set.
	 *
	 * @return the result of {@link Set#add(Object)}
	 */
	@Override
	public boolean addHandler(MessageHandler<T> handler) {
		return this.handlers.add(handler);
	}

	/**
	 * Remove the handler from the internal handler Set.
	 *
	 * @return the result of {@link Set#remove(Object)}
	 */
	@Override
	public boolean removeHandler(MessageHandler<T> handler) {
		return this.handlers.remove(handler);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName());
		sb.append("[");
		sb.append(adapterName);
		sb.append("]");
		sb.append(" with handlers: ");
		sb.append(this.handlers);
		sb.append(" with sendAdapter: ");
		sb.append(this.sendAdapter);
		return sb.toString();
	}

	@Override
	public boolean subscribe(MessageHandler<T> handler) {
		return addHandler(handler);
	}

	@Override
	public boolean unsubscribe(MessageHandler<T> handle) {
		return removeHandler(handle);
	}

    // Hash based routing based on AR type and ID
    public boolean send(Message<T> message) {
		return sendAdapter.send(serializer.serialize(message), message.getPayload().toString(), message.getRoutingKey());
	}

	@Override
	public boolean dispatch(byte[] bytes) {
		Message<T> message = serializer.deserialize(bytes);
		boolean dispatched = false;
		message.getMessageHeaders().setDelivered();
		List<MessageHandler<T>> hs = this.getHandlers();
		for (final MessageHandler<T> handler : hs) {
			boolean success = this.invokeHandler(handler, message);
			dispatched = (success || dispatched);
		}
		return dispatched;
	}

	private boolean invokeHandler(MessageHandler<T> handler, Message<T> message) {
		try {
			handler.handleMessage(message);
			return true;
		} catch (MessagingException e) {
			if (e.getFailedMessage() == null) {
				e.setFailedMessage(message);
			}
			throw e;
		}
	}

}
