/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.commandhandling.interceptors;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageFactory;
import org.aktivecortex.core.message.channel.MessageChannel;

import java.util.Iterator;
import java.util.Map;

public class DefaultBeforeSendInterceptorChain implements BeforeSendInterceptorChain {

    private final Message<Command> message;
    private final MessageChannel<Command> channel;
    private final MessageFactory<Command> factory;
    private final Iterator<? extends BeforeSendMessageHandlerInterceptor<Command>> chain;

    // TBD Insert BeforeSendMessageHandlerInterceptor provider
    // provider.provideMetadata(command);
    public DefaultBeforeSendInterceptorChain(
            Iterable<? extends BeforeSendMessageHandlerInterceptor<Command>> chain,
            MessageChannel<Command> channel,
            MessageFactory<Command> factory,
            Map<String, String> optionalHeaders, Command... commands) {
        if (commands.length > 1) {
            message = factory.createBatch(commands);
        } else {
            message = factory.createMessage(commands[0]);
        }
        if (null!=optionalHeaders && optionalHeaders.size() > 0) {
            message.getMessageHeaders().putAll(optionalHeaders);
        }
        this.chain = chain.iterator();
        this.channel = channel;
        this.factory = factory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object proceed(Message<Command> message) {
        if (chain.hasNext()) {
            return chain.next().handle(message, this);
        } else {
            channel.send(message);
            return message;
        }
    }

    @Override
    public Object proceed() {
        return proceed(message);
    }
}
