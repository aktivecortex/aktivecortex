/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification;

import org.aktivecortex.api.notification.Progress;
import org.aktivecortex.api.notification.ProgressEvaluator;
import org.aktivecortex.core.notification.ProgressImpl;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

/**
 * Default implementation for {@link ProgressEvaluator} interface.
 * <p>Direct instantiation from the clients is not permitted.
 * <p>It's expected that clients will use the appropriate builder {@link ProgressEvaluatorBuilder}
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
class DefaultProgressEvaluator implements ProgressEvaluator {


    private static final int DEF_SCALE = 2;
    private static final double POW = Math.pow(10, DEF_SCALE);
    private Map<String, Double> weights = newHashMap();
    private Map<String, String> names = newHashMap();
    private Map<String, String> descriptions = newHashMap();

    DefaultProgressEvaluator(List<Step> steps) {
        for (Step step : steps) {
            final String name = step.getName();
            final String description = step.getDescription();
            for (String commandFQCN: step.getCommands()){
                weights.put(commandFQCN, step.getSingleCommandWeight());
                names.put(commandFQCN, name);
                descriptions.put(commandFQCN, description);
            }
        }
    }


    @Override
    public Progress evaluate(Progress currentProgress, String payloadFQCN) {
        return new ProgressImpl()
                .withCompletion(getCompletion(currentProgress, payloadFQCN))
                .withName(names.get(payloadFQCN))
                .withDescription(descriptions.get(payloadFQCN));
    }

    private double getCompletion(Progress currentProgress, String payloadFQCN) {
        return currentProgress.getCompletion() + weights.get(payloadFQCN);
    }

}
