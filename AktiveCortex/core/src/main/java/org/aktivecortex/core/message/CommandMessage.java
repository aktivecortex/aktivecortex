/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message;

import java.util.Map;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageHeaders;
import org.aktivecortex.api.message.MessageHeadersConstants;
import org.aktivecortex.core.utils.MapUtils;
import org.axonframework.util.Assert;

import com.google.common.collect.ImmutableBiMap;

public class CommandMessage implements Message<Command> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9142428812442932646L;
	
	private final Command payload;
	
	private final MessageHeaders headers;

	public CommandMessage(Command payload) {
		this.payload = payload;
		this.headers = new DefaultMessageHeaders();
	}

    CommandMessage(Command payload, MessageHeaders headers) {
        this.payload = payload;
        this.headers = new DefaultMessageHeaders(headers);
    }
	
	@Override
	public String getRoutingKey() {
		return payload.getRoutingKey();
	}

	@Override
	public MessageHeaders getMessageHeaders() {
		return headers;
	}

	@Override
	public Command getPayload() {
		return payload;
	}

}
