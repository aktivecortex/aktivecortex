/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification.support;

import org.aktivecortex.api.notification.ProgressNotifier;
import org.aktivecortex.core.axon2backport.saga.repository.jpa.JpaSagaRepository;
import org.aktivecortex.core.message.channel.MessageHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Aspect
public class NotificationAdviceAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationAdviceAspect.class);

    private NotificationSagaLifeCycleListener lifeCycleListener;

    @Autowired
    private void setProgressNotifier(ProgressNotifier progressNotifier){
        lifeCycleListener = new NotificationSagaLifeCycleListener(progressNotifier);
    }

    public NotificationAdviceAspect() {
        LOGGER.info("Notification Enabled.");
    }

    @Pointcut(value = "execution(* org.aktivecortex.core.message.channel.adapter.ChannelAdapter.subscribe(..))")
    public void onChannelSubscription() {
    }

    @Pointcut(value = "execution(* org.aktivecortex.core.axon2backport.saga.SagaRepositoryFactory+.createSagaRepository(..))")
    public void onSagaRepositoryCreation(){}

   @Around("onChannelSubscription()")
    public Object adviseMessageHandler(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof MessageHandler) {
                MessageHandler handler = (MessageHandler) args[i];
                AspectJProxyFactory factory = new AspectJProxyFactory(handler);
                factory.addAspect(ServerNotificationAspect.class);
                LOGGER.info("[{}] instrumented with server side notification aspect", handler);
                return joinPoint.proceed(new Object[]{factory.getProxy()});
            }
        }
        LOGGER.error("Unable to advice MessageHandler: {}", Arrays.toString(joinPoint.getArgs()));
        return Boolean.FALSE;
    }

    @Around("onSagaRepositoryCreation()")
    public Object adviseRepository(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = joinPoint.proceed();
        if (result instanceof JpaSagaRepository) {
            JpaSagaRepository repository = (JpaSagaRepository) result;
            repository.setSagaLifeCycleListener(lifeCycleListener);
            LOGGER.info("[{}] configured to notify saga end information", repository);
        }
        return result;
    }


    @SuppressWarnings("unchecked")
    private <T> T getTargetObject(Object proxy, Class<T> targetClass) throws Exception {
        while( (AopUtils.isJdkDynamicProxy(proxy))) {
            return (T) getTargetObject(((Advised)proxy).getTargetSource().getTarget(), targetClass);
        }
        return (T) proxy; // expected to be cglib proxy then, which is simply a specialized class
    }

}
