/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification.support;

import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageHeaders;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;

import java.util.Map;

import static org.aktivecortex.api.message.MessageHeadersConstants.*;
import static org.aktivecortex.core.notification.NotificationUtils.restoreContext;

/**
 *
 * @author Domenico Maria Giffone.
 * @since 1.x
 */
@Aspect
public class ServerNotificationAspect {

    @Pointcut(value = "execution(* org.aktivecortex.core.message.channel.MessageHandler.handleMessage(..))")
    public void handlePointcut() {}

    @Around("handlePointcut()")
    public Object injectNotificationContext(ProceedingJoinPoint joinPoint) throws Throwable {
        final Message message = (Message) joinPoint.getArgs()[0];
        final MessageHeaders messageHeaders = message.getMessageHeaders();
        final Map contextMap = MDC.getCopyOfContextMap();

        // Inject Diagnostic Context
        if (null != messageHeaders.get(NOTIFICATION_TASK_ID)) {
            MDC.put(NOTIFICATION_TASK_ID, (String) messageHeaders.get(NOTIFICATION_TASK_ID));
        }
        if (null != messageHeaders.get(NOTIFICATION_PROCESS_ID)) {
            MDC.put(NOTIFICATION_PROCESS_ID, (String) messageHeaders.get(NOTIFICATION_PROCESS_ID));
        }
        if (null != messageHeaders.get(SHIPPING_DISPATCHER)) {
            MDC.put(SHIPPING_DISPATCHER, (String) messageHeaders.get(SHIPPING_DISPATCHER));
        }
        if (null!= messageHeaders.get(SHIPPING_PROCESS_DISPATCHER)) {
            MDC.put(SHIPPING_PROCESS_DISPATCHER, (String) messageHeaders.get(SHIPPING_PROCESS_DISPATCHER));
        }
        try {
            // Handle message
            return joinPoint.proceed();
        } finally {
            restoreContext(contextMap);
        }

    }


}
