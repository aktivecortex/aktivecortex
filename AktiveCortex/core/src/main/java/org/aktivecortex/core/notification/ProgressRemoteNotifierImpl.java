/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification;


import com.google.common.collect.ImmutableList;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.notification.NotificationProgressEvent;
import org.aktivecortex.api.notification.ProgressNotifier;
import org.aktivecortex.api.notification.ProgressUpdate;
import org.axonframework.eventhandling.EventBus;
import org.joda.time.DateTime;
import org.slf4j.MDC;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.aktivecortex.api.message.MessageHeadersConstants.*;



public class ProgressRemoteNotifierImpl implements ProgressNotifier {
	
	private List<ProgressUpdate> taskDetails = new CopyOnWriteArrayList<ProgressUpdate>();
	
	private EventBus eventBus;
	
	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	@Override
	public void taskCompleted(Message<Command> message) {
		taskDetails.add(ProgressUpdate.forCompletedCommand(
                MDC.get(NOTIFICATION_PROCESS_ID),
                message.getPayload().getClass(),
                MDC.get(SHIPPING_PROCESS_DISPATCHER),
                new DateTime()));
	}

	@Override
	public void taskFailed(Message<Command> message, Throwable t) {
		taskDetails.add(ProgressUpdate.forFailedCommand(
                MDC.get(NOTIFICATION_PROCESS_ID),
                message.getPayload().getClass(),
                MDC.get(SHIPPING_PROCESS_DISPATCHER),
                new DateTime(),
                t.getLocalizedMessage()));
	}

    @Override
    public void processCompleted() {
        taskDetails.add(ProgressUpdate.forCompletedProcess(
                MDC.get(NOTIFICATION_PROCESS_ID),
                MDC.get(SHIPPING_PROCESS_DISPATCHER),
                new DateTime()));
    }

    void publish(){
		if (!taskDetails.isEmpty()) {
			List<ProgressUpdate> toPublish = ImmutableList.copyOf(taskDetails);
			eventBus.publish(new NotificationProgressEvent(toPublish));
			taskDetails.removeAll(toPublish);
		}
	}

}
