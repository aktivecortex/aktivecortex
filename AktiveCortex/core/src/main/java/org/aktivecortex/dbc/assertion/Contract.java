/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.dbc.assertion;

/**
 * Preconditions: Inherited classes can only weak
 * the conditions expressed in parent classes.
 * (Usually accomplished evaluating with OR operator all conditions)
 *
 * PostConditions: Inherited classes can only enforce
 * the conditions expressed in parent classes.
 * (Usually accomplished evaluating with AND operator all conditions)
 */
public enum Contract {

    REQUIRE(AssertionType.PRE_CONDITION),
    ENSURE(AssertionType.POST_CONDITION),
    INVARIANT(AssertionType.INVARIANT);

	private AssertionType type;

	private Contract(AssertionType type) {
		this.type = type;
	}

	public void isTrue(boolean logicCondition, String businessRuleName) {
		isTrue(logicCondition, businessRuleName, "");
	}

	public void isTrue(boolean logicCondition, String businessRuleName, String errorMessageTemplate) {
		isTrue(logicCondition, businessRuleName, errorMessageTemplate, new Object[]{});
	}

	public void isTrue(boolean logicCondition, String businessRuleName, String errorMessageTemplate, Object... errorMessageArgs) {
		if (!logicCondition) {
            type.violate(businessRuleName, errorMessageTemplate, errorMessageArgs);
        }
	}

	public void isFalse(boolean logicCondition, String businessRuleName, String errorMessageTemplate) {
		isFalse(logicCondition, businessRuleName, errorMessageTemplate, new Object[]{});
	}

	public void isFalse(boolean logicCondition, String businessRuleName, String errorMessageTemplate, Object... errorMessageArgs) {
		if (logicCondition) {
            type.violate(businessRuleName, errorMessageTemplate, errorMessageArgs);
        }
	}

	public void notNull(Object value, String businessRuleName, String errorMessageTemplate) {
		notNull(value, businessRuleName, errorMessageTemplate, new Object[]{});
	}

	public void notNull(Object value, String businessRuleName, String errorMessageTemplate, Object... errorMessageArgs) {
		if (value == null) {
            type.violate(businessRuleName, errorMessageTemplate, errorMessageArgs);
        }
	}

	public void isNull(Object value, String businessRuleName, String errorMessageTemplate) {
		isNull(value, businessRuleName, errorMessageTemplate, new Object[]{});
	}

	public void isNull(Object value, String businessRuleName, String errorMessageTemplate, Object... errorMessageArgs) {
		if (value != null) {
            type.violate(businessRuleName, errorMessageTemplate, errorMessageArgs);
        }
	}

	public void areEqual(Object first, Object second, String businessRuleName, String errorMessageTemplate) {
		areEqual(first, second, businessRuleName, errorMessageTemplate, new Object[]{});
	}

	public void areEqual(Object first, Object second, String businessRuleName, String errorMessageTemplate, Object... errorMessageArgs) {
		notNull(first, businessRuleName, "Objects must be equal but First is null.");
		notNull(second, businessRuleName, "Objects must be equal but Second is null.");
		if (!first.equals(second)) {
            type.violate(businessRuleName, errorMessageTemplate, errorMessageArgs);
        }
	}
}
