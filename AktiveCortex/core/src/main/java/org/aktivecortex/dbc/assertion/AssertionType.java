/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.dbc.assertion;

import org.aktivecortex.dbc.exception.InvariantException;
import org.aktivecortex.dbc.exception.PostConditionException;
import org.aktivecortex.dbc.exception.PreConditionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
  * A simple enumeration to map DBC assertion types,
 */
public enum AssertionType {

	PRE_CONDITION, POST_CONDITION, INVARIANT;

    private static final Logger LOGGER = LoggerFactory.getLogger(AssertionType.class);

    void violate(String businessRule, String format, Object... args) {
        String errorDescription = "";
        try {
            errorDescription = format(format, args);
        } catch (RuntimeException e) {
            LOGGER.error("uhoh! Something went wrong", e);
        }
        throw createException(businessRule, errorDescription);
    }

    private String format(String template, Object[] args) {
        template = String.valueOf(template); // null -> "null"
        if (null == args || args.length == 0) {
            return template;
        }
        StringBuilder sb = new StringBuilder(template.length() + 16 * args.length);
        int templateStart = 0;
        int i = 0;
        while (i < args.length) {
            int placeHolderStart = template.indexOf("{}", templateStart);
            if (placeHolderStart == -1) {
                break;
            }
            sb.append(template.substring(templateStart, placeHolderStart));
            sb.append(args[i++]);
            templateStart = placeHolderStart + 2;
        }
        sb.append(template.substring(templateStart));
        if (i < args.length) { // more args than placeholders ?
            sb.append(" [");
            sb.append(args[i++]);
            while (i < args.length) {
                sb.append(", ");
                sb.append(args[i++]);
            }
            sb.append("]");
        }
        return sb.toString();
    }

    private String getCallerClass() {
        final Throwable t = new Throwable();
        final StackTraceElement[] stes = t.getStackTrace();
        String callerClass = "UNKNOWN_CLASS";
        for (int i = 0; i < stes.length; i++) {
            String clsName = stes[i].getClassName();
            if (!(clsName.equals(this.getClass().getName()) || clsName.equals(Contract.class.getName()))) {
               callerClass = clsName;
               break;
            }
        }
        return callerClass;
    }

    private RuntimeException createException(String businessRule, String message) {
        StringBuilder brBuilder = new StringBuilder();
        brBuilder.append(getCallerClass());
        brBuilder.append(" - ");
        brBuilder.append(businessRule);
        String rule = brBuilder.toString();
        StringBuilder msgBuilder = new StringBuilder();
        msgBuilder.append("Violation of business rule: ");
        msgBuilder.append(rule);
        msgBuilder.append("\nCause: ");
        msgBuilder.append(message);
        String msg = msgBuilder.toString();
        switch (this) {
            case PRE_CONDITION:
                return new PreConditionException(rule, msg, message);
            case POST_CONDITION:
                return new PostConditionException(rule, msg, message);
            case INVARIANT:
                return new InvariantException(rule, msg, message);
        }
        return null;
    }

}

