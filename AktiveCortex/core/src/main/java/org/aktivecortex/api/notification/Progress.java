/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

import java.util.Date;

/**
 * The interface that describes the {@link Process} progress.
 * <p>
 * The intent is to provide an insight of process execution to the client.
 * </p>
 *
 * @author Domenico Maria Giffone
 * @since 1.2
 */
public interface Progress {

    /**
     * The date to which the information relates.
     *
     * @return The date to which the information relates
     */
    Date getTimestamp();

    /**
     * The process completion expressed as a value between 0 and 1,
     * where 1 represents 100% of the progress
     * @return double value between 0 and 1
     */
    double getCompletion();

    /**
     * An optional short description of the current state.
     * I.e. WithdrawalCompleted  or a key value that match a i18n resource.
     * @return  An optional short description of the current state
     */
    String getName();

    /**
     * An optional more meaningful description of the current state.
     * I.e.: money withdrawal completed
     *
     * @return An optional more meaningful description of the current state
     */
    String getDescription();

}
