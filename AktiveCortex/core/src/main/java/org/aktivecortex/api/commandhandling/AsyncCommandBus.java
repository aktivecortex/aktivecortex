/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.commandhandling;

import org.aktivecortex.api.command.Command;

import java.util.Map;

/**
 * The asynchronous bridge that dispatch commands to commandhandlers where they get executed.
 * 
 * @author Domenico Maria Giffone <domenico.giffone at gmail,com>
 */
public interface AsyncCommandBus {

    /**
     * <p>Enqueue one or more commands on the command queue and returns control to the caller.
     * <p>The commands are enqueued within a 
     * single message in order to ensure the serial execution order. </br>
     * A sole recipient for the whole batch is admitted. </br>
     * All the commands of the batch are executed within the same transaction. </br>
     * All commands must be serializable. </br>
     * Command execution return value is not admitted. </br>
     * 
     * @param commands
     */
    void dispatchAsync(Command... commands);

    /**
     * <p>Enqueue one or more commands on the command queue and returns control to the caller.
     * <p>The commands are enqueued within a
     * single message in order to ensure the serial execution order. </br>
     * A sole recipient for the whole batch is admitted. </br>
     * All the commands of the batch are executed within the same transaction. </br>
     * All commands must be serializable. </br>
     * Command execution return value is not admitted. </br>

     * @param additionalHeaders a map of additional headers that will be added to the message
     * @param commands
     */
    void dispatchAsync(Map<String, String> additionalHeaders, Command... commands);
}
