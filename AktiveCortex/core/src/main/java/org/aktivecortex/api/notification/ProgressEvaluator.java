/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

/**
 * Interface that returns the progress of a process.
 * <p>
 *     ProgressEvaluator is an argument of the convenient interface {@linkplain org.aktivecortex.api.commandhandling.CommandGateway}
 *     that permits to enrich {@link java.lang.Process} description with {@link Progress} information.
 * <p>
 *      The implementations are stateless and are specific to a single process.
 *
 * @author Domenico Maria Giffone.
 * @since 1.2
 */
public interface ProgressEvaluator {

    /**
     * Return the process progress from the last known progress and execution
     * of the command indicated by the {@code payloadFQCN}  parameter.
     *
     * @param lastProgress the last know {@link Progress}
     * @param payloadFQCN the fully qualified class name of the executed command
     * @return {@link Progress} the calculated progress
     */
    Progress evaluate(Progress lastProgress, String payloadFQCN);

}
