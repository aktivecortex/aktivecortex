/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Page<T> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private long itemsCount;
	private List<T> pageItems = new ArrayList<T>();

	public Page(){		
	}
	
	public Page(long itemsCount, List<T> pageItems) {
		this.itemsCount = itemsCount;
		this.pageItems = pageItems;
	}

	public Page(long itemsCount) {
		this.itemsCount = itemsCount;
	}

	public long getItemsCount() {
		return itemsCount;
	}

	public List<T> getPageItems() {
		return pageItems;
	}	

	public void setItemsCount(long itemsCount) {
		this.itemsCount = itemsCount;
	}

	public void setPageItems(List<T> pageItems) {
		this.pageItems = pageItems;
	}
	
	
	
	@Override
	public String toString() {
		return (new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE) {
			@Override
			protected boolean accept(Field f) {
				return super.accept(f);
			}
		}).toString();
	}
}
