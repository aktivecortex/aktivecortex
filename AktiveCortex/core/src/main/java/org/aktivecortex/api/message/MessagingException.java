/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.message;

public class MessagingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public <T> MessagingException(Message<T> message)
    {
        failedMessage = message;
    }

    public MessagingException(String description)
    {
        super(description);
        failedMessage = null;
    }

    public MessagingException(String description, Throwable cause)
    {
        super(description, cause);
        failedMessage = null;
    }

    public <T> MessagingException(Message<T> message, String description)
    {
        super(description);
        failedMessage = message;
    }

    public <T> MessagingException(Message<T> message, Throwable cause)
    {
        super(cause);
        failedMessage = message;
    }

    public <T> MessagingException(Message<T> message, String description, Throwable cause)
    {
        super(description, cause);
        failedMessage = message;
    }

    @SuppressWarnings("rawtypes")
	public Message getFailedMessage()
    {
        return failedMessage;
    }

    public <T> void setFailedMessage(Message<T> message)
    {
        failedMessage = message;
    }

    @SuppressWarnings("rawtypes")
	private volatile Message failedMessage;

}
