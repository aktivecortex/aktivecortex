/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

/**
 * The enumeration that describes the {@link Process} outcome if available.
 * <p>Result is available only for completed processes.</p>
 *  <p>
 * Accepted Values are:
 * <ul>
 * <li>SUCCESS</li>
 * <li>FAILURE</li>
 *
 */
public interface Result {

    /**
     * Return the string value of the process result
     * @return
     */
    String value();
}
