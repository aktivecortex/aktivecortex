/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

import java.util.Collection;

/**
 * Query Service that provides a representation for running processes.
 *
 * @author Domenico Maria Giffone
 * @since 1.0
 */
public interface ProcessQueryService {


    /**
     * Returns the current status of the process identified by the supplied id
     *
     * @param processId
     * @return Process
     */
    Process findProcess(String processId);

    /**
     * Returns a list of all current Processes in progress or recently completed
     *
     * @return
     */
    Collection<Process> getAllProcesses();

    /**
     * Returns the list of current Processes submitted by the user is currently logged
     *
     * @return
     */
    Collection<Process> getMyProcesses();

    /**
     * Returns the list of current Processes submitted by the user is currently logged
     * filtered by Process status
     *
     * @param progress
     * @return
     */
    Collection<Process> getProcesses(State progress);

    /**
     * Returns a list of all current Processes in progress
     *
     * @return
     */
    Collection<Process> getPendingProcesses();

}
