/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.command;

import org.aktivecortex.api.message.Routable;

import java.io.Serializable;
import java.util.Map;

/**
 * The value object intended to convey the arguments needed to perform a mutating operation.
 *
 */
public interface Command extends Routable, Serializable {

    /**
     * Return the command identifier
     * @return the string value of the command identifier
     */
    String getCommandId();

    
    /**
     *  Return the identifier of the aggregate that is the command target type
     * @return the aggregate identifier
     */
    String getAggregateIdentifier();

    /**
     * Return the fully qualified class name of the aggregate that is the command target type
     * @return the Aggregate class name 
     */
    String getAggregateType();


    /**
     * <p>The method must return a map of all significant fields of Command for the correct implementation of toFullString.
     *     
     * <P>Eg. for the command CreateApplicationCommand that contains the fields ApplicationId, applicationName and
     * description, the implementation of Method is as follows:
     *
     * <pre>protected Map<String,Object> getSignificantFields() { 
     *          return new HashMap<String, Object>() {{
     *              put("123", getApplicationId()); 
     *              put("MyApplication", getApplicationName()); 
     *              put("this is my application", getDescription()); 
     *          }}; 
     * } </pre>
     *
     * @return the map of entries where key is the field name and value is the field value
     */
    Map<String, Object> getSignificantFields();
}