/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

import java.io.Serializable;

/**
 * The enumeration that describes the {@link Process} current state.
 * <p>
 * Accepted Values are:
 * <ul>
 * <li>SUBMITTED</li>
 * <p>Represent a process for which the first command  was dispatched
 * on the command bus but not yet delivered to the pertaining command handler</p>
 * <li>STARTED</li>
 * <p>Represent a running process not yet completed</p>
 * <li>COMPLETED</li>
 * <p>Represent an already ended process</p>
 * </ul>
 * <p/>
 * </p>
 *
 * @author Domenico Maria Giffone
 * @since 1.2
 */
public interface State extends Serializable {

    /**
     * Return the string value of the process state
     * @return
     */
    String value();

}
