/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.event;

import org.aktivecortex.api.message.Routable;
import org.axonframework.domain.ApplicationEvent;
import org.slf4j.MDC;

import static org.aktivecortex.api.message.MessageHeadersConstants.*;

public abstract class AbstractApplicationEvent extends ApplicationEvent implements Routable {

    /**
     *
     */
    private static final long serialVersionUID = -1807668537216137574L;

    protected AbstractApplicationEvent(Object source) {
        super(source);
        injectDiagnosticContext();
    }

    protected void injectDiagnosticContext() {
        if (null != MDC.get(NOTIFICATION_PROCESS_ID)) {
            addMetaData(NOTIFICATION_PROCESS_ID, MDC.get(NOTIFICATION_PROCESS_ID));
        }
        if (null!= MDC.get(SHIPPING_PROCESS_DISPATCHER)) {
            addMetaData(SHIPPING_PROCESS_DISPATCHER, MDC.get(SHIPPING_PROCESS_DISPATCHER));
        }
    }

    /**
     * Return the routing key value for the event.
     * <p>The RK is used to determine the appropriate consumer
     * for the command among the known consumers.
     * <p>Derived classes can override this method to enable fine-grained distribution
     * of events to concurrent consumers.</p>
     * <p>Default RK value for competing listeners is: <pre>{EventListenerName}</pre></p>
     * <p>If you want to enable the distribution of events handled by sagas of the same type
     * to different consumers in a concurrent fashion You can override the method and return
     * the following value:<pre>{SagaName}-{correlationId}</pre>
     * Where correlationId is the value of the associationProperty
     * that is <u>common to all the events handled by the same saga type</u>.</p>
     * <p><em>Warning: an incorrect implementation of the following method
     * may cause concurrency issues and lead to stale or corrupted data.</em></p>
     * @return the string value of the routing key 
     */
    @Override
    public String getRoutingKey() {
        return (String) getMetaDataValue(ROUTING_KEY);
    }

    @Override
    public String toString() {
        return String.format("%s-%s", getClass().getSimpleName(), hashCode());
    }
}
