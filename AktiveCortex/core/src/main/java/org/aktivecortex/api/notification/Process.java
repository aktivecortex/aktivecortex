/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

import java.io.Serializable;
import java.util.Date;

/**
 * Process description read-only interface
 * that represent an asynchronous background process.
 * <p>The primary purpose is client notification</p>
 *
 * @author Domenico Maria Giffone
 * @since 1.2
 */
public interface Process extends Serializable {

    /**
     *  Returns the process identifier
     * @return
     */
    String getId();

    /**
     * Return the process type determined
     * by the first command that start the process.
     *
     * @return the command type, that consists of the first command class simple name
     */
    String getType();

    /**
     * Return the username that launched the process.
     * @return
     */
    String getUsername();

    /**
     * Return the Session ID, if avaliable, that launched the process.
     * @return
     */
    String getSessionId();

    /**
     * The task id, that launched the process.
     * The task describes a process single command.
     * @return
     */
    String getStartTaskId();

    /**
     * Process start time
     * @return
     */
    Date getStartTime();

    /**
     * Process current state
     * @return
     */
    State getState();

    /**
     * Process result if available.
     * @return
     */
    Result getResult();

    /**
     * The exception message that caused the process failure if available.
     *
     * @return
     */
    String getFailureCause();

    /**
     * utility value that indicates if the Process message was notified to clients.
     * @return boolean value
     */
    boolean isRead();

    /**
     *  The URI that return this process representation.
     * @return
     */
    Link getLink();

    /**
     * The process duration for completed process only.
     * @return
     */
    String getDuration();

}
