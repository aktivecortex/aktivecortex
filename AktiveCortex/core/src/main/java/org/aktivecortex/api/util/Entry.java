/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.util;

/**
 * An utility class that helps to return Key/Value pair from a single function.
 * 
 * @author Domenico Maria Giffone <domenico.giffone at gmail,com>
 * @param <K>
 * @param <V> 
 */
public class Entry<K, V> {
	 
	  private final K key;
	  private final V value;
	 
	  public Entry(K k,V v) {  
	    key = k;
	    value = v;   
	  }
	 
	  public K getKey() {
	    return key;
	  }
	 
	  public V getValue() {
	    return value;
	  }
	 
	  public String toString() { 
	    return String.format("( %s, %s)", key,value);  
	  }
	}
