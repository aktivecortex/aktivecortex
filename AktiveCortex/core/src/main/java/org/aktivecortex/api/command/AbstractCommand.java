/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.command;

import java.io.Serializable;
import java.util.Set;

import com.eaio.uuid.UUID;

public abstract class AbstractCommand implements Serializable, Command {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -8496610246112970169L;
	private final String commandString;

	private final String commandId; 
	
	@SuppressWarnings("all")
    protected AbstractCommand() {
		commandId = new UUID().toString();
		commandString = String.format("%s-%s", getClass().getSimpleName(), commandId.hashCode());
	}

	@Override
	public String getCommandId() {
		return commandId;
	}

	@Override
	public String getRoutingKey() {
		StringBuilder sb = new StringBuilder();
		sb.append(getAggregateType());
		sb.append("-");
		sb.append(getAggregateIdentifier());
		return sb.toString();
	}



	public String toFullString() {
		StringBuilder sb = new StringBuilder();
		Set<String> keys = getSignificantFields().keySet();
		boolean previousLine = false;
		sb.append("[");
		for (String key : keys) {
			if (previousLine) {
				sb.append(", ");
			}
			sb.append("'")
			.append(key)
			.append("':'")
			.append(getSignificantFields().get((key)))
			.append("'");
			previousLine=true;
		}
		return sb.append("]").toString();
	}

	@Override
	public String toString() {
		return commandString;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
            return false;
        }
		if (obj == this) {
            return true;
        }
		if (obj.getClass() != getClass()) {
            return false;
        }
		AbstractCommand command = (AbstractCommand) obj;
		return commandId.equals(command.getCommandId());
	}

	@Override
	public int hashCode() {
		return commandId.hashCode();
	}
	

}
