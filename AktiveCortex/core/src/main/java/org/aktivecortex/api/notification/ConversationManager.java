/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;


import org.joda.time.DateTime;

/**
 * The interface that supports Process tracking capabilities
 * needed to provide clients with real time notifications of system running processes.
 * <p>
 *     Implementations are responsible for managing process <a href="http://www.eaipatterns.com/ramblings/09_correlation.html">
 *         conversations</a> between participants where each participant
 * is represented by a command handler.
 *
 * @see ProgressNotifier
 * @author Domenico Maria Giffone
 * @since 1.0
 */
public interface ConversationManager {

    /**
     * Create a task instance for the command identified by the identifier provided
     *
     * @return {@link Task}
     */
    public Task createTask(String payloadId);

    /**
     * Create a task instance for the command identified by the identifier provided
     *
     * @param payloadId
     * @param evaluator the function responsible for progress evaluation
     * @return {@link Task}
     */
    public Task createTask(String payloadId, ProgressEvaluator evaluator);

    /**
     * Set task status as COMPLETED (enumeration {@link Progress}) <p>Set task result a SUCCESS o FAILURE
     * (enumeration {@link org.aktivecortex.api.notification.Result}) <p> set update time with the current time.
     *
     * @param payloadFQCN
     * @param result
     */
    void completeTask(String processId, String payloadFQCN, Result result);

    /**
     * Set task status as COMPLETED (enumeration {@link Progress}) <p>Set task result a SUCCESS o FAILURE
     * (enumeration {@link Result})
     * <p> It also set the failure case with the string provided
     *
     * @param payloadFQCN
     * @param result
     */
    void completeTask(String processId, String payloadFQCN, Result result, String cause);

    /**
     * Set task status as COMPLETED (enumeration {@link Progress}) <p>Set task result a SUCCESS o FAILURE
     * (enumeration {@link Result}) <p> set update time with the current time.
     * <p> It also set the failure case with the string provided
     *
     * @param payloadFQCN
     * @param result
     * @param at
     */
    void completeTask(String processId, String payloadFQCN, Result result, String cause, DateTime at);

    /**
     * Set process status as COMPLETED (enumeration {@link Progress}) <p>Set task result a SUCCESS o FAILURE
     * (enumeration {@link org.aktivecortex.api.notification.Result}) <p> set update time with the current time.
     *
     * @param processId
     * @param result
     */
    void completeProcess(String processId, Result result);

     /**
     * Set process status as COMPLETED (enumeration {@link Progress}) <p>Set task result a SUCCESS o FAILURE
     * (enumeration {@link org.aktivecortex.api.notification.Result}) <p> set update time with the current time.
     *
      * @param processId
      * @param result
      * @param cause
      */
    void completeProcess(String processId, Result result, String cause, DateTime at);

}