/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.message;

import java.io.Serializable;

/**
 * <p>A wrapper for asynchronously handled payloads (i.e. Commands and Events)
 *  to support messaging and routing part of the application.
 * 
 * @author Domenico Maria Giffone <domenico.giffone at gmail,com>
 * @param <T> the payload type 
 * <p> Concrete types are Command, Event
 */
public interface Message<T> extends Serializable, Routable {

    /**
     * Return the MessageHeaders that contains the message's metadata 
     * @return the MessageHeaders with message metadata
     */
    public MessageHeaders getMessageHeaders();

    /**
     * Return the payload carried by the message.
     * @return the message payload
     */
    public T getPayload();
}
