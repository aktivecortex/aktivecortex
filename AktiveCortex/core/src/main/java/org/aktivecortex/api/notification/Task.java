/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

import java.io.Serializable;

/**
 * Interface to represent a  as a step of a {@link Process} execution flow.
 * <p>The primary purpose is client notification.</p>
 *
 * @author Domenico Maria Giffone
 * @since 1.2
 */
public interface Task extends Serializable {

    /**
     * Return the task identifier.
     * <p>Please note that the task identifier differs from Command identifier returned by method
     * {@link org.aktivecortex.api.command.Command#getCommandId()}</p>
     * @return the task identifier
     */
    String getId();

    /**
     * Return the {@link org.aktivecortex.api.notification.Process} identifier of the process to which this task belong to.
     * @return the process identifier
     */
    String getProcessId();
}
