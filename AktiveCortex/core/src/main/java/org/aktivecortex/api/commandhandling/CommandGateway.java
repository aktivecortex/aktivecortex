/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.commandhandling;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.notification.Process;
import org.aktivecortex.api.notification.ProgressEvaluator;

/**
 * Convenient interface to simplify the queuing commands from clients.
 *
 * @author Domenico Maria Giffone
 * @since 1.2
 */
public interface CommandGateway {

    /**
     * Send an array of commands and returns the associated process descriptor.
     *
     * @param commands the array of commands to be sent
     * @return the associated process descriptor
     */
    Process send(Command... commands);

    /**
     * Dispatch a bunch of commands on the {@link org.axonframework.commandhandling.CommandBus}
     * with a {@link ProgressEvaluator}
     * to permit subsequent {@link Process} {@link org.aktivecortex.api.notification.Progress} tracking.
     *
     * @param progressEvaluator the ProgressEvaluator that permit subsequent {@link Process} {@link org.aktivecortex.api.notification.Progress} tracking
     * @param commands The commands to dispatche
     * @return the associated process descriptor
     */
    Process send(ProgressEvaluator progressEvaluator, Command... commands);

}
