/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.event;

import java.util.Map;
import java.util.Set;
import org.aktivecortex.api.command.Command;
import static org.aktivecortex.api.message.MessageHeadersConstants.ROUTING_KEY;

import org.aktivecortex.api.message.Routable;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.axonframework.domain.DomainEvent;

public abstract class AbstractDomainEvent extends DomainEvent implements Routable {

    /**
     *
     */
    private static final long serialVersionUID = -1737902094782814631L;

    /**
     * Return the routing key value for the event.
     * <p>The RK is used to determine the appropriate consumer
     * for the command among the known consumers.
     * <p>Derived classes can override this method to enable fine-grained distribution
     * of events to concurrent consumers.</p>
     * <p>Default RK value for competing listeners is: <pre>{EventListenerName}</pre></p>
     * <p>If you want to enable the distribution of events handled by sagas of the same type
     * to different consumers in a concurrent fashion You can override the method and return
     * the following value:<pre>{SagaName}-{correlationId}</pre>
     * Where correlationId is the value of the associationProperty
     * that is <u>common to all the events handled by the same saga type</u>.</p>
     * <p><em>Warning: an incorrect implementation of the following method
     * may cause concurrency issues and lead to stale or corrupted data.</em></p>
     * @return the string value of the routing key 
     */
    @Override
    public String getRoutingKey() {
        return (String) getMetaDataValue(ROUTING_KEY);
    }

    public String toFullString() {
        StringBuilder sb = new StringBuilder();
        Set<String> eventKeys = getMetaData().keySet();
        boolean previousLine = false;
        for (String key : eventKeys) {
            if (previousLine) {
                sb.append(", ");
            }
            sb.append("'").append(key).append("':'").append(getMetaDataValue(key)).append("'");
            previousLine = true;
        }
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("Event:", toStringEvent()).append(
                "Meta-data:[", sb.toString()).append("]").toString();
    }

    private String toStringEvent() {
        StringBuilder sb = new StringBuilder();
        Set<String> keys = getSignificantFields().keySet();
        boolean previousLine = false;
        sb.append("[");
        for (String key : keys) {
            if (previousLine) {
                sb.append(", ");
            }
            sb.append("'")
                    .append(key)
                    .append("':'")
                    .append(getSignificantFields().get((key)))
                    .append("'");
            previousLine = true;
        }
        return sb.append("]").toString();
    }

    @Override
    public String toString() {
        return String.format("%s-%s", getClass().getSimpleName(), hashCode());
    }

    /**
     * <p>The method must return a map of all significant fields of Event for the correct implementation of toFullString.
     *     
     * @see Command#getSignificantFields() 
     *
     * @return the map of entries where key is the field name and value is the field value
     */
    protected abstract Map<String, Object> getSignificantFields();
}
