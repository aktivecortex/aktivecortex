/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.core.notification.ResultImpl;
import org.joda.time.DateTime;


/**
 * DTO that contains {@link Process} execution data used for client notification.
 * <p>
 * This dto became necessary when involved processes started from remote JVMs.
 * In such cases the {@link NotificationProgressEvent} application event is dispatched on the
 * non competing {@link org.aktivecortex.core.eventbus.DistributedCluster} so that each JVM can update
 * his pertaining processes.</p>
 *
 *
 * @author Domenico Maria Giffone
 * @since 1.0
 */
public final class ProgressUpdate {

    private final String processId;
    private final String payloadFQCN;
    private final String destinationHost;
    private final DateTime timeStamp;
    private final Result result;
    private final String failureMessage;
    private final boolean processCompleted;


    private ProgressUpdate(String processId, String payloadFQCN, String destinationHost, DateTime timeStamp,
                           Result result, String failureMessage, boolean processCompleted) {
        this.processId = processId;
        this.payloadFQCN = payloadFQCN;
        this.destinationHost = destinationHost;
        this.timeStamp = timeStamp;
        this.result = result;
        this.failureMessage = failureMessage;
        this.processCompleted = processCompleted;
    }

    public DateTime getTimeStamp() {
        return timeStamp;
    }

    public Result getResult() {
        return result;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public String getPayloadFQCN() {
        return payloadFQCN;
    }

    public String getDestinationHost() {
        return destinationHost;
    }

    public String getProcessId() {
        return processId;
    }

    public boolean isProcessCompleted() {
        return processCompleted;
    }

    public static <T extends Command> ProgressUpdate forCompletedCommand(
            String processId,
            Class<T> payloadType,
            String destinationHost,
            DateTime receptionDate) {
        return new ProgressUpdate(processId,
                payloadType.getCanonicalName(),
                destinationHost,
                receptionDate,
                ResultImpl.SUCCESS,
                null, false);
    }

    public static <T extends Command> ProgressUpdate forFailedCommand(
            String processId,
            Class<T> payloadType,
            String destinationHost,
            DateTime receptionDate,
            String failureMessage) {
        return new ProgressUpdate(processId,
                payloadType.getCanonicalName(),
                destinationHost,
                receptionDate,
                ResultImpl.FAILURE,
                failureMessage, false);
    }

    public static ProgressUpdate forCompletedProcess(
            String processId,
            String destinationHost,
            DateTime receptionDate) {
        return new ProgressUpdate(processId,
                null,
                destinationHost,
                receptionDate,
                ResultImpl.SUCCESS,
                null, true);
    }

}