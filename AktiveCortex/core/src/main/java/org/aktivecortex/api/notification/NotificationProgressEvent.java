/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

import org.aktivecortex.api.event.AbstractApplicationEvent;
import org.aktivecortex.dbc.assertion.Contract;

import java.util.List;


/**
 * An application event that carries remote progress notifications.
 * <p>This event is a container for the list of {@link ProgressUpdate} DTO
 * that contains the progress actual data.</p>
 *
 * @author Domenico Maria Giffone
 * @since 1.0
 */
public class NotificationProgressEvent extends AbstractApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ProgressUpdate> taskDetails;
	

	public NotificationProgressEvent(List<ProgressUpdate> taskDetails) {
		super(null);
		Contract.REQUIRE.isTrue((null!= taskDetails && !taskDetails.isEmpty()), "Cannot send an empty task list");
		this.taskDetails = taskDetails;
	}

	
	public List<ProgressUpdate> getTaskDetails() {
		return taskDetails;
	}
	
}
