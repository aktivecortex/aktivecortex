/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.notification;

import java.io.Serializable;
import java.util.List;

/**
 * URL pointing to where the Process Description of the background process is located.
 *
 * @author Domenico Maria Giffone
 * @since 1.2
 */
public interface Link extends Serializable {

    /**
     * The relations between the current document and the linked resource.
     * <p>for example: <pre>{@code
     *  - self: this link acts as the primary representation for the resource.
     *  - alternate: this link acts as alternate (i.e.: print version) representation for the resource}</pre></p>
     * @return  list of the relations
     */
    List<String> getRel();

    /**
     *  returns the location (URL) of the external resource.
     * @return the URL of the resource
     */
    String getHref();
}
