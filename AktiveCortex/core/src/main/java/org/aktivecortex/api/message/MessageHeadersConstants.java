/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.message;

 public final class MessageHeadersConstants {

     private MessageHeadersConstants(){}
	
	// Delivery Infos
	 public static final String MESSAGE_ID = "_cortex_id";
	 public static final String ROUTING_KEY = "_routing_key";
	 public static final String DELIVERY_COUNT = "_delivery_count";

	// Security Infos
	 public static final String SECURITY_USER_ID = "_user_id";
	 public static final String SECURITY_USER_NAME = "_user_name";
	 public static final String SECURITY_USER_CTX = "_user_ctx";
	 public static final String SECURITY_USER_PO = "_user_po";
	 public static final String SECURITY_USER_ROLEID = "_user_roleid";
	 public static final String SECURITY_USER_ROLENAME = "_user_rolename";

	// Shipping Infos
     public static final String SHIPPING_PROCESS_DISPATCHER = "_proc_dispatcher_host";
     public static final String SHIPPING_DISPATCHER = "_dispatcher_host";
     public static final String SHIPPING_RECEIVER = "_receiver_host";
     public static final String SHIPPING_RECEIVED_ON = "_received";
     public static final String SHIPPING_DELIVERED = "_delivered";
     public static final String SHIPPING_DURATION = "_shipping_duration";

     // Notification infos
     public static final String NOTIFICATION_PROCESS_ID = "_proc_id";
     public static final String NOTIFICATION_TASK_ID = "_task_id";

     // grouped values
     public static final String[] ROUTING_CONSTANTS = new String[] {
             MESSAGE_ID, ROUTING_KEY, DELIVERY_COUNT
     };

     public static final String[] SECURITY_CONSTANTS = new String[]{
             SECURITY_USER_ID,
             SECURITY_USER_NAME,
             SECURITY_USER_CTX,
             SECURITY_USER_PO,
             SECURITY_USER_ROLEID,
             SECURITY_USER_ROLENAME
     };

     public static final String[]  SHIPPING_CONSTANTS = new String[]{
             SHIPPING_PROCESS_DISPATCHER,
             SHIPPING_DISPATCHER,
             SHIPPING_RECEIVER,
             SHIPPING_RECEIVED_ON,
             SHIPPING_DELIVERED,
             SHIPPING_DURATION
     };

     public static final String[] NOTIFICATION_CONSTANTS = new String[]{
             NOTIFICATION_PROCESS_ID, NOTIFICATION_TASK_ID
     };

 }