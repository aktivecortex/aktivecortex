/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.api.message;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 *  A type that holds the message metadata
 * 
 * @author Domenico Maria Giffone <domenico.giffone at gmail,com>
 */
public interface MessageHeaders extends Serializable {

	DateTime getTimestamp();

	String getMessageIdentifier();

	int getDeliveryCount();

	void setDelivered();

	boolean isDelivered();

	Serializable get(String s);

	void put(String key, Serializable value);

	boolean containsKey(String s);

	Set<String> keySet();

	void putAll(Map<? extends String, ? extends Serializable> arg0);
	
}
