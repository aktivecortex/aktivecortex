/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dyuproject.protostuff.runtime;

import com.dyuproject.protostuff.CollectionSchema;
import org.axonframework.saga.AssociationValue;
import org.axonframework.saga.AssociationValues;
import org.axonframework.saga.annotation.AssociationValuesImpl;

import java.util.Collection;

public class IdStrategyFactory implements IdStrategy.Factory {

	private DefaultIdStrategy strategy = new DefaultIdStrategy();

	public IdStrategyFactory() {
	}

	public IdStrategy create() {
		return strategy;
	}

	public void postCreate() {
		strategy.registerCollection(new CollectionSchema.MessageFactory() {

			@Override
			public Collection<AssociationValue> newMessage() {
				return new AssociationValuesImpl();
			}

			@Override
			public Class<AssociationValues> typeClass() {
				return AssociationValues.class;
			}

		});
	}

}
