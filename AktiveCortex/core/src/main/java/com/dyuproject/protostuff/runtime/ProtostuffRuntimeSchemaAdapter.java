/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dyuproject.protostuff.runtime;

import static com.dyuproject.protostuff.runtime.RuntimeSchema.register;

import org.aktivecortex.core.serializer.schema.jodatime.DateMidnightSchema;
import org.aktivecortex.core.serializer.schema.jodatime.DateTimeSchema;
import org.aktivecortex.core.serializer.schema.jodatime.IntervalSchema;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.dyuproject.protostuff.Schema;



public class ProtostuffRuntimeSchemaAdapter implements RuntimeSchemaAdapter {

	static {
		System.setProperty("protostuff.runtime.collection_schema_on_repeated_fields", "true");
		System.setProperty("protostuff.runtime.id_strategy_factory", "com.dyuproject.protostuff.runtime.IdStrategyFactory");
		register(DateTime.class, new DateTimeSchema());
		register(DateMidnight.class, new DateMidnightSchema());
		register(Interval.class, new IntervalSchema());
	}

	private final boolean isCacheEnabled;

	/**
	 *
	 */
	public ProtostuffRuntimeSchemaAdapter() {
		this.isCacheEnabled = true;
	}

	/**
	 * @param isCacheEnabled
	 */
	public ProtostuffRuntimeSchemaAdapter(boolean isCacheEnabled) {
		this.isCacheEnabled = isCacheEnabled;
	}

	@Override
	public <T> Schema<T> getSchema(Class<T> typeClass) {
		if (isCacheEnabled && RuntimeSchema.isRegistered(typeClass)) {
			return RuntimeSchema.getSchema(typeClass);
		}
		Schema<T> schema = RuntimeSchema.createFrom(typeClass);
		if (isCacheEnabled) {
            RuntimeSchema.register(typeClass, schema);
        }
		return schema;
	}

}
