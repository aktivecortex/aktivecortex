/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification.evaluator;

import org.aktivecortex.api.command.AbstractCommand;
import org.aktivecortex.api.notification.Progress;
import org.aktivecortex.api.notification.ProgressEvaluator;
import org.aktivecortex.core.notification.ProgressEvaluatorBuilder;
import org.aktivecortex.core.notification.ProgressImpl;
import org.aktivecortex.dbc.exception.PostConditionException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Description.
 *
 * @author Domenico Maria Giffone.
 * @since 1.x
 */
public class DefaultProcessStateEvaluatorTest {

    private static final String STEP_1 = "Step1";
    private static final String STEP_2 = "Step2";
    private static final String STEP_3 = "Step3";
    private static final double STEP_1_WEIGHT = 0.2;
    private static final double STEP_2_WEIGHT = 0.5;
    private static final double STEP_3_WEIGHT = 0.3;
    private static final int EXPECTED_AMOUNT_1 = 10;
    private static final int EXPECTED_AMOUNT_2 = 2;
    private static final int EXPECTED_AMOUNT_3 = 50;
    private static final int EXPECTED_AMOUNT_4 = 70;
    private static final double POW = Math.pow(10, 5);
    private static final double EXPECTED_PROGRESS_1 = Math.round((EXPECTED_AMOUNT_1 * STEP_1_WEIGHT / (EXPECTED_AMOUNT_1  + EXPECTED_AMOUNT_2)) * POW) / POW;
    private static final double DELTA = 0.01D;


    @Test(expected = IllegalStateException.class)
    public void testSumOfWeightLowerThan1() throws PostConditionException {
        ProgressEvaluatorBuilder.newBuilder()
                .addStep(STEP_1, STEP_1_WEIGHT)
                .composedBy(Command1.class, EXPECTED_AMOUNT_1)
                .composedBy(Command2.class, EXPECTED_AMOUNT_2)
                .addStep(STEP_2, STEP_2_WEIGHT)
                .composedBy(Command3.class, 50)
                .addStep(STEP_3, 0.1)
                .composedBy(Command4.class, 70)
                .build();
        fail("The sum of steps weight must be equal to 1");
    }

    @Test(expected = IllegalStateException.class)
    public void testSumOfWeightGreaterThan1() throws PostConditionException {
        ProgressEvaluatorBuilder.newBuilder()
                .addStep(STEP_1, STEP_1_WEIGHT)
                .composedBy(Command1.class, EXPECTED_AMOUNT_1)
                .composedBy(Command2.class, EXPECTED_AMOUNT_2)
                .addStep(STEP_2, STEP_2_WEIGHT)
                .composedBy(Command3.class, 50)
                .addStep(STEP_3, 0.4)
                .composedBy(Command4.class, 70)
                .build();
        fail("The sum of steps weight must be equal to 1");
    }

    @Test(expected = IllegalStateException.class)
    public void testNoIntersectionBetweenSteps() throws PostConditionException {
        ProgressEvaluatorBuilder.newBuilder()
                .addStep(STEP_1, STEP_1_WEIGHT)
                .composedBy(Command1.class, EXPECTED_AMOUNT_1)
                .composedBy(Command2.class, EXPECTED_AMOUNT_2)
                .addStep(STEP_2, STEP_2_WEIGHT)
                .composedBy(Command3.class, 50)
                .addStep(STEP_3, STEP_3_WEIGHT)
                .composedBy(Command2.class, 70)
                .build();
        fail("The same command can not belong to more than one step");
    }

    @Test
    public void testDefaultEvaluator() {
        ProgressEvaluator evaluator = ProgressEvaluatorBuilder.newBuilder()
                .addStep(STEP_1, STEP_1_WEIGHT)
                .composedBy(Command1.class, EXPECTED_AMOUNT_1)
                .composedBy(Command2.class, EXPECTED_AMOUNT_2)
                .addStep(STEP_2, STEP_2_WEIGHT)
                .composedBy(Command3.class, EXPECTED_AMOUNT_3)
                .addStep(STEP_3, STEP_3_WEIGHT)
                .composedBy(Command4.class, EXPECTED_AMOUNT_4)
                .build();
        assertNotNull("Evaluator can't be null", evaluator);
        Progress p = new ProgressImpl();
        for (int i = 0; i < EXPECTED_AMOUNT_1; i++) {
            p = evaluator.evaluate(p, Command1.class.getCanonicalName());
        }
        Assert.assertEquals("Evaluated progress differs from expected", EXPECTED_PROGRESS_1, p.getCompletion(), DELTA);
        for (int i = 0; i < EXPECTED_AMOUNT_2; i++) {
            p = evaluator.evaluate(p, Command2.class.getCanonicalName());
        }
        for (int i = 0; i < EXPECTED_AMOUNT_3; i++) {
            p = evaluator.evaluate(p, Command3.class.getCanonicalName());
        }
        for (int i = 0; i < EXPECTED_AMOUNT_4; i++) {
            p = evaluator.evaluate(p, Command4.class.getCanonicalName());
        }
        System.out.println(p);
        Assert.assertEquals("Evaluated progress differs from expected", 1, p.getCompletion(), DELTA);



    }

    private class Command1 extends AbstractCommand {
        @Override
        public String getAggregateIdentifier() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public String getAggregateType() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public Map<String, Object> getSignificantFields() {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    private class Command2 extends Command1 {
    }

    private class Command3 extends Command1 {
    }

    private class Command4 extends Command1 {
    }
}
