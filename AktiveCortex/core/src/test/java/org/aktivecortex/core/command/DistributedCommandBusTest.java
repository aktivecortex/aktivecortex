/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.command;

import com.eaio.uuid.UUID;
import com.google.common.collect.ImmutableMap;
import org.aktivecortex.api.audit.SecurityContext;
import org.aktivecortex.api.command.AbstractCommand;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageFactory;
import org.aktivecortex.api.notification.ConversationManager;
import org.aktivecortex.api.notification.Task;
import org.aktivecortex.core.audit.AuditEventDataProvider;
import org.aktivecortex.core.audit.ReflectionBasedAuditCommandLogger;
import org.aktivecortex.core.commandhandling.DistributedCommandBus;
import org.aktivecortex.core.commandhandling.interceptors.AfterReceiveMessageHandlerInterceptor;
import org.aktivecortex.core.commandhandling.interceptors.BeforeSendInterceptorChain;
import org.aktivecortex.core.commandhandling.interceptors.BeforeSendMessageHandlerInterceptor;
import org.aktivecortex.core.commandhandling.interceptors.afterreceive.AuditingInterceptor;
import org.aktivecortex.core.commandhandling.interceptors.beforesend.AuditCommandDataProvider;
import org.aktivecortex.core.message.CommandMessage;
import org.aktivecortex.core.message.DefaultMessageFactory;
import org.aktivecortex.core.message.channel.MessageChannel;
import org.aktivecortex.core.message.channel.MessageHandler;
import org.aktivecortex.core.notification.TaskImpl;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.axonframework.commandhandling.CommandCallback;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.unitofwork.UnitOfWork;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * @author d.giffone
 *
 */
/**
 * @author d.giffone
 *
 */
public class DistributedCommandBusTest {
	
	private DistributedCommandBus testSubject;
	
	private ConversationManager mockConversationManager;
	
	private MessageFactory<Command> messageFactory;
	
	private static final Logger logger = LoggerFactory.getLogger(DistributedCommandBusTest.class);
	
	private int taskId;
	
	private SecurityContext securityContext = new SecurityContext() {
		
		private String sessionId = new UUID().toString();
		
		private Map<String, String> attrs = ImmutableMap.of(
				"_user_id", "SYSTEM", 
				"_user_name","SYSTEM", 
				"_user_roleid","100", 
				"_user_rolename","ADMIN");

		@Override
		public String getUsername() {
			return "SYSTEM";
		}
		
		@Override
		public String getSessionId() {
			return sessionId ;
		}
		
		@Override
		public Map<String, String> getContextAttributes() {
			return attrs;
		}
	};

	// here to test BeforeSendInterceptorChain behavior with multiple interceptors
	private BeforeSendMessageHandlerInterceptor<Command> dummyBeforeSendHandlerInterceptor;
	
	private AuditingInterceptor auditingAfterReceiveInterceptor;

	@Before
	public void setUp() throws Exception {
		this.testSubject = new DistributedCommandBus();
		mockConversationManager = mock(ConversationManager.class);
		when(mockConversationManager.createTask(anyString())).thenAnswer(new Answer<Task>() {

			@Override
			public Task answer(InvocationOnMock invocation) throws Throwable {
				return new TaskImpl().withId(String.valueOf(++taskId));
			}
		});
		messageFactory = new DefaultMessageFactory(mockConversationManager);
		dummyBeforeSendHandlerInterceptor = new BeforeSendMessageHandlerInterceptor<Command>() {

			@Override
			public Object handle(Message<Command> message, BeforeSendInterceptorChain chain) {
				logger.debug(ReflectionToStringBuilder.toString(message, ToStringStyle.SHORT_PREFIX_STYLE));
				return chain.proceed();
			}
		};
		auditingAfterReceiveInterceptor = new AuditingInterceptor();
		auditingAfterReceiveInterceptor.setAuditDataProvider(new AuditEventDataProvider());
		auditingAfterReceiveInterceptor.setAuditLogger(new ReflectionBasedAuditCommandLogger());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDispatchObject() {
		HashMap<Class<TestCommand>, TestCommandHandler> subscriptions = new HashMap<Class<TestCommand>, TestCommandHandler>();
		subscriptions.put(TestCommand.class, new TestCommandHandler());
		testSubject.setSubscriptions(subscriptions);
		testSubject.dispatch(new CommandMessage(new TestCommand("Hello World!")), new CommandCallback<String>() {

			@Override
			public void onSuccess(String result) {
				assertEquals("Hello World!", result);
			}

			@Override
			public void onFailure(Throwable cause) {
				cause.printStackTrace();
				fail("Unexpected exception thrown");
				
			}
		});
	}

	@Test
	public void testDispatchAsyncCommand() throws Exception {
		AsyncTestCommandHandler handler = new AsyncTestCommandHandler();
		HashMap<Class<TestCommand>, AsyncTestCommandHandler> subscriptions = new HashMap<Class<TestCommand>, DistributedCommandBusTest.AsyncTestCommandHandler>();
		subscriptions.put(TestCommand.class, handler);
		List<BeforeSendMessageHandlerInterceptor<Command>> beforeSendInterceptors = newArrayList();
		beforeSendInterceptors.add(new AuditCommandDataProvider(securityContext));
		beforeSendInterceptors.add(dummyBeforeSendHandlerInterceptor);
		List<AfterReceiveMessageHandlerInterceptor<Command>> afterReceiveInterceptors = newArrayList();
		afterReceiveInterceptors.add(auditingAfterReceiveInterceptor);
		testSubject.setBeforeSendInterceptors(beforeSendInterceptors);
		testSubject.setAfterReceiveInterceptors(afterReceiveInterceptors);
		testSubject.setSubscriptions(subscriptions);
		testSubject.setChannel(memoryMessageChannel);
		testSubject.setMessageFactory(messageFactory);
		testSubject.afterPropertiesSet();
		testSubject.dispatchAsync(new TestCommand("Hello World!"));
		assertEquals(handler.getResult(), "Hello World!");
	}
	
	@Test
	public void testDispatchAsyncCommandBatch() throws Exception {
		AsyncTestCommandHandler handler = new AsyncTestCommandHandler();
		HashMap<Class<TestCommand>, AsyncTestCommandHandler> subscriptions = new HashMap<Class<TestCommand>, DistributedCommandBusTest.AsyncTestCommandHandler>();
		subscriptions.put(TestCommand.class, handler);
		testSubject.setBeforeSendInterceptors(newArrayList(new AuditCommandDataProvider(securityContext)));
		testSubject.setSubscriptions(subscriptions);
		testSubject.setChannel(memoryMessageChannel);
		testSubject.setMessageFactory(messageFactory);
		testSubject.afterPropertiesSet();
		testSubject.dispatchAsync(new TestCommand("Hello World!"), new TestCommand(" This a second command"));
		assertEquals(handler.getResult(), "Hello World! This a second command");
	}
	
	
	
	private static MessageChannel<Command> memoryMessageChannel = new MessageChannel<Command>() {
		
		MessageHandler<Command> handler;

		@Override
		public boolean send(Message<Command> message) {
			handler.handleMessage(message);
			return true;
		}

		@Override
		public boolean subscribe(MessageHandler<Command> messagehandler) {
			this.handler = messagehandler;
			return true;
		}

		@Override
		public boolean unsubscribe(MessageHandler<Command> messagehandler) {
			return true;
		}
		
	};
	
	private static final class TestCommand extends AbstractCommand {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private final String text;
		
		public TestCommand(String text) {
			this.text = text;
		}
		
		@Override
		public String getAggregateIdentifier() {
			return "ABC";
		}

		@Override
		public String getAggregateType() {
			return String.class.getSimpleName();
		}

		@Override
		public Map<String, Object> getSignificantFields() {
			return new HashMap<String, Object>() {
				{
					put("text", getText());
				}
			};
		}


		public String getText() {
			return text;
		}
		
		
	}
	
	private static final class AsyncTestCommandHandler implements CommandHandler<TestCommand> {
		
		String result = "";

		@Override
		public Object handle(TestCommand command, UnitOfWork unitOfWork) throws Throwable {
			result = result.concat(command.getText());
			return result;
		}
		
		Object getResult() {
			return result;
		}
		
	}
	
	private static final class TestCommandHandler implements CommandHandler<TestCommand> {

		@Override
		public Object handle(TestCommand command, UnitOfWork unitOfWork) throws Throwable {
			return command.getText();
		}
		
	}
	
	

}
