/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.spi;

import org.aktivecortex.core.message.spi.IMessagingSystemFactory.ConfigurationKey;
import org.aktivecortex.core.message.spi.impl.MessagingSystemFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MessagingSystemFactoryTest {
	

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetSendAdapter() {
		SendAdapter adapter = MessagingSystemFactory.getSendAdapter();
		assertTrue(adapter != null);
		assertEquals(adapter.getClass().getName(), "org.aktivecortex.core.message.spi.impl.NOPSendAdapter");
	}

	@Test
	public void testGetProperty() {
		assertNull(MessagingSystemFactory.getProperty(ConfigurationKey.ORDERED_DELIVERY_PROPERTY));
	}

	@Test
	public void testIsPropertyDefined() {
		for (ConfigurationKey key : ConfigurationKey.values()) {
			assertFalse(MessagingSystemFactory.isPropertyDefined(key));
		}
	}

}
