/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.audit;

import static com.google.common.collect.Lists.newArrayList;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.aktivecortex.api.command.AbstractCommand;
import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.event.AbstractDomainEvent;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.core.message.CommandMessage;
import org.axonframework.domain.Event;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.eaio.uuid.UUID;
import com.google.common.collect.ImmutableMap;

public class AuditCommandLoggerTest {

	private static final int MILLIS_TO_NANO = 1000000;

	private static final int ITERATIONS = 5000;

	private ReflectionBasedAuditCommandLogger testReference;

	private ToStringBasedAuditCommandLogger testSubject;

	private List<Event> testEvents;

	private Command testCommand;

	private Message<Command> testMessage;

	private Throwable testFailure;

	@Before
	public void setUp() throws Exception {
		this.testReference = new ReflectionBasedAuditCommandLogger();
		this.testSubject = new ToStringBasedAuditCommandLogger();
		provideEvents(testEvents);
		this.testCommand = new TestCommand();
		this.testMessage = new CommandMessage(testCommand);
		this.testFailure = new RuntimeException("testFailure");
		provideHeaders(testMessage);
		System.gc();
	}

	private void provideEvents(List<Event> events) {
		events = newArrayList();
		events.add(new TestEvent("Test1"));
		events.add(new TestEvent("Test2"));
		events.add(new TestEvent("Test3"));
	}

	private void provideHeaders(Message<Command> message) {
		message.getMessageHeaders().putAll(
				ImmutableMap.of("_user_id", "SYSTEM", "_user_name", "SYSTEM", "_user_roleid", "100", "_user_rolename",
						"ADMIN"));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReferenceLogFailed() {
		long start = System.nanoTime();
		for (int i = 0; i < ITERATIONS; i++) {
			testReference.logFailed(testMessage, testFailure, testEvents);
		}
		printResult("Reflection logFailed", System.nanoTime(), start, ITERATIONS);
	}

	@Test
	public void testReferenceLogSuccessful() {
		long start = System.nanoTime();
		for (int i = 0; i < ITERATIONS; i++) {
			testReference.logSuccessful(testMessage, null, testEvents);
		}
		printResult("Reflection logSuccessful", System.nanoTime(), start, ITERATIONS);
	}

	@Test
	public void testSubjectLogFailed() {
		long start = System.nanoTime();
		for (int i = 0; i < ITERATIONS; i++) {
			testSubject.logFailed(testMessage, testFailure, testEvents);
		}
		printResult("ToString logFailed", System.nanoTime(), start, ITERATIONS);
	}

	@Test
	public void testSubjectLogSuccessful() {
		long start = System.nanoTime();
		run();
		printResult("ToString logSuccessful", System.nanoTime(), start, ITERATIONS);
	}

	@Test
	public void testMicroBenchmark() {
		System.out.println("warm-up");
		int runs = 5;
		for (int i = 0; i < runs; i++) {
			run();
		}
		System.out.println("measurement");
		runs = 50;
		long start = System.nanoTime();
		for (int i = 0; i < runs; i++) {
			run();
		}
		printResult("ToString MicroBenchmark", System.nanoTime(), start, ITERATIONS * runs);
	}

	private void run() {
		for (int i = 0; i < ITERATIONS; i++) {
			testSubject.logSuccessful(testMessage, null, testEvents);
		}
	}

	private void printResult(String testName, long end, long start, long times) {
		StringBuilder sb = new StringBuilder();
		final long duration = end - start;
		final double iterationTime = iterationTime(duration, times);
		sb.append("---------------------------------------------\n ").append(testName).append(" (").append(times)
				.append(" iterations)").append("\n---------------------------------------------\n").append(" Avg: ")
				.append(iterationTime).append(" nanos. Total: ").append(duration).append(" nanos.\n Avg: ")
				.append(BigDecimal.valueOf(iterationTime).longValue() / MILLIS_TO_NANO).append(" millis. Total: ")
				.append(duration / MILLIS_TO_NANO).append(" millis.\n");
		System.out.println(sb.toString());
	}

	protected static double iterationTime(long delta, long iterations) {
		return (double) delta / (double) (iterations);
	}

	private static final class TestCommand extends AbstractCommand {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4294153224259434017L;

		@Override
		public String getAggregateIdentifier() {
			return new UUID().toString();
		}

		@Override
		public String getAggregateType() {
			return "TestAggregate";
		}

		public String getName() {
			return "A Name";
		}

		public String getSurame() {
			return "A Surname";
		}

		@Override
		public Map<String, Object> getSignificantFields() {
			return ImmutableMap.of("name", (Object) getName(), "surname", (Object) getSurame());
		}

	}

	public static final class TestEvent extends AbstractDomainEvent {

		private static final long serialVersionUID = 1657550542124835062L;
		private String name;
		private DateMidnight date;
		private DateTime dateTime;
		private Period period;
		private Interval week;

		/**
		 * 
		 */
		protected TestEvent() {
			super();
		}

		public TestEvent(String name) {
			this.name = name;
			this.date = new DateMidnight();
			this.dateTime = new DateTime();
			this.period = new Period(100);
			this.week = new Interval(this.dateTime, this.dateTime.plus(Days.SEVEN));
		}

		public String getName() {
			return name;
		}

		public static long getSerialVersionUID() {
			return serialVersionUID;
		}

		public DateMidnight getDate() {
			return date;
		}

		public DateTime getDateTime() {
			return dateTime;
		}

		public Period getPeriod() {
			return period;
		}

		public Interval getWeek() {
			return week;
		}

		@Override
		protected Map<String, Object> getSignificantFields() {
			return ImmutableMap.of("name", (Object) getName(), "date", (Object) getDate(), "dateTime",
					(Object) getDateTime(), "period", (Object) getPeriod(), "week", (Object) getWeek());
		}

	}
}
