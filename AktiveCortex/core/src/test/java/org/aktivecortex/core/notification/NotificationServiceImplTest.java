/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.notification;

import org.aktivecortex.api.audit.SecurityContext;
import org.aktivecortex.api.notification.Process;
import org.aktivecortex.api.notification.Task;
import org.aktivecortex.core.utils.io.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.MDC;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.aktivecortex.api.message.MessageHeadersConstants.NOTIFICATION_TASK_ID;
import static org.aktivecortex.core.notification.ResultImpl.FAILURE;
import static org.aktivecortex.core.notification.ResultImpl.SUCCESS;
import static org.aktivecortex.core.notification.StateImpl.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Description.
 *
 * @author Domenico Maria Giffone.
 * @since 1.x
 */
public class NotificationServiceImplTest implements PropertyChangeListener {

    private static final String PAYLOAD = "pelham-123";
    private static final String PROCESS_0 = Utils.getHostname() + '-' + 0;
    private static final TaskImpl FIRST_TASK = new TaskImpl()
            .withId(PAYLOAD)
            .withProcessId(PROCESS_0);
    private static final ProcessImpl FIRST_PROCESS = new ProcessImpl()
            .withId(PROCESS_0)
            .withStartTaskId(PAYLOAD)
            .withState(SUBMITTED);
    private ConversationManagerImpl testSubject;
    private SecurityContext ryderSecurityContext;
    private SecurityContext garberSecurityContext;

    private int successes;
    private int failures;



    @Before
    public void setUp() throws Exception {
        testSubject = new ConversationManagerImpl();
        ryderSecurityContext = mock(SecurityContext.class);
        garberSecurityContext = mock(SecurityContext.class);
        when(ryderSecurityContext.getSessionId()).thenReturn("123");
        when(ryderSecurityContext.getUsername()).thenReturn("Ryder");
        when(garberSecurityContext.getSessionId()).thenReturn("321");
        when(garberSecurityContext.getUsername()).thenReturn("Garber");
        testSubject.setSecurityContextProvider(ryderSecurityContext);
        successes = 0;
        testSubject.addPropertyChangeListener(this);
        FIRST_PROCESS.withState(SUBMITTED);
    }

    @After
    public void tearDown() throws Exception {
        testSubject.removePropertyChangeListener(this);
    }

    private void assertTaskEquals(Task expected, Task actual) {
        assertEquals(expected.getId(), actual.getId());
    }

    private void assertProcessEquals(Process expected,  Process actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getStartTaskId(), actual.getStartTaskId());
        assertEquals(expected.getState(), actual.getState());
    }

    @Test
    public void createTaskTest() throws Exception {
        Task actual = testSubject.createTask(PAYLOAD);
        assertTaskEquals(FIRST_TASK, actual);
        assertProcessEquals(FIRST_PROCESS, testSubject.findProcess(PROCESS_0));
        assertEquals(testSubject.getMyProcesses().size(), 1);
        assertEquals(testSubject.getPendingProcesses().size(), 1);
        assertEquals(testSubject.getProcesses(SUBMITTED).size(), 1);
        testSubject.setSecurityContextProvider(garberSecurityContext);
        assertEquals(testSubject.getMyProcesses().size(), 0);
        assertEquals(0, successes);
    }

    @Test
    public void updateTaskWithCurrentStatusTest() throws Exception {
        Task actual = testSubject.createTask(PAYLOAD);
        assertTaskEquals(FIRST_TASK, actual);
        assertProcessEquals(FIRST_PROCESS, testSubject.findProcess(PROCESS_0));
        assertEquals(testSubject.getMyProcesses().size(), 1);
        assertEquals(testSubject.getPendingProcesses().size(), 1);
        assertEquals(testSubject.getProcesses(SUBMITTED).size(), 1);
        testSubject.setSecurityContextProvider(garberSecurityContext);
        assertEquals(testSubject.getMyProcesses().size(), 0);
        assertEquals(0, successes);
    }

    @Test
    public void completeTaskTest() throws Exception {
        Task actual = testSubject.createTask(PAYLOAD);
        testSubject.completeTask(PROCESS_0, PAYLOAD, SUCCESS);
        assertTaskEquals(FIRST_TASK, actual);
        FIRST_PROCESS.withState(STARTED);
        assertProcessEquals(FIRST_PROCESS, testSubject.findProcess(PROCESS_0));
        assertEquals(testSubject.getMyProcesses().size(), 1);
        assertEquals(testSubject.getPendingProcesses().size(), 1);
        assertEquals(testSubject.getProcesses(STARTED).size(), 1);
        testSubject.setSecurityContextProvider(garberSecurityContext);
        assertEquals(testSubject.getMyProcesses().size(), 0);
        assertEquals(0, successes);
        assertEquals(0, failures);
    }

    @Test
    public void completeProcessTest() throws Exception {
        Task actual = testSubject.createTask(PAYLOAD);
        testSubject.completeProcess(PROCESS_0, SUCCESS);
        assertTaskEquals(FIRST_TASK, actual);
        FIRST_PROCESS.withState(COMPLETED);
        assertNull(testSubject.findProcess(PROCESS_0));
        assertEquals(testSubject.getMyProcesses().size(), 0);
        assertEquals(testSubject.getPendingProcesses().size(), 0);
        testSubject.setSecurityContextProvider(garberSecurityContext);
        assertEquals(testSubject.getMyProcesses().size(), 0);
        assertEquals(1, successes);
    }

    @Test
    public void failProcessTest() throws Exception {
        Task actual = testSubject.createTask(PAYLOAD);
        MDC.put(NOTIFICATION_TASK_ID, PAYLOAD);
        testSubject.completeTask(PROCESS_0, PAYLOAD, FAILURE, "Boom!");
        assertTaskEquals(FIRST_TASK, actual);
        assertNull(testSubject.findProcess(PROCESS_0));
        assertEquals(testSubject.getMyProcesses().size(), 0);
        assertEquals(testSubject.getPendingProcesses().size(), 0);
        testSubject.setSecurityContextProvider(garberSecurityContext);
        assertEquals(testSubject.getMyProcesses().size(), 0);
        assertEquals(1, failures);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        ProcessImpl process = (ProcessImpl) evt.getNewValue();
        if (SUCCESS.equals(process.getResult())) {
            successes++;
        }
        if (FAILURE.equals(process.getResult())) {
            failures++;
        }
    }

}
