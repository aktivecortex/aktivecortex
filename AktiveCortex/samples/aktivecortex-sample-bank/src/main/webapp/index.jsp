<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bank Sample</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="resources/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

 <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#" style="padding-bottom: 0px; padding-top: 4px;">
              <img src="resources/img/logo_small.png" style="padding-right: 10px;">Aktive Cortex</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <p><img src="resources/img/logo.png"></p>
        <p>Aktive Cortex is a framework that facilitates the development of applications based on the CQRS pattern,
        maximizing scalability through the use of asynchronous messaging subsystem which allows 
        the deployment in a distributed execution context.</p>
        <p><a href="http://www.aktivecortex.org" class="btn btn-primary btn-large">Go to Project homepage &raquo;</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="span12">
          <h2>Bank Sample</h2>
           <p>The example shows the main features of the framework declined through a web application that exposes a set of minimal operations of a typical application of internet banking.</p>
          <p><a class="btn" href="banking">Start sample &raquo;</a></p>
        </div>        
      </div>

      <hr>

      <footer>
        <p>&copy; Aktive Cortex 2012-2013</p>
      </footer>

    </div> <!-- /container -->


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="resources/lib/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="resources/lib/underscore-min.js"></script>
    <script type="text/javascript" src="resources/lib/backbone-min.js"></script>
    <script type="text/javascript" src="resources/lib/bootstrap.min.js"></script>
    

  </body>
</html>