<?xml version="1.0" encoding="UTF-8"?>
<beans
        xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:context="http://www.springframework.org/schema/context"
        xmlns:p="http://www.springframework.org/schema/p"
        xmlns:tx="http://www.springframework.org/schema/tx"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/tx
		http://www.springframework.org/schema/tx/spring-tx.xsd
		http://www.springframework.org/schema/context
		http://www.springframework.org/schema/context/spring-context.xsd">

    <!--<import resource="classpath:/aktivecortex-tracing-dev.xml" />-->

    <import resource="classpath:/aktivecortex-core-default.xml" />

    <import resource="spring-jms.xml" />

	<context:component-scan base-package="org.aktivecortex" />
	
	<tx:annotation-driven transaction-manager="transactionManager" />

    <!-- JNDI lookup env -->
    <bean id="jndiProperties" class="org.springframework.beans.factory.config.PropertiesFactoryBean">
        <property name="location" value="classpath:jndi.properties"/>
    </bean>

	<!-- Task Executors -->
	<bean id="taskExecutor" class="org.springframework.scheduling.commonj.WorkManagerTaskExecutor" lazy-init="false">
		<property name="workManagerName" value="wm/cortex-async"/>
		<property name="resourceRef" value="true"/>
		<property name="jndiEnvironment" ref="jndiProperties" />
	</bean>
	
	<bean id="sagaExecutor" class="org.springframework.scheduling.commonj.WorkManagerTaskExecutor" lazy-init="false">
		<property name="workManagerName" value="wm/cortex-saga"/>
		<property name="resourceRef" value="true"/>
		<property name="jndiEnvironment" ref="jndiProperties" />
	</bean>

    <!-- JMS Adapters -->
    <!-- Commands -->
    <bean id="commandSendAdapter" class="org.aktivecortex.jms.producer.ActiveMQSendAdapter">
        <property name="jmsTemplate" ref="commandTemplate" />
    </bean>

    <!-- Competing Events -->
    <bean id="competingEventSendAdapter" class="org.aktivecortex.jms.producer.ActiveMQSendAdapter">
        <property name="jmsTemplate" ref="compEventTemplate" />
    </bean>

    <!-- Non Competing Events -->
    <bean id="nonCompetingEventSendAdapter" class="org.aktivecortex.jms.producer.ActiveMQSendAdapter">
        <property name="jmsTemplate" ref="nonCompEventTemplate" />
    </bean>
	
	<!-- JPA EMF -->
	<bean id="entityManagerFactory"
		class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
		<property name="dataSource" ref="dataSource" />
		<property name="persistenceUnitName" value="cortexPU" />
		<property name="jpaVendorAdapter" ref="hibernateJpaAdapter" />
	</bean>
	
	<bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
		<property name="entityManagerFactory" ref="entityManagerFactory" />
		<property name="dataSource" ref="dataSource" />
	</bean> 

    <bean id="securityContext" class="org.aktivecortex.sample.bank.service.DummySecurityContext" />
    
    <bean id="commandListener" class="org.aktivecortex.jms.listener.DefaultMessageListener">
    	<property name="messageDispatcher" ref="commandChannel"/>
    </bean>

    <bean id="competingCluster"
          class="org.aktivecortex.core.eventbus.DistributedCompetingCluster"
          p:destinationName="Q_Event_1" p:destinationType="QUEUE">
        <property name="competingListeners">
            <set>
                <value>org.aktivecortex.core.axon2backport.saga.annotation.AsyncAnnotatedSagaManager</value>
            </set>
        </property>
        <property name="sagas">
            <set>
                <value>org.aktivecortex.sample.bank.service.bankaccount.MoneyTransferSaga</value>
                <value>org.aktivecortex.sample.bank.service.tests.TestSuiteSaga</value>
            </set>
        </property>
    </bean>
	
    <bean id="nonCompetingEventListener" class="org.aktivecortex.jms.listener.DefaultMessageListener">
    	<property name="messageDispatcher" ref="nonCompetingEventChannel"/>
    </bean>
	
    <bean id="competingEventListener" class="org.aktivecortex.jms.listener.DefaultMessageListener">
    	<property name="messageDispatcher" ref="competingEventChannel"/>
    </bean>
	
	<!-- Saga Manager -->
	<!-- MUST PROVIDE -->
	<!-- N.B. Enable property p:executor-ref="taskExecutor" to use workmanager executorservice -->
	<!--      in this case you must edit config.xml domain configuration file and include property deployment-principal-name -->
	<!--      see https://jira.springsource.org/browse/SPR-4670 for details and Oracle Metalink CR374022 -->
	<!--      and also http://aktiveqc.cloud.reply.eu/jira/browse/COLI-390 issue -->
	<!-- default executor service is jvm cached executor service obtained through Executors.newCachedThreadPool() -->
    <bean id="sagaManager" class="org.aktivecortex.core.axon2backport.saga.annotation.AsyncAnnotatedSagaManager"
          init-method="start" destroy-method="stop"
          p:bufferSize="8192"
          p:processorCount="4"
          p:sagaFactory-ref="sagaFactory"
          p:sagaRepositoryFactory-ref="sagaRepositoryFactory"
          p:transactionManager-ref="axonTxManager"
          p:executor-ref="sagaExecutor"
          p:waitStrategy-ref="sagaWaitStrategy"
          p:unhandledAsyncSagaEventListener-ref="unHandledEventNotifier" >
        <constructor-arg index="0" ref="eventBus"/>
        <constructor-arg index="1">
            <list>
                <value>org.aktivecortex.sample.bank.service.bankaccount.MoneyTransferSaga</value>
                <value>org.aktivecortex.sample.bank.service.tests.TestSuiteSaga</value>
            </list>
        </constructor-arg>
    </bean>
	
	<!-- N.B. Set p:taskExecutor-ref="taskExecutor"  if necessary -->
	<!-- MUST PROVIDE -->
    <bean id="scheduler" class="org.springframework.scheduling.quartz.SchedulerFactoryBean" lazy-init="false"
          depends-on="taskExecutor" p:taskExecutor-ref="taskExecutor">
        <property name="triggers">
            <list>
                <ref bean="remoteNotifierTrigger"/>
            </list>
        </property>
        <property name="configLocation" value="classpath:/quartz.properties"/>
    </bean>

    <beans profile="test">
        <!-- Datasource -->
        <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"  destroy-method="close">
            <property name="driverClassName" value="${hsqldb.database.driver}" />
            <property name="url" value="${hsqldb.database.url}" />
            <property name="username" value="${hsqldb.database.username}" />
            <property name="password" value="${hsqldb.database.password}" />
            <property name="maxActive" value="20"/>
            <property name="maxIdle" value="8"/>
            <property name="initialSize" value="4"/>
        </bean>

        <!-- JPA Persistence provider -->
        <bean id="hibernateJpaAdapter"
              class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
            <property name="database" value="${hsqldb.hibernate.sql.db}" />
            <property name="databasePlatform" value="${hsqldb.hibernate.sql.dialect}" />
            <property name="generateDdl" value="${hsqldb.hibernate.sql.generateddl}" />
            <property name="showSql" value="${hsqldb.hibernate.sql.show}"/>
        </bean>

    </beans>

    <beans profile="perf">
        <!-- Datasource -->
        <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"  destroy-method="close">
            <property name="driverClassName" value="${mysql.database.driver}" />
            <property name="url" value="${mysql.database.url}" />
            <property name="username" value="${mysql.database.username}" />
            <property name="password" value="${mysql.database.password}" />
            <property name="maxActive" value="20"/>
            <property name="maxIdle" value="8"/>
            <property name="initialSize" value="4"/>
        </bean>

        <!-- JPA Persistence provider -->
        <bean id="hibernateJpaAdapter"
              class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
            <property name="database" value="${mysql.hibernate.sql.db}" />
            <property name="databasePlatform" value="${mysql.hibernate.sql.dialect}" />
            <property name="generateDdl" value="${mysql.hibernate.sql.generateddl}" />
            <property name="showSql" value="${mysql.hibernate.sql.show}"/>
        </bean>

    </beans>

</beans>