//******************************************
//CUSTOMER
//******************************************

window.Customer = Backbone.Model.extend({
	urlRoot: "bank/customers",
	initialize: function () {},
	defaults: {
    	id: null,
        name: ""        
    }
});

window.CustomerCollection = Backbone.Collection.extend({
    model: Customer,
    url: "bank/customers"    
});





//******************************************
//ACCOUNT
//******************************************

window.Account = Backbone.Model.extend({

    urlRoot: "bank/accounts",

    initialize: function () {
        this.validators = {};

        this.validators.accountNumber = function (value) {
            return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a account number"};
        };
        
        this.validators.accountNumber = function (value) {
            return value.length <= 12  ? {isValid: true} : {isValid: false, message: "Account number must be max 12 char lenght"};
        }; 
        
        this.validators.accountNumber = function (value) {
        	return $.isNumeric(value)  ? {isValid: true} : {isValid: false, message: "Account number must be numeric"};
        }; 
        
        this.validators.currency = function (value) {
        	return $.trim(value) != '' ? {isValid: true} : {isValid: false, message: "Please select a Currency"};
        };        

    },

    validateItem: function (key) {
        return (this.validators[key]) ? this.validators[key](this.get(key)) : {isValid: true};
    },

    // TODO: Implement Backbone's standard validate() method instead.
    validateAll: function () {

        var messages = {};

        for (var key in this.validators) {
            if(this.validators.hasOwnProperty(key)) {
                var check = this.validators[key](this.get(key));
                if (check.isValid === false) {
                    messages[key] = check.message;
                }
            }
        }

        return _.size(messages) > 0 ? {isValid: false, messages: messages} : {isValid: true};
    },

    defaults: {
    	id: null,
        accountNumber: "",
        customerId: "",
        iban: "",
        currency: "",
        balance: 0,
        accountBal: 0,
        accountDate: new Date()        
    },
    
    parse:function (response) {
    	//response.accountDate=new Date(response.accountDate);
        return response;
    }
});

window.AccountCollection = Backbone.Collection.extend({
    model: Account
});






//******************************************
//MOVEMENT
//******************************************

window.Movement = Backbone.Model.extend({
	urlRoot: function(){
		return "bank/accounts/"+this.get("accountId")+"/movements";
	},
	
		
	initialize: function () {
        this.validators = {};

        this.validators.description = function (value) {
            return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a valid description"};
        };

    },

    validateItem: function (key) {
        return (this.validators[key]) ? this.validators[key](this.get(key)) : {isValid: true};
    },

    // TODO: Implement Backbone's standard validate() method instead.
    validateAll: function () {

        var messages = {};

        for (var key in this.validators) {
            if(this.validators.hasOwnProperty(key)) {
                var check = this.validators[key](this.get(key));
                if (check.isValid === false) {
                    messages[key] = check.message;
                }
            }
        }

        return _.size(messages) > 0 ? {isValid: false, messages: messages} : {isValid: true};
    },
	
	
	defaults: {
		movementId:null,
	    accountId:null,
	    accountDate:new Date(),
	    direction:0,
	    amount:"",
	    currency:"",
	    recipient:"",
	    dispDate:new Date(),
	    description:"",
	    type:"",
	    state:"",
	    sign:"",
	    cross:false,
	    minAccountBal:"",
	    minAccountDate:"",
	    maxAccountBal:"",
	    maxAccountDate:""
    },
    parse:function (response) {
        //console.log(response); 
        response.id=response.movementId;
        //response.accountDate=new Date(response.accountDate);
        //response.dispDate=new Date(response.dispDate);
        response.sign=(response.direction==0) ? '-' : "+";
        return response;
    }
});

window.MovementCollection = Backbone.Collection.extend({
    model: Movement
});





//******************************************
//TASK
//******************************************

window.Task = Backbone.Model.extend({
	urlRoot: "asynch/processes",
	initialize: function () {},
	defaults: {
    	taskId: 	null  ,
    	read:		false ,
    	result:		""    ,
    	important:	false
    }
});

window.TaskCollection = Backbone.Collection.extend({
    model: Task,
    url: "asynch/processes"
});

