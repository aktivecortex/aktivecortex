window.HeaderView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

    render: function () {
        $(this.el).html(this.template());
        
        for (var i = 0; i < usrs.length; i++) {
        	$('#cid',this.el).append(new CustomerItemView({model: usrs[i]}).render().el);
        	
    	}
        
        return this;
    },

    events: {
        "change" : "change"
    },
    
    change:function(event){    	
    	 var target = event.target;      	
    	 app.navigate('use/'+target.value,  {trigger: true, replace: true});
    },
    
    selectMenuItem: function (menuItem) {
        $('.nav li').removeClass('active');
        if (menuItem) {
            $('.' + menuItem).addClass('active');
        }
    }

});

window.CustomerItemView = Backbone.View.extend({

    tagName: "option",

   // className: "customer",

    initialize: function () {
       
    },

    render: function () {
    	$(this.el).val(this.model.id);
    	if(CURRENT_USER == this.model.id){
    		$(this.el).attr("selected","selected");
    	}    	
    	$(this.el).html(this.model.nome);
        return this;
    }

});