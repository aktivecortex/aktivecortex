;$(function(){

        window.Notifications  = window.Notifications || {};

        // The process model
        Notifications.Process = Backbone.Model.extend({

            EMPTY_DESC: 'Missing step description...',
            MISSING_I18N_DESC: 'Missing translation...',

            url: function () {
                return this.id ? 'async/processes' + this.id : 'async/processes';
            },

            defaults: {
                progress: {
                    description: '',
                    completion: 0
                }
            },

            // Put here all init logic (i18n etc.)
            initialize: function () {
                var i18Name = I18n.t("process." + this.get('type') + ".name");
                var name = (i18Name.indexOf("[missing") == 0) ?  this.get('type') : i18Name;
                this.set('name', name.replace('Command', ''));
                var progress = this.get('progress');
                var percent = progress.completion;
                if (!progress.name) {
                    if (1 == percent.toFixed(0)) {
                        var i18Desc = I18n.t("process." + this.get('type') + ".successCaseText");
                        var description = (i18Desc.indexOf("[missing") == 0) ?  this.MISSING_I18N_DESC : i18Desc;
                        progress.description = description;
                    } else {
                        progress.description = this.EMPTY_DESC;
                    }
                } else {
                    // translate here i18n description
                    var i18Desc = I18n.t("process." + this.get('type') + ".progress." + progress.name);
                    var description = (i18Desc.indexOf("[missing") == 0) ?  this.MISSING_I18N_DESC : i18Desc;
                    progress.description = description;
                }
                progress.completion = utils.toPercent(percent);
            },

            clear: function () {
                this.destroy();
                this.view.remove();
            }

        });

        // Processes collection
        Notifications.ProcessList = Backbone.Collection.extend({

            model: Notifications.Process,

            url: function () {
                return 'async/processes';
            },

            add: function(model){
                var duplicates = this.filter(function(_model){
                    return _model.id === model.id;
                });
                if (! _(duplicates).isEmpty()) {
                    this.remove(duplicates);
                }
                Backbone.Collection.prototype.add.call(this, model);
            },


            running: function () {
                return this.filter(function (process) {
                    return process.get('state') != 'COMPLETED';
                });
            },

            completed: function () {
                return this.filter(function (process) {
                    return process.get('state') == 'COMPLETED';
                });
            },

            successed: function () {
                return this.filter(function (process) {
                    return process.get('state') == 'COMPLETED' && process.get('result') == 'SUCCESS' ;
                });
            },

            failed: function () {
                return this.filter(function (process) {
                    return process.get('state') == 'COMPLETED' && process.get('result') == 'FAILURE' ;
                });
            },

            comparator: function (process) {
                return process.get('id');
            }
        });

        Notifications.list = new Notifications.ProcessList;

        Notifications.ProcessLinkView = Backbone.View.extend({

            tagName: 'li',
            template:  _.template($('#template_ProcessLinkView').html()),

            initialize: function () {
                _.bindAll(this, 'render');
            },

            render: function () {
                $(this.el).html(this.template());
                return this;
            }

        });

        Notifications.ProcessView = Backbone.View.extend({

            tagName: 'li',
            template:  _.template($('#template_ProcessView').html()),

            initialize: function () {
                _.bindAll(this, 'render');
                this.model.bind('remove', this.remove);
                this.model.bind('change', this.render);
                this.model.view = this;
            },

            render: function () {
                var model = this.model.toJSON();
                $(this.el).html(this.template(model));
                this.setContent();
                return this;
            },

            setContent: function () {
                var completion = this.model.get('progress').completion;
                this.$('.bar').css('width', completion);
                if ('100%' === completion) {
                    this.$('.bar').addClass('bar-success');
                }
                if ('FAILURE' === this.model.get('result')) {
                    this.$('.bar').addClass('bar-danger');
                }
            },

            remove: function () {
                $(this.el).remove();
            },

            clear: function () {
                this.model.clear();
            }
        });

        Notifications.ProcessListView = Backbone.View.extend({

            template:  _.template($('#template_ProcessStatsView').html()),
            initialized: false,

            initialize: function () {
                _.bindAll(this, 'render');

                Notifications.list.bind('add', this.render);
                Notifications.list.bind('remove', this.render);
                Notifications.list.bind('reset', this.render);
                Notifications.list.bind('all', this.render);

//                Notifications.list.fetch();
            },

            render: function () {
                var remaining = Notifications.list.running().length;
                var done = Notifications.list.successed().length;
                var failed = Notifications.list.failed().length;
                if ($('#process-stats-ins').length > 0) {
                    $('#process-stats-ins').replaceWith(this.template({
                        remaining: remaining,
                        done: done,
                        failed: failed
                    }));
                }
                else {
                    if (! (this.initialized)) {
                        this.initialized = true;
                        this.el = $('#process-stats');
                    }
                    $(this.el).prepend(this.template({
                        remaining: remaining,
                        done: done,
                        failed: failed
                    }));
                }

                // render Process Items
                $('#process-list').empty();
                Notifications.list.each(function(process){
                    var li = new Notifications.ProcessView({model: process});
                    $('#process-list').append(li.render().el);
                });

                // render link
                var lastLi = new Notifications.ProcessLinkView();
                $('#process-list').append(lastLi.render().el);
            },

            clearCompleted: function () {
                _.each(Notifications.list.completed(), function (process) {
                    process.clear();
                });

                return false;
            }
        });

});
