window.TransferView = Backbone.View.extend({

    initialize: function () { 
        this.render();
    },

    render: function () {
    	utils.hideAlert();
        $(this.el).html(this.template(this.model.toJSON()));
        $('.header', this.el).append(new MovementHeadView({model: this.options.parent}).render().el);
        $('.header', this.el).append("<ul class='nav nav-tabs'><li><a href='#editAccount/"+this.options.parent.id+"'>"+I18n.t("label.movement.list")+"</a></li><li class='active'><a href='#addMovement/"+this.options.parent.id+"'>"+I18n.t("label.movement.new")+"</a></li></ul>");
        return this;
    },

    events: {
        "change"        	: "change",
        "click .execute"   	: "beforeAct"      
    },

    change: function (event) {
        // Remove any existing alert message
        utils.hideAlert();

        // Apply the change to the model
        var target = event.target;        
                        
        if("type"==target.name && "BON" == target.value){
        	$('#recipient').attr('disabled', false);
        }else if("type"==target.name && "BON" != target.value){
        	$('#recipient').attr('disabled', true);
        }
        
        var change = {};

//       if("amount"==target.name){
//    	   var value = parseFloat(this.value).toFixed(2);
//    	   $('#amount').val(value);
//    	   change[target.name] = value;
//       }else{
    	   change[target.name] = target.value;
       //}
        
        
        this.model.set(change);

        // Run validation rule (if any) on changed item
        var check = this.model.validateItem(target.id);
        if (check.isValid === false) {
            utils.addValidationError(target.id, check.message);
        } else {
            utils.removeValidationError(target.id);
        }
    },

    
    
    
    beforeAct: function () {
        var check = this.model.validateAll();
        if (check.isValid === false) {
            utils.displayValidationErrors(check.messages);
            return false;
        }
                        
        this.action();
        return false;
    },
   
    
    action: function () { 
    	this.model.save(null, {
    		success: function (model,response) {
    			routeList=function(process){
    				app.navigate('editAccount/'+ model.get('accountId'),{trigger: true, replace: true}); 
    				utils.showProcessAlert(process);
    			};
    			$('.execute').hide();
    			utils.showAlert(I18n.t("info.command.disposed"), I18n.t("info.command.wait"), 'alert-info');
    			eventManager.subscribeProcess(response,routeList,routeList);
    		},
    		error: function () {
    			utils.showAlert(I18n.t("error.head"),I18n.t("error.message"), 'alert-error');
    		}
    	});
    }
});