I18n.translations["en"] = {
    "label": {
        "common": {
            "save": "Save",
            "execute": "Execute"
        }, "account": {
            "list": "Owner accounts",
            "new": "New account",
            "number": "Account Number",
            "iban": "Iban",
            "currency": "Currency",
            "amount": "Available amount",
            "balance": "Total balance",
            "balanceat": "Balance at date "
        }, "movement": {
            "list": "Movement list",
            "new": "New disposition",
            "date": "Date",
            "currdate": "Currency date",
            "type": "Tipology",
            "description": "Description",
            "amount": "Amount",
            "recipient": "Recipient"
        }
    }, "option": {
        "movement": {
            "type": {
                "DEP": "Deposit",
                "PRE": "Withdrawal",
                "BON": "Money transfer",
                "GRA": "Account distribution"
            }
        }
    }, "info": {
        "head": "",
        "command": {
            "disposed": "Your request has been successfully submitted",
            "wait": "Please wait for the execution notice"
        }
    }, "error": {
        "head": "Error",
        "message": "An error occurred while trying to submit your request",
        "command": {}
    }, "process": {
        "CreateBankAccountCommand": {
            successCaseText: "Account successfully added",
            failureCaseText: "An error has occurred while try to add an account"
        },
        "DeleteBankAccountCommand": {
            successCaseText: "Account successfully removed",
            failureCaseText: "An error has occurred while try to remove an account"
        },
        "DisposeDepositCommand": {
            successCaseText: "Deposit successfully committed",
            failureCaseText: "An error has occurred while try to execute a deposit"
        },
        "DisposeWithdrawalCommand": {
            successCaseText: "Withdrawal successfully committed",
            failureCaseText: "An error has occurred while try to execute a withdrawal"
        },
        "ConfirmWithdrawalCommand": {
            successCaseText: "Giro transfer successfully committed",
            failureCaseText: "An error has occurred while try to execute an giro transfer"
        },
        "MoneyTransferCommand": {
            successCaseText: "Bank Transfer successfully completed",
            failureCaseText: "An error has occurred while try to execute a Bank Transfer!",
            progress: {
                BankTransferRequest: "Transfer order accepted",
                WithdrawalOrder: "Withdrawal order accepted",
                DepositOrder: "Deposit order accepted",
                Confirmation: "Orders confirmed"
            }
        },
        "LaunchSuiteCommand": {
            successCaseText: "Test suite successfully completed",
            failureCaseText: "An error has occurred while try to start a test suite",
            progress: {
                TestSuiteLaunch: "Suite Launched",
                TestsLaunch: "Tests Launched",
                TestsEnd: "Tests Completed",
                SuiteEnd: "Suite Completed"
            }
        }
    }
};