
I18n.translations = {};

var usrs=[{id:'0001',nome:'Mark Brown'},
          {id:'0002',nome:'Linda Red'},
          {id:'0003',nome:'Jason Black'}
		 ];
var CURRENT_USER=usrs[0].id;

var movtype=[{id:'DEP',description:'Deposito'},
             {id:'PRE',description:'Prelievo'},
             {id:'BON',description:'Bonifico'},
             {id:'GRA',description:'Giroconto'}
            ];


var eventManager = _.extend({
	subscribeProcess: function(processId, okCall, koCall){
		
		function handle(callback) {
			window.setTimeout( function(){
				(callback)();
	    	}, 1000);			
		}

		this.on("ok_"+processId, okCall);

		this.on("ko_"+processId, koCall);

//        this.on("ff_" + processId, utils.notifyProgress);

	}
}, Backbone.Events);

var AppRouter = Backbone.Router.extend({

    routes: {
    	""						: "list",       
    	"use/:cid"				: "select",       
        "allAccount"			: "list",       
        "allAccount/:page"		: "list",       
        "addAccount"     		: "addAccount",
        "editAccount/:id"	    : "movementList",
        "editAccount/:id/:page" : "movementList",
        "addMovement/:id"  		: "addMovement"
       
    },



    initialize: function () {
        this.headerView = new HeaderView();
        $('.header').html(this.headerView.el);
    },

       
    userSelector: function(page) {
		 if (!this.selectUserView) {
	         this.selectUserView = new SelectUserView();
	     }
	     $('#content').html(this.selectUserView.el);
    },
    
    select: function(cid) {
        if(cid){
        	CURRENT_USER=cid;
        }
        var accountList = new AccountCollection({customerId:CURRENT_USER});
        accountList.url='bank/customers/'+CURRENT_USER+'/accounts';
        accountList.fetch({success: function(){
        	if(accountList.length==0){
            	 var account = new Account({customerId: CURRENT_USER});
                 $('#content').html(new AccountView({model: account}).el);
            }else{
            	$("#content").html(new AccountListView({model: accountList, page: 1}).el);
            }
        }});
        
       
    },
    
	list: function(page) {
        var p = page ? parseInt(page, 10) : 1;
        var accountList = new AccountCollection({customerId:CURRENT_USER});
        accountList.url='bank/customers/'+CURRENT_USER+'/accounts';
        accountList.fetch({success: function(){
            $("#content").html(new AccountListView({model: accountList, page: p}).el);
        }});
        this.headerView.selectMenuItem('home-menu');
    },

    addAccount: function() {
        var account = new Account({customerId: CURRENT_USER});
        $('#content').html(new AccountView({model: account}).el);
        utils.hideAlert();    	
	},

	movementList: function (id,page) {			
		var p = page ? parseInt(page, 10) : 1;
		var account = new Account({id: id});
		account.fetch({success: function(){
	    		var movementList=new MovementCollection();
	    		movementList.url='bank/accounts/'+id+'/movements';	
	    		movementList.fetch({success: function(){
	    			 $("#content").html(new MovementListView({model: movementList, parent:account, page:p}).el);	    			 
	    		}});
	     }});       
    },
    
    addMovement: function (id) {
        var account = new Account({id: id});
        account.fetch({success: function(model){
    		var mov=new Movement({accountId: id,currency:account.get('currency')});
            $("#content").html(new TransferView({model: mov, parent:account}).el);
        }});   
        utils.hideAlert(); 
	}
    	   	
});

utils.loadTemplate(['HeaderView', 
                   'AccountView','AccountListView', 'AccountListItemView',
                   'MovementListView','MovementHeadView','MovementListItemView','MovementFootView','TransferView'], function() {
	
    app = new AppRouter();  
    Backbone.history.start();
    window.NotificationInstance = new Notifications.ProcessListView;


});

$('.alert .close').bind("click",function(){
	utils.hideAlert();
});


(function push() {

	var detectedTransport = null;
	var socket = $.atmosphere;
	var subSocket;

	function subscribe() {
		var request = {
				url : 'notification/processes/',
				logLevel: 'debug',
				transport : "streaming", //this seem to work well with both chrome, ie, and firefox
                trackMessageLength : true,
				fallbackTransport : "long-polling"
		};
		
	    request.onOpen = function(response) {
	    	console.log('Atmosphere connected using ' + response.transport);
	    };
	    
	    request.onError = function(response) {
	    	console.log('Sorry, but there\'s some problem with your socket or the server is down');
	     };
	    
		request.onMessage = function(response) {
			//console.log('status:'+response.status);
			detectedTransport = response.transport;
			if (response.status == 200) {
				var message = response.responseBody;
                try {
                    var process = JSON.parse(message);

                    if (process.read == false) {
                        if (process.result == 'SUCCESS') {
//                            eventManager.trigger('ff_' + process.id, process);
                            utils.notifyProgress(process);
                            eventManager.trigger('ok_' + process.id, process);
                        } else if (process.result == 'FAILURE') {
//                            eventManager.trigger('ff_' + process.id, process);
                            utils.notifyProgress(process);
                            eventManager.trigger('ko_' + process.id, process);
                            console.log('failure message received for process:' + process);
                        }  else if (process.state == 'STARTED') {
//                             eventManager.trigger('ff_' + process.id, process);
                             utils.notifyProgress(process);
                        }
                    }
                } catch (err) {
                    console.log('unexpected response body: ' + message + '\nerror: ' + err);
                }
			}
		};
		
		subSocket = socket.subscribe(request);
	}

	function unsubscribe() {
		socket.unsubscribe();
	}
	
	subscribe();
	
})();

//(function poll(){
//setTimeout(function(){	
//    $.ajax({ 
//    	url: "asynch/processes/events",
//    	dataType: "json",  
//    	success: function(data){
//    		var count=0; 		
//    		
//    		$.each(data, function(index, _task) {
//    			var task=_task.data;
//    			
//    			if (task.read == false) {
//    				count++; 
//         			if (task.result == 'SUCCESS' && task.important) {
//         				 events.trigger("ok_"+task.id,task);
//         			}else if (task.result == 'FAILURE') {
//         				utils.notify(title,text,"alert-error",false);
//         				events.trigger("ko_"+task.id,task);
//         			}    			
//         		}	    			
//    		 });
//    		
//    		if(count==0 && !utils.isAlertOfState("alert-error")){    				
//    			utils.hideAlert();
//    		}
//    		poll();
//		}
//	});
//},5000);	
//})();
