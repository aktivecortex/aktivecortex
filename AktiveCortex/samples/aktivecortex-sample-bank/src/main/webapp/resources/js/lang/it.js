//I18n.translations = {};
I18n.translations["it"] = {
	"label":{
		"common":{
			"save":"Salva",
			"execute":"Esegui"
		},"account":{
			"list":"Conti presenti",
			"new": "Nuovo conto",  
			"number": "Numero conto",   
			"iban": "Iban",     
			"currency": "Valuta",       
			"amount": "Saldo disponibile",     
			"balance": "Saldo contabile",   
			"balanceat": "Saldo al "  
		},"movement":{
			"list":"Lista movimenti",
			"new": "Nuova disposizione",
			"date": "Data",
			"currdate": "Data valuta",
			"type": "Tipologia",
			"description": "Descrizione",
			"amount": "Importo",
			"recipient": "Destinatario"
		}				
	},"option":{
		"movement":{
			"type":{
				"dep":"Versamento",
				"pre":"Prelievo",
				"bon":"Bonifico",
				"gra":"Giroconto"
			}
		}
	}
};