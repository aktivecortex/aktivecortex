window.utils = {

    // Asynchronously load templates located in separate .html files
    loadTemplate: function (views, callback) {

        var deferreds = [];

        $.each(views, function (index, view) {
            if (window[view]) {
                deferreds.push($.get('resources/tpl/' + view + '.html', function (data) {
                    window[view].prototype.template = _.template(data);
                }));
            } else {
                alert(view + " not found");
            }
        });

        $.when.apply(null, deferreds).done(callback);
    },

    require_template: function (templateName) {
        var template = $('#template_' + templateName);
        if (template.length === 0) {
            var tmpl_dir = 'resources/tpl';
            var tmpl_url = tmpl_dir + '/' + templateName + '.html';
            var tmpl_string = '';

            $.ajax({
                url: tmpl_url,
                method: 'GET',
                async: false,
                contentType: 'text',
                success: function (data) {
                    tmpl_string = data;
                }
            });

            $('head').append('<script id="template_' +
                templateName + '" type="text/template">' + tmpl_string + '<\/script>');
        }
    },


    displayValidationErrors: function (messages) {
        for (var key in messages) {
            if (messages.hasOwnProperty(key)) {
                this.addValidationError(key, messages[key]);
            }
        }
        this.showAlert('Warning!', 'Fix validation errors and try again', 'alert-warning');
    },

    addValidationError: function (field, message) {
        var controlGroup = $('#' + field).parent().parent();
        controlGroup.addClass('error');
        $('.help-inline', controlGroup).html(message);
    },

    removeValidationError: function (field) {
        var controlGroup = $('#' + field).parent().parent();
        controlGroup.removeClass('error');
        $('.help-inline', controlGroup).html('');
    },

    showAlert: function (title, text, klass) {
        $('.alert').removeClass("alert-error alert-warning alert-success alert-info");
        $('.alert').addClass(klass);
        $('.alert .content').html('<strong>' + title + '</strong><br>' + text);
        $('.alert').show();
    },

    showProcessAlert: function (process) {
        var text;
        var resource;
        var title;
        if (process.result == 'SUCCESS') {
            title = 'Success!';
            resource = I18n.t("process." + process.type + ".successCaseText");
            text = (resource == undefined) ? '' : resource;
			utils.showAlert(title, text, 'alert-success');
        } else if (process.result == 'FAILURE') {
            title = 'Error!';
            resource = I18n.t("process." + process.type + ".failureCaseText");
            text = (resource == undefined) ? '' : resource;
			utils.showAlert(title, text, 'alert-error');
        }
    },


    hideAlert: function () {
        $('.alert').hide();
    },

    isAlertHidden: function () {
        return !$('.alert').is(":visible");
    },

    isAlertOfState: function (klass) {
        return $('.alert').is(":visible") && klass == $('.alert').attr('class');
    },

    notify: function (title, text, klass, _stiky) {
        $('.alert').removeClass("alert-error alert-warning alert-success alert-info");
        $('.alert').addClass(klass);
        $('.alert .content').html('<strong>' + title + '</strong><br>' + text);
        $('.alert').show();
    },

    notifyProgress: function(process) {
        return Notifications.list.add(process);
    },
    getMovTypeDesc: function (type) {
        for (var i = 0; i < movtype.length; i++) {
            if (movtype[i].id == type) {
                return movtype[i].description;
            }
        }
        return "";
    },

// round decimal to percentage
    toPercent: function (percent) {
        var val = percent * 100;
        return val.toFixed(0) + '%';
    }

};



