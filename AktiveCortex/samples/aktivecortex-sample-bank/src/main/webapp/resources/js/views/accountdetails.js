window.AccountView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

    render: function () {
    	
        $(this.el).html(
        		this.template(
        				{model: 		this.model.toJSON(),
        			    currencyOptions: [{id: "EUR", name: "Euro"},
        			                      {id: "USD", name: "United States Dollars"}]
        				}));
        return this;
    },

    events: {
        "change"        : "change",
        "click .save"   : "beforeSave"
    },

    change: function (event) {
        // Remove any existing alert message
        utils.hideAlert();

        // Apply the change to the model
        var target = event.target;
        var change = {};
       
        change[target.name] = target.value;
        
        this.model.set(change);

        // Run validation rule (if any) on changed item
        var check = this.model.validateItem(target.id);
        if (check.isValid === false) {
            utils.addValidationError(target.id, check.message);
        } else {
            utils.removeValidationError(target.id);
        }
    },

    beforeSave: function () {
        var check = this.model.validateAll();
        if (check.isValid === false) {
            utils.displayValidationErrors(check.messages);
            return false;
        }
        // Upload picture file if a new file was dropped in the drop area
        this.saveAccount();
        return false;
    },

    saveAccount: function () {       
        this.model.save(null, {
            success: function (model,response) {            	 
            	routeList=function (process){
            		app.navigate('allAccount/1',{trigger: true, replace: true});             		
            		utils.showProcessAlert(process);
    			};
    			$('.save').hide();
    			utils.showAlert(I18n.t("info.command.disposed"), I18n.t("info.command.wait"), 'alert-info');
    			eventManager.subscribeProcess(response,routeList,routeList);
            },
            error: function () {
            	utils.showAlert(I18n.t("error.head"),I18n.t("error.message"), 'alert-error');
            }
        });
    }  

});