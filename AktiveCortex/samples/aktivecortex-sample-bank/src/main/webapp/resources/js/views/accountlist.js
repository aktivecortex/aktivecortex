window.AccountListView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

        
    render: function () {
    	var rows=10;
        var accounts = this.model.models;
        var len = accounts.length;
        var startPos = (this.options.page - 1) * rows;
        var endPos = Math.min(startPos + rows, len);

        $(this.el).html(this.template());
        
        for (var i = startPos; i < endPos; i++) {
            $('.item', this.el).append(new AccountListItemView({model: accounts[i]}).render().el);
        }

        if(len > 10){
        	//$(this.el).append(new Paginator({model: this.model, page: this.options.page}).render().el);
        	$(this.el).append(new Paginator({
        		model: this.model, 
        		page: this.options.page,
        		toLink:"allAccount",//
        		rows:rows
			}).render().el);
        }

        return this;
    }
    
    
});

window.AccountListItemView = Backbone.View.extend({

    tagName: "tr",

    initialize: function () {
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },
    
    events: {           
        "click .delete" 	: "deleteAccount"
    },
    
    deleteAccount: function () {
        this.model.destroy({
        	success: function (model,response) {
    			routeList=function (process){
    				app.navigate('allAccount/1',{trigger: true, replace: true});
    				utils.showProcessAlert(process);
    			};
    			utils.showAlert(I18n.t("info.command.disposed"), I18n.t("info.command.wait"), 'alert-info');
    			eventManager.subscribeProcess(response,routeList,routeList);
    			app.navigate('allAccount',{trigger: true, replace: true}); 
    		},error: function () {
    			utils.showAlert(I18n.t("error.head"),I18n.t("error.message"), 'alert-error');
            }
        });
        return false;
    }   

});