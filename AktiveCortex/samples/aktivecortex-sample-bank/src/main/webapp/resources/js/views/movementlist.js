window.MovementListView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

        
    render: function () {    	
    	
        var movements = this.model.models;
        var len = movements.length;
        
        var rows=10;
        
        var startPos = (this.options.page - 1) * rows;
        var endPos = Math.min(startPos + rows, len -1);

        $(this.el).html(this.template());
        
        $('.header', this.el).append(new MovementHeadView({model: this.options.parent}).render().el);
        $('.header', this.el).append("<ul class='nav nav-tabs'><li class='active'><a href='#editAccount/"+this.options.parent.id+"'>"+I18n.t("label.movement.list")+"</a></li><li><a href='#addMovement/"+this.options.parent.id+"'>"+I18n.t("label.movement.new")+"</a></li></ul>");
        
        
        for (var i = startPos; i < endPos; i++) { 
        	if(movements[i] !=null  && movements[i].get("type") != "###"){
        		$('.item', this.el).append(new MovementListItemView({model: movements[i]}).render().el);
        	}
        }
        
        if(movements[i]!=null && movements[len-1].get("type") == "###"){        	
        	$('.footer', this.el).append(new MovementFootView({model: movements[len-1]}).render().el);
        }
        
        if(len > rows){
        	$(this.el).append(new Paginator({
        		model: this.model, 
        		page: this.options.page,
        		toLink:"editAccount/"+this.options.parent.id,//allAccount
        		rows:rows
			}).render().el);
        }

        return this;
    }
});

window.MovementHeadView = Backbone.View.extend({
	
	initialize: function () {
		this.render();
	},
	
	
	render: function () {
		$(this.el).html(this.template(this.model.toJSON()));
		return this;
	}
});



window.MovementListItemView = Backbone.View.extend({

    tagName: "tr",

    initialize: function () {
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }

});

window.MovementFootView = Backbone.View.extend({
	
	initialize: function () {
		this.render();
	},
	
	
	render: function () {
		$(this.el).html(this.template(this.model.toJSON()));
		return this;
	}
});