
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Bank Sample</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="resources/css/bootstrap.css" rel="stylesheet">
        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>
        <link href="resources/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="resources/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/notifications.css" rel="stylesheet" type="text/css" />

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


    </head>

    <body data-spy="scroll" data-target=".subnav" data-offset="50">

        <div class="header"></div>    

        <div class="container">
            <div class="row">
                <div id="content" class="span12"></div>
            </div>

            <hr>

            <div class="row">
                <div class="span8">
                    <div class="alert alert-success hide" style="display: none">
                        <button class="close">&times;</button>
                        <div class="content"></div>
                    </div>
                </div>
            </div>

            <!-- 		<footer><p>&copy; Company 2012</p></footer> -->

        </div> <!-- /container -->



        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="resources/lib/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="resources/lib/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resources/lib/jquery.dateFormat-1.0.min.js"></script>
        <script type="text/javascript" src="resources/lib/jshashtable-2.1.js"></script>
        <script type="text/javascript" src="resources/lib/jquery.numberformatter-1.2.3.min.js"></script>

        <script type="text/javascript" src="resources/lib/jquery.atmosphere.js"></script>
        <script type="text/javascript" src="resources/lib/underscore-min.js"></script>
        <script type="text/javascript" src="resources/lib/backbone-min.js"></script>
        <script type="text/javascript" src="resources/lib/bootstrap.min.js"></script>
        <script type="text/javascript" src="resources/lib/lang.js"></script>
        <script type="text/javascript" src="resources/lib/i18n.js"></script>

        <!--datepicker-->
        <!--<script type="text/javascript" src="resources/lib/bootstrap-datepicker.js"></script> -->

        <script type="text/javascript" src="resources/js/models/models.js"></script>
        <script type="text/javascript" src="resources/js/views/accountdetails.js"></script>
        <script type="text/javascript" src="resources/js/views/accountlist.js"></script>
        <script type="text/javascript" src="resources/js/views/transferdetails.js"></script>
        <script type="text/javascript" src="resources/js/views/header.js"></script>
        <script type="text/javascript" src="resources/js/views/paginator.js"></script>
        <script type="text/javascript" src="resources/js/views/movementlist.js"></script>
        <script type="text/javascript" src="resources/js/views/select.js"></script>
        <script type="text/javascript" src="resources/js/utils.js"></script>
        <script type="text/javascript" src="resources/js/templates.js"></script>
        <script type="text/javascript" src="resources/js/main.js"></script>
        <script type="text/javascript" src="resources/js/lang/en.js"></script>
        <script type="text/javascript" src="resources/js/notification.js"></script>
    </body>
</html>