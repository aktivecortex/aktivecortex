/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.bankaccount;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

public class BankAccountWithdrawalDisposedEvent extends AbstractBankAccountEvent {	
	private static final long serialVersionUID = 1L;
	
	private DateTime dispDate; 
	private String type; 
	private String description; 
	private String currency; 
	private BigDecimal amount; 
	private BigDecimal fee;
	private String movementId; 
	private DateTime currencyDate;
	
	public BankAccountWithdrawalDisposedEvent(DateTime dispDate, String type,
			String description, String currency, BigDecimal amount,
			BigDecimal fee, String movementId, DateTime currencyDate) {
		super();
		this.dispDate = dispDate;
		this.type = type;
		this.description = description;
		this.currency = currency;
		this.amount = amount;
		this.fee = fee;
		this.movementId = movementId;
		this.currencyDate = currencyDate;
	}

	public DateTime getDispDate() {
		return dispDate;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public String getCurrency() {
		return currency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public String getMovementId() {
		return movementId;
	}

	public DateTime getCurrencyDate() {
		return currencyDate;
	}

	@Override
	protected Map<String, Object> getSignificantFields() {
		Map ret=new HashMap<String, Object>();
		ret.put("accountId", super.getAccountId());
		ret.put("type", type);
		ret.put("dispDate", dispDate);
		return ret;
	}



	

}
