/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.service.bankaccount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class DisposeDepositCommand extends AbstractBankAccountCommand {
	private static final long serialVersionUID = 1L;
	
	private String movementId; 
	
	private Date dispDate; 
	private String type; 
	private String description; 
	private String currency; 
	private BigDecimal amount; 
	private BigDecimal fee;
	private Date currencyDate;
	
	public DisposeDepositCommand(String accountId, String movementId,
			Date dispDate, String type, String description, String currency,
			BigDecimal amount, BigDecimal fee, Date currencyDate) {
		super(accountId);
		this.movementId = movementId;
		this.dispDate = dispDate;
		this.type = type;
		this.description = description;
		this.currency = currency;
		this.amount = amount;
		this.fee = fee;
		this.currencyDate = currencyDate;
	}
	public String getMovementId() {
		return movementId;
	}
	public Date getDispDate() {
		return dispDate;
	}
	public String getType() {
		return type;
	}
	public String getDescription() {
		return description;
	}
	public String getCurrency() {
		return currency;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public BigDecimal getFee() {
		return fee;
	}
	public Date getCurrencyDate() {
		return currencyDate;
	}
	
	@Override
	public Map<String, Object> getSignificantFields() {
		Map ret=new HashMap<String, Object>();
		ret.put("accountId", getAccountId());			
		ret.put("movementId", movementId);			
		return ret;
	}
}
