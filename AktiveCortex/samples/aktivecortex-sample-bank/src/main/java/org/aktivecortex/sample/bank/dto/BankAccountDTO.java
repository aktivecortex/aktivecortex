/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankAccount", propOrder = {
	    "id",
	    "customerId",
	    "accountNumber",
	    "iban",
	    "currency",
	    "balance",
	    "accountBal",
	    "accountDate"	    	    
	})
public class BankAccountDTO implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String id;
	
	@XmlElement(required = true)
	private String customerId;
	
	@XmlElement(required = true)
	private String accountNumber;
	
	@XmlElement(required = true)
	private String iban;
	
	@XmlElement(required = true)
	private String currency;
	
	@XmlElement(required = true)
	private Double balance;
	
	@XmlElement(required = true)
	private Double accountBal;
	
	@XmlElement(required = true)
	private Date accountDate;
	
	
	
	

	public BankAccountDTO() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getAccountBal() {
		return accountBal;
	}

	public void setAccountBal(Double accountbal) {
		this.accountBal = accountbal;
	}

	public Date getAccountDate() {
		return accountDate;
	}

	public void setAccountDate(Date accountDate) {
		this.accountDate = accountDate;
	}
	
	
	
	
	
}
