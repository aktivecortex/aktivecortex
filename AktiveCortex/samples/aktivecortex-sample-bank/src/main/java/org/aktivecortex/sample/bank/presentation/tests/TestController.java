/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.presentation.tests;

import com.eaio.uuid.UUID;
import org.aktivecortex.api.audit.SecurityContext;
import org.aktivecortex.api.notification.Process;
import org.aktivecortex.api.notification.support.Observable;
import org.aktivecortex.core.notification.ProcessImpl;
import org.aktivecortex.core.notification.ResultImpl;
import org.aktivecortex.core.notification.ProgressEvaluatorBuilder;
import org.aktivecortex.sample.bank.service.tests.EndSuiteCommand;
import org.aktivecortex.sample.bank.service.tests.EndTestCommand;
import org.aktivecortex.sample.bank.service.tests.LaunchSuiteCommand;
import org.aktivecortex.sample.bank.service.tests.LaunchTestCommand;
import org.aktivecortex.web.commandhandling.WebCommandGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
@RequestMapping(value = "/test")
public class TestController implements Observable {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private WebCommandGateway gateway;

    @Autowired
	private SecurityContext ctx;

    private AtomicInteger counter = new AtomicInteger();

	private Set<PropertyChangeListener> listeners = new CopyOnWriteArraySet<PropertyChangeListener>();

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		if (listeners.add(listener)) {
            LOGGER.info("Listener added: {}", listener.toString());
		}
	}

	@Override
	public boolean removePropertyChangeListener(PropertyChangeListener listener) {
		if (listeners.remove(listener)) {
            LOGGER.info("Listener removed: {}", listener.toString());
			return true;
		}
		return false;
	}

	@RequestMapping(value = "process", method = RequestMethod.GET)
	public ResponseEntity<Process> createProcess(HttpServletRequest request) throws Exception {
		Process process = new ProcessImpl().withId(String.valueOf(counter.incrementAndGet()))
                .withSessionId(ctx.getSessionId()).withResult(ResultImpl.SUCCESS);
		PropertyChangeEvent evt = new PropertyChangeEvent("PROCESS_SERVICE", "process", null, process);
		evt.setPropagationId(process.getSessionId());
		for (PropertyChangeListener listener : listeners) {
			listener.propertyChange(evt);
		}
		return new ResponseEntity<Process>(process, HttpStatus.OK);
	}

    @RequestMapping(value = "stress/{testCount}/{iterations}", method = RequestMethod.GET)
    public ResponseEntity<String> launchStressTest(
            @PathVariable Integer testCount,
            @PathVariable Integer iterations,
            HttpServletRequest request) {
        String id = new UUID().toString();
        LaunchSuiteCommand cmd = new LaunchSuiteCommand(id, testCount, iterations);
        return gateway.send(request, ProgressEvaluatorBuilder.newBuilder()
                .addStep("TestSuiteLaunch", "Suite Launched", 0.01)
                .composedBy(LaunchSuiteCommand.class, 1)
                .addStep("TestsLaunch", "Tests Launched", 0.55)
                .composedBy(LaunchTestCommand.class, testCount)
                .addStep("TestsEnd", "Tests Completed", 0.39)
                .composedBy(EndTestCommand.class, testCount)
                .addStep("SuiteEnd", "Suite Completed", 0.05)
                .composedBy(EndSuiteCommand.class, 1)
                .build(),
                cmd);
    }

}
