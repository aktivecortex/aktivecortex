/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.service.tests;

import org.aktivecortex.core.commandhandling.support.CommandHandlerUtils;
import org.aktivecortex.sample.bank.domain.tests.Test;
import org.aktivecortex.sample.bank.domain.tests.TestSuite;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.domain.AggregateIdentifier;
import org.axonframework.repository.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class TestCommandHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestCommandHandler.class);

    private Repository<Test> testRepository;

    private Repository<TestSuite> suiteRepository;

    @Autowired
    @Qualifier("testRepository")
    public void setTestRepository(Repository<Test> testRepository) {
        this.testRepository = testRepository;
    }

    @Autowired
    @Qualifier("testSuiteRepository")
    public void setSuiteRepository(Repository<TestSuite> suiteRepository) {
        this.suiteRepository = suiteRepository;
    }

    @CommandHandler
    public void onLaunchSuiteCommand(LaunchSuiteCommand cmd) {
        AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getTestSuiteId());
        TestSuite suite = new TestSuite(id, cmd.getTestCount(), cmd.getIterations());
        suiteRepository.add(suite);
        LOGGER.info("started test-suite id: {}", id);
    }

    @CommandHandler
    public void onLaunchTestCommand(LaunchTestCommand cmd) {
        AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getTestId());
        Test test = new Test(id, cmd.getTestSuiteId(), cmd.getIterations());
        testRepository.add(test);
    }

    @CommandHandler
    public void onEndTestCommand(EndTestCommand cmd) {
        AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getTestId());
        Test test = testRepository.load(id);
        test.delete();
    }

    @CommandHandler
    public void onEndSuiteCommand(EndSuiteCommand cmd) {
        AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getTestSuiteId());
        TestSuite suite = suiteRepository.load(id);
        suite.delete();
        LOGGER.info("ended test-suite id: {}", id);
    }

}
