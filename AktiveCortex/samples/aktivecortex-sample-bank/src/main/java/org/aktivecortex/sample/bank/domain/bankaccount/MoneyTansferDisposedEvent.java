/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.bankaccount;

import java.math.BigDecimal;

import org.joda.time.DateTime;

public class MoneyTansferDisposedEvent extends AbstractBankAccountEvent {	
	private static final long serialVersionUID = 1L;
	
	private String toAccount;
	
	private String movementId;
	private String movementType;
	private String description;
	private String currency; 
	private BigDecimal amount; 	
	private DateTime transferDate;
	
	public MoneyTansferDisposedEvent(String toAccount, String movementId,
			String movementType, String description, String currency,
			BigDecimal amount, DateTime transferDate) {
		super();
		this.toAccount = toAccount;
		this.movementId = movementId;
		this.movementType = movementType;
		this.description = description;
		this.currency = currency;
		this.amount = amount;
		this.transferDate = transferDate;
	}
	public String getToAccount() {
		return toAccount;
	}
	public String getMovementId() {
		return movementId;
	}
	public String getMovementType() {
		return movementType;
	}
	public String getDescription() {
		return description;
	}
	public String getCurrency() {
		return currency;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public DateTime getTransferDate() {
		return transferDate;
	}

	
	
}
