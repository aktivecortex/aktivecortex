/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.presentation.bankaccount;

import com.eaio.uuid.UUID;
import org.aktivecortex.api.notification.ProgressEvaluator;
import org.aktivecortex.core.notification.ProgressEvaluatorBuilder;
import org.aktivecortex.sample.bank.dao.bankaccount.BankAccountDao;
import org.aktivecortex.sample.bank.dto.AccountMovementDTO;
import org.aktivecortex.sample.bank.dto.BankAccountDTO;
import org.aktivecortex.sample.bank.service.bankaccount.*;
import org.aktivecortex.web.commandhandling.WebCommandGateway;
import org.aktivecortex.web.presentation.support.RestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Controller
@RequestMapping(value = "/bank")
public class BankAccountController {

    private static final String LOG_SUCCESS_MSG = "Movement executed successfully";
    private static final ProgressEvaluator BANK_TRANSFER_EVAL = ProgressEvaluatorBuilder.newBuilder()
            .addStep("BankTransferRequest", "Transfer order accepted", 0.25)
            .composedBy(MoneyTransferCommand.class, 1)
            .addStep("WithdrawalOrder", "Withdrawal order accepted", 0.25)
            .composedBy(DisposeWithdrawalCommand.class, 1)
            .addStep("DepositOrder", "Deposit order accepted", 0.25)
            .composedBy(DisposeDepositCommand.class, 1)
            .addStep("Confirmation", "Orders confirmed", 0.25)
            .composedBy(ConfirmWithdrawalCommand.class, 1)
            .composedBy(ConfirmDepositCommand.class, 1)
            .build();
    @Autowired
    private WebCommandGateway gateway;

    @Autowired
    private BankAccountDao bankAccountDao;


    @RequestMapping(value = "accounts", method = RequestMethod.GET)
    public
    @ResponseBody
    List<BankAccountDTO> listAccounts() {
        List<BankAccountDTO> rend = bankAccountDao.findAll();
        return rend;
    }

    @RequestMapping(value = "customers/{customerId}/accounts", method = RequestMethod.GET)
    public
    @ResponseBody
    List<BankAccountDTO> listAccounts(@PathVariable("customerId") String customerId) {
        return bankAccountDao.findAll(customerId);
    }


    @RequestMapping(value = "accounts/{accountId}", method = RequestMethod.GET)
    public
    @ResponseBody
    BankAccountDTO find(@PathVariable("accountId") String accountId) {
        return bankAccountDao.findById(accountId);
    }

    @RequestMapping(value = "accounts", method = RequestMethod.POST)
    public ResponseEntity<String> crea(
            @RequestBody BankAccountDTO dto,
            HttpServletRequest request) {
        String ac = StringUtils.leftPad(dto.getAccountNumber(), 12, "0");
        String iban = "IT02L".concat("30500").concat("00100").concat(ac);
        String id = new UUID().toString();
        CreateBankAccountCommand cmd = new CreateBankAccountCommand(
                id,
                dto.getCustomerId(),
                ac,
                iban,
                dto.getCurrency());
        return gateway.send(request, cmd);
    }

    @RequestMapping(value = "accounts/{accountId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable("accountId") String accountId,
                                         HttpServletRequest request) {
        DeleteBankAccountCommand cmd = new DeleteBankAccountCommand(accountId);
        return gateway.send(request, cmd);
    }


    @RequestMapping(value = "accounts/{accountId}/saldo", method = RequestMethod.GET)
    public
    @ResponseBody
    Double getSaldo(
            @PathVariable("accountId") String accountId,
            @RequestParam("date") Date date) {
        return bankAccountDao.getSaldo(accountId, date);
    }


    @RequestMapping(value = "accounts/{accountId}/movements", method = RequestMethod.GET)
    public
    @ResponseBody
    List<AccountMovementDTO> listMovements(
            @PathVariable("accountId") String accountId) {
        List<AccountMovementDTO> rend = bankAccountDao.findAllMovements(accountId, 100);

        if (rend.size() > 0) {
            AccountMovementDTO tot = new AccountMovementDTO();
            tot.setAccountId(accountId);

            AccountMovementDTO max = rend.get(0);
            tot.setMaxAccountDate(max.getDispDate());
            tot.setMaxAccountBal(bankAccountDao.getSaldo(accountId, max.getDispDate()));

            AccountMovementDTO min = rend.get(rend.size() - 1);
            tot.setMinAccountDate(min.getDispDate());
            tot.setMinAccountBal(bankAccountDao.getSaldo(accountId, min.getDispDate()));

            tot.setCurrency(min.getCurrency());
            tot.setType("###");
            rend.add(tot);
        }
        return rend;
    }


    @RequestMapping(value = "accounts/{accountId}/movements", method = RequestMethod.POST)
    public ResponseEntity<String> action(
            @PathVariable("accountId") String accountId,
            @RequestBody AccountMovementDTO dto,
            HttpServletRequest request) {

        if ("DEP".equals(dto.getType())) {
            String movId = new UUID().toString();
            DisposeDepositCommand cmd = new DisposeDepositCommand(accountId,
                    movId,
                    dto.getDispDate(),
                    dto.getType(),
                    dto.getDescription(),
                    dto.getCurrency(),
                    new BigDecimal(dto.getAmount()),
                    null,
                    dto.getDispDate());
            ConfirmDepositCommand cmdc = new ConfirmDepositCommand(accountId, movId, dto.getDispDate(), LOG_SUCCESS_MSG);
            return gateway.send(request, cmd, cmdc);
        } else if ("PRE".equals(dto.getType())) {
            String movId = new UUID().toString();
            DisposeWithdrawalCommand cmd = new DisposeWithdrawalCommand(
                    accountId,
                    movId,
                    dto.getDispDate(),
                    dto.getType(),
                    dto.getDescription(),
                    dto.getCurrency(),
                    new BigDecimal(dto.getAmount()),
                    null,
                    dto.getDispDate());
            ConfirmWithdrawalCommand cmdc = new ConfirmWithdrawalCommand(accountId, movId, new Date(), LOG_SUCCESS_MSG);
            return gateway.send(request, cmd, cmdc);
        } else if ("GRA".equals(dto.getType())) {
            String movId = new UUID().toString();
            DisposeWithdrawalCommand cmd = new DisposeWithdrawalCommand(
                    accountId,
                    movId,
                    dto.getDispDate(),
                    dto.getType(),
                    dto.getDescription(),
                    dto.getCurrency(),
                    new BigDecimal(dto.getAmount()),
                    null,
                    dto.getDispDate());
            gateway.send(cmd);

            BankAccountDTO account = bankAccountDao.findById(accountId);
            List<BankAccountDTO> rend = bankAccountDao.findAll(account.getCustomerId());
            double itemAmount = dto.getAmount() / (rend.size() - 1);
            for (BankAccountDTO bankAccountDTO : rend) {
                if (bankAccountDTO.getId().equals(accountId))
                    continue;

                DisposeDepositCommand cmda = new DisposeDepositCommand(
                        bankAccountDTO.getId(),
                        movId, dto.getDispDate(),
                        "DEP",
                        "Giroconto effettuato da " + accountId,
                        dto.getCurrency(),
                        new BigDecimal(itemAmount),
                        null, dto.getDispDate());
                gateway.send(cmda);

                ConfirmDepositCommand cmdb = new ConfirmDepositCommand(bankAccountDTO.getId(), movId, dto.getDispDate(), LOG_SUCCESS_MSG);
                gateway.send(cmdb);
            }

            ConfirmWithdrawalCommand cmdc = new ConfirmWithdrawalCommand(accountId, movId, new Date(), LOG_SUCCESS_MSG);
            return gateway.send(request, cmdc);
        } else if ("BON".equals(dto.getType()) && null != dto.getRecipient()) {
            String movId = new UUID().toString();
            BankAccountDTO destinatario;
            try {
                destinatario = bankAccountDao.findByIban(dto.getRecipient());
                if (null == destinatario) {
                    return RestUtils.INPUT_VALIDATION_FAILURE;
                }
            } catch (DataAccessException ex) {
                return RestUtils.INPUT_VALIDATION_FAILURE;
            }

            MoneyTransferCommand cmd = new MoneyTransferCommand(
                    accountId,
                    destinatario.getId(),
                    movId,
                    dto.getType(),
                    dto.getDescription(),
                    dto.getCurrency(),
                    new BigDecimal(dto.getAmount()),
                    dto.getDispDate());
            return gateway.send(request,
                    BANK_TRANSFER_EVAL,
                    cmd);
        } else {
            return RestUtils.INPUT_VALIDATION_FAILURE;
        }

    }

}
