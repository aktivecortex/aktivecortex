/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.dao.bankaccount;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.aktivecortex.sample.bank.dto.AccountMovementDTO;
import org.aktivecortex.sample.bank.dto.BankAccountDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

@Repository("bankAccountDao")
public class BankAccountDaoImpl implements BankAccountDao{

	private NamedParameterJdbcTemplate npjt;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.npjt = new NamedParameterJdbcTemplate(dataSource);
	}
	
	private ParameterizedRowMapper<BankAccountDTO> BankAccountRm = new ParameterizedRowMapper<BankAccountDTO>() {
		public BankAccountDTO mapRow(ResultSet rs, int arg1)throws SQLException {
			BankAccountDTO result=new BankAccountDTO();
			result.setId(rs.getString("ACCOUNT_ID"));
			result.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));
			result.setCustomerId(rs.getString("CUSTOMER_ID"));
			result.setCurrency(rs.getString("REF_CURRENCY"));
			result.setIban(rs.getString("IBAN"));
			result.setBalance(rs.getDouble("AVAIL_AMOUNT"));
			return result;
		}
	};
	
	private ParameterizedRowMapper<BankAccountDTO> BankAccountBalanceRm = new ParameterizedRowMapper<BankAccountDTO>() {
		public BankAccountDTO mapRow(ResultSet rs, int arg1)throws SQLException {
			BankAccountDTO result=new BankAccountDTO();
			result.setId(rs.getString("ACCOUNT_ID"));
			result.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));
			result.setCustomerId(rs.getString("CUSTOMER_ID"));
			result.setCurrency(rs.getString("REF_CURRENCY"));
			result.setIban(rs.getString("IBAN"));
			result.setBalance(rs.getDouble("AVAIL_AMOUNT"));
			
			result.setAccountBal(rs.getDouble("MOV_AMOUNT"));
			result.setAccountDate(rs.getDate("ACCOUNT_DATE"));
			
			return result;
		}
	};

	private ParameterizedRowMapper<AccountMovementDTO> AccountMovementRm = new ParameterizedRowMapper<AccountMovementDTO>() {
		public AccountMovementDTO mapRow(ResultSet rs, int arg1)throws SQLException {
			AccountMovementDTO result=new AccountMovementDTO();
			result.setAccountId(rs.getString("ACCOUNT_ID"));
			result.setMovementId(rs.getString("MOVEMENT_ID"));
			result.setAccountDate(rs.getDate("ACCOUNT_DATE"));
			result.setAmount(rs.getDouble("MOV_AMOUNT"));
			result.setCurrency(rs.getString("MOV_CURRENCY"));
			result.setDescription(rs.getString("DESCRIPTION"));
			result.setDirection(rs.getInt("DIRECTION"));
			result.setDispDate(rs.getDate("DISP_DATE"));
			result.setMessage(rs.getString("MESSAGE"));
			result.setState(rs.getString("STATE"));
			result.setType(rs.getString("TYPE"));
			return result;
		}
	};
	
	private static final String findAllQuery="SELECT acc.*, bal.* FROM CORTEX_BANK_ACCOUNT acc ,CORTEX_ACCOUNT_BALANCE bal, (select  max(account_date) d , account_id  from CORTEX_ACCOUNT_BALANCE group by account_id ) bald where bal.account_id=acc.account_id and  bal.account_id=bald.account_id and bal.account_date=bald.d";
	
	public List<BankAccountDTO> findAll(){		
		return npjt.query(findAllQuery, new HashMap<String,String>(),  BankAccountBalanceRm);
	}
	
	private static final String findAllByCustomerIdQuery="SELECT acc.*, bal.* FROM CORTEX_BANK_ACCOUNT acc ,CORTEX_ACCOUNT_BALANCE bal, (select  max(account_date) d , account_id  from CORTEX_ACCOUNT_BALANCE group by account_id ) bald where bal.account_id=acc.account_id and  bal.account_id=bald.account_id and bal.account_date=bald.d  and customer_id = :cust_id";
	
	@Override
	public List<BankAccountDTO> findAll(String customerId) {
		MapSqlParameterSource in_params = new MapSqlParameterSource("cust_id", customerId);
		return npjt.query(findAllByCustomerIdQuery, in_params,  BankAccountBalanceRm);
	}
	
		
	private static final String findByIdQuery="SELECT * FROM CORTEX_BANK_ACCOUNT acc, CORTEX_ACCOUNT_BALANCE bal" +
			" where bal.ACCOUNT_ID=acc.ACCOUNT_ID and acc.ACCOUNT_ID = :acc_id order by ACCOUNT_DATE DESC limit 1";
	
	@Override
	public BankAccountDTO findById(String id) {
		MapSqlParameterSource in_params = new MapSqlParameterSource("acc_id", id);
		return npjt.queryForObject(findByIdQuery, in_params, BankAccountBalanceRm);
	}
	
	
	private static final String findByAccNumbQuery="SELECT * FROM CORTEX_BANK_ACCOUNT where IBAN = :iban";

	@Override
	public BankAccountDTO findByIban(String iban) {
		SqlParameterSource in_params = new MapSqlParameterSource("iban", iban);
		return npjt.queryForObject(findByAccNumbQuery, in_params, BankAccountRm);
	}	
	
	private static final String findMovementsQuery="SELECT * FROM CORTEX_ACCOUNT_MOVEMENT where ACCOUNT_ID = :acc_id order by DISP_DATE desc limit :re_limit";

	@Override
	public List<AccountMovementDTO> findAllMovements(String accountId,int limit) {
		MapSqlParameterSource in_params = new MapSqlParameterSource("acc_id", accountId).addValue("re_limit", limit);
		return npjt.query(findMovementsQuery, in_params, AccountMovementRm);
	}	
	
	private static final String findSaldoQuery="SELECT MOV_AMOUNT FROM CORTEX_ACCOUNT_BALANCE " +
			" where ACCOUNT_ID = :acc_id and ACCOUNT_DATE <= :on_date order by ACCOUNT_DATE DESC limit 1";
	
	ResultSetExtractor<Double> saldoExtractor=new ResultSetExtractor<Double>(){
		@Override
		public Double extractData(ResultSet rs) throws SQLException,	DataAccessException {
			if(rs.next()){
				return rs.getDouble("MOV_AMOUNT");
			}
			return null;
		}		
	};
	
	public Double getSaldo(String account, Date onDate){
		Calendar cal=Calendar.getInstance();
		cal.setTime(onDate);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE,59);
		cal.set(Calendar.SECOND,59);
		MapSqlParameterSource in_params = new MapSqlParameterSource("acc_id", account).addValue("on_date", cal.getTime());
		Double res=npjt.query(findSaldoQuery, in_params, saldoExtractor);
//		System.out.println("Saldo per account " + account + " e data "+ cal .getTime()+" = " + res);
		return res;
	}

	@Override
	public Date evaluateCurrencyDate(String movType, Date movDate,
			String currency) {
		return movDate;
	}
	
}
