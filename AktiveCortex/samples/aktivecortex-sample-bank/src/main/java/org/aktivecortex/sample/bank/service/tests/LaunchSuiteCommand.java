/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.service.tests;

import org.aktivecortex.api.command.AbstractCommand;
import org.aktivecortex.sample.bank.domain.tests.Test;

import java.util.HashMap;
import java.util.Map;


public class LaunchSuiteCommand extends AbstractCommand {
    private static final long serialVersionUID = 1L;

    private final String testSuiteId;

    private final int testCount;

    private final int iterations;

    public String getTestSuiteId() {
        return testSuiteId;
    }

    public int getTestCount() {
        return testCount;
    }

    public int getIterations() {
        return iterations;
    }

    public LaunchSuiteCommand(String testSuiteId, int testCount, int iterations) {
        super();
        this.testSuiteId = testSuiteId;
        this.testCount = testCount;
        this.iterations = iterations;
    }

    @Override
    public String getAggregateIdentifier() {
        return  getTestSuiteId();
    }

    @Override
    public String getAggregateType() {
        return Test.class.getSimpleName();
    }

    @Override
    public Map<String, Object> getSignificantFields() {
        Map ret=new HashMap<String, Object>();
        ret.put("testSuiteId", getTestSuiteId());
        ret.put("testCount", getTestCount());
        return ret;
    }
}
