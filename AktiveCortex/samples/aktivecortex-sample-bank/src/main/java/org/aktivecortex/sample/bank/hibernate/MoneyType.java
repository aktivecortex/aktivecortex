/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.ObjectUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

public class MoneyType implements CompositeUserType {
    @Override
    public String[] getPropertyNames() {
        return new String[]{"AMOUNT", "CURRENCY"};
    }

    @Override
    public Type[] getPropertyTypes() {
        return new Type[]{BigDecimalType.INSTANCE, StringType.INSTANCE};
    }

    @Override
    public Object getPropertyValue(Object component, int propertyIndex) throws HibernateException {
        if (component == null) {
            return null;
        }
        final Money money = (Money) component;
        switch (propertyIndex) {
            case 0: {
                return money.getAmount();
            }
            case 1: {
                return money.getCurrencyUnit().getCurrencyCode();
            }
            default: {
                throw new HibernateException("Invalid property index [" + propertyIndex + "]");
            }
        }
    }

    @Override
    public void setPropertyValue(Object component, int propertyIndex, Object value) throws HibernateException {
        if (component == null) {
            return;
        }

        final Money money = (Money) component;
        switch (propertyIndex) {
            case 0: {
                money.withAmount((BigDecimal) value);
                break;
            }
            case 1: {
                money.withCurrencyUnit(CurrencyUnit.getInstance((String) value));
                break;
            }
            default: {
                throw new HibernateException("Invalid property index [" + propertyIndex + "]");
            }
        }
    }

    @Override
    public Class returnedClass() {
        return Money.class;
    }

    @Override
    @SuppressWarnings("all")
    public boolean equals(Object x, Object y) throws HibernateException {
        return ObjectUtils.equals(x, y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        assert (x != null);
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        assert names.length == 2;
        BigDecimal amount = BigDecimalType.INSTANCE.nullSafeGet(rs, names[0], session);
        String currencyCode = StringType.INSTANCE.nullSafeGet(rs, names[1], session);
        return amount == null && currencyCode == null
                ? null
                : Money.of(CurrencyUnit.getInstance(currencyCode), amount);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
        if (value == null) {
            BigDecimalType.INSTANCE.set(st, null, index, session);
            StringType.INSTANCE.set(st, null, index + 1, session);
        } else {
            final Money money = (Money) value;
            BigDecimalType.INSTANCE.set(st, money.getAmount(), index, session);
            StringType.INSTANCE.set(st, money.getCurrencyUnit().getCurrencyCode(), index + 1, session);
        }
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        if(value==null)
            return null;
        Money money = (Money)value;
        return Money.of(money);
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(Object value, SessionImplementor session) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, SessionImplementor session, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, SessionImplementor session, Object owner) throws HibernateException {
        return deepCopy(original);
    }
    
}