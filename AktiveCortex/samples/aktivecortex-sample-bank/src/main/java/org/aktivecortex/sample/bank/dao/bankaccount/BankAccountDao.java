/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.dao.bankaccount;

import java.util.Date;
import java.util.List;

import org.aktivecortex.sample.bank.dto.AccountMovementDTO;
import org.aktivecortex.sample.bank.dto.BankAccountDTO;

public interface BankAccountDao {
	
	public Double getSaldo(String account, Date onDate);

	public List<BankAccountDTO> findAll();
	
	public List<BankAccountDTO> findAll(String customerId);

	public BankAccountDTO findById(String id);
	
	public BankAccountDTO findByIban(String id);

	public List<AccountMovementDTO> findAllMovements(String accountId, int i);
	
	public Date evaluateCurrencyDate(String movType,Date movDate,String currency);
	
	
}
