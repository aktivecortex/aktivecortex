/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.repository.tests;

import org.aktivecortex.sample.bank.domain.tests.TestSuite;
import org.axonframework.eventsourcing.HybridJpaRepository;
import org.axonframework.eventstore.EventStore;
import org.axonframework.repository.LockingStrategy;
import org.axonframework.util.jpa.EntityManagerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("testSuiteRepository")
public class TestSuiteRepository extends HybridJpaRepository<TestSuite> {
	@Autowired
	protected TestSuiteRepository(EntityManagerProvider entityManagerProvider) {
		super(entityManagerProvider, TestSuite.class, LockingStrategy.NO_LOCKING);
	}

	@Override
	@Autowired
	public void setEventStore(EventStore eventStore) {
		super.setEventStore(eventStore);
	}
}
