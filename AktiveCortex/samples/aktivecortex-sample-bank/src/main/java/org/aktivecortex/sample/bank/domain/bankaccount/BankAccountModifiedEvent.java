/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.bankaccount;

import java.util.HashMap;
import java.util.Map;

public class BankAccountModifiedEvent extends AbstractBankAccountEvent {	
	private static final long serialVersionUID = 1L;
	
	private String customerId;	
	
	private String accountNumber;
	
	private String iban;
	
	private String currency;
	
	
	public BankAccountModifiedEvent(
			String customerId,
			String accountNumber, 
			String iban,
			String currency) {
		super();
		this.customerId = customerId;		
		this.accountNumber = accountNumber;		
		this.iban = iban;
		this.currency = currency;
	}


	public String getCustomerId() {
		return customerId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}




	public String getIban() {
		return iban;
	}


	public String getCurrency() {
		return currency;
	}

	
	
	
	@Override
	protected Map<String, Object> getSignificantFields() {
		Map ret=new HashMap<String, Object>();
		ret.put("accountId", super.getAccountId());
		return ret;
	}

}
