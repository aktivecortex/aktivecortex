/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.bankaccount;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.aktivecortex.dbc.assertion.Contract;
import org.axonframework.domain.AggregateIdentifier;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;
import org.hibernate.annotations.Type;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.joda.time.DateTime;

import com.eaio.uuid.UUID;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

@Entity
@Table(name = "CORTEX_BANK_ACCOUNT")
@AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "ACCOUNT_ID"))})
public class BankAccount extends AbstractAnnotatedAggregateRoot implements Serializable{
	private static final long serialVersionUID = 1L;	
	
	//ACCOUNT_ID,VERSION,LASTEVENTSEQUENCENUMBER
	
	@Column(name = "CUSTOMER_ID", nullable = false, length = 50)
	private String customerId;	
	
	@Column(name = "ACCOUNT_NUMBER", nullable = false, length = 50)
	private String accountNumber;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "CREATION_DATE", nullable = false)
	private DateTime creationDate;
	
	@Column(name = "IBAN", nullable = false, length = 100)
	private String iban;
	
	@Column(name = "REF_CURRENCY", nullable = false, length = 3)
	private String refCurrency;
	
	@Type(type = "org.aktivecortex.sample.bank.hibernate.MoneyType")
	@org.hibernate.annotations.Columns(columns = {
        @Column(name = "AVAIL_AMOUNT"),
        @Column(name = "AVAIL_CURRENCY", length = 3)
    })
	private Money availableBalance;

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch=FetchType.EAGER)	
	@MapKey(name="referenceDay")
	private Map<String,Balance> balanceMap=new HashMap<String,Balance>();	
	
	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
	//@JoinColumn(name="ACCOUNT_ID")
	private Set<Movement> movements=new HashSet<Movement>();
	
	
	public BankAccount(){
		
	}
	
	public BankAccount(AggregateIdentifier accountId){
		super(accountId);
	}
	
	public BankAccount(AggregateIdentifier accountId, String customerId , String accountNumber, String iban, String currency){
		super(accountId);
		Money initialBalance=Money.of(CurrencyUnit.getInstance(currency),new BigDecimal(0));
		BankAccountCreatedEvent event=new BankAccountCreatedEvent(customerId,accountNumber,new DateTime(),iban,currency,initialBalance);
		apply(event);
	}
	
	@EventHandler
	public void onBankAccountCreatedEvent(BankAccountCreatedEvent event){
		this.customerId=event.getCustomerId();		
		this.accountNumber=event.getAccountNumber();
		this.creationDate=event.getCreationDate();
		this.iban=event.getIban();		
		this.refCurrency=event.getCurrency();		
		this.availableBalance=event.getBalance();	
		
		//aggiorno saldo contabile
		Balance balance=new Balance(this,new UUID().toString(),event.getCreationDate(),availableBalance);
		balanceMap.put(balance.getReferenceDay(),balance);
				
	}
	
	public void update(String customerId, String accountNumber, String iban, String currency) {
		BankAccountModifiedEvent event=new BankAccountModifiedEvent(customerId,accountNumber,iban,currency);
		apply(event);
	}
	
	@EventHandler
	public void onBankAccountModifiedEvent(BankAccountModifiedEvent event){
		this.customerId=event.getCustomerId();		
		this.accountNumber=event.getAccountNumber();
		this.iban=event.getIban();		
		this.refCurrency=event.getCurrency();		
	}
	
	public void delete(){
		apply(new BankAccountDeletedEvent());
	}
	
	
	
	
	
	
	public void disposeDeposit(DateTime dispDate, String type, String description, String currency, BigDecimal amount, BigDecimal fee,String movementId,DateTime currencyDate){
		Contract.REQUIRE.areEqual(refCurrency, currency ,"Currency validation", "Currency must be equal to {}",refCurrency);		
		Contract.REQUIRE.isTrue( null!=amount && amount.doubleValue() > 0,"Amount validation", "Amount must be major than zero");
		Contract.REQUIRE.isFalse(existsMovementId(movementId), "Movement validation", "Cannot store the same movement more than once. Existing movement id is {}", movementId);
		apply(new BankAccountDepositDisposedEvent(dispDate,type,description,currency,amount,fee,movementId,currencyDate));		
	}
	
	@EventHandler
	public void onBankAccountDepositDisposedEvent(BankAccountDepositDisposedEvent event){
		
		//calcolo ammontare del movimento
		BigDecimal movementAmount=event.getAmount();
		if(null!=event.getFee()){
			movementAmount=movementAmount.subtract(event.getFee());			
		}
		
		//Aggiungo un movimento
		Money movementMoney=Money.of(CurrencyUnit.getInstance(event.getCurrency()),movementAmount);
		Movement movement=new Movement(this, event.getMovementId(),  event.getType() , event.getDescription(),
				movementMoney , event.getDispDate(), true);
		movements.add(movement);
		
	}
	
	
	
	public void confirmDeposit(DateTime confDate, String movementId, String message){	
		//esiste il movimento 		
		Contract.REQUIRE.notNull(findMovement(movementId),"Movement validation", "Movement id not valid");
		
		apply(new BankAccountDepositConfirmedEvent(movementId,confDate,message));		
	}
	
	@EventHandler
	public void onBankAccountDepositConfirmedEvent(BankAccountDepositConfirmedEvent event){
		Movement movement=findMovement(event.getMovementId());
		Money movementMoney=movement.getAmount();
		
		//calcolo ammontare del movimento
		BigDecimal accountAmount=this.availableBalance.getAmount().add(movementMoney.getAmount());		
		availableBalance=availableBalance.withAmount(accountAmount);
		
		//aggiorno saldo contabile
		Balance balance=new Balance(this,new UUID().toString(),event.getConfDate(),availableBalance);
		if(balanceMap.containsKey(balance.getReferenceDay())){
			Money balanceAmount=balanceMap.get(balance.getReferenceDay()).getAmount();
			BigDecimal balanceAccountAmount=balanceAmount.getAmount().add(movementMoney.getAmount());			
			balanceMap.get(balance.getReferenceDay()).setAmount(balanceAmount.withAmount(balanceAccountAmount));
		}else{
			balanceMap.put(balance.getReferenceDay(),balance);
		}
		movement.confirm(event.getConfDate(), event.getMessage());
		
	}
	
	public void abortDeposit(String movementId, String message){	
		//esiste il movimento 		
		Contract.REQUIRE.notNull(findMovement(movementId),"Movement validation", "Movement id not valid");
		apply(new BankAccountDepositAbortedEvent(movementId,message));		
	}
	
	@EventHandler
	public void onBankAccountDepositAbortedEvent(BankAccountDepositAbortedEvent event){
		Movement movement=findMovement(event.getMovementId());
		movement.abort(event.getMessage());
	}
	
	
	
	
	
	
	
	
	
	public void disposeWithdrawal(DateTime dispDate, String type, String description, String currency,
			BigDecimal amount, BigDecimal fee,String movementId,DateTime currencyDate){
		Contract.REQUIRE.areEqual(refCurrency, currency ,"Currency validation", "Currency must be equal to {}", refCurrency);
		Contract.REQUIRE.isTrue( null!=amount && amount.doubleValue() > 0,"Amount validation", "Amount must be major than zero");
		
		Money requestedAmount = Money.of(CurrencyUnit.getInstance(currency),amount);		
		Contract.REQUIRE.isFalse(requestedAmount.isGreaterThan(availableBalance) ,"Amount validation", "Available Balance must be major than requested amuont");
				
		apply(new BankAccountWithdrawalDisposedEvent(dispDate,type,description,currency,amount,fee,movementId,currencyDate));
	}
	
	@EventHandler
	public void onBankAccountWithdrawalDisposedEvent(BankAccountWithdrawalDisposedEvent event){
		BigDecimal movementAmount=event.getAmount();		
		if(null!=event.getFee()){
			movementAmount=movementAmount.add(event.getFee());			
		}
		
		//Aggiungo un movimento con segno negativo
		Money movementMoney=Money.of(CurrencyUnit.getInstance(event.getCurrency()),movementAmount);
		Movement movement=new Movement(this, event.getMovementId(),  event.getType() , event.getDescription(),
				movementMoney , event.getDispDate(),false);
		movements.add(movement);
		
		BigDecimal accountAmount=this.availableBalance.getAmount().subtract(movementAmount);
		availableBalance=availableBalance.withAmount(accountAmount);
	}
	
	public void confirmWithdrawal(DateTime confDate, String movementId,String message){		
		//esiste il movimento 		
		Contract.REQUIRE.notNull(findMovement(movementId),"Movement validation", "Movement id not valid");
		apply(new BankAccountWithdrawalConfirmedEvent(movementId,confDate,message));		
	}
	
	@EventHandler
	public void onBankAccountWithdrawalConfirmedEvent(BankAccountWithdrawalConfirmedEvent event){
		Movement movement=findMovement(event.getMovementId());
		Money movementMoney=movement.getAmount();
		
		//aggiorno saldo contabile
		Balance balance=new Balance(this,new UUID().toString(),event.getConfDate(),availableBalance);
		if(balanceMap.containsKey(balance.getReferenceDay())){
			Money balanceAmount=balanceMap.get(balance.getReferenceDay()).getAmount();
			BigDecimal balanceAccountAmount=balanceAmount.getAmount().subtract(movementMoney.getAmount());			
			balanceMap.get(balance.getReferenceDay()).setAmount(balanceAmount.withAmount(balanceAccountAmount));
		}else{
			balanceMap.put(balance.getReferenceDay(),balance);
		}
		movement.confirm(event.getConfDate(), event.getMessage());		
	}
	
	public void abortWithdrawal(String movementId, String message){		
		//esiste il movimento 		
		Contract.REQUIRE.notNull(findMovement(movementId),"Movement validation", "Movement id not valid");
		apply(new BankAccountWithdrawalAbortedEvent(movementId,message));		
	}
	
	@EventHandler
	public void onBankAccountWithdrawalAbortedEvent(BankAccountWithdrawalAbortedEvent event){
		Movement movement=findMovement(event.getMovementId());
		Money movementMoney=movement.getAmount();
		
		BigDecimal accountAmount=this.availableBalance.getAmount().add(movementMoney.getAmount());
		availableBalance=availableBalance.withAmount(accountAmount);
		
		movement.abort(event.getMessage());
	}
	
	
	
	
	
	
	
	
	public void disposeMoneyTasfert(String toAccount, String movementId, String movementType,
			String description, String currency, BigDecimal amount, DateTime currDate){					
		
		Contract.REQUIRE.areEqual(refCurrency, currency ,"Currency validation", "Currency must be equal to {}", refCurrency);
		Contract.REQUIRE.isTrue( null!=amount && amount.doubleValue() > 0,"Amount validation", "Amount must be major than zero");
		
		Money requestedAmount = Money.of(CurrencyUnit.getInstance(currency),amount);		
		Contract.REQUIRE.isFalse(requestedAmount.isGreaterThan(availableBalance) ,"Amount validation", "Available Balance must be major than requested amuont");
		
		apply(new MoneyTansferDisposedEvent(toAccount,movementId,movementType,description,currency,amount,currDate));
	}
	
	
	private boolean existsMovementId(final String movementId) {
		return null!=findMovement(movementId);
	}
	
	private Movement findMovement(final String id) {	
		try{
			return Iterables.find(movements, new Predicate<Movement>() {
				@Override
				public boolean apply(Movement mov) {
					return id.equals(mov.getId());
				}
			});
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	
	
}
