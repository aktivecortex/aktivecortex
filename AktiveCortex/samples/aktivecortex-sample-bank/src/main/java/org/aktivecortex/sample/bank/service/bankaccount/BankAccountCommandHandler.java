/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.service.bankaccount;

import com.eaio.uuid.UUID;
import org.aktivecortex.core.commandhandling.support.CommandHandlerUtils;
import org.aktivecortex.sample.bank.domain.bankaccount.BankAccount;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.domain.AggregateIdentifier;
import org.axonframework.repository.Repository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class BankAccountCommandHandler {
	
	private Repository<BankAccount> bankAccountRepository;

	@Autowired
	@Qualifier("bankAccountRepository")
    public void setBankAccountRepository(Repository<BankAccount> bankAccountRepository) {
		this.bankAccountRepository = bankAccountRepository;
	}	
	
	@CommandHandler
	public void onCreateBankAccountCommand(CreateBankAccountCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());
		BankAccount account=new BankAccount(id,cmd.getCustomerId(),cmd.getAccountNumber(),cmd.getIban(),cmd.getCurrency());
		bankAccountRepository.add(account);
	}
	
	@CommandHandler
	public void onAlterBankAccountCommand(AlterBankAccountCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);
		account.update(cmd.getCustomerId(),cmd.getAccountNumber(),cmd.getIban(),cmd.getCurrency());
	}
	
	@CommandHandler
	public void onDeleteBankAccountCommand(DeleteBankAccountCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);
		account.delete();
	}

	@CommandHandler
	public void onDisposeDepositCommand(DisposeDepositCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);
		
		String movementId=(null!=cmd.getMovementId()) ? cmd.getMovementId() : new UUID().toString();
		
		DateTime dispDate=new DateTime().withMillis(cmd.getDispDate().getTime());
		DateTime currDate=(null==cmd.getCurrencyDate()) ? null: new DateTime().withMillis(cmd.getCurrencyDate().getTime());
		
		account.disposeDeposit( 
				dispDate, 
				cmd.getType(), 
				cmd.getDescription(), 
				cmd.getCurrency(), 
				cmd.getAmount(), 
				cmd.getFee(), 
				movementId ,
				currDate);
	}
	
	@CommandHandler
	public void onConfirmDepositCommand(ConfirmDepositCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);		
		DateTime dispDate=new DateTime().withMillis(cmd.getConfDate().getTime());		
		account.confirmDeposit(dispDate, cmd.getMovementId(), cmd.getMessage());
	}
	
	@CommandHandler
	public void onAbortDepositCommand(AbortDepositCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);	
		account.abortDeposit(cmd.getMovementId(), cmd.getMessage());
	}

	@CommandHandler
	public void onDisposeWithdrawalCommand(DisposeWithdrawalCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);
		
		String movementId=(null!=cmd.getMovementId()) ? cmd.getMovementId() : new UUID().toString();
		
		DateTime dispDate=new DateTime().withMillis(cmd.getDispDate().getTime());
		DateTime currDate=(null==cmd.getCurrencyDate()) ? null: new DateTime().withMillis(cmd.getCurrencyDate().getTime());
		
		account.disposeWithdrawal( 
				dispDate, 
				cmd.getType(), 
				cmd.getDescription(), 
				cmd.getCurrency(), 
				cmd.getAmount(), 
				cmd.getFee(), 
				movementId ,
				currDate);
	}
	
	@CommandHandler
	public void onConfirmWithdrawalCommand(ConfirmWithdrawalCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);		
		DateTime dispDate=new DateTime().withMillis(cmd.getConfDate().getTime());		
		account.confirmWithdrawal(dispDate, cmd.getMovementId(), cmd.getMessage());
	}
	
	@CommandHandler
	public void onAbortWithdrawalCommand(AbortWithdrawalCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);	
		account.abortWithdrawal(cmd.getMovementId(), cmd.getMessage());
	}

	@CommandHandler
	public void onMoneyTranfertCommand(MoneyTransferCommand cmd){
		AggregateIdentifier id = CommandHandlerUtils.getNonNullId(cmd.getAccountId());   
		BankAccount account=bankAccountRepository.load(id);	
		
		DateTime currDate=(null==cmd.getCurrDate()) ? null: new DateTime().withMillis(cmd.getCurrDate().getTime());
		account.disposeMoneyTasfert(
				cmd.getToAccount(),
				cmd.getCommandId(),
				cmd.getMovementType(),
				cmd.getDescription(),
				cmd.getCurrency(),
				cmd.getAmount(),
				currDate);
	}
}
