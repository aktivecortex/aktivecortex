/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.tests;

import org.axonframework.domain.AggregateIdentifier;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CORTEX_STRESS_TESTSUITE")
@AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "SUITE_ID"))})
public class TestSuite extends AbstractAnnotatedAggregateRoot implements Serializable{
	private static final long serialVersionUID = 1L;

	//ACCOUNT_ID,VERSION,LASTEVENTSEQUENCENUMBER

    @Basic
    private int testCount;

    @Basic
    private int iterations;


	public TestSuite(){

	}

	public TestSuite(AggregateIdentifier testSuiteId, int testCount, int iterations){
		super(testSuiteId);
        apply(new TestSuiteCreatedEvent(testCount, iterations));
	}

	@EventHandler
	public void onTestCreatedEvent(TestSuiteCreatedEvent event) {
        this.testCount = event.getTestCount();
        this.iterations = event.getIterations();
	}

    public void delete(){
        apply(new TestSuiteDeletedEvent());
    }
}
