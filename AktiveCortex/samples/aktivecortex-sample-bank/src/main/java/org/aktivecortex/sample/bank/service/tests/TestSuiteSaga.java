/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.service.tests;

import com.eaio.uuid.UUID;
import org.aktivecortex.api.commandhandling.CommandGateway;
import org.aktivecortex.core.axon2backport.saga.annotation.EndSaga;
import org.aktivecortex.core.axon2backport.saga.annotation.SagaEventHandler;
import org.aktivecortex.core.axon2backport.saga.annotation.StartSaga;
import org.aktivecortex.core.saga.AbstractSaga;
import org.aktivecortex.sample.bank.domain.tests.*;
import org.axonframework.eventhandling.scheduling.EventScheduler;
import org.axonframework.eventhandling.scheduling.ScheduleToken;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

public class TestSuiteSaga extends AbstractSaga {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestSuiteSaga.class);

    private static final long serialVersionUID = 1L;

    private static final int MILLIS_EVENT = 200;

    private transient CommandGateway commandGateway;

    private transient EventScheduler scheduler;

    private ScheduleToken timeOutToken;

    private String testSuiteId;

    //Per test received events count
    private Map<String, Integer> testEvents = new HashMap<String, Integer>();

    // Number of tests to be started
    private int testsRequested = 0;

    // Number of events that each test i required to launch
    private int perTestEventsExpected = 0;

    // Total number of events received (needed only for logging purpose in case of failure)
    private int eventsReceived = 0;

    // Number of tests actually started
    private int testsStarted = 0;

    // Number of tests that have successfully delivered all the expected events
    private int testsCompleted = 0;

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Autowired
    public void setCommandService(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Autowired
    public void setEventScheduler(EventScheduler scheduler) {
        this.scheduler=scheduler;
    }

    @StartSaga
    @SagaEventHandler(associationProperty = "testSuiteId")
    public void onLaunchTestSuiteEvent(TestSuiteCreatedEvent event) {
        appendStartLog("Stress Test suite started");
        this.testSuiteId = event.getTestSuiteId();
        this.testsRequested = event.getTestCount();
        this.perTestEventsExpected =event.getIterations();
        appendLog(String.format("Test Suite Id: [%s]", testSuiteId), true);
        appendLog(String.format("Number of tests to launch: [%s]", testsRequested), true);
        appendLog(String.format("Number of events for each test: [%s]", perTestEventsExpected), true);
        for (int i = 0; i < testsRequested; i++) {
            final String testId = new UUID().toString();
            final LaunchTestCommand command = new LaunchTestCommand(testSuiteId, testId, perTestEventsExpected);
            commandGateway.send(command);
        }
        final String idTimeOut = new UUID().toString();
        final Duration timeOutPeriod = new Duration(MILLIS_EVENT * perTestEventsExpected * testsRequested);
        appendLog(String.format("Timeout scheduled for: [%s] from now. It will take place at: [%s]",
                timeOutPeriod, new DateTime().plus(timeOutPeriod)), true);
        timeOutToken = scheduler.schedule(timeOutPeriod,
                new TestSuiteTimeOutEvent(this, testSuiteId, idTimeOut));
        associateWith("idTimeOut",idTimeOut);

    }

    @SagaEventHandler(associationProperty = "idTimeOut")
    public void onTestSuiteTimeOut(TestSuiteTimeOutEvent event) {
        appendLog(String.format("Timeout occurred for test suite id: [%s]", testSuiteId), true);
        appendLog(String.format("Total number of events received: [Actual/Expected] - [%s/%s]",
                eventsReceived, perTestEventsExpected * testsRequested), true);
        appendLog(String.format("Per test events received: {%s}", testEvents), true);
        appendLog("Cleaning up...",true);
        for (String testId : testEvents.keySet()) {
            final EndTestCommand endTestCommand = new EndTestCommand(testId);
            commandGateway.send(endTestCommand);
        }

        final EndSuiteCommand endSuiteCommand = new EndSuiteCommand(testSuiteId);
        commandGateway.send(endSuiteCommand);
        appendEndLog(String.format("Stress Test suite [%s] failed due to timeout.",  testSuiteId));
        end();
    }

    @SagaEventHandler(associationProperty = "testSuiteId")
    public void onTestCreatedEvent(TestCreatedEvent event) {
        final String testId = event.getTestId();
        testEvents.put(testId, 0);
        testsStarted++;
        LOGGER.info(String.format("Test [%s] created. Pending tests - for creation: [%s], for completion: [%s]",
                testId, getPendingForCreation(), getPendingForCompletion()));
    }

    private int getPendingForCreation() {
        return testsRequested - testsStarted;
    }

    @SagaEventHandler(associationProperty = "testSuiteId")
    public void onTestProgressEvent(TestProgressEvent event) {
        final String testId = event.getTestId();
        if (testEvents.containsKey(testId)){
            int testProgress = testEvents.get(testId);
            eventsReceived++;
            testEvents.put(testId, ++testProgress);
            if (testProgress == perTestEventsExpected) {
                testsCompleted++;
                LOGGER.info(String.format("Test [%s] completed. Pending tests - for creation: [%s], for completion: [%s]",
                        testId, getPendingForCreation(), getPendingForCompletion()));
                commandGateway.send(new EndTestCommand(testId));
            }
        }
    }

    private int getPendingForCompletion() {
        return testsRequested - testsCompleted;
    }

    @SagaEventHandler(associationProperty = "testSuiteId")
    public void onTestDeletedEvent(TestDeletedEvent event) {
        final String testId = event.getAggregateIdentifier().toString();
        if (testEvents.containsKey(testId)){
            testEvents.keySet().remove(testId);
            if (0 == getPendingForCreation() && testEvents.isEmpty()) {
                LOGGER.info(String.format("Test suite [%s] - No. of Tests (expected: [%s] - started: [%s] - completed: [%s]) - No. of Events (expected: [%s] - received: [%s])",
                        testSuiteId, testsRequested, testsStarted, testsCompleted, testsRequested * perTestEventsExpected, eventsReceived));
                LOGGER.info(String.format("Test suite [%s] completed.",testSuiteId));
                commandGateway.send(new EndSuiteCommand(testSuiteId));
            }
        }
    }

    @EndSaga
    @SagaEventHandler(associationProperty = "testSuiteId")
    public void onTestSuiteDeletedEvent(TestSuiteDeletedEvent event) {
        scheduler.cancelSchedule(timeOutToken);
        appendEndLog("Stress Test suite [" + testSuiteId + "] successfully completed.");
    }

}
