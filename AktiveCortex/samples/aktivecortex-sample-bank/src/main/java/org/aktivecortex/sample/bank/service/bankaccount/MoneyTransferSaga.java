/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.service.bankaccount;

import com.eaio.uuid.UUID;
import org.aktivecortex.api.commandhandling.CommandGateway;
import org.aktivecortex.core.axon2backport.saga.annotation.SagaEventHandler;
import org.aktivecortex.core.axon2backport.saga.annotation.StartSaga;
import org.aktivecortex.core.saga.AbstractSaga;
import org.aktivecortex.sample.bank.dao.bankaccount.BankAccountDao;
import org.aktivecortex.sample.bank.domain.bankaccount.*;
import org.axonframework.eventhandling.scheduling.EventScheduler;
import org.axonframework.eventhandling.scheduling.ScheduleToken;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

public class MoneyTransferSaga extends AbstractSaga {
	
	//setto il timeout a 1 min
	private static final int TIMEOUT_DURATION = 1000*60;

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(MoneyTransferSaga.class);
	
	@Override
	protected Logger getLogger() {
		return log;
	}

    private transient CommandGateway commandGateway;

	@Autowired
	public void setCommandService(CommandGateway commandGateway) {
		this.commandGateway = commandGateway;
	}

	private transient EventScheduler scheduler;

	@Autowired
	public void setEventScheduler(EventScheduler scheduler) {
		this.scheduler=scheduler;
	}
	
	private transient BankAccountDao bankAccountDao;
	
	@Autowired
	public void setBankAccountDao(BankAccountDao bankAccountDao) {
		this.bankAccountDao=bankAccountDao;
	}
	
	
	private ScheduleToken startToken;
	
	private String formAccount;
	private String toAccount;
	private String movementId;
	
	private boolean isWdConfirmed;
	private boolean isDeConfirmed;
	
	@StartSaga
	@SagaEventHandler(associationProperty = "movementId")	
	public void onMoneyTasfertDisposedEvent(MoneyTansferDisposedEvent event){
		appendStartLog("Money transfer started");
		
		this.movementId=event.getMovementId();
		appendLog("movementId: {}", movementId);
		
		this.formAccount=event.getAccountId();
		appendLog("from: {}", formAccount);		
		
		this.toAccount=event.getToAccount();
		appendLog("to: {}", toAccount);		
		appendLog("amount: {}", event.getAmount());		
		
		isWdConfirmed=false;
		isDeConfirmed=false;
		
		Date currencyDate=bankAccountDao.evaluateCurrencyDate(event.getMovementType(), 
				event.getTransferDate().toDate(), event.getCurrency());
		
		//invio comando di prelievo dal conto
		DisposeWithdrawalCommand cmd=new DisposeWithdrawalCommand(
				event.getAccountId(),
				event.getMovementId(),
				event.getTransferDate().toDate(),
				event.getMovementType(),
				event.getDescription(),
				event.getCurrency(),
				event.getAmount(),
				null,//fee
				currencyDate);
		try{
			commandGateway.send(cmd);
			appendElapsedTime("Invio comando di addebito");
		}catch (Exception e) {
			log.error("Invio comando di addebito",e);
			appendEndLog("Fine prematura!");
			end();
		}
		
		
		String idTimeOut=new UUID().toString();
		startToken = scheduler.schedule(new Duration(TIMEOUT_DURATION),
				new MoneyTransferTimeoutAppEvent(this,idTimeOut));
		associateWith("idTimeOut",idTimeOut);
	}
	
	@SagaEventHandler(associationProperty = "idTimeOut")	
	public void onMoneyTasfertTimeoutAppEvent(MoneyTransferTimeoutAppEvent event){
		commandGateway.send(new AbortWithdrawalCommand(formAccount, movementId, "Fine per timeout"));
		commandGateway.send(new AbortDepositCommand(toAccount, movementId, "Fine per timeout")); 
		appendEndLog("Fine per timeout!");
		end();
	}
	
	@SagaEventHandler(associationProperty = "movementId")	
	public void onBankAccountWithdrawalDisposedEvent(BankAccountWithdrawalDisposedEvent event){
		
		Date currencyDate=bankAccountDao.evaluateCurrencyDate(event.getType(), 
				event.getDispDate().toDate(), event.getCurrency());
		
		//invio comando di deposito presso il destinatario
		DisposeDepositCommand cmd=new DisposeDepositCommand(
				toAccount,
				event.getMovementId(),
				event.getDispDate().toDate(),
				event.getType(),
				event.getDescription(),
				event.getCurrency(),
				event.getAmount(),
				null,//fee
				currencyDate);
		try{
			commandGateway.send(cmd);
			appendElapsedTime("Invio comando di accredito");
		}catch (Exception e) {
			log.error("Invio comando di accredito",e);
			commandGateway.send(new AbortWithdrawalCommand(formAccount, event.getMovementId(), e.getMessage()));	
			appendElapsedTime("Invio comando di ripristino addebito");
		}
	}
	
	@SagaEventHandler(associationProperty = "movementId")	
	public void onBankAccountWithdrawalDisposedEvent(BankAccountWithdrawalAbortedEvent event){
		scheduler.cancelSchedule(startToken);
		appendEndLog("Fine prematura!");
		end();
	}
	
	@SagaEventHandler(associationProperty = "movementId")	
	public void onBankAccountDepositDisposedEvent(BankAccountDepositDisposedEvent event){
		String serverMessage="Traferimento avvenuto";
		Date now=new Date();
		commandGateway.send(new ConfirmWithdrawalCommand(formAccount, event.getMovementId(), now, serverMessage));
		commandGateway.send(new ConfirmDepositCommand(toAccount, event.getMovementId(), now, serverMessage));
	}
	
	@SagaEventHandler(associationProperty = "movementId")	
	public void onBankAccountWithdrawalConfirmedEvent(BankAccountWithdrawalConfirmedEvent event){
		isWdConfirmed=true;
		if(isDeConfirmed){
			scheduler.cancelSchedule(startToken);
			appendEndLog(null);
			end();
		}
	}
	
	@SagaEventHandler(associationProperty = "movementId")	
	public void onBankAccountDepositConfirmedEvent(BankAccountDepositConfirmedEvent event){
		isDeConfirmed=true;
		if(isWdConfirmed){
			scheduler.cancelSchedule(startToken);
			appendEndLog(null);
			end();
		}
	}	
}
