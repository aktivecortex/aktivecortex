/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.bankaccount;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovementId implements Serializable { 
	private static final long serialVersionUID = 1L;
	
	@Column(name = "ACCOUNT_ID",nullable = false, insertable = false, updatable = false)
	private String accountId;
	
	@Column(name = "MOVEMENT_ID",nullable = false, insertable = false, updatable = false)
	private String movementId;

	public MovementId(){
		super();
	}
	
	public MovementId(String accountId, String movementId) {
		super();
		this.accountId = accountId;
		this.movementId = movementId;
	}
	
	public String getAccountId() {
		return accountId;
	}

	public String getMovementId() {
		return movementId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		result = prime * result + ((movementId == null) ? 0 : movementId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovementId other = (MovementId) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (movementId == null) {
			if (other.movementId != null)
				return false;
		} else if (!movementId.equals(other.movementId))
			return false;
		return true;
	}	
	
	

}
