/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.tests;

import org.aktivecortex.api.event.AbstractDomainEvent;

import java.util.HashMap;
import java.util.Map;


public class TestProgressEvent extends AbstractDomainEvent {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final int iteration;
    private final String testId;
    private final String testSuiteId;

    public String getTestSuiteId() {
        return testSuiteId;
    }

    public TestProgressEvent(String testSuiteId, String testId, int i) {
        this.testSuiteId = testSuiteId;
        this.testId = testId;
        this.iteration = i;
    }

    public String getTestId() {
        return testId;
    }

    public int getIteration() {
        return iteration;
    }

    @Override
    protected Map<String, Object> getSignificantFields() {
        Map ret=new HashMap<String, Object>();
        ret.put("testSuiteId", getTestId());
        ret.put("testId", getTestId());
        ret.put("iteration", getIteration());
        return ret;
    }

    @Override
    public String getRoutingKey() {
        return String.format("%s-%s", super.getRoutingKey(), getTestSuiteId());
    }
}
