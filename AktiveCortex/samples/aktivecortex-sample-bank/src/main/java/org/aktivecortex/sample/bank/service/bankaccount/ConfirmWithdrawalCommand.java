/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.service.bankaccount;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
public class ConfirmWithdrawalCommand extends AbstractBankAccountCommand {
	private static final long serialVersionUID = 1L;
	
	private String movementId; 
	private Date confDate;
	private String message;
	public ConfirmWithdrawalCommand(String accountId, String movementId, Date confDate, String message) {
		super(accountId);
		this.movementId = movementId;
		this.confDate = confDate;
		this.message = message;
	}
	public String getMovementId() {
		return movementId;
	}
	public Date getConfDate() {
		return confDate;
	}
	public String getMessage() {
		return message;
	}	
	
	@Override
	public Map<String, Object> getSignificantFields() {
		Map ret=new HashMap<String, Object>();
		ret.put("accountId", getAccountId());			
		ret.put("movementId", movementId);			
		return ret;
	}
}
