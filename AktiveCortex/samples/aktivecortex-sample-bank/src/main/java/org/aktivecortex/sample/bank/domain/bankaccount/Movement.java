/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.bankaccount;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.money.Money;
import org.joda.time.DateTime;



@Entity
@Table(name = "CORTEX_ACCOUNT_MOVEMENT")
public class Movement implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private static final String STATE_DISPOSED = "D";
	private static final String STATE_CONFIRMED = "C";
	private static final String STATE_ABORTED = "A";
	
	@SuppressWarnings("unused")
	@ManyToOne
	@JoinColumn(name = "ACCOUNT_ID", nullable=false, insertable = false, updatable=false)
	private BankAccount account;	
	
//	@SuppressWarnings("unused")
//	@Id
//	@Column(name = "MOVEMENT_ID",nullable = false, insertable = false, updatable = false)
//	private String id;
	
		
	@EmbeddedId	
	private MovementId movementId;	
	
	@SuppressWarnings("unused")
	@Column(name = "DIRECTION", nullable = false, precision = 1, scale = 0)
	private Integer direction;
	
	@SuppressWarnings("unused")
	@Column(name = "STATE",nullable = false, length = 1)
	private String state;
	
	@SuppressWarnings("unused")
	@Column(name = "TYPE",nullable = false, length = 3)
	private String type;
	
	@SuppressWarnings("unused")
	@Column(name = "DESCRIPTION",length = 2000, nullable = true)
	private String description;
	
	@SuppressWarnings("unused")
	@Column(name = "MESSAGE",length = 2000, nullable = true)
	private String message;
	
	@Type(type = "org.aktivecortex.sample.bank.hibernate.MoneyType")
	@org.hibernate.annotations.Columns(columns = {
        @Column(name = "MOV_AMOUNT"),
        @Column(name = "MOV_CURRENCY", length = 3)
    })
	private Money amount;
	
	@SuppressWarnings("unused")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "DISP_DATE", nullable = false)
	private DateTime dispDate;
	
	@SuppressWarnings("unused")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ACCOUNT_DATE", nullable = true)
	private DateTime accountDate;

	public Movement() {		
	}

	public Movement(BankAccount account, String id, String type,
			String description, Money amount, DateTime dispDate, boolean plus) {
		super();
		this.movementId=new MovementId(account.getIdentifier().asString(),id);
		this.account = account;
		
		this.state = STATE_DISPOSED;
		this.type = type;
		this.description = description;
		this.amount = amount;
		this.dispDate = dispDate;
		this.direction=(plus) ? 1 : 0;
	}
	
	public void confirm(DateTime accountDate,String message){
		this.state = STATE_CONFIRMED;
		this.accountDate = accountDate;
		this.message = message;
	}
	
	public void abort(String message){
		this.state = STATE_ABORTED;		
		this.message = message;
	}
	
	public String getId(){
		return this.movementId.getMovementId();
	}
	
	public Money getAmount(){
		return this.amount;
	}
	
}
