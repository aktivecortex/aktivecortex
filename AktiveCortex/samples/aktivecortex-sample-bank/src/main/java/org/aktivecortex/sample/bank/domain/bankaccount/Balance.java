/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.bankaccount;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Type;
import org.joda.money.Money;
import org.joda.time.DateTime;

@Entity
@Table(name = "CORTEX_ACCOUNT_BALANCE")
public class Balance implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	@ManyToOne
	@JoinColumn(name = "ACCOUNT_ID", nullable=false, updatable=false)
	private BankAccount account;	
	
	@SuppressWarnings("unused")
	@Id
	@Column(name = "BAL_ID",nullable = false, insertable = false, updatable = false)
	private String id;
	
	@SuppressWarnings("unused")
	@Column(name = "REF_DAY",nullable = false, length = 8)
	private String referenceDay;
	
	@SuppressWarnings("unused")	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ACCOUNT_DATE", nullable=false)
	private DateTime accountDate;
	
	@Type(type = "org.aktivecortex.sample.bank.hibernate.MoneyType")
	@org.hibernate.annotations.Columns(columns = {
        @Column(name = "MOV_AMOUNT"),
        @Column(name = "MOV_CURRENCY", length = 3)
    })
	private Money amount;
	
	
	

	public Balance() {		
	}

	public Balance(BankAccount account,String id,DateTime accountDate, Money amount) {
		super();
		this.id = id;		
		this.account = account;		
		this.accountDate = accountDate;
		setDay(accountDate);
		this.amount = amount;
	}
	
	private void setDay(DateTime dt){
		StringBuilder sb=new StringBuilder();
		sb.append(dt.getYear())
		.append(StringUtils.leftPad(String.valueOf(dt.getMonthOfYear()), 2))
		.append(StringUtils.leftPad(String.valueOf(dt.getDayOfMonth()), 2));
		referenceDay=sb.toString();
	}
	
	public String getReferenceDay(){
		return referenceDay;
	}
	
	public void setAmount(Money amount){
		this.amount = amount;
	}
	
	public Money getAmount(){
		return this.amount;
	}
	
}
