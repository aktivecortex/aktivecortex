/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountMovement", propOrder = {
	    "movementId",
	    "accountId",
	    "accountDate",
	    "direction",
	    "amount",
	    "currency",
	    "recipient",
	    "dispDate",
	    "description",
	    "message",
	    "type",
	    "state",
	    "sign",
	    "cross",
	    "minAccountBal",
	    "minAccountDate",
	    "maxAccountBal",
	    "maxAccountDate"
	})
public class AccountMovementDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String movementId;
	
	@XmlElement(required = true)
	private String accountId;
	
	@XmlElement(required = true)
	private Date accountDate;
	
	@XmlElement(required = true)
	private Integer direction;
	
	@XmlElement(required = true)
	private Double amount;
	
	@XmlElement(required = true)
	private String currency;
	
	@XmlElement(required = true)
	private String recipient;
	
	@XmlElement(required = true)
	private Date dispDate;

	@XmlElement(required = true)
	private String description;

	@XmlElement(required = true)
	private String message;	

	@XmlElement(required = true)
	private String type;
	
	@XmlElement(required = true)
	private String state;
	
	@XmlElement(required = true)
	private String sign;
	
	@XmlElement(required = true)
	private boolean cross;
	
	

	public AccountMovementDTO(){}

	public String getMovementId() {
		return movementId;
	}

	public void setMovementId(String movementId) {
		this.movementId = movementId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Date getAccountDate() {
		return accountDate;
	}

	public void setAccountDate(Date accountDate) {
		this.accountDate = accountDate;
	}

	public Integer getDirection() {
		return direction;
	}

	public void setDirection(Integer direction) {
		this.direction = direction;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}	
	
	public boolean isCross() {
		return cross;
	}

	public void setCross(boolean cross) {
		this.cross = cross;
	}


	@XmlElement(required = true)
	private Double minAccountBal;
	
	@XmlElement(required = true)
	private Date minAccountDate;
	
	@XmlElement(required = true)
	private Double maxAccountBal;
	
	@XmlElement(required = true)
	private Date maxAccountDate;


	public Double getMinAccountBal() {
		return minAccountBal;
	}

	public void setMinAccountBal(Double minAccountBal) {
		this.minAccountBal = minAccountBal;
	}

	public Date getMinAccountDate() {
		return minAccountDate;
	}

	public void setMinAccountDate(Date minAccountDate) {
		this.minAccountDate = minAccountDate;
	}

	public Double getMaxAccountBal() {
		return maxAccountBal;
	}

	public void setMaxAccountBal(Double maxAccountBal) {
		this.maxAccountBal = maxAccountBal;
	}

	public Date getMaxAccountDate() {
		return maxAccountDate;
	}

	public void setMaxAccountDate(Date maxAccountDate) {
		this.maxAccountDate = maxAccountDate;
	}
	
	

}
