/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.domain.repository.bankaccount;

import org.aktivecortex.sample.bank.domain.bankaccount.BankAccount;
import org.axonframework.eventsourcing.HybridJpaRepository;
import org.axonframework.eventstore.EventStore;
import org.axonframework.repository.LockingStrategy;
import org.axonframework.util.jpa.EntityManagerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("bankAccountRepository")
public class BankAccountRepository extends HybridJpaRepository<BankAccount> {
	@Autowired
	protected BankAccountRepository(EntityManagerProvider entityManagerProvider) {
		super(entityManagerProvider, BankAccount.class, LockingStrategy.NO_LOCKING);
	}

	@Override
	@Autowired
	public void setEventStore(EventStore eventStore) {
		super.setEventStore(eventStore);
	}
}
