/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.sample.bank.service;

import com.eaio.uuid.UUID;
import com.google.common.collect.ImmutableMap;
import org.aktivecortex.api.audit.SecurityContext;

import java.util.Map;

public class DummySecurityContext implements SecurityContext {

	private String sessionId = new UUID().toString();
	
	private Map<String, String> attrs = ImmutableMap.of(
			"_user_id", "SYSTEM", 
			"_user_name","SYSTEM", 
			"_user_roleid","100", 
			"_user_rolename","ADMIN");

	@Override
	public String getUsername() {
		return "SYSTEM";
	}
	
	@Override
	public String getSessionId() {
		return sessionId ;
	}
	
	@Override
	public Map<String, String> getContextAttributes() {
		return attrs;
	}

}
