/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.model;

import com.eaio.uuid.UUID;
import org.aktivecortex.core.commandhandling.support.CommandHandlerUtils;
import org.aktivecortex.sample.bank.domain.bankaccount.BankAccount;
import org.axonframework.domain.AggregateIdentifier;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-datasource-ctx.xml"})
public class BankAccountMappingTest {

	@Autowired
	private EntityManagerFactory factory;
	
	private EntityManager entityManager;
	
	@Before
	public void setUp() throws Exception {		
		entityManager = factory.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		entityManager.close();
	}
	
	@Test
	public final void testApplyDomainEventStreamEvent() {
		
		AggregateIdentifier pk= CommandHandlerUtils.getNonNullId(new UUID().toString());
		
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		
		BankAccount account=new BankAccount(pk,"CUSTOMER_ID","ACCOUNT_NUM","CIT65555554444400000000008965","EUR");
		
		entityManager.persist(account);
		entityManager.flush();
		tx.commit();
		
		//tx = entityManager.getTransaction();
		//tx.begin();
		//account = (BankAccount) entityManager.find(BankAccount.class, pk.toString());
		
		//entityManager.persist(account);
		//entityManager.flush();
		//tx.commit();
		
		
	}
	
}