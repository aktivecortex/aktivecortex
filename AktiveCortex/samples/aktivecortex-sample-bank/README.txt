====
    Copyright (C) 2012-2013. Aktive Cortex

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
====

The following is a bank account management example that demonstrates asynchronous process execution application both stand along and distributed.

Before start this sample please apply the following configuration 
choosing between single instance or distributed mode.

Tested Java version is 1.6
Tested Maven version is 3.0.4
Tested ServletContainer is Apache Tomcat => 7.0.28 and 7.0.29

Maven build instructions:
mvn clean package

Enjoy!

---------------------------------
1. Single Instance Mode
---------------------------------

1.1 Database
---------------------------------
Create and start the database sqldb instance as follows:
1.1.1 create directory "cortexsampledb" at some path. Let's presume in HOME_DIR (bash ~ or windows %HOME_DIR%) This is your DB home dir.
1.1.2 cd to your DB home dir: cd %HOME_DIR%/cortexsampledb
1.1.3 start the database server as follows:
bash 	  "java -cp ./hsqldb-2.2.8.jar org.hsqldb.server.Server --database.0 file:~/cortexsampledb --dbname.0 cortexdb"
windows   "java -cp ./hsqldb-2.2.8.jar org.hsqldb.server.Server --database.0 file:%HOME_DIR%/cortexsampledb --dbname.0 cortexdb"

1.2 ActiveMQ
---------------------------------
Build and start the JMS broker at AktiveCortex\samples\aktivecortex-amq-broker
1.2.1 execute maven task:
      "mvn clean install"

1.3 Servlet Container
---------------------------------
Configure Tomcat Servlet Container and deply the webapplication.
1.3.1 Enable NIO connector by putting in server.xml the following line:
	  <Connector connectionTimeout="20000" port="8080" protocol="org.apache.coyote.http11.Http11NioProtocol" redirectPort="8443"/>
	  Please verify no other connector is defined for port 8080 (comment out the default http connector)
1.3.2 Edit launch configuration and set the following VM arguments (replace the <hostname> with your actual hostname or ip address,
if you use command line you can set these arguments by assigning CATALINA_OPTS variable on setenv.(bat|sh)):
     -Xmx512m -Xss192k -XX:MaxPermSize=128m -Djava.naming.provider.url=tcp://<hostname>:61616
1.3.3 Deploy the war and start Tomcat
1.3.4 Use your favorite browser and navigate to http://<hostname>:8080/<war-name>

---------------------------------
2. Distributed Mode
---------------------------------
2.1 Choose a Node where to run the Database and the JMS Broker. 
The database and the broker can run on the same host or on different ones (I.E. you can choose to run the db on host1 and the broker on host2)

2.2 Database
---------------------------------
Create and start the database sqldb instance on the master node as follows:
1.1.1 create directory "cortexsampledb" at some path. Let's presume in HOME_DIR (bash ~ or windows %HOME_DIR%) This is your DB home dir.
1.1.2 cd to your DB home dir: cd %HOME_DIR%/cortexsampledb
1.1.3 start the database server as follows:
bash 	  "java -cp ./hsqldb-2.2.9.jar org.hsqldb.server.Server --database.0 file:~/cortexsampledb\;hsqldb.tx=mvcc --dbname.0 cortexdb"
windows   "java -cp ./hsqldb-2.2.9.jar org.hsqldb.server.Server --database.0 file:%HOME_DIR%/cortexsampledb\;hsqldb.tx=mvcc --dbname.0 cortexdb"

2.3 ActiveMQ
---------------------------------
Build and start the JMS broker at AktiveCortex\samples\aktivecortex-amq-broker on the master node

2.3.1 execute maven task:
      "mvn clean install"


2.4 On each Node
---------------------------------
2.4.1 edit AktiveCortex\samples\aktivecortex-web\src\main\resources\datasource.properties file and replace the database.url properties with the hostname or the ip address of the DATABASE node

2.4.1.1 build the webapplication distribution with new parameters
cd AktiveCortex (parent project directory)
mvn clean package -pl samples/aktivecortex-web

Configure Tomcat Servlet Container and deploy the webapplication.
2.4.2 Enable NIO connector by putting in server.xml the following line:
	  <Connector connectionTimeout="20000" port="8080" protocol="org.apache.coyote.http11.Http11NioProtocol" redirectPort="8443"/>
	  Please verify no other connector is defined for port 8080 (comment out the default http connector)
	  
2.4.3 Edit launch configuration and set the following VM arguments (replace the <hostname> with the BROKER hostname or ip address,
if you use command line you can set these arguments by assigning CATALINA_OPTS variable on setenv.(bat|sh)):
     -Xmx512m -Xss192k -XX:MaxPermSize=128m -Djava.naming.provider.url=tcp://<hostname>:61616
2.4.4 Deploy the war and start Tomcat
2.4.5 Use your favorite browser and navigate to http://<hostname>:8080/<war-name>

---------------------------------
3. Distributed Performance Test Mode
---------------------------------
This mode requires the use of MySQL 5.5 as database and a network of ActiveMQ brokers connected to each other.
The database is running on a single node while brokers runs on each node.

3.1 Database
---------------------------------
Choose an host where to run the Database.

3.1.1.Install MySQL 5.5
Follow the instruction on the manual available here:
    http://dev.mysql.com/doc/refman/5.5/en/installing.html

3.1.2 Create the Cortex database
Login to the db then apply the following commands:

    mysql>create database cortex;
    mysql>GRANT ALL PRIVILEGES ON cortex.* to 'build'@'%'  IDENTIFIED BY 'build';

3.2 ActiveMQ
---------------------------------
Repeat the following steps for each host:
3.2.1
Edit the file at AktiveCortex\samples\aktivecortex-amq-broker\src\main\resources\perftests-activemq.xml
- Uncomment the xml tag that starts with '<amq:networkConnectors>'
- Replace the uri attribute value (value to replace is 'nearestBroker') with the address of the other host
  for both xml nodes with name 'networkConnector'

3.2.2
Build and start the JMS broker at AktiveCortex\samples\aktivecortex-amq-broker
by executing the following steps:
    - "cd AktiveCortex" #(parent project directory)
    - "mvn clean install -Denv=perf samples/aktivecortex-amq-broker"

3.3 Tomcat
----------------------------------
Repeat the following steps for each host:

3.3.1 Enable NIO connector by putting in server.xml the following line:
	  <Connector connectionTimeout="20000" port="8080" protocol="org.apache.coyote.http11.Http11NioProtocol" redirectPort="8443"/>
	  Please verify no other connector is defined for port 8080 (comment out the default http connector)

3.3.2 Edit launch configuration and set the following VM arguments (replace the <hostname> with the BROKER hostname or ip address,
  - Define the CATALINA_OPTS variable on setenv.(bat|sh) with the following value:
     -Xmx512m -Xss192k -XX:MaxPermSize=128m -Djava.naming.provider.url=failover:(tcp://localhost:61616,tcp://remotehost:61616)?randomize=false
  - Define the JAVA_OPTS variable on the same file with the following value:
      $JAVA_OPTS -Dspring.profiles.active=perf (please replace $JAVA_OPTS with %JAVA_OPTS% if you use Windows)

3.3.3 edit AktiveCortex\samples\aktivecortex-web\src\main\resources\mysql.datasource.properties file and replace the
      database.url properties with the hostname or the ip address of the DATABASE node

3.3.4 build the webapplication
 -  "cd AktiveCortex" #(parent project directory)
 -  "mvn clean package -pl samples/aktivecortex-web"

3.3.5 Deploy the war and start Tomcat

3.3.6 Use your favorite browser and navigate to http://<hostname>:8080/<war-name>
