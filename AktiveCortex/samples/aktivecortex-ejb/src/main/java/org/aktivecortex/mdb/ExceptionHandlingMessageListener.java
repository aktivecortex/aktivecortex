/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.mdb;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.aktivecortex.jms.utils.JMSUtils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * Temporary and unrecoverable exceptions handling
 * In the context of a time window configured using the methods
 * {@link #getControlWindowUnit()} and {@link #getControlWindowPeriod()}.
 *
 * Redelivery is attempted for messages causing transient exceptions
 * The limit indicated by the redefined method {@link #getMaxRedeliveryAttempts()}.
 *
 * Messages that cause permanent exceptions are discarded
 * immediately (in the case of transactions BMT) or at
 * the next attempt (in the case of transactions CMT).
 *
 * In the latter case (CMT), the message is delivered a second time but not processed.
 * The behavior just described obviously depends on the configuration of the
 * JMS destination from which you receive.
 *
 * @author d.giffone
 *
 */
public abstract class ExceptionHandlingMessageListener extends AbstractMessageListener {

	static final TimeUnit CONTROL_WINDOW_UNIT = TimeUnit.MINUTES;
	static final int CONTROL_WINDOW_PERIOD = 3;
	static final int MAX_ATTEMPTS = 5;

	protected static TimeUnit getControlWindowUnit() {
		return CONTROL_WINDOW_UNIT;
	}

	protected static int getControlWindowPeriod() {
		return CONTROL_WINDOW_PERIOD;
	}

	protected static int getMaxRedeliveryAttempts() {
		return MAX_ATTEMPTS;
	}

	protected abstract ConnectionFactory getConnectionFactory();

	protected abstract Queue getErrorDestination();

	private static final LoadingCache<String, Boolean> permanentlyFailedMsgs = CacheBuilder
				.newBuilder()
				.maximumSize(1000)
				.expireAfterAccess(getControlWindowPeriod(), getControlWindowUnit())
				.build(new CacheLoader<String, Boolean>() {
					@Override
					public Boolean load(String arg0) throws Exception {
						return Boolean.TRUE;
					}
				});

	private static final LoadingCache<String, Boolean> temporaryFailedMsgs = CacheBuilder
		.newBuilder()
		.maximumSize(1000)
		.expireAfterAccess(getControlWindowPeriod(), getControlWindowUnit())
		.build(new CacheLoader<String, Boolean>() {
			@Override
			public Boolean load(String arg0) throws Exception {
				return Boolean.TRUE;
			}

		});

	private static final LoadingCache<String, AtomicInteger> deliveryAttempts = CacheBuilder
		.newBuilder()
		.maximumSize(1000)
		.expireAfterAccess(getControlWindowPeriod(), getControlWindowUnit())
		.build(new CacheLoader<String, AtomicInteger>() {
			@Override
			public AtomicInteger load(String arg0) throws Exception {
				return new AtomicInteger();
			}
		});

	public ExceptionHandlingMessageListener() {
		super();
	}

	@Override
	protected boolean reject(Message msg) throws JMSException {
		final String messageID = msg.getJMSMessageID();
			if (permanentlyFailedMsgs.asMap().containsKey(messageID)) {
				getLogger().warn("message identified by JMSMessageID [{}] previously marked as permanently failed",
						messageID);
				return true;
			}
			if (temporaryFailedMsgs.asMap().containsKey(messageID)) {
				getLogger().warn("message identified by JMSMessageID [{}] previously marked as temporary failed",
						messageID);
				discard(msg);
				return true;
			}
		return false;
	}

	@Override
	protected void markAsTemporaryFailed(Message msg) {
			try {
				final String messageID = msg.getJMSMessageID();
				final AtomicInteger attempts = deliveryAttempts.getUnchecked(messageID);
				final int attemptsNo = attempts.incrementAndGet();
				if (attemptsNo >= getMaxRedeliveryAttempts()) {
					getLogger().warn("marking message identified by id [{}] as temporary failed", messageID);
					deliveryAttempts.asMap().remove(messageID);
					temporaryFailedMsgs.getUnchecked(messageID);
				} else {
					getLogger().warn("marking message identified by JMSMessageID [{}] as temporary failed for the [{}]# time",
							messageID, attemptsNo);
				}
			} catch (JMSException e) {
				getLogger().error("Can't mark message as temporary failed due to exception", e);
			}
	}

	@Override
	protected void markAsPermanentlyFailed(Message msg) {
		try {
			String messageID = msg.getJMSMessageID();
				getLogger().warn("marking message identified by id [{}] as permanently failed", messageID);
				permanentlyFailedMsgs.getUnchecked(messageID);
		} catch (JMSException e) {
			getLogger().error("Can't mark message as permanently failed due to exception", e);
		}
	}


	private void discard(Message msg) {
		Connection connection = null; //NOSONAR closed in finally block
		Session session = null;
		MessageProducer producer = null;
		try {
			String messageID = msg.getJMSMessageID();
			getLogger()
			.warn(
					"discarding message with id [{}] failed for a number of attempts over the limit allowed",
					messageID);
			connection = getConnectionFactory().createConnection();
			connection.start();
			session = connection.createSession(true, 0);
			producer = session.createProducer(getErrorDestination());
			producer.send(msg);
			getLogger().warn("message [{}] redirected to the error destination", msg);
		} catch (JMSException e) {
			getLogger().error("Can't connect to the error destination", e);
		} finally {
			JMSUtils.close(connection, session, producer);
		}
	}

}