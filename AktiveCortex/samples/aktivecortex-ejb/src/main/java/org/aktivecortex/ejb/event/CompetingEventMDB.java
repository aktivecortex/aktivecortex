/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.ejb.event;


import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import org.aktivecortex.core.message.channel.MessageDispatcher;
import org.aktivecortex.mdb.AbstractMessageListener;
import org.axonframework.serializer.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

/**
 * @author d.giffone
 *
 * Transactional MessageListener for idempotent events distributed on the queue.
 *
 */
@MessageDriven(name= "common.CompetingEventMDB", mappedName = "jms/Q_Event_1", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class CompetingEventMDB extends AbstractMessageListener  {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompetingEventMDB.class);

	@Autowired
	@Qualifier("CompetingEventChannelAdapter")
	private MessageDispatcher<?> eventDispatcher;

	@Resource
    private MessageDrivenContext mdc;

	@Resource(mappedName = "jms/CF_Command")
    private ConnectionFactory connectionFactory;

	@Resource(mappedName = "jms/Q_DeadLetter")
    private Queue errorDestination;

	@Autowired
	@Qualifier("protostuffMessageSerializer")
	private Serializer<Message> serializer;

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	protected MessageDrivenContext getMessageDrivenContext() {
		return mdc;
	}

	@Override
	protected void rollback() {
        // nothing to do
	}

	@Override
	protected void markAsPermanentlyFailed(Message inMessage) {
        // nothing to do
	}

	@Override
	protected void markAsTemporaryFailed(Message inMessage) {
        // nothing to do
	}

	@Override
	protected MessageDispatcher<?> getDispatcher() {
		return eventDispatcher;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Collection<MessageDispatcher<?>> getDispatchers() {
		Collection<MessageDispatcher<?>> result = new ArrayList<MessageDispatcher<?>>();
		result.add(eventDispatcher);
		return result;
	}

	@Override
	protected boolean reject(@SuppressWarnings("unused") Message inMessage) throws JMSException  {
		return false;
	}

	@Override
	protected void begin() throws NotSupportedException, SystemException {
        // nothing to do
	}

	@Override
	protected void commit() throws RollbackException, HeuristicMixedException, HeuristicRollbackException,
			SystemException {
        // nothing to do
	}



}
