/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.mdb;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

/**
 * Supporto alle gestione delle transazioni dichiarativa (gestite dal
 * container).
 * L'eventuale MDB derivato deve dichiare l'uso delle transazioni CMT
 * tramite la seguente annotazione oppure tramite deployment descriptor
 *
 * {@link @TransactionManagement(TransactionManagementType.CONTAINER)}
 *
 * N.B. La classe è adatta nel caso di transaction batching.
 * A causa della mancanza di ereditarietà multipla in java
 * è necessario estendere la classe {@link ExceptionHandlingMessageListener}
 *
 * @author d.giffone
 */
public abstract class DeclarativeTxMessageListener extends ExceptionHandlingMessageListener {

	public DeclarativeTxMessageListener() {
		super();
	}

	@Override
	protected void begin() throws NotSupportedException, SystemException {
		// nothing to do
	}

	@Override
	protected void commit() throws  RollbackException, HeuristicMixedException, HeuristicRollbackException,
			SystemException {
		// nothing to do
	}

	@Override
	protected void rollback() {
		try {
			if (!getMessageDrivenContext().getRollbackOnly()) {
				getMessageDrivenContext().setRollbackOnly();
				getLogger().info("Transaction marked as rollback");
			} else {
				getLogger().info("Transaction already marked as rollback");
			}
		} catch (Exception e) {
			getLogger().error("Can't mark transaction as rollback due to exception", e);
		}
	}

}