/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.mdb;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;


/**
 * Supporto alle gestione delle transazioni programmatica (gestite dall'utente).
 * L'eventuale MDB derivato deve dichiare l'uso delle transazioni BMT
 * tramite la seguente annotazione oppure tramite deployment descriptor
 *
 **{@link @TransactionManagement(TransactionManagementType.BEAN)}
 *
 * N.B. La classe non è adatta nel caso di transaction batching.
 * A causa della mancanza di ereditarietà multipla in java è necessario
 * estendere la classe {@link ExceptionHandlingMessageListener}
 *
 * @author d.giffone
 */
public abstract class ProgrammaticTxMessageListener extends ExceptionHandlingMessageListener {

	public ProgrammaticTxMessageListener() {
		super();
	}

	@Override
	protected void begin() throws NotSupportedException, SystemException {
		getMessageDrivenContext().getUserTransaction().begin();
	}

	@Override
	protected void commit() throws RollbackException, HeuristicMixedException, HeuristicRollbackException,
			SystemException {
				getMessageDrivenContext().getUserTransaction().commit();
			}

	@Override
	protected void rollback() {
		try {
			int txStatus = getMessageDrivenContext().getUserTransaction().getStatus();
			if (txStatus == Status.STATUS_ACTIVE || txStatus == Status.STATUS_MARKED_ROLLBACK) {
				getMessageDrivenContext().getUserTransaction().rollback();
				getLogger().info("Transaction rolled back");
			} else {
				getLogger().info("rollback not necessary due to transaction status [{}]", getMessageDrivenContext().getUserTransaction().getStatus());
			}
		} catch (Exception e) {
			getLogger().error("Can't rollback due to exception", e);
		}
	}

}