/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.ejb.command;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;

import org.aktivecortex.core.message.channel.MessageDispatcher;
import org.aktivecortex.mdb.ProgrammaticTxMessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;


@MessageDriven(name = "common.CommandMDB", mappedName = "jms/Q_Command", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
@Interceptors(SpringBeanAutowiringInterceptor.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class CommandMDB extends ProgrammaticTxMessageListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommandMDB.class);

	@Autowired
	@Qualifier("CommandChannelAdapter")
	private MessageDispatcher<?> commandDispatcher;

	@Resource
    private MessageDrivenContext mdc;

	@Resource(mappedName = "jms/CF_Command")
    private ConnectionFactory commandConnectionFactory;

	@Resource(mappedName = "jms/Q_DeadLetter")
    private Queue commandErrorDestination;

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	protected MessageDrivenContext getMessageDrivenContext() {
		return mdc;
	}

	@Override
	protected ConnectionFactory getConnectionFactory() {
		return commandConnectionFactory;
	}

	@Override
	protected Queue getErrorDestination() {
		return commandErrorDestination;
	}

	@Override
	protected MessageDispatcher<?> getDispatcher() {
		return commandDispatcher;
	}

	@Override
	protected Collection<MessageDispatcher<?>> getDispatchers() {
		Collection<MessageDispatcher<?>> result = new ArrayList<MessageDispatcher<?>>();
		result.add(commandDispatcher);
		return result;
	}


}
