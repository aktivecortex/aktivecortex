/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.ejb.command;

import org.aktivecortex.api.command.Command;
import org.aktivecortex.api.commandhandling.AsyncCommandBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

import javax.interceptor.Interceptors;
import java.util.Map;

/**
 * To deploy the class as a SLSB provide a ejb-jar.xml 
 * descriptor that defines the EJB class and local interface.
 * 
    <ejb-jar xmlns="http://java.sun.com/xml/ns/javaee"
	         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/ejb-jar_3_0.xsd"
	         version="3.0">
	  <enterprise-beans>
	    <session>
	      <ejb-name>CommandDispatcherBean</ejb-name>
	      <business-local>org.aktivecortex.api.command.CommandDispatcher</business-local>
	      <ejb-class> org.aktivecortex.ejb.command.CommandDispatcherBean</ejb-class>
	      <session-type>Stateless</session-type>
	      <transaction-type>Container</transaction-type>
	    </session>
	  </enterprise-beans>
	 </ejb-jar>
 * 
 * @author d.giffone
 *
 */
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class CommandDispatcherBean implements AsyncCommandBus {
	
	@Autowired
	private AsyncCommandBus delegate;

	@Override
	public void dispatchAsync(Command... commands) {
		delegate.dispatchAsync(commands);
	}

    @Override
    public void dispatchAsync(Map<String, String> additionalHeaders, Command... commands) {
        delegate.dispatchAsync(additionalHeaders, commands);
    }
}
