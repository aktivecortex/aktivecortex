/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.ejb.event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import org.aktivecortex.core.message.channel.MessageDispatcher;
import org.aktivecortex.mdb.AbstractMessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;

/**
 * @author d.giffone
 *
 * Non-transactional MessageListener intended for topic events consumed by Idempotent consumer
 */
@MessageDriven(name= "common.NonCompetingEventMDB", mappedName = "jms/T_Event", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic") })
@Interceptors(SpringBeanAutowiringInterceptor.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class NonCompetingEventMDB extends AbstractMessageListener {

	private static final TimeUnit CONTROL_WINDOW_UNIT = TimeUnit.SECONDS;

	private static final int CONTROL_WINDOW_PERIOD = 30;

    private static final int SIZE = 1000;

    private static final Logger LOGGER = LoggerFactory.getLogger(NonCompetingEventMDB.class);

    // cache values must be shared between all instances
	private static final Cache<String, Boolean> RECEIVED_EVENTS = CacheBuilder
			.newBuilder()
			.maximumSize(SIZE)
			.expireAfterAccess(NonCompetingEventMDB.CONTROL_WINDOW_PERIOD, NonCompetingEventMDB.CONTROL_WINDOW_UNIT)
			.build(new CacheLoader<String, Boolean>() {
				@Override
				public Boolean load(String arg0) throws Exception {
					return Boolean.TRUE;
				}
			});

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Autowired
	@Qualifier("NonCompetingEventChannelAdapter")
	private MessageDispatcher<?> messageDispatcher;

	@Resource
    private MessageDrivenContext mdc;

	@Override
	protected MessageDrivenContext getMessageDrivenContext() {
		return mdc;
	}

	@Override
	protected MessageDispatcher<?> getDispatcher() {
		return messageDispatcher;
	}

	@Override
	protected Collection<MessageDispatcher<?>> getDispatchers() {
		Collection<MessageDispatcher<?>> result = new ArrayList<MessageDispatcher<?>>();
		result.add(messageDispatcher);
		return result;
	}

	@Override
	protected void begin() throws NotSupportedException, SystemException {
        // nothing to do

	}

	@Override
	protected void commit() throws RollbackException, HeuristicMixedException, HeuristicRollbackException,
			SystemException {

	}

	@Override
	protected void markAsPermanentlyFailed(Message inMessage) {
	}

	@Override
	protected void markAsTemporaryFailed(Message inMessage) {
	}

	@Override
	protected boolean reject(Message inMessage) throws JMSException  {
		final String messageID = inMessage.getJMSMessageID();
		final Boolean exists = RECEIVED_EVENTS.asMap().putIfAbsent(messageID, Boolean.TRUE);
		if (null != exists) {
			LOGGER.debug("message identified by JMSMessageID [{}] already received",
                    messageID);
			return true;
		}
		return false;
	}

	@Override
	protected void rollback() {
        // nothing to do
	}

}
