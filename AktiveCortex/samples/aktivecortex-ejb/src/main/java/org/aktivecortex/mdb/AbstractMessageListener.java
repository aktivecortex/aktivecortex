/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.mdb;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.MessageDrivenContext;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import org.aktivecortex.api.message.MessageHeadersConstants;
import org.aktivecortex.api.message.MessagingException;
import org.aktivecortex.core.message.channel.MessageDispatcher;
import org.aktivecortex.core.message.spi.impl.MessagingSystemFactory;
import org.aktivecortex.core.message.spi.IMessagingSystemFactory.ConfigurationKey;
import org.slf4j.Logger;

public abstract class AbstractMessageListener implements MessageListener {
	
	private static final String UNKNOWN_CORTEX_ID = "UNKNOWN_ID";
	private static final String MESSAGE_ORDERING_PROPERTY = (String) MessagingSystemFactory
			.getProperty(ConfigurationKey.ORDERED_DELIVERY_PROPERTY);
	private static final String UNORDERED_MSG_LOG_TEMPLATE = "<-- MessageID [{}] - JMSMessageID [{}]";
	private static final String ORDERED_MSG_LOG_TEMPLATE = "<-- MessageID [{}]  - RoutingKey [{}] - JMSMessageID [{}]";
	
	protected abstract Logger getLogger();

	protected abstract MessageDrivenContext getMessageDrivenContext();

	protected abstract MessageDispatcher<?> getDispatcher();

	protected abstract Collection<MessageDispatcher<?>> getDispatchers();

	public void onMessage(Message inMessage) {
		BytesMessage msg = null;
		boolean begun = false;
		String cortexMessageID = UNKNOWN_CORTEX_ID;
		boolean temporaryFailed = false;
		try {
			if (inMessage instanceof BytesMessage) {
				msg = (BytesMessage) inMessage;
				final String messageID = msg.getJMSMessageID();
				final byte[] bytes = new byte[(int) msg.getBodyLength()];
				cortexMessageID = msg.getStringProperty(MessageHeadersConstants.MESSAGE_ID);
				msg.readBytes(bytes);
				if (null != msg.getStringProperty(MESSAGE_ORDERING_PROPERTY)) {
					getLogger().info(ORDERED_MSG_LOG_TEMPLATE,
							new Object[] { cortexMessageID, 
										   msg.getStringProperty(MESSAGE_ORDERING_PROPERTY),
									       messageID });
				} else {
					getLogger().info(UNORDERED_MSG_LOG_TEMPLATE, new Object[] { cortexMessageID, messageID });
				}
				if (reject(inMessage)) {
					getLogger().warn("Rejected message [{}]", cortexMessageID);
					return;
				}
				begin();
				begun = true;
				getDispatcher().dispatch(bytes);
				commit();
			} else {
				throw new IllegalArgumentException("Message of wrong type: " + inMessage.getClass().getName());
			}
		} catch (JMSException e) {
			getLogger().error("Unrecoverable exception", e);
			markAsPermanentlyFailed(inMessage);
		} catch (IllegalStateException e) {
			// BEGIN - COMMIT
			getLogger().error("Unrecoverable exception", e);
			markAsPermanentlyFailed(inMessage);
			rollbackIfNecessary(begun);
		} catch (NotSupportedException e) {
			// BEGIN
			getLogger().error("Unrecoverable exception", e);
			markAsPermanentlyFailed(inMessage);
		} catch (SystemException e) {
			// BEGIN - COMMIT
			getLogger().error("Unrecoverable exception", e);
			markAsPermanentlyFailed(inMessage);
			rollbackIfNecessary(begun);
		} catch (SecurityException e) {
			// COMMIT
			getLogger().error("Unrecoverable exception", e);
			markAsPermanentlyFailed(inMessage);
			rollbackIfNecessary(begun);
		} catch (RollbackException e) {
			// COMMIT
			getLogger().error("Recoverable exception", e);
			markAsTemporaryFailed(inMessage);
			rollbackIfNecessary(begun);
			temporaryFailed = true;
		} catch (HeuristicMixedException e) {
			// COMMIT
			getLogger().error("Recoverable exception", e);
			markAsTemporaryFailed(inMessage);
			rollbackIfNecessary(begun);
			temporaryFailed = true;
		} catch (HeuristicRollbackException e) {
			// COMMIT
			getLogger().error("Recoverable exception", e);
			markAsTemporaryFailed(inMessage);
			rollbackIfNecessary(begun);
			temporaryFailed = true;
		} catch (RuntimeException e) {
			// DISPATCH
			getLogger().error("Unrecoverable exception", e);
			markAsPermanentlyFailed(inMessage);
			rollbackIfNecessary(begun);
		} finally {
			if (temporaryFailed) {
				throw new MessagingException(String.format("%s Message rejected due to a temporary failure", cortexMessageID));
			}
		}
	}

	protected abstract boolean reject(Message inMessage) throws JMSException;

	protected abstract void markAsTemporaryFailed(Message inMessage);

	protected abstract void markAsPermanentlyFailed(Message inMessage);

	protected abstract void begin() throws NotSupportedException, SystemException;

	protected abstract void commit() throws RollbackException, HeuristicMixedException, HeuristicRollbackException,
			SystemException;

	protected abstract void rollback();

	private void rollbackIfNecessary(boolean rollbackNecessary) {
		if (rollbackNecessary) {
			rollback();
		}
	}

	@PostConstruct
	protected void logCreate() {
		getLogger().info("created: [{}]", getDispatchers());
	}

	@PreDestroy
	protected void logDestroy() {
		getLogger().info("Destroying: [{}]", getDispatchers());
	}

}
