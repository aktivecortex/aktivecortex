/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.tracing.util;

import com.github.kristofa.brave.ServerSpanThreadBinder;
import org.aktivecortex.core.axon2backport.saga.annotation.AsyncAnnotatedSagaManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.Assert;


/**
 * Helper class to provide SagaManager with a Cross Thread span binder implementation
 */
public class AsyncAnnotatedSagaManagerPostProcessor implements BeanPostProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncAnnotatedSagaManagerPostProcessor.class);
    private ServerSpanThreadBinder serverSpanThreadBinder;

    public void setServerSpanThreadBinder(ServerSpanThreadBinder serverSpanThreadBinder) {
        Assert.notNull(serverSpanThreadBinder, "ServerSpanThreadBinder can't be null");
        this.serverSpanThreadBinder = serverSpanThreadBinder;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof AsyncAnnotatedSagaManager) {
            AsyncAnnotatedSagaManager sagaManager = (AsyncAnnotatedSagaManager) bean;
            sagaManager.setTracingThreadBinder(serverSpanThreadBinder);
            LOGGER.info("AsyncAnnotatedSagaManager configured to propagate tracing information to event handlers");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
