/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.tracing;

import com.github.kristofa.brave.HeaderConstants;
import org.aktivecortex.api.message.MessageHeaders;

public final class Trace {

    private static final String NULL = "null";
    private Long traceId;
    private Long spanId;
    private Long parentSpanId;
    private Boolean shouldBeSampled;

    private Trace() {}

    private void setTraceId(final Long traceId) {
        if (traceId != null) {
            this.traceId = traceId;
        }
    }

    private void setSpanId(final Long spanId) {
        if (spanId != null) {
            this.spanId = spanId;
        }
    }

    private void setParentSpanId(final Long parentSpanId) {
        this.parentSpanId = parentSpanId;
    }

    private void setShouldBeSampled(final Boolean shouldBeSampled) {
        this.shouldBeSampled = shouldBeSampled;
    }

    public Long getTraceId() {
        return traceId;
    }

    public Long getSpanId() {
        return spanId;
    }

    public Long getParentSpanId() {
        return parentSpanId;
    }

    public boolean shouldBeTraced() {
        if (shouldBeSampled == null || Boolean.TRUE.equals(shouldBeSampled)) {
            return true;
        }
        return false;
    }


    public static Trace from(MessageHeaders headers) {
        final Trace trace = new Trace();
        for (final String key : headers.keySet()) {
            if (HeaderConstants.TRACE_ID.equalsIgnoreCase(key)) {
                trace.setTraceId(Long.valueOf((String) headers.get(key)));
            } else if (HeaderConstants.SPAN_ID.equalsIgnoreCase(key)) {
                trace.setSpanId(Long.valueOf((String) headers.get(key)));
            } else if (HeaderConstants.PARENT_SPAN_ID.equalsIgnoreCase(key)
                    && null != headers.get(key)
                    && !NULL.equalsIgnoreCase((String) headers.get(key))) {
                trace.setParentSpanId(Long.valueOf((String) headers.get(key)));
            } else if (HeaderConstants.SHOULD_GET_TRACED.equalsIgnoreCase(key)) {
                trace.setShouldBeSampled(Boolean.valueOf((String) headers.get(key)));
            }
        }
        return trace;
    }

    /*static Trace from(final HttpServletRequest request) {
        final HttpHeaders httpHeaders = getHeaders(request);
        final Trace traceData = new Trace();
        for (final Map.Entry<String, List<String>> headerEntry : httpHeaders.entrySet()) {
            if (HeaderConstants.TRACE_ID.equalsIgnoreCase(headerEntry.getKey())) {
                traceData.setTraceId(getFirstLongValueFor(headerEntry));
            } else if (HeaderConstants.SPAN_ID.equalsIgnoreCase(headerEntry.getKey())) {
                traceData.setSpanId(getFirstLongValueFor(headerEntry));
            } else if (HeaderConstants.PARENT_SPAN_ID.equalsIgnoreCase(headerEntry.getKey())) {
                traceData.setParentSpanId(getFirstLongValueFor(headerEntry));
            } else if (HeaderConstants.SHOULD_GET_TRACED.equalsIgnoreCase(headerEntry.getKey())) {
                traceData.setShouldBeSampled(getFirstBooleanValueFor(headerEntry));
            }
        }
        return traceData;
    }

    private static HttpHeaders getHeaders(HttpServletRequest request) {
        HttpHeaders result = new HttpHeaders();
        for (Enumeration names = request.getHeaderNames(); names.hasMoreElements(); ) {
            String name = (String) names.nextElement();
            for (Enumeration values = request.getHeaders(name); values.hasMoreElements(); ) {
                String value = (String) values.nextElement();
                result.add(name, value);
            }
        }
        return result;
    }

    private static Long getFirstLongValueFor(final Map.Entry<String, List<String>> headerEntry) {
        final List<String> values = headerEntry.getValue();
        if (values != null && values.size() > 0) {
            return Long.valueOf(headerEntry.getValue().get(0));
        }
        return null;
    }

    private static Boolean getFirstBooleanValueFor(final Map.Entry<String, List<String>> headerEntry) {
        final List<String> values = headerEntry.getValue();
        if (values != null && values.size() > 0) {
            return Boolean.valueOf(headerEntry.getValue().get(0));
        }
        return null;
    }*/

}