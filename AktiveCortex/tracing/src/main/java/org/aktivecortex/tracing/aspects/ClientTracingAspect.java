/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.tracing.aspects;

import com.github.kristofa.brave.ClientTracer;
import com.github.kristofa.brave.EndPointSubmitter;
import com.github.kristofa.brave.HeaderConstants;
import com.github.kristofa.brave.SpanId;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageHeaders;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.axonframework.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class ClientTracingAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientTracingAspect.class);

    private static final String TRACE_ENABLED = "true";

    private final ClientTracer clientTracer;

    private final EndPointSubmitter endPointSubmitter;

    public ClientTracingAspect(ClientTracer tracer, EndPointSubmitter submitter) {
        Assert.notNull(tracer, "Client tracer can't be null");
        Assert.notNull(submitter, "EndPointSubmitter can't be null");
        this.clientTracer = tracer;
        this.endPointSubmitter = submitter;
        LOGGER.info("Client side tracing enabled");
    }

    @Pointcut(value = "execution(* org.aktivecortex.core.message.channel.MessageChannel+.send(..))")
    public void traceableClient() {}

    @Around("traceableClient()")
    public Object traceClient(ProceedingJoinPoint joinPoint) throws Throwable {
        final Message message = (Message) joinPoint.getArgs()[0];
        final MessageHeaders headers = message.getMessageHeaders();

        // Create a span with payload name: e.g. myCommand-123
        Object payload = message.getPayload();
        SpanId span = clientTracer.startNewSpan(payload.toString());

        // Span can be null if trace filter drop the request (for example to enforce tracing rate limit respect)
        if (null != span) {
            // Populate Message Headers with tracing data
            headers.put(HeaderConstants.TRACE_ID, String.valueOf(span.getTraceId()));
            headers.put(HeaderConstants.SPAN_ID, String.valueOf(span.getSpanId()));
            headers.put(HeaderConstants.PARENT_SPAN_ID, String.valueOf(span.getParentSpanId()));
            headers.put(HeaderConstants.SHOULD_GET_TRACED, TRACE_ENABLED);

            // Mark span with CS - client send
            clientTracer.setClientSent();
        }

        try {
            // Send message
            return joinPoint.proceed();
        } finally {
            if (null != span) {
                // Mark span with Fictional CR  - client received
                clientTracer.setClientReceived();
            }
        }


    }
}