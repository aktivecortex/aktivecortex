/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.tracing.aspects;

import com.github.kristofa.brave.ServerTracer;
import org.aktivecortex.api.message.Message;
import org.aktivecortex.api.message.MessageHeaders;
import org.aktivecortex.tracing.Trace;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.axonframework.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class ServerTracingAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerTracingAspect.class);

    private final ServerTracer serverTracer;

    public ServerTracingAspect(ServerTracer tracer) {
        Assert.notNull(tracer, "Server tracer can't be null");
        this.serverTracer = tracer;
        LOGGER.info("Server side tracing enabled");
    }

    @Pointcut(value = "execution(* org.aktivecortex.core.message.channel.MessageHandler.handleMessage(..))")
    public void traceableServer() {
    }

    @Around("traceableServer()")
    public Object traceServer(ProceedingJoinPoint joinPoint) throws Throwable {
        final Message message = (Message) joinPoint.getArgs()[0];
        final MessageHeaders headers = message.getMessageHeaders();
        // Get Trace data
        final Trace traceData = Trace.from(headers);
        Object payload = message.getPayload();

        // Enable || Disable tracing
        serverTracer.setShouldTrace(traceData.shouldBeTraced());
        if (traceData.getTraceId() == null && traceData.getSpanId() == null) {
            // Clear span as no tracing infos were received
            serverTracer.clearCurrentSpan();
        } else {
            // Set the span with received values
            serverTracer.setSpan(
                    traceData.getTraceId(),
                    traceData.getSpanId(),
                    traceData.getParentSpanId(),
                    payload.toString());

            // Mark span with SR - server received
            serverTracer.setServerReceived();
        }

        try {
            // Handle message
            return joinPoint.proceed();
        } finally {
            // mark span with fictional SS - server send.
            // (Safe even if server state is not set. Nothing will happen in this case)
            serverTracer.setServerSend();
        }

    }
}