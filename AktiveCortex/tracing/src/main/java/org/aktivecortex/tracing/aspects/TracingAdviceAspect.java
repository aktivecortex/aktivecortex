/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.tracing.aspects;

import com.github.kristofa.brave.ServerTracer;
import org.aktivecortex.core.message.channel.MessageHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;

import java.util.Arrays;

@Aspect
public class TracingAdviceAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(TracingAdviceAspect.class);

    private ServerTracingAspect serverTracingAspect;

    public void setServerTracer(ServerTracer serverTracer) {
        serverTracingAspect = new ServerTracingAspect(serverTracer);
    }

    public TracingAdviceAspect() {
        LOGGER.info("Request Tracing Enabled.");
    }

    @Pointcut(value = "execution(* org.aktivecortex.core.message.channel.adapter.ChannelAdapter.subscribe(..))")
    public void onChannelSubscription() {
    }

    @Around("onChannelSubscription()")
    public Object adviceMessageHandler(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof MessageHandler) {
                MessageHandler handler = (MessageHandler) args[i];
                AspectJProxyFactory factory = new AspectJProxyFactory(handler);
                factory.addAspect(serverTracingAspect);
                LOGGER.info("[{}] instrumented with server side tracing aspect", handler);
                return joinPoint.proceed(new Object[]{factory.getProxy()});
            }
        }
        LOGGER.error("Unable to advice MessageHandler: {}", Arrays.toString(joinPoint.getArgs()));
        return Boolean.FALSE;
    }

}
