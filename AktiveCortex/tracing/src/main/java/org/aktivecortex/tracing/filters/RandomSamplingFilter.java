/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.tracing.filters;


import com.github.kristofa.brave.ServerSpanState;
import com.github.kristofa.brave.TraceFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.Random;

/**
 * Tracing Filter that decide if we should sample a particular trace or not
 * depending on a configurable sampling rate only
 * if no pre-existing server-side request was found.
 * <p>
 *     Earlier started traces are preserved and followed till the end.
 * <p>
 *     In other words this implementation ignores the random
 * verdict in case of invocation in the course of a tracing flow initiated upstream.
 * <p>
 *     Please note that this implementation may affect the rate limit observance.
 *
 * @author Domenico Maria Giffone
 */
public class RandomSamplingFilter implements TraceFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RandomSamplingFilter.class);

    // Default rate is 0.001 = 0.1%
    private static final double DEF_SAMPLE_RATE = 0.001;

    // This is the sample rate
    private double sampleRate = DEF_SAMPLE_RATE;

    private final Random random = new Random();

    private ServerSpanState serverState;

    public void setServerState(ServerSpanState serverState) {
        Assert.notNull(serverState, "ServerSpanState can't be null");
        this.serverState = serverState;
    }

    /**
     * Set the filter sampling rate
     *
     * @param sampleRate a double which value is within 0-1
     */
    public void setSampleRate(double sampleRate) {
        if (!validSampleRate(sampleRate)) {
            throw new IllegalArgumentException("Sample rate not within the valid range of 0-1, was " + sampleRate);
        }
        this.sampleRate = sampleRate;
        LOGGER.info("sample rate setted to: [{}] ", this.sampleRate);
    }

    /**
     * Ensure that sampleRate sits within 0-1
     *
     * @param rate sampleRate is this sample rate valid (0-1f range)?
     * @return true if the rate is valid
     */
    private boolean validSampleRate(double rate) {
        return rate >= 0 && rate <= 1;
    }

    /**
     * Should we drop this particular trace or collect it.
     * <p>
     *     Return true if a pre-existing server-side request was found or
     * the random value r<= sampleRate. Return false otherwise.
     * <p>
     * True means keep.
     * False means drop.
     *
     * @param requestName the name associated with the current span
     * @return true if the span must be traced, false otherwise
     */
    @Override
    public boolean shouldTrace(String requestName) {
        return (null != serverState.getCurrentServerSpan()) || (random.nextDouble() <= sampleRate);
    }

}
