/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * #(@) FooWorkItem.java May 19, 2006
 */
package de.myfoo.commonj.work;

import java.util.List;

import commonj.work.Work;
import commonj.work.WorkEvent;
import commonj.work.WorkException;
import commonj.work.WorkItem;
import commonj.work.WorkListener;

/**
 * Implementation of the work item.
 * 
 * @author Andreas Keldenich
 * @version 1.0
 */
public class FooWorkItem implements WorkItem, Runnable {

	private Work work = null;
	private List<WorkItem> daemons = null;
	private WorkException ex = null;
	private ResultCollector resultCollector = null;
	private int status = WorkEvent.WORK_ACCEPTED;
	private WorkListener wl = null;

    protected void workRelease() {
        work.release();
    }

    /**
	 * Creates a new instance of FooWorkItem.
	 * 
	 * @param work
	 *            the work
	 */
	public FooWorkItem(Work work, WorkListener wl) {
		this.work = work;
		this.wl = wl;
	}

	/**
	 * Compare to another work item.
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Object arg0) {
		return 0;
	}

	/**
	 * Get the result of the work.
	 * 
	 * @return the work.
	 * @see commonj.work.WorkItem#getResult()
	 */
	@Override
	public Work getResult() throws WorkException {
		if (ex != null) {
			throw ex;
		}
		return work;
	}

	/**
	 * Retruns the status of the in-flight work.
	 * 
	 * @see commonj.work.WorkItem#getStatus()
	 */
	@Override
	public int getStatus() {
		return status;
	}

	/**
	 * Wrapper around the run method of the work.
	 */
	@Override
	public void run() {

		// check if we want to reject it
		if (work == null) {
			status = WorkEvent.WORK_REJECTED;
			if (wl != null) {
				wl.workRejected(new FooWorkEvent(this, WorkEvent.WORK_REJECTED));
			}
			return;
		}
		status = WorkEvent.WORK_ACCEPTED;
		if (wl != null) {
			wl.workAccepted(new FooWorkEvent(this, WorkEvent.WORK_ACCEPTED));
		}

		// start the work
		status = WorkEvent.WORK_STARTED;
		if (wl != null) {
			wl.workStarted(new FooWorkEvent(this, WorkEvent.WORK_STARTED));
		}

		// run the work
		FooWorkEvent event = new FooWorkEvent(this, WorkEvent.WORK_COMPLETED);
		try {
			work.run();
		} catch (Exception ex) {
			this.ex = new WorkException("Failed to execute work: " + ex.toString(), ex);
			event.setException(ex);
		} finally {
			status = WorkEvent.WORK_COMPLETED;
			if (wl != null) {
				wl.workCompleted(event);
			}

			// notify the result collector that the work is done.
			if (resultCollector != null) {
				resultCollector.workDone();
			}

			// remove from deamon list
			if (daemons != null) {
				daemons.remove(this);
			}
		}

	}

	/**
	 * Setter for daemon list.
	 * 
	 * @param daemons
	 *            The daemons to set.
	 */
	public void setDaemons(List<WorkItem> daemons) {
		this.daemons = daemons;
	}

	/**
	 * Set the ResultCollector to support notification
	 * 
	 * @param collector
	 *            the result collector
	 */
	public void setResultCollector(ResultCollector collector) {
		this.resultCollector = collector;
	}

	/**
	 * Set the status of the work.
	 * 
	 * @param status
	 *            status of the work.
	 */
	public void setStatus(int status) {
		this.status = status;
	}

}
