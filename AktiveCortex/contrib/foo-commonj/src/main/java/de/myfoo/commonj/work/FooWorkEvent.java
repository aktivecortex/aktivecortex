/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * #(@) FooWorkEvent.java May 21, 2006
 */
package de.myfoo.commonj.work;

import commonj.work.WorkEvent;
import commonj.work.WorkException;
import commonj.work.WorkItem;

/**
 * Implementation of a work event.
 * 
 * @author Andreas Keldenich
 * @version 1.0
 */
public class FooWorkEvent implements WorkEvent {

	private WorkException ex = null;
	private int type = 0;
	private WorkItem wi = null;

	/**
	 * Creates a new instance of FooWorkEvent.
	 * 
	 * @param wi
	 *            the work item
	 * @param type
	 *            type of the event.
	 */
	public FooWorkEvent(WorkItem wi, int type) {
		this.wi = wi;
		this.type = type;
	}

	/**
	 * Get the Exception if one was thorwn by the work.
	 * 
	 * @see commonj.work.WorkEvent#getException()
	 */
	@Override
	public WorkException getException() {
		return ex;
	}

	/**
	 * Get the event type.
	 * 
	 * @see commonj.work.WorkEvent#getType()
	 */
	@Override
	public int getType() {
		return type;
	}

	/**
	 * Get the work item.
	 * 
	 * @see commonj.work.WorkEvent#getWorkItem()
	 */
	@Override
	public WorkItem getWorkItem() {
		return wi;
	}

	/**
	 * Set Exception
	 * 
	 * @param th
	 *            thorwable
	 */
	public void setException(Throwable th) {
		this.ex = new WorkException("Error Executing work: " + th.toString(), th);
	}

}
