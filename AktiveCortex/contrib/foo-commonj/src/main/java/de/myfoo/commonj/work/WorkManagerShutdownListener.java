/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.myfoo.commonj.work;

import commonj.work.WorkManager;
import de.myfoo.commonj.work.FooWorkManager;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class performs the shutdown of the ThreadPool(s) used by the application during the context destroy phase of the
 * WebApplication.<p> To work correctly, the class must be registered as a listener in web.xml
 *
 * @author Domenico Maria Giffone
 */
public class WorkManagerShutdownListener implements ServletContextListener {

    private static final Logger LOGGER = LoggerFactory.getLogger("CommonJ Shutdown Listener");
    public static final String RESOURCE_REF_NAMESPACE = "java:comp/env/";
    public static final String JNDI_PROPERTIES = "jndi.properties";
    public static final String INIT_PARM = "work-manager-names";
    private List<String> jndiNames;
    private Map<String, FooWorkManager> workManagers = new HashMap<String, FooWorkManager>();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext c = sce.getServletContext();
        if (c != null) {
            if (c.getInitParameter(INIT_PARM) != null) {
                jndiNames = Arrays.asList(c.getInitParameter(INIT_PARM).split(";"));
            } else {
                LOGGER.warn("Missing init parameter with name {}. Please check your configuration.", INIT_PARM);
            }
            try {
                InitialContext ic = new InitialContext();
                for (String name : jndiNames) {
                    final FooWorkManager fwm = (FooWorkManager) ic.lookup(RESOURCE_REF_NAMESPACE + name);
                    if (null != fwm) {
                        workManagers.put(name, fwm);
                    }
                }
                if (workManagers.size() > 0) {
                    LOGGER.info("Registering {} workmanager(s) instances for subsequent shutdown handling. WorkManager names are: {}", workManagers.size(), jndiNames);
                } else {
                    LOGGER.warn("No {} entries found on the JNDI Context. Please check your configuration.", WorkManager.class.getName());
                }
            } catch (NamingException ex) {
                LOGGER.error("Unable to lookup WorkManager refs from JNDI Context.", ex);
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (workManagers.size() > 0) {
            LOGGER.info("Shutting down WorkManager(s)...");
            for (String name : workManagers.keySet()) {
                workManagers.get(name).shutdown();
                LOGGER.info("WorkManager with name [{}] shutted down.", name);
            }
            workManagers.clear();
            workManagers = null;
            try {
                LOGGER.info("Waiting some seconds to let shutdown terminate...");
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                LOGGER.warn("Ignoring thread interrupt during workmanagers shutdown.");
            }
        }
    }
}
