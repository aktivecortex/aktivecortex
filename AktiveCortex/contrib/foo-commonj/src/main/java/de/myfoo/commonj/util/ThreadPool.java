/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * #(
 *
 * @) ThreadPool.java Aug 16, 2006
 */
package de.myfoo.commonj.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Thread pool implemetation to execute
 * <code>Work</code> and
 * <code>Timer</code>s.
 *
 * @author Andreas Keldenich
 * @author Domenico Maria Giffone
 * @version 1.0
 */
public final class ThreadPool {

    private static final Logger LOGGER = LoggerFactory.getLogger("Commonj Thread Pool");
    public static final long IDLE_TIME_SECONDS = 60L;

    /**
     * The default rejected execution handler
     */
    private static final RejectedExecutionHandler DEFAULT_HANDLER = new ThreadPoolExecutor.CallerRunsPolicy();

    /**
     * Estimated number of CPUs available
     */
    public static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();

    /**
     * Suggested maximum number of threads for I/O bound tasks
     * I.e. JMS Listeners
     */
    private static final int MAX_IO_THREADS = 8 + (AVAILABLE_PROCESSORS / 2);

    /**
     * Suggested maximum concurrent high-priority pool threads
     * I.e. Saga Event Handlers
     */
    private static final int MAX_HI_PRI_THREADS = 1 + ((5 * AVAILABLE_PROCESSORS) / 4);

    /**
     * Suggested maximum concurrent low-priority pool threads
     * I.e. Scheduled tasks
     */
    private static final int MAX_LO_PRI_THREADS = Math.max(1, (AVAILABLE_PROCESSORS / 2));

    private ThreadPoolExecutor pool;
    private ThreadFactory threadFactory;
    private final ThreadFactory backingThreadFactory;
    private final String poolName;

    static {
        LOGGER.info("Suggested thread pools configuration based on JVM [{}] available processors: [Max High Priority Threads: [{}]  - Max I/O Threads:  [{}] - Low Priority Threads:  [{}]]",
                    AVAILABLE_PROCESSORS, MAX_HI_PRI_THREADS, MAX_IO_THREADS, MAX_LO_PRI_THREADS);
    }

    public ThreadPool(String poolName, int minThreads, int maxThreads, int queueLength, ThreadFactory backingThreadFactory) {
        this.poolName = poolName;
        BlockingQueue<Runnable> workQueue = null;
        this.backingThreadFactory = backingThreadFactory;
        this.threadFactory = getDaemonThreadFactory(poolName + "-%d");
        if (queueLength == 0) {
            workQueue = new SynchronousQueue<Runnable>();
            pool = new ThreadPoolExecutor(
                    minThreads,
                    maxThreads,
                    IDLE_TIME_SECONDS,
                    TimeUnit.SECONDS,
                    workQueue,
                    threadFactory,
                    DEFAULT_HANDLER);
        } else {
            workQueue = new ArrayBlockingQueue(queueLength);
            pool = new ThreadPoolExecutor(
                    minThreads,
                    maxThreads,
                    IDLE_TIME_SECONDS,
                    TimeUnit.SECONDS,
                    workQueue,
                    threadFactory,
                    DEFAULT_HANDLER);
        }
        pool.prestartAllCoreThreads();
        LOGGER.info("WorkManager [{}] Thread Pool created with values: [minThreads:{}, maxThreads:{}, queueLength:{}]",
                new Object[]{poolName, minThreads, maxThreads, queueLength});
    }

    /**
     * Creates a new instance of ThreadPool.
     *
     * @param minThreads mininum number of threads
     * @param maxThreads maximum number of threads
     * @param queueLength length of the execution queue
     */
    public ThreadPool(String poolName, int minThreads, int maxThreads, int queueLength) {
        this(poolName, minThreads, maxThreads, queueLength, null);
    }

    /**
     * Arrange for the given command to be executed by a thread in this pool. The method normally returns when the
     * command has been handed off for (possibly later) execution.
     *
     * @param command command to execute
     * @throws InterruptedException if execution fails
     */
    public void execute(Runnable command) {
        final int poolSize = pool.getPoolSize();
        pool.execute(command);
        final int newPoolSize = pool.getPoolSize();
        if (newPoolSize != poolSize) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("WorkManager [{}] Thread Pool size changed from {} to {} out of {}-{}",
                        poolName, poolSize, newPoolSize, pool.getCorePoolSize(), pool.getMaximumPoolSize());
            }
        }
        if (newPoolSize == pool.getMaximumPoolSize() && newPoolSize != poolSize) {
            LOGGER.info("WorkManager [{}] Thread Pool reached maximum pool size: [{}]. Exceeding requests will be treated with policy: [{}].",
                    poolName, pool.getMaximumPoolSize(), DEFAULT_HANDLER.getClass().getName());
        }
    }
    
    public boolean isShutdown() {
      return pool.isShutdown();
    }
    
    /**
     * Shutdown the pool after processing the currently queue tasks.
     */
    public void shutdown() {
        LOGGER.info("WorkManager [{}] Thread Pool shutdown requested.", poolName);
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                forceShutdown();
            } else {
                LOGGER.info("WorkManager [{}] Thread Pool shutdown completed.", poolName);
            }
        } catch (InterruptedException ie) {
            forceShutdown();
        }
    }

    /**
     * Force shutdown the pool immediately.
     */
    public void forceShutdown() {
        LOGGER.warn("WorkManager [{}] Thread Pool force shutdown requested.", poolName);
        try {
            pool.shutdownNow(); // Cancel currently executing tasks
            // Wait a while for tasks to respond to being cancelled
            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                LOGGER.error("WorkManager [{}] Thread Pool did not terminate", poolName);
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
        LOGGER.warn("WorkManager [{}] Thread Pool force shutdown completed.", poolName);
    }

    private ThreadFactory getDaemonThreadFactory(final String nameFormat) {
        final Boolean daemon = Boolean.TRUE;
        final Integer priority = null;
        final Thread.UncaughtExceptionHandler uncaughtExceptionHandler = null;
        final ThreadFactory currentThreadFactory = null!= backingThreadFactory ? backingThreadFactory: Executors.defaultThreadFactory();
        final AtomicLong count = (nameFormat != null) ? new AtomicLong(0) : null;
        return new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = currentThreadFactory.newThread(runnable);
                if (nameFormat != null) {
                    thread.setName(String.format(nameFormat, count.getAndIncrement()));
                }
                if (daemon != null) {
                    thread.setDaemon(daemon);
                }
                if (priority != null) {
                    thread.setPriority(priority);
                }
                if (uncaughtExceptionHandler != null) {
                    thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
                }
                return thread;
            }
        };
    }
}