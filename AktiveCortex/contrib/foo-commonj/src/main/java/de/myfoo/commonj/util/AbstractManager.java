/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * #(@) AbstractManager.java Aug 16, 2006
 */
package de.myfoo.commonj.util;

/**
 * Abtract manger class - parent of all <code>TimerManager</code> and 
 * <code>WorkManagers</code>
 *
 * @author  Andreas Keldenich
 * @version 1.0
 */
public abstract class AbstractManager {

	private ThreadPool pool;

    protected void execute(Runnable command) {
        pool.execute(command);
    }

    /**
	 * Creates a new instance of AbstractManager.
	 */
	public AbstractManager(ThreadPool pool) {
		this.pool = pool;
	}
	
	/**
	 * Shutdown the thread pool.
	 */
	public void shutdown() {
		// shutdown the thread pool
		pool.shutdown();
	}
	
	/**
	 * Force shutdown the thread pool.
	 */
	public void forceShutdown() {
		// shutdown the thread pool
		pool.forceShutdown();
	}
	

}
