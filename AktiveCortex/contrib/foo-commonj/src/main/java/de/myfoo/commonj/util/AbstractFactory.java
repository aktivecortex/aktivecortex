/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * #(@) AbstractFactory.java Aug 16, 2006
 */
package de.myfoo.commonj.util;

import java.util.HashMap;
import java.util.Map;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.spi.ObjectFactory;

/**
 * Abstract factory class.
 * 
 * @author Andreas Keldenich
 * @version 1.0
 */
public abstract class AbstractFactory<T> implements ObjectFactory {

	/** max number of threads in the pool */
	public static final String CONFIG_MAX_THREADS = "maxThreads";
	/** min number of threads in the pool */
	public static final String CONFIG_MIN_THREADS = "minThreads";
	/** length of the queue */
	public static final String CONFIG_QUEUE_LENGTH = "queueLenght";

	private Map<Name, T> managers = new HashMap<Name, T>();

    protected Map<Name, T> getManagers() {
        return managers;
    }

    /**
	 * Get an integer config value.
	 * 
	 * @param name
	 *            config value name
	 * @param value
	 *            config value
	 * @return integer value
	 */
	protected int getValue(String name, String value) throws NamingException {
		int x = 0;
		try {
			x = Integer.parseInt(value);
		} catch (NumberFormatException e) {
            final NamingException namingException = new NamingException("Value " + name + " must be an integer.");
            namingException.setRootCause(e);
            throw namingException;
		}
		if (x < 0 || x > 100) {
			throw new NamingException("Value " + name + " out of range [0..100]");
		}
		return x;
	}

}
