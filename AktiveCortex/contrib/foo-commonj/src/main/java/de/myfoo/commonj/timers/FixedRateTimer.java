/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * #(@) FixedRateTimer.java Aug 16, 2006
 */
package de.myfoo.commonj.timers;

import commonj.timers.TimerListener;

/**
 * Fixed rate timer.
 * 
 * @author Kelly
 * @version 1.0
 */
public final class FixedRateTimer extends FooTimer {

	/**
	 * Creates a new instance of FixedRateTimer.
	 * 
	 * @param startTime
	 *            start time
	 * @param period
	 *            execution period
	 * @param listener
	 *            the timer listener for this timer.
	 */
	public FixedRateTimer(long startTime, long period, TimerListener listener) {
		super(startTime, period, listener);
	}

	/**
	 * Compute the next execution time.
	 * 
	 * @see de.myfoo.commonj.timers.FooTimer#computeNextExecutionTime()
	 */
	@Override
	protected void computeNextExecutionTime() {
		long currentTime = System.currentTimeMillis();
		long execTime = getScheduledExecutionTime() + getPeriod();

		while (execTime <= currentTime) {
			execTime += getPeriod();
		}

		setScheduledExecutionTime(execTime);
	}

}
