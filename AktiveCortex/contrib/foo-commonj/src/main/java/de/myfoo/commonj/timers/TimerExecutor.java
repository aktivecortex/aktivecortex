/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * #(@) TimerExecutor.java Aug 16, 2006
 */
package de.myfoo.commonj.timers;

/**
 * Timer executor.
 * 
 * @author Kelly
 * @version 1.0
 */
public final class TimerExecutor implements Runnable {

	private boolean running = false;
	private FooTimer timer;
	private FooTimerManager timerManager;

	/**
	 * Creates a new instance of TimerExecutor.
	 */
	public TimerExecutor(FooTimer timer, FooTimerManager timerManager) {
		this.timer = timer;
		this.timerManager = timerManager;
	}

	/**
	 * Getter for timer
	 * 
	 * @return Returns the timer.
	 */
	public FooTimer getTimer() {
		return timer;
	}

	/**
	 * Getter for timerManager
	 * 
	 * @return Returns the timerManager.
	 */
	public FooTimerManager getTimerManager() {
		return timerManager;
	}

	/**
	 * Is timer running
	 * 
	 * @return <code>true</code> if timer is running
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * run method to execute a timer
	 */
	@Override
	public void run() {
		running = true;
		try {
			// execute the timer
			timer.execute();

			// compute next execution time
			timer.computeNextExecutionTime();
		} catch (Exception e) {
			// ignore
		} finally {
			running = false;
			// timerManager.notifyAll();
		}
	}

	/**
	 * Setter for timer
	 * 
	 * @param timer
	 *            The timer to set.
	 */
	public void setTimer(FooTimer timer) {
		this.timer = timer;
	}

	/**
	 * Setter for timerManager
	 * 
	 * @param timerManager
	 *            The timerManager to set.
	 */
	public void setTimerManager(FooTimerManager timerManager) {
		this.timerManager = timerManager;
	}

}
