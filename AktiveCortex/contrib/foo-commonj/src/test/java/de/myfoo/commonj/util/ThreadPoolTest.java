/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.myfoo.commonj.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Domenico Maria Giffone
 */
public class ThreadPoolTest {

    private static final String POOL_NAME = "TestPool";
    private static final int MIN_THREADS = 5;
    private static final int MAX_THREADS = 10;
    private static final int QUEUE_LENGHT = 0;
    private static final int WAIT_TIME_MS = 1000;
    
    private CountDownLatch latch; 
    
    private AtomicInteger completedTasks;
    
    private volatile long startedThreads;

    private ThreadPool testSubject;
    
    private ThreadFactory mockedThreadFactory = new ThreadFactory() {
        @Override
        public Thread newThread(Runnable runnable) {
            Thread thread = Executors.defaultThreadFactory().newThread(runnable);
            startedThreads++;
            return thread;
        }
    };

//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
    
    @Before
    public void setUp() {
        completedTasks = new AtomicInteger();
        startedThreads = 0;
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreateMinThreadsOnStart() throws InterruptedException {
        System.out.println("createMinThreadsOnStart");
        testSubject = new ThreadPool(POOL_NAME, MIN_THREADS, MAX_THREADS, QUEUE_LENGHT, mockedThreadFactory);
        assertEquals(MIN_THREADS, startedThreads);
        latch = new CountDownLatch(MIN_THREADS);
        for (int i = 0; i < MIN_THREADS; i++) {
            testSubject.execute(getMonitorableJob(0));
        }
        latch.await(5, TimeUnit.SECONDS);
        assertEquals(MIN_THREADS, completedTasks.get());
        testSubject.shutdown();
        assertTrue(testSubject.isShutdown());
    }

    @Test
    public void testSubmittedTasksExceedMaxThreads() throws InterruptedException {
        System.out.println("submittedTasksExceedMaxThreads");
        testSubject = new ThreadPool(POOL_NAME, 2, 2, 2, mockedThreadFactory);
        assertEquals(2, startedThreads);
        latch = new CountDownLatch(1000);
        for (int i = 0; i < 1000; i++) {
            testSubject.execute(getMonitorableJob(10));
        }
        latch.await(10, TimeUnit.SECONDS);
        assertEquals(1000, completedTasks.get());
        testSubject.shutdown();
        assertTrue(testSubject.isShutdown());
    }

    @Test
    public void testCreateMaxThreadsOnDemand() throws InterruptedException {
        System.out.println("createMaxThreadsOnDemand");
        testSubject = new ThreadPool(POOL_NAME, MIN_THREADS, MAX_THREADS, QUEUE_LENGHT, mockedThreadFactory);
        assertEquals(MIN_THREADS, startedThreads);
        latch = new CountDownLatch(MAX_THREADS);
        for (int i = 0; i < MAX_THREADS; i++) {
            testSubject.execute(getMonitorableJob(WAIT_TIME_MS));
        }
        latch.await(5, TimeUnit.SECONDS);
        assertEquals(MAX_THREADS, startedThreads);
        assertEquals(MAX_THREADS, completedTasks.get());
        testSubject.shutdown();
        assertTrue(testSubject.isShutdown());
    }
    
    @Test
    public void testEnqueueOnThreshold() throws InterruptedException {
        System.out.println("enqueueOnThreshold");
        testSubject = new ThreadPool(POOL_NAME, MIN_THREADS, MAX_THREADS, 10, mockedThreadFactory);
        assertEquals(MIN_THREADS, startedThreads);
        latch = new CountDownLatch(20);
        for (int i = 0; i < 20; i++) {
            testSubject.execute(getMonitorableJob(WAIT_TIME_MS));
        }
        latch.await(5, TimeUnit.SECONDS);
        assertEquals(MAX_THREADS, startedThreads);
        assertEquals(20, completedTasks.get());
        testSubject.shutdown();
        assertTrue(testSubject.isShutdown());
    }
    
    @Test
    public void testShutdownWithRunningTask() throws InterruptedException {
        System.out.println("shutdownWithRunningTask");
        testSubject = new ThreadPool(POOL_NAME, MIN_THREADS, MAX_THREADS, QUEUE_LENGHT, mockedThreadFactory);
        assertEquals(MIN_THREADS, startedThreads);
        latch = new CountDownLatch(1);
        testSubject.execute(getMonitorableJob(WAIT_TIME_MS * 10));
        testSubject.shutdown();
//        assertEquals(5, startedThreads);
        assertEquals(1, completedTasks.get());
        assertTrue(testSubject.isShutdown());
    }
    
    @Test
    public void testShutdownWithBlockedTask() throws InterruptedException {
        System.out.println("shutdownWithBlockedTask");
        testSubject = new ThreadPool(POOL_NAME, MIN_THREADS, MAX_THREADS, QUEUE_LENGHT, mockedThreadFactory);
        assertEquals(MIN_THREADS, startedThreads);
        latch = new CountDownLatch(1);
        testSubject.execute(getMonitorableJob(WAIT_TIME_MS * WAIT_TIME_MS));
        testSubject.forceShutdown();
//        assertEquals(5, startedThreads);
        assertEquals(0, completedTasks.get());
        assertTrue(testSubject.isShutdown());
    }

    private Runnable getMonitorableJob(final long sleepTime) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    if (sleepTime > 0) {
                        Thread.sleep(sleepTime);
                    }
                    completedTasks.incrementAndGet();
                    latch.countDown();
                } catch (InterruptedException ex) {
                   //
                }
            }
        };
    }
}
