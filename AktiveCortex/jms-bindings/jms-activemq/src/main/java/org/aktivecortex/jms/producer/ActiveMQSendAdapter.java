/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.jms.producer;

import javax.jms.*;

import org.aktivecortex.api.message.MessageHeadersConstants;
import org.aktivecortex.core.message.spi.IMessagingSystemFactory.ConfigurationKey;
import org.aktivecortex.core.message.spi.impl.MessagingSystemFactory;
import org.aktivecortex.core.message.spi.SendAdapter;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.axonframework.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.ProducerCallback;

public class ActiveMQSendAdapter implements SendAdapter {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActiveMQSendAdapter.class);

	//4 ActiveMQ is "JMSXGroupID"
	private static final String MESSAGE_ORDERING_PROPERTY = (String) MessagingSystemFactory
			.getProperty(ConfigurationKey.ORDERED_DELIVERY_PROPERTY);

    private static final String ORDERED_MSG_LOG_TEMPLATE = "messageID [{}] - RoutingKey [{}] --> [{}]";

    private static final String UNORDERED_MSG_LOG_TEMPLATE = "messageID [{}] --> [{}]";

    private JmsTemplate template;


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
        Assert.notNull(jmsTemplate, "JmsTemplate can't be null");
		this.template = jmsTemplate;
	}

	@Override
    public boolean send(final byte[] message, final String messageId, final String routingKey) {
        template.send(new BytesMessageCreator(message, messageId, routingKey));
        return true;
    }

    @Override
	public void afterPropertiesSet() throws Exception {     //NOSONAR
        Assert.notNull(template, "JmsTemplate property required");
        LOGGER.info("initialized: {}", this);
    }

	@Override
	public String toString() {
		return String.format("%s [template=%s]",
                this.getClass().getSimpleName(),
                ToStringBuilder.reflectionToString(
                        template,
                        ToStringStyle.SHORT_PREFIX_STYLE));
	}

    private class BytesMessageCreator implements MessageCreator {

        private final byte[] payload;

        private final String messageGroup;

        private final String messageId;


        public BytesMessageCreator(final byte[] message, final String messageId, final String routingKey) {
            this.payload = message;
            this.messageId = messageId;
            this.messageGroup = routingKey;
        }

        @Override
        public Message createMessage(Session session) throws JMSException {
            BytesMessage msg = session.createBytesMessage();
            msg.writeBytes(payload);
            msg.setStringProperty(MessageHeadersConstants.MESSAGE_ID, messageId);
            if (null != messageGroup) {
                msg.setStringProperty(MESSAGE_ORDERING_PROPERTY, messageGroup);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(ORDERED_MSG_LOG_TEMPLATE,
                            new Object[]{messageId, messageGroup, template.getDefaultDestination()});
                }
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(UNORDERED_MSG_LOG_TEMPLATE, new Object[]{messageId, template.getDefaultDestination()});
                }
            }
           return msg;
        }
    }
}