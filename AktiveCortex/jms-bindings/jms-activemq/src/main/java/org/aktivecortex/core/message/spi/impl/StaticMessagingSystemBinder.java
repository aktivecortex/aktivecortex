/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.spi.impl;

import org.aktivecortex.core.message.spi.IMessagingSystemFactory;


/**
 * ActiveMQ Binding
 * 
 * @author d.giffone
 *
 */
public final class StaticMessagingSystemBinder {
	
	private static final StaticMessagingSystemBinder instance = new StaticMessagingSystemBinder();
	private static final String bindingClassAsStr = ActiveMQMessagingSystemFactory.class.getName();
	private IMessagingSystemFactory factory;

	public static final StaticMessagingSystemBinder getInstance() {
		return instance;
	}

	private StaticMessagingSystemBinder() {
		this.factory = new ActiveMQMessagingSystemFactory();
	}

	public IMessagingSystemFactory getFactory() {
		return this.factory;
	}

	public String getBindingClassAsStr() {
		return bindingClassAsStr;
	}

}
