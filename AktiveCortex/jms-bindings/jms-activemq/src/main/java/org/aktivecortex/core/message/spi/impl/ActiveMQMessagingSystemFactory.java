/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.core.message.spi.impl;

import java.util.HashMap;
import java.util.Map;

import org.aktivecortex.core.message.spi.IMessagingSystemFactory;
import org.aktivecortex.core.message.spi.SendAdapter;
import org.aktivecortex.jms.producer.ActiveMQSendAdapter;

public class ActiveMQMessagingSystemFactory implements IMessagingSystemFactory {
	 
	private static final String MESSAGE_GROUPS = "JMSXGroupID";
	private static final Map<ConfigurationKey, Object> properties = new HashMap<IMessagingSystemFactory.ConfigurationKey, Object>();
	
	static {
		properties.put(ConfigurationKey.ORDERED_DELIVERY_PROPERTY, MESSAGE_GROUPS);
	}

	@Override
	public SendAdapter getSendAdapter() {
		return new ActiveMQSendAdapter();
	}

	@Override
	public Object getProperty(ConfigurationKey k) {
		return properties.get(k);
	}

	@Override
	public boolean isPropertyDefined(ConfigurationKey k) {
		return properties.containsKey(k);
	}

}
