/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.jms.producer;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.aktivecortex.api.message.MessageHeadersConstants;
import org.aktivecortex.core.message.spi.SendAdapter;
import org.aktivecortex.jms.utils.JMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.ProducerCallback;

import weblogic.jms.extensions.JMSOrderException;
import weblogic.jms.extensions.WLMessageProducer;

public class WLSJMSSendAdapter implements SendAdapter {

	private static final Logger logger = LoggerFactory.getLogger(WLSJMSSendAdapter.class);

	private static final int WAIT_TIME_MS = 3000; // Time accuracy is very low

	protected JmsTemplate template;

	protected boolean failoverEnabled;

	protected int maxRetries = 5;

	protected String fallbackDestinationName;

	protected Destination destination;

	protected Destination fallbackDestination;

	public void setConnFactory(ConnectionFactory factory) {
		this.template = new JmsTemplate(factory);
		this.template.setSessionTransacted(true);
	}

	/**
	 * 4 unit testing
	 * 
	 * @param failoverEnabled
	 */
	void setFailoverEnabled(boolean failoverEnabled) {
		this.failoverEnabled = failoverEnabled;
	}

	public void setMaxRetries(int unitOfOrderMaxRetries) {
		this.maxRetries = unitOfOrderMaxRetries;
	}

	public void setfallbackDestinationName(String fallbackDestinationName) {
		this.fallbackDestinationName = fallbackDestinationName;
	}

	/**
	 * 4 unit testing
	 * 
	 * @param template
	 */
	void setJMSTemplate(JmsTemplate template) {
		this.template = template;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	/**
	 * 4 unit testing
	 * 
	 * @param fallbackDestination
	 */
	void setFallbackDestination(Destination fallbackDestination) {
		this.fallbackDestination = fallbackDestination;
	}

	@Override
	public boolean send(final byte[] message, final String messageId, final String routingKey) {
		int retriesCount = 0;
		while (true) {
			try {
				template.execute(destination, new WLSMessageCallback(message, messageId, routingKey, false));
				return true;
			} catch (JmsException e) {
				if (++retriesCount < maxRetries) {
					try {
						logger.warn("Send failed due to a {}. Reason: {}. Waiting some seconds before retry...",
								e.getMostSpecificCause(), e.getMessage());
						Thread.sleep(WAIT_TIME_MS);
					} catch (InterruptedException e1) {
						// swallowed
					}
				} else if (e.getCause() instanceof JMSOrderException && failoverEnabled) {
					logger.warn("Remote destination not available due to exception", e.getCause());
					logger.warn("failover enabled. Switching to local destination [{}]", fallbackDestination);
					template.execute(fallbackDestination, new WLSMessageCallback(message, messageId, routingKey, true));
					return true;
				} else {
					throw e;
				}
			}
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			if (null != fallbackDestinationName) {
				logger.debug("Looking up failover destination {}", fallbackDestinationName);
				fallbackDestination = JMSUtils.getLocalDestination(fallbackDestinationName);
				logger.info("Failover destination [{}] found. Enabled failover for JmsOrderException",
						fallbackDestination);
				failoverEnabled = true;
			} else {
				failoverEnabled = false;
			}
		} catch (Exception ex) {
			logger.warn(
					"failover destination [{}] not found due to the following exception. Disabling failover support. [{}]",
					fallbackDestinationName, ex);
			failoverEnabled = false;
		}
		logger.info("initialized: {}", this);
	}

	@Override
	public String toString() {
		return String
				.format("%s [fallbackDestinationName=%s, failoverEnabled=%s, unitOfOrderMaxRetries=%s, template=%s, destination=%s, fallbackDestination=%s]",
						this.getClass().getSimpleName(), fallbackDestinationName, failoverEnabled, maxRetries,
						template, destination, fallbackDestination);
	}

	private class WLSMessageCallback implements ProducerCallback<Void> {

		private byte[] payload;

		private String unitOfOrder;
		
		private String messageId;

		private boolean isLocalDelivery;

		/**
		 * @param payload
		 */
		protected WLSMessageCallback(byte[] payload, String messageId, String routingKey, boolean isLocalDelivery) {
			this.payload = payload;
			this.isLocalDelivery = isLocalDelivery;
			this.messageId = messageId;
			this.unitOfOrder = routingKey;
		}

		@Override
		public Void doInJms(Session sn, MessageProducer producer) throws JMSException {
			WLMessageProducer wlp = (WLMessageProducer) producer;
			BytesMessage msg = sn.createBytesMessage();
			msg.writeBytes(payload);
			msg.setStringProperty(MessageHeadersConstants.MESSAGE_ID, messageId);
			if (null != unitOfOrder) {
				wlp.setUnitOfOrder(unitOfOrder);
				logger.info("messageID [{}] - RoutingKey [{}] --> [{}]", 
						new Object[] { messageId, unitOfOrder,  
						!isLocalDelivery ? destination : fallbackDestination });
			} else {
				logger.info("messageID [{}] --> [{}]", 
						new Object[] {messageId,
						!isLocalDelivery ? destination : fallbackDestination });
			}
			producer.send(msg);
			return null;
		}

	}
}