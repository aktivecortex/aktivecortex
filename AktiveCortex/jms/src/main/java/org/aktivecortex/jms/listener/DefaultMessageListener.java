/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.jms.listener;

import org.aktivecortex.core.message.channel.MessageDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultMessageListener extends AbstractMessageListener {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultMessageListener.class);
	
	private MessageDispatcher<?> messageDispatcher;

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	protected MessageDispatcher<?> getDispatcher() {
		return messageDispatcher;
	}

	public void setMessageDispatcher(MessageDispatcher<?> messageDispatcher) {
		this.messageDispatcher = messageDispatcher;
	}
	
}
