/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.jms.utils;

import static com.google.common.collect.Iterables.find;

import java.util.List;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.NamingException;

import org.aktivecortex.core.utils.io.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jndi.JndiTemplate;

import com.google.common.base.Predicate;

public final class JMSUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(JMSUtils.class);

	private JMSUtils() {}

	  public static void closeConnection(Connection connection) {
	    try {
	      if (connection == null) {
	        return;
	      }
	      connection.close();
	    }
	    catch (JMSException ignored) {
	    }
	  }

	  public static void closeSession(Session session) {
	    if (session != null) {
	      try {
	        session.close();
	      }
	      catch (JMSException ignored) {
	      }
	    }
	  }

	  public static void closeMessageProducer(MessageProducer producer) {
	    if (producer != null) {
	      try {
	        producer.close();
	      }
	      catch (JMSException ignored) {
	      }
	    }
	  }

	  public static void closeMessageConsumer(MessageConsumer consumer) {
	    if (consumer != null) {
	      try {
	        consumer.close();
	      }
	      catch (JMSException ignored) {
	      }
	    }
	  }

	  public static void close(Connection connection, Session session) {
	    closeSession(session);
	    closeConnection(connection);
	  }

	  public static void close(Connection connection, Session session, MessageProducer producer) {
	    closeMessageProducer(producer);
	    closeSession(session);
	    closeConnection(connection);
	  }

	  public static void close(Connection connection, Session session, MessageConsumer consumer) {
	    closeConnection(connection);
	    closeSession(session);
	    closeMessageConsumer(consumer);
	  }
	  
	  public static Destination getLocalDestination(String destinationName) throws NamingException {
			final JndiTemplate jndi = new JndiTemplate();
			List<String> matchingJndiEntries = Utils.getPath(jndi.getContext(), "", destinationName);
			logger.debug("{} jndi matches found for name: [{}]", matchingJndiEntries, destinationName);
			String jndiEntry = find(matchingJndiEntries, new Predicate<String>(){

				@Override
				public boolean apply(String item) {
					return item.contains("@jms");
				}});

			logger.debug("selected name is: [{}]", jndiEntry);
			Destination result = (Destination) jndi.lookup(jndiEntry);
			logger.debug("Destination found: [{}]", (null!=result));
			return result;
		}

}
