/*
 * Copyright (C) 2012-2013. Aktive Cortex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aktivecortex.jms.listener;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.aktivecortex.api.message.MessageHeadersConstants;
import org.aktivecortex.api.message.MessagingException;
import org.aktivecortex.core.message.channel.MessageDispatcher;
import org.aktivecortex.core.message.spi.IMessagingSystemFactory.ConfigurationKey;
import org.aktivecortex.core.message.spi.impl.MessagingSystemFactory;
import org.slf4j.Logger;
import org.springframework.transaction.TransactionException;

public abstract class AbstractMessageListener implements MessageListener {

	private static final String UNKNOWN_CORTEX_ID = "UNKNOWN_ID";
	private static final String MESSAGE_ORDERING_PROPERTY = (String) MessagingSystemFactory
			.getProperty(ConfigurationKey.ORDERED_DELIVERY_PROPERTY);
	private static final String UNORDERED_MSG_LOG_TEMPLATE = "<-- MessageID [{}] - JMSMessageID [{}]";
	private static final String ORDERED_MSG_LOG_TEMPLATE = "<-- MessageID [{}]  - RoutingKey [{}] - JMSMessageID [{}]";

	protected abstract Logger getLogger();

	protected abstract MessageDispatcher<?> getDispatcher();

	public void onMessage(Message message) {
		String cortexMessageID = UNKNOWN_CORTEX_ID;
		BytesMessage msg = null;
		boolean temporaryFailed = false;
		try {
			if (message instanceof BytesMessage) {
				msg = (BytesMessage) message;
				final String messageID = msg.getJMSMessageID();
				final byte[] bytes = new byte[(int) msg.getBodyLength()];
				msg.readBytes(bytes);
				cortexMessageID = msg.getStringProperty(MessageHeadersConstants.MESSAGE_ID);
                logReceived(cortexMessageID, msg, messageID);
                getDispatcher().dispatch(bytes);
			} else {
				throw new IllegalArgumentException("Message of wrong type: " + message.getClass().getName());
			}
		} catch (JMSException e) {
			getLogger().error("Unrecoverable exception", e);
		} catch (IllegalStateException e) {
			// BEGIN - COMMIT
			getLogger().error("Unrecoverable exception", e);
		} catch (SecurityException e) {
			// COMMIT
			getLogger().error("Unrecoverable exception", e);
		} catch (TransactionException e) {
			// COMMIT
			getLogger().error("Recoverable exception", e);
			temporaryFailed = true;
		} catch (RuntimeException e) {
			// DISPATCH
			getLogger().error("Unrecoverable exception", e);
		} finally {
			if (temporaryFailed) {
				throw new MessagingException(String.format("%s Message rejected due to a temporary failure", cortexMessageID));
			}
		}
	}

    private void logReceived(String cortexMessageID, BytesMessage msg, String messageID) throws JMSException {
        if (getLogger().isDebugEnabled()) {
            if (null != msg.getStringProperty(MESSAGE_ORDERING_PROPERTY)) {
                getLogger().debug(ORDERED_MSG_LOG_TEMPLATE,
                        new Object[]{cortexMessageID,
                                msg.getStringProperty(MESSAGE_ORDERING_PROPERTY),
                                messageID});
            } else {
                getLogger().debug(UNORDERED_MSG_LOG_TEMPLATE, new Object[]{cortexMessageID, messageID});
            }
        }
    }

    @PostConstruct
	protected void logCreate() {
		getLogger().info("created: [{}]", getDispatcher());
	}

	@PreDestroy
	protected void logDestroy() {
		getLogger().info("Destroying: [{}]", getDispatcher());
	}

}
